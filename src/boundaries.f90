!---------------------------------------------------------------------------------------------------
! File: boundaries.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines fill_ghost_cells(), boundary_face_1(), boundary_face_2(),
!! boundary_face_3(), boundary_face_4(), boundary_face_5(), boundary_face_6(),
!! boundary_corner_2D_1(),boundary_corner_2D_2(),boundary_corner_2D_3(),
!! boundary_corner_2D_4(),boundary_edge_3D_1(),boundary_edge_3D_2(),boundary_edge_3D_3(),
!! boundary_edge_3D_4(),boundary_edge_3D_5(),boundary_edge_3D_6(),boundary_edge_3D_7(),
!! boundary_edge_3D_8(),boundary_edge_3D_9(),boundary_edge_3D_10(),boundary_edge_3D_11(),
!! boundary_edge_3D_12(),boundary_corner_3D_1(),boundary_corner_3D_2(),boundary_corner_3D_3(),
!! boundary_corner_3D_4(),boundary_corner_3D_5(),boundary_corner_3D_6(),boundary_corner_3D_7(),
!! boundary_corner_3D_8()
!!
!! This file contains all the routines necessary to the gestion of boundary conditions
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine FILL_GHOST_CELLS:
!
!> The ghost cells are either filled with the values from the boundary conditions and/or with
!! the values from the neighbouring processors in the case of MPI through the use of the
!! boundary_face_* and MPI_COMM_* subroutines. If full padding is required (normally should
!! not be needed in hydro), then the ghost cells in the grid corners and edges are also filled.
!<
subroutine fill_ghost_cells(array)

  use mod_variables, only : debug_l1,nvar
  use mod_grid
  use mod_mpi      , only : rank

  implicit none
  
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array

  if(debug_l1) write(*,*) 'begin FILL_GHOST_CELLS',rank

#if MPI==1
  call mpi_communications(array)
#endif

  if(nxlower(1) == 1    ) call boundary_face_1(array)
  if(nxupper(1) == nx(1)) call boundary_face_2(array)
  
  if(ndim > 1)then
     if(nxlower(2) == 1    ) call boundary_face_3(array)
     if(nxupper(2) == nx(2)) call boundary_face_4(array)
  endif
  if(ndim > 2)then
     if(nxlower(3) == 1    ) call boundary_face_5(array)
     if(nxupper(3) == nx(3)) call boundary_face_6(array)
  endif
  
  if(ndim == 2)then
     if((nxlower(1) == 1    ).and.(nxlower(2) == 1    )) call boundary_corner_2D_1(array)
     if((nxlower(1) == 1    ).and.(nxupper(2) == nx(2))) call boundary_corner_2D_2(array)
     if((nxupper(1) == nx(1)).and.(nxlower(2) == 1    )) call boundary_corner_2D_3(array)
     if((nxupper(1) == nx(1)).and.(nxupper(2) == nx(2))) call boundary_corner_2D_4(array)
  endif

  if(ndim == 3)then

     if((nxlower(1) == 1    ).and.(nxlower(2) == 1    )) call boundary_edge_3D_01(array)
     if((nxlower(1) == 1    ).and.(nxupper(2) == nx(2))) call boundary_edge_3D_02(array)
     if((nxupper(1) == nx(1)).and.(nxlower(2) == 1    )) call boundary_edge_3D_03(array)
     if((nxupper(1) == nx(1)).and.(nxupper(2) == nx(2))) call boundary_edge_3D_04(array)
     if((nxlower(2) == 1    ).and.(nxlower(3) == 1    )) call boundary_edge_3D_05(array)
     if((nxlower(2) == 1    ).and.(nxupper(3) == nx(3))) call boundary_edge_3D_06(array)
     if((nxupper(2) == nx(2)).and.(nxlower(3) == 1    )) call boundary_edge_3D_07(array)
     if((nxupper(2) == nx(2)).and.(nxupper(3) == nx(3))) call boundary_edge_3D_08(array)
     if((nxlower(1) == 1    ).and.(nxlower(3) == 1    )) call boundary_edge_3D_09(array)
     if((nxupper(1) == nx(1)).and.(nxlower(3) == 1    )) call boundary_edge_3D_10(array)
     if((nxlower(1) == 1    ).and.(nxupper(3) == nx(3))) call boundary_edge_3D_11(array)
     if((nxupper(1) == nx(1)).and.(nxupper(3) == nx(3))) call boundary_edge_3D_12(array)
     
     if((nxlower(1) == 1    ).and.(nxlower(2) == 1    ).and.(nxlower(3) == 1    )) call boundary_corner_3D_1(array)
     if((nxlower(1) == 1    ).and.(nxupper(2) == nx(2)).and.(nxlower(3) == 1    )) call boundary_corner_3D_2(array)
     if((nxupper(1) == nx(1)).and.(nxlower(2) == 1    ).and.(nxlower(3) == 1    )) call boundary_corner_3D_3(array)
     if((nxupper(1) == nx(1)).and.(nxupper(2) == nx(2)).and.(nxlower(3) == 1    )) call boundary_corner_3D_4(array)
     if((nxlower(1) == 1    ).and.(nxlower(2) == 1    ).and.(nxupper(3) == nx(3))) call boundary_corner_3D_5(array)
     if((nxlower(1) == 1    ).and.(nxupper(2) == nx(2)).and.(nxupper(3) == nx(3))) call boundary_corner_3D_6(array)
     if((nxupper(1) == nx(1)).and.(nxlower(2) == 1    ).and.(nxupper(3) == nx(3))) call boundary_corner_3D_7(array)
     if((nxupper(1) == nx(1)).and.(nxupper(2) == nx(2)).and.(nxupper(3) == nx(3))) call boundary_corner_3D_8(array)
     
  endif

  if(debug_l1) write(*,*) 'end   FILL_GHOST_CELLS',rank

  return

end subroutine fill_ghost_cells

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_FACE_1:
!
!> Fills ghost cells at the left (lower x) boundary with values from boundary conditions:
!!    - 1 : free-flow
!!    - 2 : reflexive
!!    - 3 : periodic
!!    - 4 : inflow
!<
subroutine boundary_face_1(array)

  use mod_gridarrays, only : bc1,inflow1
  use mod_variables , only : nvar,debug_l2,str,adim
  use mod_constants
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: j,k,h,n
  real(dp), dimension(nvar                                    ) :: kron
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  character (len=str)                                           :: string

  if(debug_l2) write(*,*) 'begin BOUNDARY_FACE_1',rank

  kron    =  one
  kron(2) = -one

  if(same_bc(1))then

     select case(boundc_1)

     case(1)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,1-n,j,k) = array(h,1,j,k)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case(2)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,1-n,j,k) = kron(h)*array(h,1+n-1,j,k)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case(3)

#if MPI==0
        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,1-n,j,k) = array(h,nxloc(1)-n+1,j,k)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL
#endif

     case(4)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,1-n,j,k) = inflow1(h,nghost-n+1,j,k)
                 enddo
                 !!call timestep_cell(1-n,j,k,nvar,array(1:nvar,1-n,j,k))
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case default

        write(string,*) 'Wrong general boundary condition 1',boundc_1
        call close_program(trim(string))

     end select

  else

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,h,n)
     !$OMP DO COLLAPSE(2)
     do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
        do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost

           select case(bc1(j,k))

           case(1)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,1-n,j,k) = array(h,1,j,k)
                 enddo
              enddo

           case(2)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,1-n,j,k) = kron(h)*array(h,1+n-1,j,k)
                 enddo
              enddo

           case(3)

#if MPI==0
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,1-n,j,k) = array(h,nxloc(1)-n+1,j,k)
                 enddo
              enddo
#endif

           case(4)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,1-n,j,k) = inflow1(h,nghost-n+1,j,k)
                 enddo
                 !!call timestep_cell(1-n,j,k,nvar,array(1:nvar,1-n,j,k))
              enddo

           case default

              write(string,*) 'Wrong boundary condition 1',j,k,bc1(j,k)
              call close_program(trim(string))

           end select

        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  endif

  if(debug_l2) write(*,*) 'end   BOUNDARY_FACE_1',rank

  return

end subroutine boundary_face_1

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_FACE_2:
!
!> Fills ghost cells at the right (upper x) boundary with values from boundary conditions:
!!    - 1 : free-flow
!!    - 2 : reflexive
!!    - 3 : periodic
!!    - 4 : inflow
!<
subroutine boundary_face_2(array)

  use mod_gridarrays, only : bc2,inflow2
  use mod_variables , only : nvar,debug_l2,str,adim
  use mod_constants
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: j,k,h,n
  real(dp), dimension(nvar                                    ) :: kron
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  character (len=str)                                           :: string

  if(debug_l2) write(*,*) 'begin BOUNDARY_FACE_2',rank

  kron    =  one
  kron(2) = -one

  if(same_bc(2))then

     select case(boundc_2)

     case(1)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,nxloc(1)+n,j,k) = array(h,nxloc(1),j,k)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case(2)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,nxloc(1)+n,j,k) = kron(h)*array(h,nxloc(1)-n+1,j,k)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case(3)

#if MPI==0
        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,nxloc(1)+n,j,k) = array(h,1+n-1,j,k)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL
#endif

     case(4)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,nxloc(1)+n,j,k) = inflow2(h,n,j,k)
                 enddo
                 !!call timestep_cell(nxloc(1)+n,j,k,nvar,array(1:nvar,nxloc(1)+n,j,k))
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case default

        write(string,*) 'Wrong general boundary condition 2',boundc_2
        call close_program(trim(string))

     end select

  else

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,h,n)
     !$OMP DO COLLAPSE(2)
     do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
        do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost

           select case(bc2(j,k))

           case(1)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,nxloc(1)+n,j,k) = array(h,nxloc(1),j,k)
                 enddo
              enddo

           case(2)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,nxloc(1)+n,j,k) = kron(h)*array(h,nxloc(1)-n+1,j,k)
                 enddo
              enddo

           case(3)

#if MPI==0
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,nxloc(1)+n,j,k) = array(h,1+n-1,j,k)
                 enddo
              enddo
#endif

           case(4)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,nxloc(1)+n,j,k) = inflow2(h,n,j,k)
                 enddo
                 !!call timestep_cell(nxloc(1)+n,j,k,nvar,array(1:nvar,nxloc(1)+n,j,k))
              enddo

           case default

              write(string,*) 'Wrong boundary condition 2',j,k,bc2(j,k)
              call close_program(trim(string))

           end select

        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  endif

  if(debug_l2) write(*,*) 'end BOUNDARY_FACE_2',rank

  return

end subroutine boundary_face_2

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_FACE_3:
! 
!> Fills ghost cells at the bottom (lower y) boundary with values from boundary conditions:
!!    - 1 : free-flow
!!    - 2 : reflexive
!!    - 3 : periodic
!!    - 4 : inflow
!<
subroutine boundary_face_3(array)

  use mod_gridarrays, only : bc3,inflow3
  use mod_variables , only : nvar,debug_l2,str,adim
  use mod_constants
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,k,h,n
  real(dp), dimension(nvar                                    ) :: kron
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  character (len=str)                                           :: string

  if(debug_l2) write(*,*) 'begin BOUNDARY_FACE_3',rank

  kron    =  one
  kron(3) = -one

  if(same_bc(3))then

     select case(boundc_3)

     case(1)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,1-n,k) = array(h,i,1,k)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case(2)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,1-n,k) = kron(h)*array(h,i,1+n-1,k)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case(3)

#if MPI==0
        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,1-n,k) = array(h,i,nxloc(2)-n+1,k)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL
#endif

     case(4)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,1-n,k) = inflow3(h,nghost-n+1,i,k)
                 enddo
                 !!call timestep_cell(i,1-n,k,nvar,array(1:nvar,i,1-n,k))
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case default

        write(string,*) 'Wrong general boundary condition 3',boundc_1
        call close_program(trim(string))

     end select

  else

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,h,n)
     !$OMP DO COLLAPSE(2)
     do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
        do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost

           select case(bc3(i,k))

           case(1)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,1-n,k) = array(h,i,1,k)
                 enddo
              enddo

           case(2)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,1-n,k) = kron(h)*array(h,i,1+n-1,k)
                 enddo
              enddo

           case(3)

#if MPI==0
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,1-n,k) = array(h,i,nxloc(2)-n+1,k)
                 enddo
              enddo
#endif

           case(4)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,1-n,k) = inflow3(h,nghost-n+1,i,k)
                 enddo
                 !!call timestep_cell(i,1-n,k,nvar,array(1:nvar,i,1-n,k))
              enddo

           case default

              write(string,*) 'Wrong boundary condition 3',i,k
              call close_program(trim(string))

           end select

        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  endif

  if(debug_l2) write(*,*) 'end   BOUNDARY_FACE_3',rank
  
  return

end subroutine boundary_face_3

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_FACE_4:
!
!> Fills ghost cells at the top (upper y) boundary with values from boundary conditions:
!!    - 1 : free-flow
!!    - 2 : reflexive
!!    - 3 : periodic
!!    - 4 : inflow
!<
subroutine boundary_face_4(array)

  use mod_gridarrays, only : bc4,inflow4
  use mod_variables , only : nvar,debug_l2,str,adim
  use mod_constants
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,k,h,n
  real(dp), dimension(nvar                                    ) :: kron
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  character (len=str)                                           :: string

  if(debug_l2) write(*,*) 'begin BOUNDARY_FACE_4',rank

  kron    =  one
  kron(3) = -one

  if(same_bc(4))then

     select case(boundc_4)

     case(1)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,nxloc(2)+n,k) = array(h,i,nxloc(2),k)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case(2)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,nxloc(2)+n,k) = kron(h)*array(h,i,nxloc(2)-n+1,k)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case(3)

#if MPI==0
        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,nxloc(2)+n,k) = array(h,i,1+n-1,k)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL
#endif

     case(4)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,h,n)
        !$OMP DO COLLAPSE(2)
        do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,nxloc(2)+n,k) = inflow4(h,n,i,k)
                 enddo
                 !!call timestep_cell(i,nxloc(2)+n,k,nvar,array(1:nvar,i,nxloc(2)+n,k))
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case default

        write(string,*) 'Wrong general boundary condition 4',boundc_4
        call close_program(trim(string))

     end select

  else

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,h,n)
     !$OMP DO COLLAPSE(2)  
     do k = 1-adim(3)*nghost,nxloc(3)+adim(3)*nghost
        do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost

           select case(bc4(i,k))

           case(1)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,nxloc(2)+n,k) = array(h,i,nxloc(2),k)
                 enddo
              enddo

           case(2)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,nxloc(2)+n,k) = kron(h)*array(h,i,nxloc(2)-n+1,k)
                 enddo
              enddo

           case(3)

#if MPI==0
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,nxloc(2)+n,k) = array(h,i,1+n-1,k)
                 enddo
              enddo
#endif

           case(4)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,nxloc(2)+n,k) = inflow4(h,n,i,k)
                 enddo
                 !!call timestep_cell(i,nxloc(2)+n,k,nvar,array(1:nvar,i,nxloc(2)+n,k))
              enddo

           case default

              write(string,*) 'Wrong boundary condition 4',i,k,bc4(i,k)
              call close_program(trim(string))

           end select

        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  endif

  if(debug_l2) write(*,*) 'end   BOUNDARY_FACE_4',rank

  return

end subroutine boundary_face_4

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_FACE_5:
!
!> Fills ghost cells at the front (lower z) boundary with values from boundary conditions:
!!    - 1 : free-flow
!!    - 2 : reflexive
!!    - 3 : periodic
!!    - 4 : inflow
!<
subroutine boundary_face_5(array)

  use mod_gridarrays, only : bc5,inflow5
  use mod_variables , only : nvar,debug_l2,str,adim
  use mod_constants
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,h,n
  real(dp), dimension(nvar                                    ) :: kron
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  character (len=str)                                           :: string

  if(debug_l2) write(*,*) 'begin BOUNDARY_FACE_5',rank

  kron    =  one
  kron(4) = -one

  if(same_bc(5))then

     select case(boundc_5)

     case(1)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,h,n)
        !$OMP DO COLLAPSE(2)
        do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,1-n) = array(h,i,j,1)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case(2)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,h,n)
        !$OMP DO COLLAPSE(2)
        do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,1-n) = kron(h)*array(h,i,j,1+n-1)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case(3)

#if MPI==0
        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,h,n)
        !$OMP DO COLLAPSE(2)
        do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,1-n) = array(h,i,j,nxloc(3)-n+1)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL
#endif

     case(4)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,h,n)
        !$OMP DO COLLAPSE(2)
        do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,1-n) = inflow5(h,nghost-n+1,i,j)
                 enddo
                 !!call timestep_cell(i,j,1-n,nvar,array(1:nvar,i,j,1-n))
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case default

        write(string,*) 'Wrong general boundary condition 5',boundc_5
        call close_program(trim(string))

     end select

  else

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,h,n)
     !$OMP DO COLLAPSE(2)
     do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
        do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost

           select case(bc5(i,j))

           case(1)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,1-n) = array(h,i,j,1)
                 enddo
              enddo

           case(2)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,1-n) = kron(h)*array(h,i,j,1+n-1)
                 enddo
              enddo

           case(3)

#if MPI==0
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,1-n) = array(h,i,j,nxloc(3)-n+1)
                 enddo
              enddo
#endif

           case(4)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,1-n) = inflow5(h,nghost-n+1,i,j)
                 enddo
                 !call timestep_cell(i,j,1-n,nvar,array(1:nvar,i,j,1-n))
              enddo

           case default

              write(string,*) 'Wrong boundary condition 5',i,j,bc5(i,j)
              call close_program(trim(string))

           end select

        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  endif

  if(debug_l2) write(*,*) 'end   BOUNDARY_FACE_5',rank

  return

end subroutine boundary_face_5

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_FACE_6:
!
!> Fills ghost cells at the back (upper z) boundary with values from boundary conditions:
!!    - 1 : free-flow
!!    - 2 : reflexive
!!    - 3 : periodic
!!    - 4 : inflow
!<
subroutine boundary_face_6(array)

  use mod_gridarrays, only : bc6,inflow6
  use mod_variables , only : nvar,debug_l2,str,adim
  use mod_constants
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,h,n
  real(dp), dimension(nvar                                    ) :: kron
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  character (len=str)                                           :: string

  if(debug_l2) write(*,*) 'begin BOUNDARY_FACE_6',rank

  kron    =  one
  kron(4) = -one

  if(same_bc(6))then

     select case(boundc_6)

     case(1)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,h,n)
        !$OMP DO COLLAPSE(2)
        do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,nxloc(3)+n) = array(h,i,j,nxloc(3))
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case(2)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,h,n)
        !$OMP DO COLLAPSE(2)
        do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,nxloc(3)+n) = kron(h)*array(h,i,j,nxloc(3)-n+1)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case(3)

#if MPI==0
        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,h,n)
        !$OMP DO COLLAPSE(2)
        do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,nxloc(3)+n) = array(h,i,j,1+n-1)
                 enddo
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL
#endif

     case(4)

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,h,n)
        !$OMP DO COLLAPSE(2)
        do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
           do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,nxloc(3)+n) = inflow6(h,n,i,j)
                 enddo
                 !call timestep_cell(i,j,nxloc(3)+n,nvar,array(1:nvar,i,j,nxloc(3)+n))
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     case default

        write(string,*) 'Wrong general boundary condition 6',boundc_6
        call close_program(trim(string))

     end select

  else

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,h,n)
     !$OMP DO COLLAPSE(2)
     do j = 1-adim(2)*nghost,nxloc(2)+adim(2)*nghost
        do i = 1-adim(1)*nghost,nxloc(1)+adim(1)*nghost

           select case(bc6(i,j))

           case(1)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,nxloc(3)+n) = array(h,i,j,nxloc(3))
                 enddo
              enddo

           case(2)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,nxloc(3)+n) = kron(h)*array(h,i,j,nxloc(3)-n+1)
                 enddo
              enddo

           case(3)

#if MPI==0
              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,nxloc(3)+n) = array(h,i,j,1+n-1)
                 enddo
              enddo
#endif

           case(4)

              do n = 1,nghost
                 do h = 1,nvar
                    array(h,i,j,nxloc(3)+n) = inflow6(h,n,i,j)
                 enddo
                 !call timestep_cell(i,j,nxloc(3)+n,nvar,array(1:nvar,i,j,nxloc(3)+n))
              enddo

           case default

              write(string,*) 'Wrong boundary condition 6',i,j
              call close_program(trim(string))

           end select

        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  endif

  if(debug_l2) write(*,*) 'end   BOUNDARY_FACE_6',rank

  return

end subroutine boundary_face_6

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_CORNER_2D_1:
!
!> Fills ghost cells at the lower left corner of a 2D grid.
!<
subroutine boundary_corner_2D_1(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,p1,p2
  
  if(debug_l2) write(*,*) 'begin BOUNDARY_CORNER_2D_1',rank

  i = 1 - 1
  j = 1 - 1

  do n = 1,nghost*nghost
     d1 = array(1,i  ,j+1,1)
     d2 = array(1,i+1,j  ,1)
     u1 = array(2,i  ,j+1,1)/d1
     u2 = array(2,i+1,j  ,1)/d2
     v1 = array(3,i  ,j+1,1)/d1
     v2 = array(3,i+1,j  ,1)/d2
     p1 = g1*(array(5,i,j+1,1) - half*(array(2,i,j+1,1)**2+array(3,i,j+1,1)**2+array(4,i,j+1,1)**2)/array(1,i,j+1,1))
     p2 = g1*(array(5,i+1,j,1) - half*(array(2,i+1,j,1)**2+array(3,i+1,j,1)**2+array(4,i+1,j,1)**2)/array(1,i+1,j,1))
  
     array(1,i,j,1) = half*(d1+d2)
     array(2,i,j,1) = half*(u1+u2) * array(1,i,j,1)
     array(3,i,j,1) = half*(v1+v2) * array(1,i,j,1)
     array(4,i,j,1) = zero
     array(5,i,j,1) = half*(array(2,i,j,1)**2+array(3,i,j,1)**2+array(4,i,j,1)**2)/array(1,i,j,1) + half*(p1+p2)/g1
     j = j - 1
     if(j < 1-nghost)then
        j = 1 - 1
        i = i - 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_CORNER_2D_1',rank

  return

end subroutine boundary_corner_2D_1

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_CORNER_2D_2:
!
!> Fills ghost cells at the upper left corner of a 2D grid.
!<
subroutine boundary_corner_2D_2(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_CORNER_2D_2',rank
  
  i = 1 - 1
  j = nxloc(2)   + 1

  do n = 1,nghost*nghost
     d1 = array(1,i  ,j-1,1)
     d2 = array(1,i+1,j  ,1)
     u1 = array(2,i  ,j-1,1)/d1
     u2 = array(2,i+1,j  ,1)/d2
     v1 = array(3,i  ,j-1,1)/d1
     v2 = array(3,i+1,j  ,1)/d2
     p1 = g1*(array(5,i,j-1,1) - half*(array(2,i,j-1,1)**2+array(3,i,j-1,1)**2+array(4,i,j-1,1)**2)/array(1,i,j-1,1))
     p2 = g1*(array(5,i+1,j,1) - half*(array(2,i+1,j,1)**2+array(3,i+1,j,1)**2+array(4,i+1,j,1)**2)/array(1,i+1,j,1))

     array(1,i,j,1) = half*(d1+d2)
     array(2,i,j,1) = half*(u1+u2) * array(1,i,j,1)
     array(3,i,j,1) = half*(v1+v2) * array(1,i,j,1)
     array(4,i,j,1) = zero
     array(5,i,j,1) = half*(array(2,i,j,1)**2+array(3,i,j,1)**2+array(4,i,j,1)**2)/array(1,i,j,1) + half*(p1+p2)/g1
     j = j + 1
     if(j > nxloc(2)+nghost)then
        j = nxloc(2) + 1
        i = i - 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_CORNER_2D_2',rank

  return

end subroutine boundary_corner_2D_2

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_CORNER_2D_3:
!
!> Fills ghost cells at the lower right corner of a 2D grid.
!<
subroutine boundary_corner_2D_3(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_CORNER_2D_3',rank

  i = nxloc(1)   + 1
  j = 1 - 1

  do n = 1,nghost*nghost
     d1 = array(1,i  ,j+1,1)
     d2 = array(1,i-1,j  ,1)
     u1 = array(2,i  ,j+1,1)/d1
     u2 = array(2,i-1,j  ,1)/d2
     v1 = array(3,i  ,j+1,1)/d1
     v2 = array(3,i-1,j  ,1)/d2
     p1 = g1*(array(5,i,j+1,1) - half*(array(2,i,j+1,1)**2+array(3,i,j+1,1)**2+array(4,i,j+1,1)**2)/array(1,i,j+1,1))
     p2 = g1*(array(5,i-1,j,1) - half*(array(2,i-1,j,1)**2+array(3,i-1,j,1)**2+array(4,i-1,j,1)**2)/array(1,i-1,j,1))
     
!      write(*,*) i,j,d1,d2,u1,u2,v1,v2,p1,p2
     
     array(1,i,j,1) = half*(d1+d2)
     array(2,i,j,1) = half*(u1+u2) * array(1,i,j,1)
     array(3,i,j,1) = half*(v1+v2) * array(1,i,j,1)
     array(4,i,j,1) = zero
     array(5,i,j,1) = half*(array(2,i,j,1)**2+array(3,i,j,1)**2+array(4,i,j,1)**2)/array(1,i,j,1) + half*(p1+p2)/g1
!      array(5,i,j,1) = half * (array(5,i,j+1,1) + array(5,i-1,j,1))
     j = j - 1
     if(j < 1-nghost)then
        j = 1 - 1
        i = i + 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_CORNER_2D_3',rank

  return

end subroutine boundary_corner_2D_3

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_CORNER_2D_4:
!
!> Fills ghost cells at the upper right corner of a 2D grid.
!<
subroutine boundary_corner_2D_4(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_CORNER_2D_4',rank

  i = nxloc(1) + 1
  j = nxloc(2) + 1

  do n = 1,nghost*nghost
     d1 = array(1,i  ,j-1,1)
     d2 = array(1,i-1,j  ,1)
     u1 = array(2,i  ,j-1,1)/d1
     u2 = array(2,i-1,j  ,1)/d2
     v1 = array(3,i  ,j-1,1)/d1
     v2 = array(3,i-1,j  ,1)/d2
     p1 = g1*(array(5,i,j-1,1) - half*(array(2,i,j-1,1)**2+array(3,i,j-1,1)**2+array(4,i,j-1,1)**2)/array(1,i,j-1,1))
     p2 = g1*(array(5,i-1,j,1) - half*(array(2,i-1,j,1)**2+array(3,i-1,j,1)**2+array(4,i-1,j,1)**2)/array(1,i-1,j,1))
     
     array(1,i,j,1) = half*(d1+d2)
     array(2,i,j,1) = half*(u1+u2) * array(1,i,j,1)
     array(3,i,j,1) = half*(v1+v2) * array(1,i,j,1)
     array(4,i,j,1) = zero
     array(5,i,j,1) = half*(array(2,i,j,1)**2+array(3,i,j,1)**2+array(4,i,j,1)**2)/array(1,i,j,1) + half*(p1+p2)/g1
!      array(5,i,j,1) = half * (array(5,i,j-1,1) + array(5,i-1,j,1))
     j = j + 1
     if(j > nxloc(2)+nghost)then
        j = nxloc(2) + 1
        i = i + 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_CORNER_2D_4',rank

  return

end subroutine boundary_corner_2D_4

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_EDGE_3D_01:
!
!> Fills ghost cells in the lower left edge along the z direction of a 3D grid.
!<
subroutine boundary_edge_3D_01(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,w1,w2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_EDGE_3D_1',rank

  i = 1 - 1
  j = 1 - 1

  do n = 1,nghost*nghost
     do k = 1,nxloc(3)
        d1 = array(1,i  ,j+1,k)
        d2 = array(1,i+1,j  ,k)
        u1 = array(2,i  ,j+1,k)/d1
        u2 = array(2,i+1,j  ,k)/d2
        v1 = array(3,i  ,j+1,k)/d1
        v2 = array(3,i+1,j  ,k)/d2
        w1 = array(4,i  ,j+1,k)/d1
        w2 = array(4,i+1,j  ,k)/d2
        p1 = g1*(array(5,i,j+1,k) - half*(array(2,i,j+1,k)**2+array(3,i,j+1,k)**2+array(4,i,j+1,k)**2)/array(1,i,j+1,k))
        p2 = g1*(array(5,i+1,j,k) - half*(array(2,i+1,j,k)**2+array(3,i+1,j,k)**2+array(4,i+1,j,k)**2)/array(1,i+1,j,k))
        
        array(1,i,j,k) = half*(d1+d2)
        array(2,i,j,k) = half*(u1+u2) * array(1,i,j,k)
        array(3,i,j,k) = half*(v1+v2) * array(1,i,j,k)
        array(4,i,j,k) = half*(w1+w2) * array(1,i,j,k)
        array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + half*(p1+p2)/g1
        
!         array(1,i,j,k) = array(1,i+1,j+1,k)
!         array(2,i,j,k) = array(2,i  ,j+1,k)
!         array(3,i,j,k) = array(3,i+1,j  ,k)
!         array(4,i,j,k) = array(4,i+1,j+1,k)
!         array(5,i,j,k) = half * (array(5,i,j+1,k) + array(5,i+1,j,k))
     enddo
     j = j - 1
     if(j < 1-nghost)then
        j = 1 - 1
        i = i - 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_EDGE_3D_1',rank

  return

end subroutine boundary_edge_3D_01

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_EDGE_3D_02:
!
!> Fills ghost cells in the upper left edge along the z direction of a 3D grid.
!<
subroutine boundary_edge_3D_02(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,w1,w2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_EDGE_3D_2',rank

  i = 1 - 1
  j = nxloc(2)   + 1

  do n = 1,nghost*nghost
     do k = 1,nxloc(3)
        d1 = array(1,i  ,j-1,k)
        d2 = array(1,i+1,j  ,k)
        u1 = array(2,i  ,j-1,k)/d1
        u2 = array(2,i+1,j  ,k)/d2
        v1 = array(3,i  ,j-1,k)/d1
        v2 = array(3,i+1,j  ,k)/d2
        w1 = array(4,i  ,j-1,k)/d1
        w2 = array(4,i+1,j  ,k)/d2
        p1 = g1*(array(5,i,j-1,k) - half*(array(2,i,j-1,k)**2+array(3,i,j-1,k)**2+array(4,i,j-1,k)**2)/array(1,i,j-1,k))
        p2 = g1*(array(5,i+1,j,k) - half*(array(2,i+1,j,k)**2+array(3,i+1,j,k)**2+array(4,i+1,j,k)**2)/array(1,i+1,j,k))
        
        array(1,i,j,k) = half*(d1+d2)
        array(2,i,j,k) = half*(u1+u2) * array(1,i,j,k)
        array(3,i,j,k) = half*(v1+v2) * array(1,i,j,k)
        array(4,i,j,k) = half*(w1+w2) * array(1,i,j,k)
        array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + half*(p1+p2)/g1
     
!         array(1,i,j,k) = array(1,i+1,j-1,k)
!         array(2,i,j,k) = array(2,i  ,j-1,k)
!         array(3,i,j,k) = array(3,i+1,j  ,k)
!         array(4,i,j,k) = array(4,i+1,j-1,k)
!         array(5,i,j,k) = half * (array(5,i,j-1,k) + array(5,i+1,j,k))
     enddo
     j = j + 1
     if(j > nxloc(2)+nghost)then
        j = nxloc(2) + 1
        i = i - 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_EDGE_3D_2',rank

  return

end subroutine boundary_edge_3D_02

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_EDGE_3D_03:
!
!> Fills ghost cells in the lower right edge along the z direction of a 3D grid.
!<
subroutine boundary_edge_3D_03(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,w1,w2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_EDGE_3D_3',rank

  i = nxloc(1)   + 1
  j = 1 - 1

  do n = 1,nghost*nghost
     do k = 1,nxloc(3)
        d1 = array(1,i  ,j+1,k)
        d2 = array(1,i-1,j  ,k)
        u1 = array(2,i  ,j+1,k)/d1
        u2 = array(2,i-1,j  ,k)/d2
        v1 = array(3,i  ,j+1,k)/d1
        v2 = array(3,i-1,j  ,k)/d2
        w1 = array(4,i  ,j+1,k)/d1
        w2 = array(4,i-1,j  ,k)/d2
        p1 = g1*(array(5,i,j+1,k) - half*(array(2,i,j+1,k)**2+array(3,i,j+1,k)**2+array(4,i,j+1,k)**2)/array(1,i,j+1,k))
        p2 = g1*(array(5,i-1,j,k) - half*(array(2,i-1,j,k)**2+array(3,i-1,j,k)**2+array(4,i-1,j,k)**2)/array(1,i-1,j,k))
        
        array(1,i,j,k) = half*(d1+d2)
        array(2,i,j,k) = half*(u1+u2) * array(1,i,j,k)
        array(3,i,j,k) = half*(v1+v2) * array(1,i,j,k)
        array(4,i,j,k) = half*(w1+w2) * array(1,i,j,k)
        array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + half*(p1+p2)/g1
     
!         array(1,i,j,k) = array(1,i-1,j+1,k)
!         array(2,i,j,k) = array(2,i  ,j+1,k)
!         array(3,i,j,k) = array(3,i-1,j  ,k)
!         array(4,i,j,k) = array(4,i-1,j+1,k)
!         array(5,i,j,k) = half * (array(5,i,j+1,k) + array(5,i-1,j,k))
     enddo
     j = j - 1
     if(j < 1-nghost)then
        j = 1 - 1
        i = i + 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_EDGE_3D_3',rank

  return

end subroutine boundary_edge_3D_03

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_EDGE_3D_04:
!
!> Fills ghost cells in the upper right edge along the z direction of a 3D grid.
!<
subroutine boundary_edge_3D_04(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,w1,w2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_EDGE_3D_4',rank

  i = nxloc(1) + 1
  j = nxloc(2) + 1

  do n = 1,nghost*nghost
     do k = 1,nxloc(3)
        d1 = array(1,i  ,j-1,k)
        d2 = array(1,i-1,j  ,k)
        u1 = array(2,i  ,j-1,k)/d1
        u2 = array(2,i-1,j  ,k)/d2
        v1 = array(3,i  ,j-1,k)/d1
        v2 = array(3,i-1,j  ,k)/d2
        w1 = array(4,i  ,j-1,k)/d1
        w2 = array(4,i-1,j  ,k)/d2
        p1 = g1*(array(5,i,j-1,k) - half*(array(2,i,j-1,k)**2+array(3,i,j-1,k)**2+array(4,i,j-1,k)**2)/array(1,i,j-1,k))
        p2 = g1*(array(5,i-1,j,k) - half*(array(2,i-1,j,k)**2+array(3,i-1,j,k)**2+array(4,i-1,j,k)**2)/array(1,i-1,j,k))
        
        array(1,i,j,k) = half*(d1+d2)
        array(2,i,j,k) = half*(u1+u2) * array(1,i,j,k)
        array(3,i,j,k) = half*(v1+v2) * array(1,i,j,k)
        array(4,i,j,k) = half*(w1+w2) * array(1,i,j,k)
        array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + half*(p1+p2)/g1
     
!         array(1,i,j,k) = array(1,i-1,j-1,k)
!         array(2,i,j,k) = array(2,i  ,j-1,k)
!         array(3,i,j,k) = array(3,i-1,j  ,k)
!         array(4,i,j,k) = array(4,i-1,j-1,k)
!         array(5,i,j,k) = half * (array(5,i,j-1,k) + array(5,i-1,j,k))
     enddo
     j = j + 1
     if(j > nxloc(2)+nghost)then
        j = nxloc(2) + 1
        i = i + 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_EDGE_3D_4',rank

  return

end subroutine boundary_edge_3D_04

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_EDGE_3D_05:
!
!> Fills ghost cells in the lower left edge along the x direction of a 3D grid.
!<
subroutine boundary_edge_3D_05(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,w1,w2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_EDGE_3D_5',rank

  j = 1 - 1
  k = 1 - 1

  do n = 1,nghost*nghost
     do i = 1,nxloc(1)
        d1 = array(1,i,j+1,k  )
        d2 = array(1,i,j  ,k+1)
        u1 = array(2,i,j+1,k  )/d1
        u2 = array(2,i,j  ,k+1)/d2
        v1 = array(3,i,j+1,k  )/d1
        v2 = array(3,i,j  ,k+1)/d2
        w1 = array(4,i,j+1,k  )/d1
        w2 = array(4,i,j  ,k+1)/d2
        p1 = g1*(array(5,i,j+1,k) - half*(array(2,i,j+1,k)**2+array(3,i,j+1,k)**2+array(4,i,j+1,k)**2)/array(1,i,j+1,k))
        p2 = g1*(array(5,i,j,k+1) - half*(array(2,i,j,k+1)**2+array(3,i,j,k+1)**2+array(4,i,j,k+1)**2)/array(1,i,j,k+1))
        
        array(1,i,j,k) = half*(d1+d2)
        array(2,i,j,k) = half*(u1+u2) * array(1,i,j,k)
        array(3,i,j,k) = half*(v1+v2) * array(1,i,j,k)
        array(4,i,j,k) = half*(w1+w2) * array(1,i,j,k)
        array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + half*(p1+p2)/g1
     
!         array(1,i,j,k) = array(1,i,j+1,k+1)
!         array(2,i,j,k) = array(2,i,j+1,k+1)
!         array(3,i,j,k) = array(3,i,j  ,k+1)
!         array(4,i,j,k) = array(4,i,j+1,k  )
!         array(5,i,j,k) = half * (array(5,i,j+1,k) + array(5,i,j,k+1))
     enddo
     j = j - 1
     if(j < 1-nghost)then
        j = 1 - 1
        k = k - 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_EDGE_3D_5',rank

  return

end subroutine boundary_edge_3D_05

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_EDGE_3D_06:
!
!> Fills ghost cells in the upper left edge along the x direction of a 3D grid.
!<
subroutine boundary_edge_3D_06(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,w1,w2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_EDGE_3D_6',rank

  j = 1 - 1
  k = nxloc(3)   + 1

  do n = 1,nghost*nghost
     do i = 1,nxloc(1)
        d1 = array(1,i,j+1,k  )
        d2 = array(1,i,j  ,k-1)
        u1 = array(2,i,j+1,k  )/d1
        u2 = array(2,i,j  ,k-1)/d2
        v1 = array(3,i,j+1,k  )/d1
        v2 = array(3,i,j  ,k-1)/d2
        w1 = array(4,i,j+1,k  )/d1
        w2 = array(4,i,j  ,k-1)/d2
        p1 = g1*(array(5,i,j+1,k) - half*(array(2,i,j+1,k)**2+array(3,i,j+1,k)**2+array(4,i,j+1,k)**2)/array(1,i,j+1,k))
        p2 = g1*(array(5,i,j,k-1) - half*(array(2,i,j,k-1)**2+array(3,i,j,k-1)**2+array(4,i,j,k-1)**2)/array(1,i,j,k-1))
        
        array(1,i,j,k) = half*(d1+d2)
        array(2,i,j,k) = half*(u1+u2) * array(1,i,j,k)
        array(3,i,j,k) = half*(v1+v2) * array(1,i,j,k)
        array(4,i,j,k) = half*(w1+w2) * array(1,i,j,k)
        array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + half*(p1+p2)/g1
     
!         array(1,i,j,k) = array(1,i,j+1,k-1)
!         array(2,i,j,k) = array(2,i,j+1,k-1)
!         array(3,i,j,k) = array(3,i,j  ,k-1)
!         array(4,i,j,k) = array(4,i,j+1,k  )
!         array(5,i,j,k) = half * (array(5,i,j+1,k) + array(5,i,j,k-1))
     enddo
     j = j - 1
     if(j < 1-nghost)then
        j = 1 - 1
        k = k + 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_EDGE_3D_6',rank

  return

end subroutine boundary_edge_3D_06

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_EDGE_3D_07:
!
!> Fills ghost cells in the lower right edge along the x direction of a 3D grid.
!<
subroutine boundary_edge_3D_07(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,w1,w2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_EDGE_3D_7',rank

  j = nxloc(2)   + 1
  k = 1 - 1

  do n = 1,nghost*nghost
     do i = 1,nxloc(1)
        d1 = array(1,i,j-1,k  )
        d2 = array(1,i,j  ,k+1)
        u1 = array(2,i,j-1,k  )/d1
        u2 = array(2,i,j  ,k+1)/d2
        v1 = array(3,i,j-1,k  )/d1
        v2 = array(3,i,j  ,k+1)/d2
        w1 = array(4,i,j-1,k  )/d1
        w2 = array(4,i,j  ,k+1)/d2
        p1 = g1*(array(5,i,j-1,k) - half*(array(2,i,j-1,k)**2+array(3,i,j-1,k)**2+array(4,i,j-1,k)**2)/array(1,i,j-1,k))
        p2 = g1*(array(5,i,j,k+1) - half*(array(2,i,j,k+1)**2+array(3,i,j,k+1)**2+array(4,i,j,k+1)**2)/array(1,i,j,k+1))
        
        array(1,i,j,k) = half*(d1+d2)
        array(2,i,j,k) = half*(u1+u2) * array(1,i,j,k)
        array(3,i,j,k) = half*(v1+v2) * array(1,i,j,k)
        array(4,i,j,k) = half*(w1+w2) * array(1,i,j,k)
        array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + half*(p1+p2)/g1
     
!         array(1,i,j,k) = array(1,i,j-1,k+1)
!         array(2,i,j,k) = array(2,i,j-1,k+1)
!         array(3,i,j,k) = array(3,i,j  ,k+1)
!         array(4,i,j,k) = array(4,i,j-1,k  )
!         array(5,i,j,k) = half * (array(5,i,j-1,k) + array(5,i,j,k+1))
     enddo
     j = j + 1
     if(j > nxloc(2)+nghost)then
        j = nxloc(2) + 1
        k = k - 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_EDGE_3D_7',rank

  return

end subroutine boundary_edge_3D_07

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_EDGE_3D_08:
!
!> Fills ghost cells in the upper right edge along the x direction of a 3D grid.
!<
subroutine boundary_edge_3D_08(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,w1,w2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_EDGE_3D_8',rank

  j = nxloc(2) + 1
  k = nxloc(3) + 1

  do n = 1,nghost*nghost
     do i = 1,nxloc(1)
        d1 = array(1,i,j-1,k  )
        d2 = array(1,i,j  ,k-1)
        u1 = array(2,i,j-1,k  )/d1
        u2 = array(2,i,j  ,k-1)/d2
        v1 = array(3,i,j-1,k  )/d1
        v2 = array(3,i,j  ,k-1)/d2
        w1 = array(4,i,j-1,k  )/d1
        w2 = array(4,i,j  ,k-1)/d2
        p1 = g1*(array(5,i,j-1,k) - half*(array(2,i,j-1,k)**2+array(3,i,j-1,k)**2+array(4,i,j-1,k)**2)/array(1,i,j-1,k))
        p2 = g1*(array(5,i,j,k-1) - half*(array(2,i,j,k-1)**2+array(3,i,j,k-1)**2+array(4,i,j,k-1)**2)/array(1,i,j,k-1))
        
        array(1,i,j,k) = half*(d1+d2)
        array(2,i,j,k) = half*(u1+u2) * array(1,i,j,k)
        array(3,i,j,k) = half*(v1+v2) * array(1,i,j,k)
        array(4,i,j,k) = half*(w1+w2) * array(1,i,j,k)
        array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + half*(p1+p2)/g1
     
!         array(1,i,j,k) = array(1,i,j-1,k-1)
!         array(2,i,j,k) = array(2,i,j-1,k-1)
!         array(3,i,j,k) = array(3,i,j-1,k  )
!         array(4,i,j,k) = array(4,i,j  ,k-1)
!         array(5,i,j,k) = half * (array(5,i,j-1,k) + array(5,i,j,k-1))
     enddo
     j = j + 1
     if(j > nxloc(2)+nghost)then
        j = nxloc(2) + 1
        k = k + 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_EDGE_3D_8',rank

  return

end subroutine boundary_edge_3D_08

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_EDGE_3D_09:
!
!> Fills ghost cells in the lower left edge along the y direction of a 3D grid.
!<
subroutine boundary_edge_3D_09(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,w1,w2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_EDGE_3D_9',rank

  i = 1 - 1
  k = 1 - 1

  do n = 1,nghost*nghost
     do j = 1,nxloc(2)
        d1 = array(1,i+1,j,k  )
        d2 = array(1,i  ,j,k+1)
        u1 = array(2,i+1,j,k  )/d1
        u2 = array(2,i  ,j,k+1)/d2
        v1 = array(3,i+1,j,k  )/d1
        v2 = array(3,i  ,j,k+1)/d2
        w1 = array(4,i+1,j,k  )/d1
        w2 = array(4,i  ,j,k+1)/d2
        p1 = g1*(array(5,i+1,j,k) - half*(array(2,i+1,j,k)**2+array(3,i+1,j,k)**2+array(4,i+1,j,k)**2)/array(1,i+1,j,k))
        p2 = g1*(array(5,i,j,k+1) - half*(array(2,i,j,k+1)**2+array(3,i,j,k+1)**2+array(4,i,j,k+1)**2)/array(1,i,j,k+1))
        
        array(1,i,j,k) = half*(d1+d2)
        array(2,i,j,k) = half*(u1+u2) * array(1,i,j,k)
        array(3,i,j,k) = half*(v1+v2) * array(1,i,j,k)
        array(4,i,j,k) = half*(w1+w2) * array(1,i,j,k)
        array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + half*(p1+p2)/g1
     
!         array(1,i,j,k) = array(1,i+1,j,k+1)
!         array(2,i,j,k) = array(2,i  ,j,k+1)
!         array(3,i,j,k) = array(3,i+1,j,k+1)
!         array(4,i,j,k) = array(4,i+1,j,k  )
!         array(5,i,j,k) = half * (array(5,i+1,j,k) + array(5,i,j,k+1))
     enddo
     i = i - 1
     if(i < 1-nghost)then
        i = 1 - 1
        k = k - 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_EDGE_3D_9',rank

  return

end subroutine boundary_edge_3D_09

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_EDGE_3D_10:
!
!> Fills ghost cells in the upper left edge along the y direction of a 3D grid.
!<
subroutine boundary_edge_3D_10(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,w1,w2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_EDGE_3D_10',rank

  i = nxloc(1)   + 1
  k = 1 - 1

  do n = 1,nghost*nghost
     do j = 1,nxloc(2)
        d1 = array(1,i-1,j,k  )
        d2 = array(1,i  ,j,k+1)
        u1 = array(2,i-1,j,k  )/d1
        u2 = array(2,i  ,j,k+1)/d2
        v1 = array(3,i-1,j,k  )/d1
        v2 = array(3,i  ,j,k+1)/d2
        w1 = array(4,i-1,j,k  )/d1
        w2 = array(4,i  ,j,k+1)/d2
        p1 = g1*(array(5,i-1,j,k) - half*(array(2,i-1,j,k)**2+array(3,i-1,j,k)**2+array(4,i-1,j,k)**2)/array(1,i-1,j,k))
        p2 = g1*(array(5,i,j,k+1) - half*(array(2,i,j,k+1)**2+array(3,i,j,k+1)**2+array(4,i,j,k+1)**2)/array(1,i,j,k+1))
        
        array(1,i,j,k) = half*(d1+d2)
        array(2,i,j,k) = half*(u1+u2) * array(1,i,j,k)
        array(3,i,j,k) = half*(v1+v2) * array(1,i,j,k)
        array(4,i,j,k) = half*(w1+w2) * array(1,i,j,k)
        array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + half*(p1+p2)/g1
        
!         array(1,i,j,k) = array(1,i-1,j,k+1)
!         array(2,i,j,k) = array(2,i  ,j,k+1)
!         array(3,i,j,k) = array(3,i-1,j,k+1)
!         array(4,i,j,k) = array(4,i-1,j,k  )
!         array(5,i,j,k) = half * (array(5,i-1,j,k) + array(5,i,j,k+1))
     enddo
     i = i + 1
     if(i > nxloc(1)+nghost)then
        i = nxloc(1) + 1
        k = k - 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_EDGE_3D_10',rank

  return

end subroutine boundary_edge_3D_10

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_EDGE_3D_11:
!
!> Fills ghost cells in the lower right edge along the y direction of a 3D grid.
!<
subroutine boundary_edge_3D_11(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,w1,w2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_EDGE_3D_11',rank

  i = 1 - 1
  k = nxloc(3)   + 1

  do n = 1,nghost*nghost
     do j = 1,nxloc(2)
        d1 = array(1,i+1,j,k  )
        d2 = array(1,i  ,j,k-1)
        u1 = array(2,i+1,j,k  )/d1
        u2 = array(2,i  ,j,k-1)/d2
        v1 = array(3,i+1,j,k  )/d1
        v2 = array(3,i  ,j,k-1)/d2
        w1 = array(4,i+1,j,k  )/d1
        w2 = array(4,i  ,j,k-1)/d2
        p1 = g1*(array(5,i+1,j,k) - half*(array(2,i+1,j,k)**2+array(3,i+1,j,k)**2+array(4,i+1,j,k)**2)/array(1,i+1,j,k))
        p2 = g1*(array(5,i,j,k-1) - half*(array(2,i,j,k-1)**2+array(3,i,j,k-1)**2+array(4,i,j,k-1)**2)/array(1,i,j,k-1))
        
        array(1,i,j,k) = half*(d1+d2)
        array(2,i,j,k) = half*(u1+u2) * array(1,i,j,k)
        array(3,i,j,k) = half*(v1+v2) * array(1,i,j,k)
        array(4,i,j,k) = half*(w1+w2) * array(1,i,j,k)
        array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + half*(p1+p2)/g1
     
!         array(1,i,j,k) = array(1,i+1,j,k-1)
!         array(2,i,j,k) = array(2,i  ,j,k-1)
!         array(3,i,j,k) = array(3,i+1,j,k-1)
!         array(4,i,j,k) = array(4,i+1,j,k  )
!         array(5,i,j,k) = half * (array(5,i+1,j,k) + array(5,i,j,k-1))
     enddo
     i = i - 1
     if(i < 1-nghost)then
        i = 1 - 1
        k = k + 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_EDGE_3D_11',rank

  return

end subroutine boundary_edge_3D_11

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_EDGE_3D_12:
!
!> Fills ghost cells in the upper right edge along the y direction of a 3D grid.
!<
subroutine boundary_edge_3D_12(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,u1,u2,v1,v2,w1,w2,p1,p2

  if(debug_l2) write(*,*) 'begin BOUNDARY_EDGE_3D_12',rank

  i = nxloc(1) + 1
  k = nxloc(3) + 1

  do n = 1,nghost*nghost
     do j = 1,nxloc(2)
        d1 = array(1,i-1,j,k  )
        d2 = array(1,i  ,j,k-1)
        u1 = array(2,i-1,j,k  )/d1
        u2 = array(2,i  ,j,k-1)/d2
        v1 = array(3,i-1,j,k  )/d1
        v2 = array(3,i  ,j,k-1)/d2
        w1 = array(4,i-1,j,k  )/d1
        w2 = array(4,i  ,j,k-1)/d2
        p1 = g1*(array(5,i-1,j,k) - half*(array(2,i-1,j,k)**2+array(3,i-1,j,k)**2+array(4,i-1,j,k)**2)/array(1,i-1,j,k))
        p2 = g1*(array(5,i,j,k-1) - half*(array(2,i,j,k-1)**2+array(3,i,j,k-1)**2+array(4,i,j,k-1)**2)/array(1,i,j,k-1))
        
        array(1,i,j,k) = half*(d1+d2)
        array(2,i,j,k) = half*(u1+u2) * array(1,i,j,k)
        array(3,i,j,k) = half*(v1+v2) * array(1,i,j,k)
        array(4,i,j,k) = half*(w1+w2) * array(1,i,j,k)
        array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + half*(p1+p2)/g1
     
!         array(1,i,j,k) = array(1,i-1,j,k-1)
!         array(2,i,j,k) = array(2,i  ,j,k-1)
!         array(3,i,j,k) = array(3,i-1,j,k-1)
!         array(4,i,j,k) = array(4,i-1,j,k  )
!         array(5,i,j,k) = half * (array(5,i-1,j,k) + array(5,i,j,k-1))
     enddo
     i = i + 1
     if(i > nxloc(1)+nghost)then
        i = nxloc(1) + 1
        k = k + 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_EDGE_3D_12',rank

  return

end subroutine boundary_edge_3D_12

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_CORNER_3D_1:
!
!> Fills ghost cells at the bottom lower left corner of a 3D grid.
!<
subroutine boundary_corner_3D_1(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,d3,u1,u2,u3,v1,v2,v3,w1,w2,w3,p1,p2,p3

  if(debug_l2) write(*,*) 'begin BOUNDARY_CORNER_3D_1',rank

  i = 1 - 1
  j = 1 - 1
  k = 1 - 1

  do n = 1,nghost**3
     d1 = array(1,i+1,j  ,k  )
     d2 = array(1,i  ,j+1,k  )
     d3 = array(1,i  ,j  ,k+1)
     u1 = array(2,i+1,j  ,k  )/d1
     u2 = array(2,i  ,j+1,k  )/d2
     u3 = array(2,i  ,j  ,k+1)/d3
     v1 = array(3,i+1,j  ,k  )/d1
     v2 = array(3,i  ,j+1,k  )/d2
     v3 = array(3,i  ,j  ,k+1)/d3
     w1 = array(4,i+1,j  ,k  )/d1
     w2 = array(4,i  ,j+1,k  )/d2
     w3 = array(4,i  ,j  ,k+1)/d3
     p1 = g1*(array(5,i+1,j,k) - half*(array(2,i+1,j,k)**2+array(3,i+1,j,k)**2+array(4,i+1,j,k)**2)/array(1,i+1,j,k))
     p2 = g1*(array(5,i,j+1,k) - half*(array(2,i,j+1,k)**2+array(3,i,j+1,k)**2+array(4,i,j+1,k)**2)/array(1,i,j+1,k))
     p3 = g1*(array(5,i,j,k+1) - half*(array(2,i,j,k+1)**2+array(3,i,j,k+1)**2+array(4,i,j,k+1)**2)/array(1,i,j,k+1))
          
     array(1,i,j,k) = (d1+d2+d3)/three
     array(2,i,j,k) = (u1+u2+u3)/three * array(1,i,j,k)
     array(3,i,j,k) = (v1+v2+v3)/three * array(1,i,j,k)
     array(4,i,j,k) = (w1+w2+w3)/three * array(1,i,j,k)
     array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + (p1+p2+p3)/(three*g1)
  
!      array(1,i,j,k) = array(1,i+1,j+1,k+1)
!      array(2,i,j,k) = array(2,i  ,j  ,k+1)
!      array(3,i,j,k) = array(3,i+1,j  ,k  )
!      array(4,i,j,k) = array(4,i  ,j+1,k  )
!      array(5,i,j,k) = (array(5,i+1,j,k) + array(5,i,j+1,k) + array(5,i,j,k+1)) / three
     j = j - 1
     if(j < 1-nghost)then
        j = 1 - 1
        i = i - 1
     endif
     if(i < 1-nghost)then
        i = 1 - 1
        k = k - 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_CORNER_3D_1',rank

  return

end subroutine boundary_corner_3D_1

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_CORNER_3D_2:
!
!> Fills ghost cells at the bottom upper left corner of a 3D grid.
!<
subroutine boundary_corner_3D_2(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,d3,u1,u2,u3,v1,v2,v3,w1,w2,w3,p1,p2,p3

  if(debug_l2) write(*,*) 'begin BOUNDARY_CORNER_3D_2',rank

  i = 1 - 1
  j = nxloc(2)   + 1
  k = 1 - 1

  do n = 1,nghost**3
     d1 = array(1,i+1,j  ,k  )
     d2 = array(1,i  ,j-1,k  )
     d3 = array(1,i  ,j  ,k+1)
     u1 = array(2,i+1,j  ,k  )/d1
     u2 = array(2,i  ,j-1,k  )/d2
     u3 = array(2,i  ,j  ,k+1)/d3
     v1 = array(3,i+1,j  ,k  )/d1
     v2 = array(3,i  ,j-1,k  )/d2
     v3 = array(3,i  ,j  ,k+1)/d3
     w1 = array(4,i+1,j  ,k  )/d1
     w2 = array(4,i  ,j-1,k  )/d2
     w3 = array(4,i  ,j  ,k+1)/d3
     p1 = g1*(array(5,i+1,j,k) - half*(array(2,i+1,j,k)**2+array(3,i+1,j,k)**2+array(4,i+1,j,k)**2)/array(1,i+1,j,k))
     p2 = g1*(array(5,i,j-1,k) - half*(array(2,i,j-1,k)**2+array(3,i,j-1,k)**2+array(4,i,j-1,k)**2)/array(1,i,j-1,k))
     p3 = g1*(array(5,i,j,k+1) - half*(array(2,i,j,k+1)**2+array(3,i,j,k+1)**2+array(4,i,j,k+1)**2)/array(1,i,j,k+1))
          
     array(1,i,j,k) = (d1+d2+d3)/three
     array(2,i,j,k) = (u1+u2+u3)/three * array(1,i,j,k)
     array(3,i,j,k) = (v1+v2+v3)/three * array(1,i,j,k)
     array(4,i,j,k) = (w1+w2+w3)/three * array(1,i,j,k)
     array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + (p1+p2+p3)/(three*g1)
  
!      array(1,i,j,k) = array(1,i+1,j-1,k+1)
!      array(2,i,j,k) = array(2,i  ,j  ,k+1)
!      array(3,i,j,k) = array(3,i+1,j  ,k  )
!      array(4,i,j,k) = array(4,i  ,j-1,k  )
!      array(5,i,j,k) = (array(5,i+1,j,k) + array(5,i,j-1,k) + array(5,i,j,k+1)) / three
     j = j + 1
     if(j > nxloc(2)+nghost)then
        j = nxloc(2) + 1
        i = i - 1
     endif
     if(i < 1-nghost)then
        i = 1 - 1
        k = k - 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_CORNER_3D_2',rank

  return

end subroutine boundary_corner_3D_2

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_CORNER_3D_3:
!
!> Fills ghost cells at the bottom lower right corner of a 3D grid.
!<
subroutine boundary_corner_3D_3(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,d3,u1,u2,u3,v1,v2,v3,w1,w2,w3,p1,p2,p3

  if(debug_l2) write(*,*) 'begin BOUNDARY_CORNER_3D_3',rank

  i = nxloc(1)   + 1
  j = 1 - 1
  k = 1 - 1

  do n = 1,nghost**3
     d1 = array(1,i-1,j  ,k  )
     d2 = array(1,i  ,j+1,k  )
     d3 = array(1,i  ,j  ,k+1)
     u1 = array(2,i-1,j  ,k  )/d1
     u2 = array(2,i  ,j+1,k  )/d2
     u3 = array(2,i  ,j  ,k+1)/d3
     v1 = array(3,i-1,j  ,k  )/d1
     v2 = array(3,i  ,j+1,k  )/d2
     v3 = array(3,i  ,j  ,k+1)/d3
     w1 = array(4,i-1,j  ,k  )/d1
     w2 = array(4,i  ,j+1,k  )/d2
     w3 = array(4,i  ,j  ,k+1)/d3
     p1 = g1*(array(5,i-1,j,k) - half*(array(2,i-1,j,k)**2+array(3,i-1,j,k)**2+array(4,i-1,j,k)**2)/array(1,i-1,j,k))
     p2 = g1*(array(5,i,j+1,k) - half*(array(2,i,j+1,k)**2+array(3,i,j+1,k)**2+array(4,i,j+1,k)**2)/array(1,i,j+1,k))
     p3 = g1*(array(5,i,j,k+1) - half*(array(2,i,j,k+1)**2+array(3,i,j,k+1)**2+array(4,i,j,k+1)**2)/array(1,i,j,k+1))
          
     array(1,i,j,k) = (d1+d2+d3)/three
     array(2,i,j,k) = (u1+u2+u3)/three * array(1,i,j,k)
     array(3,i,j,k) = (v1+v2+v3)/three * array(1,i,j,k)
     array(4,i,j,k) = (w1+w2+w3)/three * array(1,i,j,k)
     array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + (p1+p2+p3)/(three*g1)
  
!      array(1,i,j,k) = array(1,i-1,j+1,k+1)
!      array(2,i,j,k) = array(2,i  ,j  ,k+1)
!      array(3,i,j,k) = array(3,i-1,j  ,k  )
!      array(4,i,j,k) = array(4,i  ,j+1,k  )
!      array(5,i,j,k) = (array(5,i-1,j,k) + array(5,i,j+1,k) + array(5,i,j,k+1)) / three
     j = j - 1
     if(j < 1-nghost)then
        j = 1 - 1
        i = i + 1
     endif
     if(i > nxloc(1)+nghost)then
        i = nxloc(1) + 1
        k = k - 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_CORNER_3D_3',rank

  return

end subroutine boundary_corner_3D_3

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_CORNER_3D_4:
!
!> Fills ghost cells at the bottom upper right corner of a 3D grid.
!<
subroutine boundary_corner_3D_4(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,d3,u1,u2,u3,v1,v2,v3,w1,w2,w3,p1,p2,p3

  if(debug_l2) write(*,*) 'begin BOUNDARY_CORNER_3D_4',rank

  i = nxloc(1)   + 1
  j = nxloc(2)   + 1
  k = 1 - 1

  do n = 1,nghost**3
     d1 = array(1,i-1,j  ,k  )
     d2 = array(1,i  ,j-1,k  )
     d3 = array(1,i  ,j  ,k+1)
     u1 = array(2,i-1,j  ,k  )/d1
     u2 = array(2,i  ,j-1,k  )/d2
     u3 = array(2,i  ,j  ,k+1)/d3
     v1 = array(3,i-1,j  ,k  )/d1
     v2 = array(3,i  ,j-1,k  )/d2
     v3 = array(3,i  ,j  ,k+1)/d3
     w1 = array(4,i-1,j  ,k  )/d1
     w2 = array(4,i  ,j-1,k  )/d2
     w3 = array(4,i  ,j  ,k+1)/d3
     p1 = g1*(array(5,i-1,j,k) - half*(array(2,i-1,j,k)**2+array(3,i-1,j,k)**2+array(4,i-1,j,k)**2)/array(1,i-1,j,k))
     p2 = g1*(array(5,i,j-1,k) - half*(array(2,i,j-1,k)**2+array(3,i,j-1,k)**2+array(4,i,j-1,k)**2)/array(1,i,j-1,k))
     p3 = g1*(array(5,i,j,k+1) - half*(array(2,i,j,k+1)**2+array(3,i,j,k+1)**2+array(4,i,j,k+1)**2)/array(1,i,j,k+1))
          
     array(1,i,j,k) = (d1+d2+d3)/three
     array(2,i,j,k) = (u1+u2+u3)/three * array(1,i,j,k)
     array(3,i,j,k) = (v1+v2+v3)/three * array(1,i,j,k)
     array(4,i,j,k) = (w1+w2+w3)/three * array(1,i,j,k)
     array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + (p1+p2+p3)/(three*g1)
  
!      array(1,i,j,k) = array(1,i-1,j-1,k+1)
!      array(2,i,j,k) = array(2,i  ,j  ,k+1)
!      array(3,i,j,k) = array(3,i-1,j  ,k  )
!      array(4,i,j,k) = array(4,i  ,j-1,k  )
!      array(5,i,j,k) = (array(5,i-1,j,k) + array(5,i,j-1,k) + array(5,i,j,k+1)) / three
     j = j + 1
     if(j > nxloc(2)+nghost)then
        j = nxloc(2) + 1
        i = i + 1
     endif
     if(i > nxloc(1)+nghost)then
        i = nxloc(1) + 1
        k = k - 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_CORNER_3D_4',rank

  return

end subroutine boundary_corner_3D_4

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_CORNER_3D_5:
!
!> Fills ghost cells at the top lower left corner of a 3D grid.
!<
subroutine boundary_corner_3D_5(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,d3,u1,u2,u3,v1,v2,v3,w1,w2,w3,p1,p2,p3

  if(debug_l2) write(*,*) 'begin BOUNDARY_CORNER_3D_5',rank

  i = 1 - 1
  j = 1 - 1
  k = nxloc(3)   + 1

  do n = 1,nghost**3
     d1 = array(1,i+1,j  ,k  )
     d2 = array(1,i  ,j+1,k  )
     d3 = array(1,i  ,j  ,k-1)
     u1 = array(2,i+1,j  ,k  )/d1
     u2 = array(2,i  ,j+1,k  )/d2
     u3 = array(2,i  ,j  ,k-1)/d3
     v1 = array(3,i+1,j  ,k  )/d1
     v2 = array(3,i  ,j+1,k  )/d2
     v3 = array(3,i  ,j  ,k-1)/d3
     w1 = array(4,i+1,j  ,k  )/d1
     w2 = array(4,i  ,j+1,k  )/d2
     w3 = array(4,i  ,j  ,k-1)/d3
     p1 = g1*(array(5,i+1,j,k) - half*(array(2,i+1,j,k)**2+array(3,i+1,j,k)**2+array(4,i+1,j,k)**2)/array(1,i+1,j,k))
     p2 = g1*(array(5,i,j+1,k) - half*(array(2,i,j+1,k)**2+array(3,i,j+1,k)**2+array(4,i,j+1,k)**2)/array(1,i,j+1,k))
     p3 = g1*(array(5,i,j,k-1) - half*(array(2,i,j,k-1)**2+array(3,i,j,k-1)**2+array(4,i,j,k-1)**2)/array(1,i,j,k-1))
          
     array(1,i,j,k) = (d1+d2+d3)/three
     array(2,i,j,k) = (u1+u2+u3)/three * array(1,i,j,k)
     array(3,i,j,k) = (v1+v2+v3)/three * array(1,i,j,k)
     array(4,i,j,k) = (w1+w2+w3)/three * array(1,i,j,k)
     array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + (p1+p2+p3)/(three*g1)
  
!      array(1,i,j,k) = array(1,i+1,j+1,k-1)
!      array(2,i,j,k) = array(2,i  ,j  ,k-1)
!      array(3,i,j,k) = array(3,i+1,j  ,k  )
!      array(4,i,j,k) = array(4,i  ,j+1,k  )
!      array(5,i,j,k) = (array(5,i+1,j,k) + array(5,i,j+1,k) + array(5,i,j,k-1)) / three
     j = j - 1
     if(j < 1-nghost)then
        j = 1 - 1
        i = i - 1
     endif
     if(i < 1-nghost)then
        i = 1 - 1
        k = k + 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_CORNER_3D_5',rank

  return

end subroutine boundary_corner_3D_5

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_CORNER_3D_6:
!
!> Fills ghost cells at the top upper left corner of a 3D grid.
!<
subroutine boundary_corner_3D_6(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,d3,u1,u2,u3,v1,v2,v3,w1,w2,w3,p1,p2,p3

  if(debug_l2) write(*,*) 'begin BOUNDARY_CORNER_3D_6',rank

  i = 1 - 1
  j = nxloc(2)   + 1
  k = nxloc(3)   + 1

  do n = 1,nghost**3
     d1 = array(1,i+1,j  ,k  )
     d2 = array(1,i  ,j-1,k  )
     d3 = array(1,i  ,j  ,k-1)
     u1 = array(2,i+1,j  ,k  )/d1
     u2 = array(2,i  ,j-1,k  )/d2
     u3 = array(2,i  ,j  ,k-1)/d3
     v1 = array(3,i+1,j  ,k  )/d1
     v2 = array(3,i  ,j-1,k  )/d2
     v3 = array(3,i  ,j  ,k-1)/d3
     w1 = array(4,i+1,j  ,k  )/d1
     w2 = array(4,i  ,j-1,k  )/d2
     w3 = array(4,i  ,j  ,k-1)/d3
     p1 = g1*(array(5,i+1,j,k) - half*(array(2,i+1,j,k)**2+array(3,i+1,j,k)**2+array(4,i+1,j,k)**2)/array(1,i+1,j,k))
     p2 = g1*(array(5,i,j-1,k) - half*(array(2,i,j-1,k)**2+array(3,i,j-1,k)**2+array(4,i,j-1,k)**2)/array(1,i,j-1,k))
     p3 = g1*(array(5,i,j,k-1) - half*(array(2,i,j,k-1)**2+array(3,i,j,k-1)**2+array(4,i,j,k-1)**2)/array(1,i,j,k-1))
          
     array(1,i,j,k) = (d1+d2+d3)/three
     array(2,i,j,k) = (u1+u2+u3)/three * array(1,i,j,k)
     array(3,i,j,k) = (v1+v2+v3)/three * array(1,i,j,k)
     array(4,i,j,k) = (w1+w2+w3)/three * array(1,i,j,k)
     array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + (p1+p2+p3)/(three*g1)
  
!      array(1,i,j,k) = array(1,i+1,j-1,k-1)
!      array(2,i,j,k) = array(2,i  ,j  ,k-1)
!      array(3,i,j,k) = array(3,i+1,j  ,k  )
!      array(4,i,j,k) = array(4,i  ,j-1,k  )
!      array(5,i,j,k) = (array(5,i+1,j,k) + array(5,i,j-1,k) + array(5,i,j,k-1)) / three
     j = j + 1
     if(j > nxloc(2)+nghost)then
        j = nxloc(2) + 1
        i = i - 1
     endif
     if(i < 1-nghost)then
        i = 1 - 1
        k = k + 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_CORNER_3D_6',rank

  return

end subroutine boundary_corner_3D_6

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_CORNER_3D_7:
!
!> Fills ghost cells at the top lower right corner of a 3D grid.
!<
subroutine boundary_corner_3D_7(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,d3,u1,u2,u3,v1,v2,v3,w1,w2,w3,p1,p2,p3

  if(debug_l2) write(*,*) 'begin BOUNDARY_CORNER_3D_7',rank

  i = nxloc(1)   + 1
  j = 1 - 1
  k = nxloc(3)   + 1

  do n = 1,nghost**3
     d1 = array(1,i-1,j  ,k  )
     d2 = array(1,i  ,j+1,k  )
     d3 = array(1,i  ,j  ,k-1)
     u1 = array(2,i-1,j  ,k  )/d1
     u2 = array(2,i  ,j+1,k  )/d2
     u3 = array(2,i  ,j  ,k-1)/d3
     v1 = array(3,i-1,j  ,k  )/d1
     v2 = array(3,i  ,j+1,k  )/d2
     v3 = array(3,i  ,j  ,k-1)/d3
     w1 = array(4,i-1,j  ,k  )/d1
     w2 = array(4,i  ,j+1,k  )/d2
     w3 = array(4,i  ,j  ,k-1)/d3
     p1 = g1*(array(5,i-1,j,k) - half*(array(2,i-1,j,k)**2+array(3,i-1,j,k)**2+array(4,i-1,j,k)**2)/array(1,i-1,j,k))
     p2 = g1*(array(5,i,j+1,k) - half*(array(2,i,j+1,k)**2+array(3,i,j+1,k)**2+array(4,i,j+1,k)**2)/array(1,i,j+1,k))
     p3 = g1*(array(5,i,j,k-1) - half*(array(2,i,j,k-1)**2+array(3,i,j,k-1)**2+array(4,i,j,k-1)**2)/array(1,i,j,k-1))
          
     array(1,i,j,k) = (d1+d2+d3)/three
     array(2,i,j,k) = (u1+u2+u3)/three * array(1,i,j,k)
     array(3,i,j,k) = (v1+v2+v3)/three * array(1,i,j,k)
     array(4,i,j,k) = (w1+w2+w3)/three * array(1,i,j,k)
     array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + (p1+p2+p3)/(three*g1)
  
!      array(1,i,j,k) = array(1,i-1,j+1,k-1)
!      array(2,i,j,k) = array(2,i  ,j  ,k-1)
!      array(3,i,j,k) = array(3,i-1,j  ,k  )
!      array(4,i,j,k) = array(4,i  ,j+1,k  )
!      array(5,i,j,k) = (array(5,i-1,j,k) + array(5,i,j+1,k) + array(5,i,j,k-1)) / three
     j = j - 1
     if(j < 1-nghost)then
        j = 1 - 1
        i = i + 1
     endif
     if(i > nxloc(1)+nghost)then
        i = nxloc(1) + 1
        k = k + 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_CORNER_3D_7',rank

  return

end subroutine boundary_corner_3D_7

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine BOUNDARY_CORNER_3D_8:
!
!> Fills ghost cells at the top upper right corner of a 3D grid.
!<
subroutine boundary_corner_3D_8(array)

  use mod_constants
  use mod_variables , only : debug_l2,nvar
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer                                                       :: i,j,k,n
  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                      :: d1,d2,d3,u1,u2,u3,v1,v2,v3,w1,w2,w3,p1,p2,p3

  if(debug_l2) write(*,*) 'begin BOUNDARY_CORNER_3D_8',rank

  i = nxloc(1) + 1
  j = nxloc(2) + 1
  k = nxloc(3) + 1

  do n = 1,nghost**3
     d1 = array(1,i-1,j  ,k  )
     d2 = array(1,i  ,j-1,k  )
     d3 = array(1,i  ,j  ,k-1)
     u1 = array(2,i-1,j  ,k  )/d1
     u2 = array(2,i  ,j-1,k  )/d2
     u3 = array(2,i  ,j  ,k-1)/d3
     v1 = array(3,i-1,j  ,k  )/d1
     v2 = array(3,i  ,j-1,k  )/d2
     v3 = array(3,i  ,j  ,k-1)/d3
     w1 = array(4,i-1,j  ,k  )/d1
     w2 = array(4,i  ,j-1,k  )/d2
     w3 = array(4,i  ,j  ,k-1)/d3
     p1 = g1*(array(5,i-1,j,k) - half*(array(2,i-1,j,k)**2+array(3,i-1,j,k)**2+array(4,i-1,j,k)**2)/array(1,i-1,j,k))
     p2 = g1*(array(5,i,j-1,k) - half*(array(2,i,j-1,k)**2+array(3,i,j-1,k)**2+array(4,i,j-1,k)**2)/array(1,i,j-1,k))
     p3 = g1*(array(5,i,j,k-1) - half*(array(2,i,j,k-1)**2+array(3,i,j,k-1)**2+array(4,i,j,k-1)**2)/array(1,i,j,k-1))
          
     array(1,i,j,k) = (d1+d2+d3)/three
     array(2,i,j,k) = (u1+u2+u3)/three * array(1,i,j,k)
     array(3,i,j,k) = (v1+v2+v3)/three * array(1,i,j,k)
     array(4,i,j,k) = (w1+w2+w3)/three * array(1,i,j,k)
     array(5,i,j,k) = half*(array(2,i,j,k)**2+array(3,i,j,k)**2+array(4,i,j,k)**2)/array(1,i,j,k) + (p1+p2+p3)/(three*g1)
  
!      array(1,i,j,k) = array(1,i-1,j-1,k-1)
!      array(2,i,j,k) = array(2,i  ,j  ,k-1)
!      array(3,i,j,k) = array(3,i-1,j  ,k  )
!      array(4,i,j,k) = array(4,i  ,j-1,k  )
!      array(5,i,j,k) = (array(5,i-1,j,k) + array(5,i,j-1,k) + array(5,i,j,k-1)) / three
     j = j + 1
     if(j > nxloc(2)+nghost)then
        j = nxloc(2) + 1
        i = i + 1
     endif
     if(i > nxloc(1)+nghost)then
        i = nxloc(1) + 1
        k = k + 1
     endif
  enddo

  if(debug_l2) write(*,*) 'end   BOUNDARY_CORNER_3D_8',rank

  return

end subroutine boundary_corner_3D_8
