!---------------------------------------------------------------------------------------------------
! File: gravity.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines allocate_grav_arrays(), deallocate_grav_arrays(), calc_grav_force(),
!! constant_gravity(), point_mass_origin(), point_mass_arbitrary(), init_self_gravity(),
!! poisson_solver(), cal_barycentre(), gravity_boundaries(), conjugate_gradient(), vector_product(),
!! matrix_product(), compute_preconditionning() and compute_difference().
!!
!! This file contains the routines which calculates the gravitational force on the fluid in
!! computational domain if gravity is to be included. The routines account for constant gravity
!! field, point masses and self-gravity through the use of a Poisson solver.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine ALLOCATE_GRAV_ARRAYS:
!
!> Allocates arrays for gravity variables.
!<
subroutine allocate_grav_arrays

  use mod_variables, only : debug_l1
  use mod_gravity
  use mod_grid
  use mod_mpi      , only : rank

  implicit none

  if(debug_l1) write(*,*) 'begin ALLOCATE_GRAV_ARRAYS',rank

  allocate(gravity_force(3,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))
  if(gravity_type == 4)then
     allocate(gravity_phi    (nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))
     allocate(gravity_phi_old(nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))
  else
     allocate(gravity_params(4,gravity_npmass))
  endif

  if(debug_l1) write(*,*) 'end   ALLOCATE_GRAV_ARRAYS',rank

  return

end subroutine allocate_grav_arrays

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine DEALLOCATE_GRAV_ARRAYS:
!
!> Deallocates arrays for gravity variables.
!<
subroutine deallocate_grav_arrays

  use mod_variables, only : debug_l1
  use mod_gravity
  use mod_grid
  use mod_mpi      , only : rank

  implicit none

  if(debug_l1) write(*,*) 'begin DEALLOCATE_GRAV_ARRAYS',rank

  deallocate(gravity_force)
  if(gravity_type == 4)then
     deallocate(gravity_phi    )
     deallocate(gravity_phi_old)
  else
     deallocate(gravity_params)
  endif

  if(debug_l1) write(*,*) 'end   DEALLOCATE_GRAV_ARRAYS',rank

  return

end subroutine deallocate_grav_arrays

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine CALC_GRAV_FORCE:
!
!> Computes gravitational potential from point masses defined in the gpmass array.
!! This routine currently only works in cartesian 2D and 3D.
!<
subroutine calc_grav_force

  use mod_variables, only : debug_l1,str
  use mod_constants
  use mod_gravity
  use mod_grid
  use mod_mpi      , only : rank

  implicit none

  character(len=str) :: string

  if(debug_l1) write(*,*) 'begin CALC_GRAV_FORCE',rank

  !$OMP PARALLEL WORKSHARE
  gravity_force = zero
  !$OMP END PARALLEL WORKSHARE

  select case(gravity_type)
  case(1)
     call constant_gravity      ! Constant gravitational field
  case(2)
     call point_mass_origin     ! Point source mass located at the grid origin
  case(3)
     call point_mass_arbitrary  ! Point source mass located at the grid origin
  case(4)
     call poisson_solver        ! Self-gravity Poisson solver (Conjugate Gradient)
  case default
     write(string,*) 'Unknown gravity type:',gravity_type
     call close_program(trim(string))
  end select

  if(debug_l1) write(*,*) 'end   CALC_GRAV_FORCE',rank

  return

end subroutine calc_grav_force

!###################################################################################################
!###################################################################################################
!###################################################################################################

! Subroutine CONSTANT_GRAVITY
!
!> This routine computes a potential for a constant gravitational field.
!! Currently only works for cartesian coordinates.
!!
!! g = gravity_params(2:ndim+1)
!<
subroutine constant_gravity

  use mod_gravity  , only : gravity_params,gravity_force
  use mod_grid     , only : ndim
  use mod_variables, only : debug_l1
  use mod_mpi      , only : rank

  implicit none

  if(debug_l1) write(*,*) 'begin CONSTANT_GRAVITY',rank

  if(ndim == 1)then
     !$OMP PARALLEL WORKSHARE
     gravity_force(1,:,:,:) = gravity_params(2,1)
     !$OMP END PARALLEL WORKSHARE
  elseif(ndim == 2)then
     !$OMP PARALLEL WORKSHARE
     gravity_force(1,:,:,:) = gravity_params(2,1)
     gravity_force(2,:,:,:) = gravity_params(3,1)
     !$OMP END PARALLEL WORKSHARE
  elseif(ndim == 3)then
     !$OMP PARALLEL WORKSHARE
     gravity_force(1,:,:,:) = gravity_params(2,1)
     gravity_force(2,:,:,:) = gravity_params(3,1)
     gravity_force(3,:,:,:) = gravity_params(4,1)
     !$OMP END PARALLEL WORKSHARE
  endif

  if(debug_l1) write(*,*) 'end   CONSTANT_GRAVITY',rank

  return

end subroutine constant_gravity

!###################################################################################################
!###################################################################################################
!###################################################################################################

! Subroutine POINT_MASS_ORIGIN
!
!> This routine computes a gravitational potential for a single point mass located at the
!! grid origin.
!!
!! The mass is = gravity_params(1).
!<
subroutine point_mass_origin

  use mod_gravity   , only : gravity_params,gravity_force
  use mod_gridarrays, only : xn1,xn2,xn3
  use mod_variables , only : debug_l1
  use mod_grid
  use mod_constants
  use mod_mpi       , only : rank

  implicit none

  integer  :: i,j,k
  real(dp) :: dist,distsq,th,ph,fgrav,atan3

  if(debug_l1) write(*,*) 'begin POINT_MASS_ORIGIN',rank

  select case(geometry)

  case(1)

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,distsq,dist,fgrav,th,ph)
     !$OMP DO COLLAPSE(3)
     do k = 1,nxloc(3)
        do j = 1,nxloc(2)
           do i = 1,nxloc(1)
              distsq = xn1(i)*xn1(i) + xn2(j)*xn2(j) + xn3(k)*xn3(k)
              dist   = sqrt(distsq)
              fgrav  = - grav * gravity_params(1,1) / distsq
              th     = atan3(sqrt(xn1(i)*xn1(i)+xn2(j)*xn2(j)),xn3(k))
              ph     = atan3(xn2(j),xn1(i))
              gravity_force(1,i,j,k) = fgrav * sin(th) * cos(ph)
              gravity_force(2,i,j,k) = fgrav * sin(th) * sin(ph)
              gravity_force(3,i,j,k) = fgrav * cos(th)
           enddo
        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  case(2)

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k)
     !$OMP DO COLLAPSE(3)
     do k = 1,nxloc(3)
        do j = 1,nxloc(2)
           do i = 1,nxloc(1)
              gravity_force(1,i,j,k) = - grav * gravity_params(1,1) / (xn1(i)*xn1(i))
           enddo
        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  case(3)

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,distsq,th,fgrav)
     !$OMP DO COLLAPSE(3)
     do k = 1,nxloc(3)
        do j = 1,nxloc(2)
           do i = 1,nxloc(1)
              distsq = xn1(i)*xn1(i) + xn2(j)*xn2(j)
              th     = atan(xn2(j)/xn1(i))
              fgrav  = - grav * gravity_params(1,1) / distsq
              gravity_force(1,i,j,k) = fgrav * cos(th)
              if(xn2(j) < zero)then
                 gravity_force(2,i,j,k) = -fgrav * sin(th)
              else
                 gravity_force(2,i,j,k) =  fgrav * sin(th)
              endif
           enddo
        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  end select

  if(debug_l1) write(*,*) 'end   POINT_MASS_ORIGIN',rank

  return

end subroutine point_mass_origin

!###################################################################################################
!###################################################################################################
!###################################################################################################

! Subroutine POINT_MASS_ARBITRARY
!
!> This routine computes a point-mass gravitational potential for an arbitrary number of
!! point masses with arbitrary positions.
!!
!! Mass = gravity_params(1)
!! Xpos = gravity_params(2) (x or r)
!! Ypos = gravity_params(3) (y or theta or z)
!! Zpos = gravity_params(4) (z or phi)
!<
subroutine point_mass_arbitrary

  use mod_gravity   , only : gravity_params,gravity_force,gravity_npmass
  use mod_gridarrays, only : xn1,xn2,xn3
  use mod_variables , only : debug_l1
  use mod_grid
  use mod_constants
  use mod_mpi       , only : rank

  implicit none

  integer  :: i,j,k,n
  real(dp), dimension(gravity_npmass) :: xmass,ymass,zmass
  real(dp) :: fsum1,fsum2,fsum3,dist,dist1,dist2,dist3,distsq,f1,f2,f3,th,ph,fgrav
  real(dp) :: xcell,ycell,zcell,frx,fry,frz,ftx,fty,ftz,fpx,fpy,fpz

  if(debug_l1) write(*,*) 'begin POINT_MASS_ARBITRARY',rank

  select case(geometry)

  case(1)

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,n,fsum1,fsum2,fsum3,dist1,dist2,dist3,&
     !$OMP distsq,dist,fgrav,th,ph,f1,f2,f3)
     !$OMP DO COLLAPSE(3)
     do k = 1,nxloc(3)
        do j = 1,nxloc(2)
           do i = 1,nxloc(1)

              fsum1 = zero ; fsum2 = zero ; fsum3 = zero

              do n = 1,gravity_npmass
                 dist1  = xn1(i) - gravity_params(2,n)
                 dist2  = xn2(j) - gravity_params(3,n)
                 dist3  = xn3(k) - gravity_params(4,n)
                 distsq = dist1*dist1 + dist2*dist2 + dist3*dist3
                 dist   = sqrt(distsq)
                 fgrav  = - grav * gravity_params(1,n) / distsq
                 if(ndim == 2)then
                    th = half*pi
                 elseif(ndim == 3)then
                    th = dist3 / dist
                 endif
                 if(dist2 > zero)then
                    ph = acos(dist1/dist*sin(th))
                 elseif(dist2 < zero)then
                    ph = acos(-dist1/dist*sin(th)) + pi
                 else
                    ph = two*pi + asin(dist2/dist*sin(th))
                 endif
                 f1 = fgrav * sin(th) * cos(ph)
                 f2 = fgrav * sin(th) * sin(ph)
                 f3 = fgrav * cos(th)
                 fsum1 = fsum1 + f1
                 fsum2 = fsum2 + f2
                 fsum3 = fsum3 + f3
              enddo

              gravity_force(1,i,j,k) = fsum1
              gravity_force(2,i,j,k) = fsum2
              gravity_force(3,i,j,k) = fsum3

           enddo
        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  case(2)

     do n = 1,gravity_npmass
        xmass(n) = gravity_params(2,n)*sin(gravity_params(3,n))*cos(gravity_params(4,n))
        ymass(n) = gravity_params(2,n)*sin(gravity_params(3,n))*sin(gravity_params(4,n))
        zmass(n) = gravity_params(2,n)*cos(gravity_params(3,n))
     enddo

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,n,fsum1,fsum2,fsum3,dist1,dist2,dist3,&
     !$OMP distsq,dist,fgrav,th,ph,f1,f2,f3,xcell,ycell,zcell,frx,fry,frz,ftx,fty,ftz,fpx,fpy,fpz)
     !$OMP DO COLLAPSE(3)
     do k = 1,nxloc(3)
        do j = 1,nxloc(2)
           do i = 1,nxloc(1)

              fsum1 = zero ; fsum2 = zero ; fsum3 = zero
              xcell = xn1(i)*sin(xn2(j))*cos(xn3(k))
              ycell = xn1(i)*sin(xn2(j))*sin(xn3(k))
              zcell = xn1(i)*cos(xn2(j))

              do n = 1,gravity_npmass
                 dist1  = xcell - xmass(n)
                 dist2  = ycell - ymass(n)
                 dist3  = zcell - zmass(n)
                 distsq = dist1*dist1 + dist2*dist2 + dist3*dist3
                 dist   = sqrt(distsq)
                 fgrav  = - grav * gravity_params(1,n) / distsq
                 if(ndim == 2)then
                    th = half*pi
                 elseif(ndim == 3)then
                    th = dist3 / dist
                 endif
                 if(dist2 > zero)then
                    ph = acos(dist1/dist*sin(th))
                 elseif(dist2 < zero)then
                    ph = acos(-dist1/dist*sin(th)) + pi
                 else
                    ph = two*pi + asin(dist2/dist*sin(th))
                 endif
                 f1 = fgrav * sin(th) * cos(ph)
                 f2 = fgrav * sin(th) * sin(ph)
                 f3 = fgrav * cos(th)
                 fsum1 = fsum1 + f1
                 fsum2 = fsum2 + f2
                 fsum3 = fsum3 + f3
              enddo

              !convert back to spherical coordinates
              frx =  fsum1 * sin(xn2(j)) * cos(xn3(k))
              fry =  fsum2 * sin(xn2(j)) * sin(xn3(k))
              frz =  fsum3 * cos(xn2(j))
              ftx =  fsum1 * cos(xn2(j)) * cos(xn3(k))
              fty =  fsum2 * cos(xn2(j)) * sin(xn3(k))
              ftz = -fsum3 * sin(xn2(j))
              fpx = -fsum1               * sin(xn3(k))
              fpy =  fsum2               * cos(xn3(k))
              fpz =  zero

              gravity_force(1,i,j,k) = frx + fry + frz
              gravity_force(2,i,j,k) = ftx + fty + ftz
              gravity_force(3,i,j,k) = fpx + fpy + fpz

           enddo
        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  case(3)

     do n = 1,gravity_npmass
        xmass(n) = gravity_params(2,n)*cos(gravity_params(4,n))
        ymass(n) = gravity_params(2,n)*sin(gravity_params(4,n))
        zmass(n) = gravity_params(3,n)
     enddo

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,n,fsum1,fsum2,fsum3,dist1,dist2,dist3,&
     !$OMP distsq,dist,fgrav,th,ph,f1,f2,f3,xcell,ycell,zcell,frx,fry,frz,ftx,fty,fpx,fpy)
     !$OMP DO COLLAPSE(3)
     do k = 1,nxloc(3)
        do j = 1,nxloc(2)
           do i = 1,nxloc(1)

              fsum1 = zero ; fsum2 = zero ; fsum3 = zero
              xcell = xn1(i)*cos(xn3(k))
              ycell = xn1(i)*sin(xn3(k))
              zcell = xn2(j)

              do n = 1,gravity_npmass
                 dist1  = xcell - xmass(n)
                 dist2  = ycell - ymass(n)
                 dist3  = zcell - zmass(n)
                 distsq = dist1*dist1 + dist2*dist2 + dist3*dist3
                 dist   = sqrt(distsq)
                 fgrav  = - grav * gravity_params(1,n) / distsq
                 if(ndim == 2)then
                    th = half*pi
                 elseif(ndim == 3)then
                    th = dist3 / dist
                 endif
                 if(dist2 > zero)then
                    ph = acos(dist1/dist*sin(th))
                 elseif(dist2 < zero)then
                    ph = acos(-dist1/dist*sin(th)) + pi
                 else
                    ph = two*pi + asin(dist2/dist*sin(th))
                 endif
                 f1 = fgrav * sin(th) * cos(ph)
                 f2 = fgrav * sin(th) * sin(ph)
                 f3 = fgrav * cos(th)
                 fsum1 = fsum1 + f1
                 fsum2 = fsum2 + f2
                 fsum3 = fsum3 + f3
              enddo

              !convert back to cylindrical coordinates
              frx =  fsum1 * cos(xn3(k))
              fry =  fsum2 * sin(xn3(k))
              fpx = -fsum1 * sin(xn3(k))
              fpy =  fsum2 * cos(xn3(k))

              gravity_force(1,i,j,k) = frx + fry
              gravity_force(2,i,j,k) = fsum2
              gravity_force(3,i,j,k) = fpx + fpy

           enddo
        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  end select

  if(debug_l1) write(*,*) 'end   POINT_MASS_ARBITRARY',rank

  return

end subroutine point_mass_arbitrary

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine INIT_SELF_GRAVITY:
!
!> Initialises gravity.
!<
subroutine init_self_gravity

  use mod_gravity
  use mod_variables, only : debug_l1
  use mod_constants
  use mod_grid
  use mod_mpi      , only : rank

  implicit none

  real(dp) :: xbar,ybar,zbar,pbar,dist

  if(debug_l1) write(*,*) 'begin INIT_GRAVITY',rank

  xbar = zero ; ybar = zero ; zbar = zero ; pbar = zero

  call cal_barycentre(xbar,ybar,zbar,pbar)

  select case(geometry)
  case(1)
     dist = sqrt(sum(lbox(1:ndim)**2))
  case(2)
     dist = lbox(1)
  case(3)
     dist = max(lbox(1),lbox(2))
  end select

  gravity_phi = - grav * pbar / dist

  if(debug_l1) write(*,*) 'end   INIT_GRAVITY',rank

  return

end subroutine init_self_gravity

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine POISSON_SOLVER
!
!> Performs the conjugate gradient algorithm for the gravity potential calculation in the
!! case of self gravity.
!!
!! Adapted from HERACLES (http://irfu.cea.fr/Projets/Site_heracles/index.html)
!! Copyright: E. Audit - CEA/DSM/IRFU/SAp, France
!<
subroutine poisson_solver

  use mod_gravity
  use mod_gridarrays, only : xn1,xn2,xn3
  use mod_variables , only : debug_l1,n_term_output,terminal_outputs
  use mod_arrays    , only : unew
  use mod_grid
  use mod_constants
  use mod_mpi       , only : rank

  implicit none

  real(dp), dimension(nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: BB
  real(dp)                                                 :: dif,gradphi
  real(dp)                                                 :: xbar,ybar,zbar,pbar
  integer                                                  :: i,j,k,nite

  if(debug_l1) write(*,*) 'begin POISSON_SOLVER',rank

  !$OMP PARALLEL WORKSHARE
  gravity_phi_old = gravity_phi
  BB              = zero
  !$OMP END PARALLEL WORKSHARE

  xbar = zero ; ybar = zero ; zbar = zero ; pbar = zero

  call cal_barycentre(xbar,ybar,zbar,pbar)

  !$OMP PARALLEL DO COLLAPSE(3)
  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)

           if(isolated_system) then
              BB(i,j,k) = four*pi*grav*unew(1,i,j,k)
           else
              BB(i,j,k) = four*pi*grav*(unew(1,i,j,k)-pbar/(lbox(1)*lbox(2)*lbox(3)))
           endif

        enddo
     enddo
  enddo
  !$OMP END PARALLEL DO

  call gravity_boundaries(gravity_phi,.true.)

  call conjugate_gradient(gravity_phi,BB,gravity_precond,gravity_eps,nite,dif)

  nitetot_grav = nitetot_grav + nite

  write(terminal_outputs(n_term_output),'(a,i4,a,es10.3,a,i9)') 'nite =',nite,'  err =',dif,'  nitetot',nitetot_grav

  ! Compute gravitational force from gravitational potential phi
  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)

           ! Gravitational acceleration along first direction
           gradphi = (gravity_phi(i+1,j,k) - gravity_phi(i-1,j,k)) / (xn1(i+1) - xn1(i-1))
           gravity_force(1,i,j,k) = - gradphi

           ! Gravitational acceleration along second direction
           if(ndim > 1)then
              gradphi = (gravity_phi(i,j+1,k) - gravity_phi(i,j-1,k)) / (xn2(j+1) - xn2(j-1))
              gravity_force(2,i,j,k) = - gradphi
           endif

           ! Gravitational acceleration along third direction
           if(ndim > 2)then
              gradphi = (gravity_phi(i,j,k+1) - gravity_phi(i,j,k-1)) / (xn3(k+1) - xn3(k-1))
              gravity_force(3,i,j,k) = - gradphi
           endif

        enddo
     enddo
  enddo

  if(debug_l1) write(*,*) 'end   POISSON_SOLVER',rank

  return

end subroutine poisson_solver

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine CAL_BARYCENTRE
!
!> Computes the barycentre of the grid.
!!
!! Adapted from HERACLES (http://irfu.cea.fr/Projets/Site_heracles/index.html)
!! Copyright: E. Audit - CEA/DSM/IRFU/SAp, France
!<
subroutine cal_barycentre(xbar,ybar,zbar,pbar)

  use mod_gridarrays, only : cell_volume,xn1,xn2,xn3
  use mod_arrays    , only : unew
  use mod_variables , only : debug_l1
  use mod_grid
  use mod_constants
  use mod_mpi

  implicit none

  integer  :: i,j,k
  real(dp) :: xbar,ybar,zbar,pbar,xp_loc,yp_loc,zp_loc,p_loc,xp,yp,zp,pp

  if(debug_l1) write(*,*) 'begin CAL_BARYCENTRE',rank

  xbar   = zero ; ybar   = zero ; zbar   = zero ; pbar   = zero
  xp_loc = zero ; yp_loc = zero ; zp_loc = zero ; p_loc  = zero
  xp     = zero ; yp     = zero ; zp     = zero ; pp     = zero

  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k) &
  !$OMP REDUCTION(+:xp_loc) REDUCTION(+:yp_loc) REDUCTION(+:zp_loc) REDUCTION(+:p_loc)
  !$OMP DO COLLAPSE(3)
  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)

           xp_loc = xp_loc + xn1(i)*unew(1,i,j,k)*cell_volume(i,j,k)
           yp_loc = yp_loc + xn2(j)*unew(1,i,j,k)*cell_volume(i,j,k)
           zp_loc = zp_loc + xn3(k)*unew(1,i,j,k)*cell_volume(i,j,k)
           p_loc  = p_loc  +        unew(1,i,j,k)*cell_volume(i,j,k)

        enddo
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

#if MPI==1

  ierror = 0
  call mpi_allreduce(xp_loc,xp,1,mpi_real_dp,mpi_sum_dp,world,ierror)
  call mpi_allreduce(yp_loc,yp,1,mpi_real_dp,mpi_sum_dp,world,ierror)
  call mpi_allreduce(zp_loc,zp,1,mpi_real_dp,mpi_sum_dp,world,ierror)
  call mpi_allreduce( p_loc,pp,1,mpi_real_dp,mpi_sum_dp,world,ierror)
 
  xbar = xp/pp
  ybar = yp/pp
  zbar = zp/pp
  pbar = pp
  
#else

  xbar = xp_loc/p_loc
  ybar = yp_loc/p_loc
  zbar = zp_loc/p_loc
  pbar = p_loc

#endif

  if(debug_l1) write(*,*) 'end   CAL_BARYCENTRE',rank

  return

end subroutine cal_barycentre

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine GRAVITY_BOUNDARIES
!
!> Fills ghost cells for gravitational potential.
!<
subroutine gravity_boundaries(array,switch)

  use mod_variables , only : debug_l1
  use mod_gravity   , only : isolated_system
  use mod_constants
  use mod_gridarrays, only : xn1,xn2,xn3
  use mod_grid
  use mod_mpi

  implicit none

  real(dp), dimension(1,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: phi_comm
  real(dp), dimension(  nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp)                                                   :: xbar,ybar,zbar,pbar,xx,yy,zz
  logical                                                    :: switch
  integer                                                    :: i,j,k,n

  if(debug_l1) write(*,*) 'begin GRAVITY_BOUNDARIES',rank

#if MPI==1

  phi_comm(1,:,:,:) = array

  call mpi_comm_faces_1D(phi_comm,1)

  if(ndim > 1) call mpi_comm_faces_2D(phi_comm,1)

  if(ndim > 2) call mpi_comm_faces_3D(phi_comm,1)

  array = phi_comm(1,:,:,:)

#endif

  if(switch)then
     xbar = zero ; ybar = zero ; zbar = zero ; pbar = zero
     call cal_barycentre(xbar,ybar,zbar,pbar)
  endif

  ! lower x boundary
  if(nxlower(1) == 1)then

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,n,xx,yy,zz)
     !$OMP DO COLLAPSE(2)
     do j = 1,nxloc(2)
        do k = 1,nxloc(3)
           do n = 1,nghost
              if(isolated_system)then
                 if(switch)then
                    xx = xn1(1-n)-xbar
                    yy = xn2(j       )-ybar
                    zz = xn3(k       )-zbar
                    array(1-n,j,k) = -grav*pbar / sqrt(xx*xx + yy*yy + zz*zz)
                 else
                    array(1-n,j,k) = zero
                 endif
              else
                 array(1-n,j,k) = array(nxloc(1)-n+1,j,k)
              endif
           enddo
        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  endif

  ! upper x boundary
  if(nxupper(1) == nx(1))then

     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,n,xx,yy,zz)
     !$OMP DO COLLAPSE(2)
     do j = 1,nxloc(2)
        do k = 1,nxloc(3)
           do n = 1,nghost
              if(isolated_system)then
                 if(switch)then
                    xx = xn1(nxloc(1)+n)-xbar
                    yy = xn2(j     )-ybar
                    zz = xn3(k     )-zbar
                    array(nxloc(1)+n,j,k) = -grav*pbar / sqrt(xx*xx + yy*yy + zz*zz)
                 else
                    array(nxloc(1)+n,j,k) = zero
                 endif
              else
                 array(nxloc(1)+n,j,k) = array(1+n-1,j,k)
              endif
           enddo
        enddo
     enddo
     !$OMP END DO
     !$OMP END PARALLEL

  endif

  if(ndim > 1)then

     ! lower y boundary
     if(nxlower(2) == 1)then

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,n,xx,yy,zz)
        !$OMP DO COLLAPSE(2)
        do k = 1,nxloc(3)
           do i = 1,nxloc(1)
              do n = 1,nghost
                 if(isolated_system)then
                    if(switch)then
                       xx = xn1(i       )-xbar
                       yy = xn2(1-n)-ybar
                       zz = xn3(k       )-zbar
                       array(i,1-n,k) = -grav*pbar / sqrt(xx*xx + yy*yy + zz*zz)
                    else
                       array(i,1-n,k) = zero
                    endif
                 else
                    array(i,1-n,k) = array(i,nxloc(2)-n+1,k)
                 endif
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     endif

     ! upper y boundary
     if(nxupper(2) == nx(2))then

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,n,xx,yy,zz)
        !$OMP DO COLLAPSE(2)
        do k = 1,nxloc(3)
           do i = 1,nxloc(1)
              do n = 1,nghost
                 if(isolated_system)then
                    if(switch)then
                       xx = xn1(i     )-xbar
                       yy = xn2(nxloc(2)+n)-ybar
                       zz = xn3(k     )-zbar
                       array(i,nxloc(2)+n,k) = -grav*pbar / sqrt(xx*xx + yy*yy + zz*zz)
                    else
                       array(i,nxloc(2)+n,k) = zero
                    endif
                 else
                    array(i,nxloc(2)+n,k) = array(i,1+n-1,k)
                 endif
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     endif

  endif

  if(ndim > 2)then

     ! lower z boundary
     if(nxlower(3) == 1)then

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,n,xx,yy,zz)
        !$OMP DO COLLAPSE(2)
        do j = 1,nxloc(2)
           do i = 1,nxloc(1)
              do n = 1,nghost
                 if(isolated_system)then
                    if(switch)then
                       xx = xn1(i       )-xbar
                       yy = xn2(j       )-ybar
                       zz = xn3(1-n)-zbar
                       array(i,j,1-n) = -grav*pbar / sqrt(xx*xx + yy*yy + zz*zz)
                    else
                       array(i,j,1-n) = zero
                    endif
                 else
                    array(i,j,1-n) = array(i,j,nxloc(3)-n+1)
                 endif
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     endif

     ! upper z boundary
     if(nxupper(3) == nx(3))then

        !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,n,xx,yy,zz)
        !$OMP DO COLLAPSE(2)
        do j = 1,nxloc(2)
           do i = 1,nxloc(1)
              do n = 1,nghost
                 if(isolated_system)then
                    if(switch)then
                       xx = xn1(i     )-xbar
                       yy = xn2(j     )-ybar
                       zz = xn3(nxloc(3)+n)-zbar
                       array(i,j,nxloc(3)+n) = -grav*pbar / sqrt(xx*xx + yy*yy + zz*zz)
                    else
                       array(i,j,nxloc(3)+n) = zero
                    endif
                 else
                    array(i,j,nxloc(3)+n) = array(i,j,1+n-1)
                 endif
              enddo
           enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

     endif

  endif

  if(debug_l1) write(*,*) 'end   GRAVITY_BOUNDARIES',rank

  return

end subroutine gravity_boundaries

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine CONJUGATE_GRADIENT
!
!> This routine performs a preconditioned conjugate gradient
!!  method to solve AX=B
!!
!! Warning: the boundary conditions on first guess X must be
!! filled before calling the conjugate_gradient
!!
!! Adapted from HERACLES (http://irfu.cea.fr/Projets/Site_heracles/index.html)
!! Copyright: E. Audit - CEA/DSM/IRFU/SAp, France
!<
subroutine conjugate_gradient(X,B,precond,eps,nite,dif)

  use mod_variables , only : debug_l1
  use mod_grid      , only : nmin1,nmax1,nmin2,nmax2,nmin3,nmax3
  use mod_constants
  use mod_mpi

  implicit none

  real(dp), dimension(nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: X,B,RR,PP,AX,ZZ
  logical , intent(in )  :: precond
  real(dp), intent(in )  :: eps
  real(dp), intent(out)  :: dif
  integer , intent(out)  :: nite
  integer                :: nitemax
  integer , dimension(3) :: ii
  logical                :: BC
  real(dp)               :: alph,pv1,pv2,pv3

  if(debug_l1) write(*,*) 'begin CONJUGATE_GRADIENT',rank

  call matrix_product(X,AX)

  !$OMP PARALLEL WORKSHARE
  RR = B - AX
  !$OMP END PARALLEL WORKSHARE
  if(precond)then
     call compute_preconditionning(RR,ZZ)
     !$OMP PARALLEL WORKSHARE
     PP = ZZ
     !$OMP END PARALLEL WORKSHARE
  else
     !$OMP PARALLEL WORKSHARE
     PP = RR
     !$OMP END PARALLEL WORKSHARE
  endif

  BC = .true. ; nite = 0 ; nitemax = 1000 ; dif = zero

  do while(BC)

     nite = nite + 1

     call gravity_boundaries(PP,.false.)

     call matrix_product(PP,AX)
     call vector_product(pv1,PP,AX)
     if(precond)then
        call vector_product(pv2,RR,ZZ)
     else
        call vector_product(pv2,RR,RR)
     endif

     if(pv1 .ne. zero)then
        alph = pv2/pv1
     else
        write(*,*) 'pv1 = 0.0'
        exit
     endif

     !$OMP PARALLEL WORKSHARE
     X  = X  + alph*PP
     RR = RR - alph*AX
     !$OMP END PARALLEL WORKSHARE
 
     call compute_difference(dif,X,PP,alph)

     if(precond)then
        call compute_preconditionning(RR,ZZ)
        call vector_product(pv3,RR,ZZ)
        !$OMP PARALLEL WORKSHARE
        PP = ZZ + pv3/pv2 * PP
        !$OMP END PARALLEL WORKSHARE
     else
        call vector_product(pv3,RR,RR)
        !$OMP PARALLEL WORKSHARE
        PP = RR + pv3/pv2 * PP
        !$OMP END PARALLEL WORKSHARE
     endif

     if((dif < eps .and. nite .ge. 2).or.nite.eq.nitemax) BC = .false.

  enddo

  if(nite.eq.nitemax)then
     ii = maxloc(abs(alph*PP/X))
     write(*,*) 'nite,dif,i,j,k,X(i,j,k)=',nite,maxval(abs(alph*PP/X)),&
                 nmin1-1+ii(1),nmin2-1+ii(2),nmin3-1+ii(3),            &
                 X(nmin1-1+ii(1),nmin2-1+ii(2),nmin3-1+ii(3))
     call close_program('Conjugate Gradient not converged: nitemax reached')
  endif

  if(debug_l1) write(*,*) 'end   CONJUGATE_GRADIENT',rank

  return

end subroutine conjugate_gradient

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine VECTOR_PRODUCT
!
!> Computes the vector product v1 * v2.
!!
!! Adapted from HERACLES (http://irfu.cea.fr/Projets/Site_heracles/index.html)
!! Copyright: E. Audit - CEA/DSM/IRFU/SAp, France
!<
subroutine vector_product(pv,v1,v2)

  use mod_grid
  use mod_variables, only : debug_l1
  use mod_constants
  use mod_mpi

  implicit none

  real(dp), dimension(nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: v1,v2
  real(dp)                                                 :: pv,pv_loc
  integer                                                  :: i,j,k

  if(debug_l1) write(*,*) 'begin VECTOR_PRODUCT',rank

  pv_loc = zero

  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k) REDUCTION(+:pv_loc)
  !$OMP DO COLLAPSE(3)
  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)

           pv_loc = pv_loc + v1(i,j,k)*v2(i,j,k)

        enddo
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

#if MPI==1
  ierror = 0
  call mpi_allreduce(pv_loc,pv,1,mpi_real_dp,mpi_sum_dp,world,ierror)
#else
  pv = pv_loc
#endif

  if(debug_l1) write(*,*) 'end   VECTOR_PRODUCT',rank

  return

end subroutine vector_product

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine MATRIX_PRODUCT
!
!> Performs the matrix product.
!!
!! Adapted from HERACLES (http://irfu.cea.fr/Projets/Site_heracles/index.html)
!! Copyright: E. Audit - CEA/DSM/IRFU/SAp, France
!<
subroutine matrix_product(T,AT)

  use mod_gridarrays
  use mod_variables , only : debug_l1
  use mod_constants
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  real(dp), dimension(nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: T,AT
  real(dp), dimension(3)                                   :: gradp,gradm,grad2
  integer                                                  :: i,j,k

  if(debug_l1) write(*,*) 'begin MATRIX_PRODUCT',rank

  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,gradp,gradm,grad2)
  !$OMP DO COLLAPSE(3)
  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)

           grad2 = zero

           gradp(1) = (T(i+1,j,k) - T(i  ,j,k)) / (xn1(i+1) - xn1(i  ))
           gradm(1) = (T(i  ,j,k) - T(i-1,j,k)) / (xn1(i  ) - xn1(i-1))
           grad2(1) = (gradp(1)   - gradm(1)  ) / (x1 (i  ) - x1 (i-1))

           if(ndim > 1)then
              gradp(2) = (T(i,j+1,k) - T(i,j  ,k)) / (xn2(j+1) - xn2(j  ))
              gradm(2) = (T(i,j  ,k) - T(i,j-1,k)) / (xn2(j  ) - xn2(j-1))
              grad2(2) = (gradp(2)   - gradm(2)  ) / (x2 (j  ) - x2 (j-1))
           endif

           if(ndim > 2)then
              gradp(3) = (T(i,j,k+1) - T(i,j,k  )) / (xn3(k+1) - xn3(k  ))
              gradm(3) = (T(i,j,k  ) - T(i,j,k-1)) / (xn3(k  ) - xn3(k-1))
              grad2(3) = (gradp(3)   - gradm(3)  ) / (x3 (k  ) - x3 (k-1))
           endif

           AT(i,j,k) = (grad2(1)+grad2(2)+grad2(3))

        enddo
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  if(debug_l1) write(*,*) 'end   MATRIX_PRODUCT',rank

  return

end subroutine matrix_product

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine COMPUTE_PRECONDITIONNING
!
!> This routine computes the preconditioned matrix.
!!
!! Adapted from HERACLES (http://irfu.cea.fr/Projets/Site_heracles/index.html)
!! Copyright: E. Audit - CEA/DSM/IRFU/SAp, France
!<
subroutine compute_preconditionning(RR,ZZ)

  use mod_grid     , only : nmin1,nmax1,nmin2,nmax2,nmin3,nmax3
  use mod_variables, only : debug_l1
  use mod_precision
  use mod_mpi      , only : rank

  implicit none

  real(dp), dimension(nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: RR,ZZ

  if(debug_l1) write(*,*) 'begin COMPUTE_PRECONDITIONNING',rank

  !$OMP PARALLEL WORKSHARE
  ZZ = RR
  !$OMP END PARALLEL WORKSHARE

  if(debug_l1) write(*,*) 'end   COMPUTE_PRECONDITIONNING',rank

  return

end subroutine compute_preconditionning

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine COMPUTE_DIFFERENCE
!
!> Computes the maximum relative difference for the X 
!! variable between two iterations.
!!
!! Adapted from HERACLES (http://irfu.cea.fr/Projets/Site_heracles/index.html)
!! Copyright: E. Audit - CEA/DSM/IRFU/SAp, France
!<
subroutine compute_difference(dif,X,PP,alpha)

  use mod_grid
  use mod_variables, only : debug_l1
  use mod_constants
  use mod_mpi

  implicit none

  integer :: i,j,k

  real(dp), dimension(nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: X,PP
  real(dp)                                                 :: dif,dif_loc,alpha

  if(debug_l1) write(*,*) 'begin COMPUTE_DIFFERENCE',rank

  dif_loc = zero ; dif = zero

  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k) REDUCTION(MAX:dif_loc)
  !$OMP DO COLLAPSE(3)
  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)

           dif_loc = max(dif_loc,abs(alpha*PP(i,j,k)/X(i,j,k)))

        enddo
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

#if MPI==1
  ierror = 0
  call mpi_allreduce(dif_loc,dif,1,mpi_real_dp,mpi_max_dp,world,ierror)
#else
  dif = dif_loc
#endif

  if(debug_l1) write(*,*) 'end   COMPUTE_DIFFERENCE',rank

  return

end subroutine compute_difference
