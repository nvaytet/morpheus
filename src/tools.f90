!---------------------------------------------------------------------------------------------------
! File: tools.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains function atan3()
!!
!! This file contains useful routines.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Function ATAN3:
!
!> Modified version of the atan2 fortran intrinsic function.
!<
function atan3(y,x)

  use mod_precision
  use mod_constants, only : zero,two,pi

  implicit none
  
  real(dp), intent(in) :: x,y
  real(dp)             :: atan3
  
  atan3 = atan2(y,x)
  if(atan3 .lt. zero) atan3 = two*pi + atan3
  
end function atan3
