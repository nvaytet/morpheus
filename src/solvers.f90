!---------------------------------------------------------------------------------------------------
! File: solvers.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines solver_exact(), solver_roe(), solver_hlle(), eigen_cons()
!! and compute_visc() and function wave()
!!
!! This file contains the solvers used to compute solutions to the Riemann problem.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine SOLVER_EXACT:
!
!> Exact iterative Riemann solver
!!
!!  Computes fluxes at the interface between cells using the left (l) and right (r) values
!! of the primitive variables.
!!
!! The left and right values are either the constant values in the left and right cells in
!! the case of first order scheme, or interpolated from the nodal coordinates up to the
!! interface using linear gradients in the case of a second order scheme.
!!
!! There is a possibility of adding some extra diffusion/viscosity at the end of this
!! routine by setting 'viscosity' to .true. in the namelist file.
!<
subroutine solver_exact(ucl,ucr,f)

  use mod_thermodynamics
  use mod_variables
  use mod_constants
  use mod_mpi           , only : rank

  implicit none

!   integer                  , intent(in ) :: ic
  real(dp), dimension(nvar), intent(in ) :: ucl,ucr
  real(dp), dimension(nvar), intent(out) :: f
  real(dp), dimension(nvar)              :: ff
  real(dp)                               :: dl,ul,vl,wl,pl,dr,ur,vr,wr,pr,cl,cr,vvl,vvr
  real(dp)                               :: p,ci,ps,v,w,cs,vc,u,d,wave
  integer                                :: n,itermax,itermax2
  logical                                :: print_errors

  if(debug_l3) write(*,*) 'begin SOLVER_EXACT',rank

  print_errors = .false.
  itermax      =  50
  itermax2     = 100

! !   if((ucl(1) <= zero) .or. (ucr(1) <= zero))then
!   write(*,*) 'got to here 0'
!   write(*,*)ucl
!   write(*,*)ucr
! !   endif
  
  ! Convert to primitive quantities
  dl = ucl(1)
  ul = ucl(2)/ucl(1)
  vl = ucl(3)/ucl(1)
  wl = ucl(4)/ucl(1)
  pl = g1*(ucl(5)-half*(ucl(2)**2+ucl(3)**2+ucl(4)**2)/ucl(1))
  
  dr = ucr(1)
  ur = ucr(2)/ucr(1)
  vr = ucr(3)/ucr(1)
  wr = ucr(4)/ucr(1)
  pr = g1*(ucr(5)-half*(ucr(2)**2+ucr(3)**2+ucr(4)**2)/ucr(1))
  
!   write(*,*) 'got to here 1'
!   write(*,*)ucl
!   write(*,*)ucr
  
  
  ! sound speed
  cr =  sqrt(g*pr*dr)
  cl =  sqrt(g*pl*dl)
  vvr =  cr
  vvl = -cl
  ! pressure at the interface
  p  = (vvr*pl-vvl*pr+vvr*vvl*(ur-ul))/(vvr-vvl)
  if(p < pcav)then
     if(print_errors) write(*,*)'p reset to pcav in flux 1',p,pcav
     p = pcav
  endif
  if((p < pl).and.(p < pr))then
     cr = cr/dr
     cl = cl/dl
     ci = cr/pr**g4
     p  = (half*g1*(ul-ur)+cr+cl)/(ci+cl/pl**g4)
     if(p < pcav)then
        if(print_errors) write(*,*)'p reset to pcav in flux 2',p,pcav
        p = pcav
     else
        p = p**g5
     endif
     vvr =  cr*dr*wave(p,pr)
     vvl = -cl*dl*wave(p,pl)
  else
     ps = p
     vvr =  cr*wave(p,pr)
     vvl = -cl*wave(p,pl)
     if((abs(p-pl) >= 0.1_dp*pl).or.(abs(p-pr) >= 0.1_dp*pr))then
        n = 0
  101   continue
        p = (vvr*pl-vvl*pr+vvl*vvr*(ur-ul))/(vvr-vvl)
        if(p < pcav)then
           if(print_errors) write(*,*)'p reset to pcav in flux 3',p,pcav
           p = pcav
           goto 102
        endif
        vvr =  cr*wave(p,pr)
        vvl = -cl*wave(p,pl)
        if(abs((p-ps)/p) >= 1.0e-04_dp)then
           ps = p
           n  = n + 1
           if(n > itermax) write(*,*)'n',n
           ! added to cleanly exit code
           if(n > itermax2)then
              call close_program('convergence failure in riemann solver')
           endif
           goto 101
        endif
     endif
  102 continue
     cr = cr/dr
     cl = cl/dl
  endif
  ! velocity at the interface
  u = (vvr*ur-vvl*ul-(pr-pl))/(vvr-vvl)
  if(u > zero)then
     d = vvl*dl/(vvl-(u-ul)*dl)
     v = vl
     w = wl
     if(p < pl)then
        cs = sqrt(g*p/d)
        if((u-cs) > zero)then
           if((ul-cl) > zero)then
              d = dl
              u = ul
              p = pl
           else
              u = g8*(ul+g7*cl)
              p = pl*(u/cl)**g5
              d = g*p/(u*u)
           endif
        endif
     elseif((vvl/dl+ul) > zero)then
        d = dl
        u = ul
        p = pl
     endif
  else
     d = vvr*dr/(vvr-(u-ur)*dr)
     v = vr
     w = wr
     if(p < pr)then
        cs = sqrt(g*p/d)
        if((u+cs) < zero)then
           if((ur+cr) < zero)then
              d = dr
              u = ur
              p = pr
           else
              u = g8*(ur-g7*cr)
              p = pr*(-u/cr)**g5
              d = g*p/(u*u)
           endif
        endif
     elseif((vvr/dr+ur) < zero)then
        d = dr
        u = ur
        p = pr
     endif
  endif

  f(1) = d*u
  f(2) = d*u*u + p
  f(3) = d*u*v
  f(4) = d*u*w
  f(5) = half*u*(g5*p+d*(u*u+v*v+w*w))

  ! Include artificial viscosity
  if(viscosity)then
     vc = sqrt(g*p/d)*visc
     call compute_visc(dl,dr,pl,pr,ul,ur,vl,vr,wl,wr,vc,ff)
     do n = 1,nvar
        f(n) = f(n) + ff(n)
     enddo
  endif

  if(debug_l4) write(*,*) dl,ul,vl,wl,pl,dr,ur,vr,wr,pr,f

  if(debug_l3) write(*,*) 'end   SOLVER_EXACT',rank

  return

end subroutine solver_exact

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Function WAVE:
!
!> Computes wave speeds in exact Riemann solver
!<
function wave(p,ps)

  use mod_variables, only : debug_l4
  use mod_constants
  use mod_mpi      , only : rank

  implicit none

  real(dp) :: p,ps,x,wave

  if(debug_l4) write(*,*) 'begin WAVE',rank

  x = p/ps
  if(abs(x-one) < 1.0e-03_dp)then
     wave = one + half*g3*(x-one)
     return
  elseif(p >= ps)then
     wave = sqrt(one+g3*(x-one))
     return
  else
     wave = g4*(one-x)/(one-x**g4)
     return
  endif

  if(debug_l4) write(*,*) p,ps,wave

  if(debug_l4) write(*,*) 'end   WAVE',rank

end function wave

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine SOLVER_ROE:
!
!> Roe Riemann solver (adapted from ATHENA3D, Stone et al. 2008)
!! 
!! Computes fluxes at the interface between cells using the left (l) and right (r) values
!! of the primitive variables.
!!
!! The left and right values are either the constant values in the left and right cells in
!! the case of first order scheme, or interpolated from the nodal coordinates up to the
!! interface using linear gradients in the case of a second order scheme.
!!
!! The solver computes the Roe system of 5 waves to obtain intermediate states.
!!
!! There is a possibility of adding some extra diffusion/viscosity at the end of this
!! routine by setting 'viscosity' to .true. in the namelist file.
!<
subroutine solver_roe(ucl,ucr,f)

  use mod_thermodynamics
  use mod_variables
  use mod_constants
  use mod_mpi           , only : rank

  implicit none

!   integer                  , intent(in ) :: ic
  real(dp), dimension(nvar), intent(in ) :: ucl,ucr
  real(dp), dimension(nvar), intent(out) :: f
  real(dp)                               :: dl,ul,vl,wl,pl,dr,ur,vr,wr,pr,vc
  real(dp)                               :: sqrtdl,sqrtdr,vxroe,vyroe,vzroe,hroe
  real(dp), dimension(nvar     )         :: ulocl,ulocr,lambda,a,fl,fr,ff
  real(dp), dimension(nvar,nvar)         :: lem,rem
  integer                                :: n,h

  if(debug_l3) write(*,*) 'begin SOLVER_ROE',rank

!   do n = 1,nvar
!      ulocl(n) = ucl(ivar(n,ic))
!      ulocr(n) = ucr(ivar(n,ic))
!   enddo
!   
!   ! Convert to primitive quantities
!   dl  = ulocl(1)
!   ul  = ulocl(2)/ulocl(1)
!   vl  = ulocl(3)/ulocl(1)
!   vvl = ulocl(4)/ulocl(1)
!   pl  = g1*(ulocl(5) - half*(ulocl(2)*ulocl(2)+ulocl(3)*ulocl(3)+ulocl(4)*ulocl(4))/ulocl(1))
!   
!   dr  = ulocr(1)
!   ur  = ulocr(2)/ulocr(1)
!   vr  = ulocr(3)/ulocr(1)
!   wr = ulocr(4)/ulocr(1)
!   pr  = g1*(ulocr(5) - half*(ulocr(2)*ulocr(2)+ulocr(3)*ulocr(3)+ulocr(4)*ulocr(4))/ulocr(1))
  
  ! Convert to primitive quantities
  dl = ucl(1)
  ul = ucl(2)/ucl(1)
  vl = ucl(3)/ucl(1)
  wl = ucl(4)/ucl(1)
  pl = g1*(ucl(5)-half*(ucl(2)**2+ucl(3)**2+ucl(4)**2)/ucl(1))
  
  dr = ucr(1)
  ur = ucr(2)/ucr(1)
  vr = ucr(3)/ucr(1)
  wr = ucr(4)/ucr(1)
  pr = g1*(ucr(5)-half*(ucr(2)**2+ucr(3)**2+ucr(4)**2)/ucr(1))
  
  ! Step 1 : Compute Roe-averaged data from left and right states
  !   These averages will be the input variables to the eigen problem
  sqrtdl = sqrt(dl)
  sqrtdr = sqrt(dr)
  vxroe  = (sqrtdl*ul + sqrtdr*ur)/(sqrtdl+sqrtdr)
  vyroe  = (sqrtdl*vl + sqrtdr*vr)/(sqrtdl+sqrtdr)
  vzroe  = (sqrtdl*wl + sqrtdr*wr)/(sqrtdl+sqrtdr)
  hroe   = ((ucl(5)+pl)/sqrtdl+(ucr(5)+pr)/sqrtdr)/(sqrtdl+sqrtdr)

  ! Step 2 : Compute eigenvalues and eigenmatrices from Roe-averaged values
  call eigen_cons(vxroe,vyroe,vzroe,hroe,lambda,rem,lem)

  ! Step 3 : Create intermediate states from eigenmatrices
  a(1:nvar) = (ucr(1)-ucl(1))*lem(1,1:nvar)
  do n = 2,nvar
     a(1:nvar) = a(1:nvar) + (ucr(n)-ucl(n))*lem(n,1:nvar)
  enddo

  ! Step 4 : Compute L/R fluxes
  !  These are computed from the left and right input primitive variables

  fl(1) = ucl(2)
  fr(1) = ucr(2)

  fl(2) = ucl(2)*ul + pl
  fr(2) = ucr(2)*ur + pr

  fl(3) = ucl(2)*vl
  fr(3) = ucr(2)*vr

  fl(4) = ucl(2)*wl
  fr(4) = ucr(2)*wr

  fl(5) = (ucl(5)+pl)*ul
  fr(5) = (ucr(5)+pr)*ur

  ! Compute Roe intermediate states
  do n = 1,nvar
     f(n) = half*(fl(n)+fr(n))
  enddo
  do n = 1,nvar
     do h = 1,nvar
        f(h) = f(h) - half*abs(lambda(n))*a(n)*rem(n,h)
     enddo
  enddo

  ! Include artificial viscosity
  if(viscosity)then
     vc = sqrt(g*half*(pl+pr)/(half*(dl+dr)))*visc
     call compute_visc(dl,dr,pl,pr,ul,ur,vl,vr,wl,wr,vc,ff)
     do n = 1,nvar
        f(n) = f(n) + ff(n)
     enddo
  endif

  if(debug_l4) write(*,*) dl,ul,vl,wl,pl,dr,ur,vr,wr,pr,f

  if(debug_l3) write(*,*) 'end   SOLVER_ROE',rank

  return

end subroutine solver_roe

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine EIGEN_CONS:
!
!> Hydro adiabatic roe eigenvalues and eigenvectors (adapted from ATHENA3D, Stone et al. 2008).
!<
subroutine eigen_cons(vx,vy,vz,hr,lambda,rem,lem)

  use mod_variables, only : nvar,debug_l4
  use mod_constants
  use mod_mpi      , only : rank

  implicit none

  real(dp)                       :: vx,vy,vz,hr,cs,vsq,norm
  real(dp), dimension(nvar     ) :: lambda
  real(dp), dimension(nvar,nvar) :: rem,lem

  if(debug_l4) write(*,*) 'begin EIGEN_CONS',rank

  vsq  = vx*vx + vy*vy + vz*vz
  cs   = sqrt(g1*max((hr-half*vsq),epsilon(vsq)))
  norm = half/(cs*cs)

  ! eigenvalues
  lambda(1) = vx - cs
  lambda(2) = vx
  lambda(3) = vx
  lambda(4) = vx
  lambda(5) = vx + cs

  ! right eigenmatrix  
  rem(1,1) = one
  rem(1,2) = vx - cs
  rem(1,3) = vy
  rem(1,4) = vz
  rem(1,5) = hr - vx*cs

  rem(2,1) = zero
  rem(2,2) = zero
  rem(2,3) = one
  rem(2,4) = zero
  rem(2,5) = vy

  rem(3,1) = zero
  rem(3,2) = zero
  rem(3,3) = zero
  rem(3,4) = one
  rem(3,5) = vz

  rem(4,1) = one
  rem(4,2) = vx
  rem(4,3) = vy
  rem(4,4) = vz
  rem(4,5) = half * vsq

  rem(5,1) = one
  rem(5,2) = vx + cs
  rem(5,3) = vy
  rem(5,4) = vz
  rem(5,5) = hr + vx*cs

  ! left eigenmatrix
  lem(1,1) =  norm*(half*g1*vsq + vx*cs)
  lem(2,1) = -norm*(g1*vx + cs)
  lem(3,1) = -norm*g1*vy
  lem(4,1) = -norm*g1*vz
  lem(5,1) =  norm*g1

  lem(1,2) = -vy
  lem(2,2) =  zero
  lem(3,2) =  one
  lem(4,2) =  zero
  lem(5,2) =  zero

  lem(1,3) = -vz
  lem(2,3) =  zero
  lem(3,3) =  zero
  lem(4,3) =  one
  lem(5,3) =  zero

  lem(1,4) =  one-norm*g1*vsq
  lem(2,4) =  g1*vx/(cs*cs)
  lem(3,4) =  g1*vy/(cs*cs)
  lem(4,4) =  g1*vz/(cs*cs)
  lem(5,4) = -g1/(cs*cs)

  lem(1,5) =  norm*(half*g1*vsq - vx*cs)
  lem(2,5) = -norm*(g1*vx - cs)
  lem(3,5) =  lem(3,1)
  lem(4,5) =  lem(4,1)
  lem(5,5) =  lem(5,1)

  if(debug_l4) write(*,*) 'end   EIGEN_CONS',rank

  return

end subroutine eigen_cons

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine SOLVER_HLLC:
!
!> HLLC Riemann solver
!! 
!! Computes fluxes at the interface between cells using the left (l) and right (r) values
!! of the primitive variables.
!!
!! The left and right values are either the constant values in the left and right cells in
!! the case of first order scheme, or interpolated from the nodal coordinates up to the
!! interface using linear gradients in the case of a second order scheme.
!!
!! The solver computes the HLLC system of 2 waves to obtain intermediate states.
!!
!! There is a possibility of adding some extra diffusion/viscosity at the end of this
!! routine by setting 'viscosity' to .true. in the namelist file.
!<
subroutine solver_hllc(ucl,ucr,f)

  use mod_thermodynamics
  use mod_variables
  use mod_constants
  use mod_mpi           , only : rank

  implicit none

! Arguments
  real(dp), dimension(nvar), intent(in ) :: ucl,ucr
  real(dp), dimension(nvar), intent(out) :: f
  
  ! Local variables
  real(dp), dimension(nvar) :: ff
  real(dp) :: dl,pl,ul,vl,wl,el,dr,pr,ur,vr,wr,er
  real(dp) :: cfastl,rcl,dstarl,cfastr,rcr,dstarr
  real(dp) :: estarl,estarr,ustar,pstar
  real(dp) :: dd,uu,pp,ee,sl,sr,vc
  integer  :: n
  
  if(debug_l3) write(*,*) 'begin SOLVER_HLLC',rank
    
  ! Convert from conservative to primitive variables
  dl = ucl(1)
  ul = ucl(2) / ucl(1)
  vl = ucl(3) / ucl(1)
  wl = ucl(4) / ucl(1)
  el = ucl(5)
  pl = g1*(ucl(5)-half*(ucl(2)**2+ucl(3)**2+ucl(4)**2)/ucl(1))
  
  dr = ucr(1)
  ur = ucr(2) / ucr(1)
  vr = ucr(3) / ucr(1)
  wr = ucr(4) / ucr(1)
  er = ucr(5)
  pr = g1*(ucr(5)-half*(ucr(2)**2+ucr(3)**2+ucr(4)**2)/ucr(1))
  
  ! Find the largest eigenvalues in the normal direction to the interface
  cfastl = sqrt(g*pl/dl)
  cfastr = sqrt(g*pr/dr)
  
  ! Compute HLL wave speed
  sl = min(min(ul,ur) - max(cfastl,cfastr),zero)
  sr = max(max(ul,ur) + max(cfastl,cfastr),zero)
  
  ! Compute lagrangian sound speed
  rcl = dl * (ul - sl)
  rcr = dr * (sr - ur)
  
  ! Compute acoustic star state
  ustar = (rcr*ur   +rcl*ul   +  (pl-pr))/(rcr+rcl)
  pstar = (rcr*pl+rcl*pr+rcl*rcr*(ul-ur))/(rcr+rcl)
  
  ! Left star region variables
  dstarl = dl*(sl-ul)/(sl-ustar)
  estarl = ((sl-ul)*el-pl*ul+pstar*ustar)/(sl-ustar)
  
  ! Right star region variables
  dstarr = dr*(sr-ur)/(sr-ustar)
  estarr = ((sr-ur)*er-pr*ur+pstar*ustar)/(sr-ustar)
  
  ! Sample the solution at x/t=0
  if(sl > zero)then
      dd = dl
      uu = ul
      pp = pl
      ee = el
  elseif(ustar > zero)then
      dd = dstarl
      uu = ustar
      pp = pstar
      ee = estarl
  elseif(sr > zero)then
      dd = dstarr
      uu = ustar
      pp = pstar
      ee = estarr
  else
      dd = dr
      uu = ur
      pp = pr
      ee = er
  endif
  
  ! Compute the Godunov flux
  f(1) = dd*uu
  f(2) = dd*uu*uu + pp
  f(5) = (ee + pp) * uu
  if(f(1) > zero)then
      f(3) = dd*uu*vl
      f(4) = dd*uu*wl
  else
      f(3) = dd*uu*vr
      f(4) = dd*uu*wr
  endif
  
  ! Include artificial viscosity
  if(viscosity)then
     vc = sqrt(g*pp/dd)*visc
     call compute_visc(dl,dr,pl,pr,ul,ur,vl,vr,wl,wr,vc,ff)
     do n = 1,nvar
        f(n) = f(n) + ff(n)
     enddo
  endif
  
  if(debug_l4) write(*,*) dl,ul,vl,wl,pl,dr,ur,vr,wr,pr,f

  if(debug_l3) write(*,*) 'end   SOLVER_HLLC',rank

  return
  
end subroutine solver_hllc

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine COMPUTE_VISC:
!
!> Computes bastard offspring of Falle & Walder artificial viscosity.
!<
subroutine compute_visc(dl,dr,pl,pr,ul,ur,vl,vr,wl,wr,vc,ff)

  use mod_constants
  use mod_variables, only : nvar,debug_l3
  use mod_mpi      , only : rank

  implicit none

  real(dp)                 , intent(in ) :: dl,dr,pl,pr,ul,ur,vl,vr,wl,wr,vc
  real(dp), dimension(nvar), intent(out) :: ff
  real(dp)                               :: el,er

  if(debug_l3) write(*,*) 'begin COMPUTE_VISC',rank

  er  = pr/g1 + half*dr*(ur*ur+vr*vr+wr*wr)
  el  = pl/g1 + half*dl*(ul*ul+vl*vl+wl*wl)

  ff(1) = vc*(dl-dr)
  ff(2) = vc*(dl*ul-dr*ur)
  ff(3) = vc*(dl*vl-dr*vr)
  ff(4) = vc*(dl*wl-dr*wr)
  ff(5) = vc*(el-er)

  if(debug_l3) write(*,*) 'end   COMPUTE_VISC',rank

  return

end subroutine compute_visc
