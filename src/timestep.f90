!---------------------------------------------------------------------------------------------------
! File: timestep.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines initial_timestep(), timestep_cell() and reduce_timestep()
!!
!! This file contains the routines which calculate the maximum timestep allowed by the CFL
!! condition or the cooling rate if cooling is present. The entire grid is searched for the minumum
!! cell crossing time as well as the boundaries.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine INITIAL_TIMESTEP:
!
!> Search through the grid for the maximum timestep allowed by stability criteria.
!! This routine is only called once at the start of the simulation.
!!
!! The dynamical timestep is either limited by gas speed or by the sound speed, whichever
!! is the largest. The dynamical timestep is limited by the Courant number 'cn' set in the
!! namelist.
!!
!! The cooling timestep is determined by the time it takes for the cell to radiate all of
!! its energy. It is limited to a fraction of its value (usually 5%) by the 'tcc' variable
!! in the namelist.
!!
!! In the case of MPI, the timestep is reduced over all nodes through the use of the
!! MPI_ALLREDUCE routine.
!<
subroutine timestep

  use mod_thermodynamics
  use mod_gridarrays    , only : x1,x2,x3,xn1,xn2
  use mod_gravity       , only : gravity,gravity_type,cn_gravity
  use mod_variables
  use mod_constants
!   use mod_timestep
  use mod_arrays        , only : unew
  use mod_grid
  use mod_mpi

  implicit none

  integer                          :: i,j,k
  character (len=11), dimension(6) :: dt_limit
  real(dp)                         :: cs,ws1,ws2,ws3,td1,td2,td3,td123,tc,tgrav
  real(dp)                         :: cooling_rate1,dx1,dx2,dx3,temp,tlog
  real(dp)                         :: d,u,v,w,p
  real(dp), dimension(3)           :: dt_array,dt_cell
  real(dp)                         :: dt_hydro,dt_cooling,dt_gravity
!     real(dp) :: dt_hydro_cell
!   real(dp) :: dt_cooling_cell
!   real(dp) :: dt_gravity_cell
  integer                          :: limit,limit_glob,ioutput
  integer           , dimension(6) :: ilim,jlim,klim,ilim_glob,jlim_glob,klim_glob
  integer           , dimension(7) :: icount,icount_glob
 
  if(debug_l1) write(*,*) 'begin INITIAL_TIMESTEP',rank

  dt_hydro   = large_number
  dt_cooling = large_number
  dt_gravity = large_number
!   limit          = 0
!   icount         = 0
!   dt_limit(1)    = geom1//' flow'
!   dt_limit(2)    = geom2//' flow'
!   dt_limit(3)    = geom3//' flow'
!   dt_limit(4)    = 'sound speed'
!   dt_limit(5)    = 'cooling'
!   dt_limit(6)    = 'gravity'
! 
!   hlimi = 0 ; hlimj = 0 ; hlimk = 0
!   climi = 0 ; climj = 0 ; climk = 0
!   glimi = 0 ; glimj = 0 ; glimk = 0

!   dt_cell = large_number

!   call fill_ghost_cells(uold)
  
  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,d,u,v,w,p,cs,ws1,ws2,ws3,dx1,td1,dx2,td2,dx3,td3,td123,temp,tlog,tc,tgrav) &
  !$OMP REDUCTION(MIN:dt_hydro) REDUCTION(MIN:dt_cooling) REDUCTION(MIN:dt_gravity)
  !$OMP DO COLLAPSE(3)
  do k = 1-cdim(3),nxloc(3)+cdim(3)
     do j = 1-cdim(2),nxloc(2)+cdim(2)
        do i = 1-cdim(1),nxloc(1)+cdim(1)
           
           d = unew(1,i,j,k)
           u = unew(2,i,j,k)/unew(1,i,j,k)
           v = unew(3,i,j,k)/unew(1,i,j,k)
           w = unew(4,i,j,k)/unew(1,i,j,k)
           p = g1*(unew(5,i,j,k) - half*(unew(2,i,j,k)**2+unew(3,i,j,k)**2+unew(4,i,j,k)**2)/unew(1,i,j,k))

!            write(*,*) i,j,k,d,u,v,w,p
!            read(*,*)
           
           ! ########### Hydro timestep ###########
           cs  = sqrt(g*p/d)        ! sound speed
           ws1 = abs(u) + cs
           ws2 = abs(v) + cs
           ws3 = abs(w) + cs
           dx1 = x1(i)-x1(i-1)
           td1 = dx1/ws1
           if(ndim > 1)then
              if(geometry == 1)then
                 dx2 = x2(j)-x2(j-1)
              elseif(geometry == 2)then
                 dx2 = abs(xn1(i))*(x2(j)-x2(j-1))
              elseif(geometry == 3)then
                 dx2 = x2(j)-x2(j-1)
              endif
              td2 = dx2/ws2
           else
              td2 = large_number
           endif
           if(ndim > 2)then
              if(geometry == 1)then
                 dx3 = x3(k)-x3(k-1)
              elseif(geometry == 2)then
                 dx3 = abs(xn1(i))*abs(sin(xn2(j)))*(x3(k)-x3(k-1))
              elseif(geometry == 3)then
                 dx3 = abs(xn1(i))*(x3(k)-x3(k-1))
              endif
              td3 = dx3/ws3
           else
              td3 = large_number
           endif
           td123 = min(td1,td2,td3)
           if(dt_hydro > td123)then
              dt_hydro = td123
!               hlimi = i
!               hlimj = j
!               hlimk = k
!               if(ws1 == max(ws1,ws2,ws3))then
!                  limith = 1
!               elseif(ws2 == max(ws1,ws2,ws3))then
!                  limith = 2
!               elseif(ws3 == max(ws1,ws2,ws3))then
!                  limith = 3
!               endif
!               if(cs == max(abs(u),abs(v),abs(w),cs)) limith = 4
           endif

           ! ########### Cooling timestep ###########
           if(cooling)then
              temp = p/(d*koverm)  !     temperature
              tlog = log10(temp)
              if((tlog > tclow).and.(tlog < tchigh))then
                 tc = p/(d*d*(ten**cooling_rate1(tlog))*cfactor)
              else
                 tc = large_number
              endif
              if(tc < dt_cooling)then
                 dt_cooling = tc
!                  climi = i
!                  climj = j
!                  climk = k
              endif
           endif

           ! ########### Gravity timestep ###########
           if(gravity .and. (gravity_type == 4))then
              tgrav = one / sqrt(grav*d)   ! free-fall time
              if(tgrav < dt_gravity)then
                 dt_gravity = tgrav
!                  glimi = i
!                  glimj = j
!                  glimk = k
              endif
           endif

        enddo
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL
  
  ! local timestep
  dt_cell(1) = cn_hydro  *dt_hydro
  dt_cell(2) = cn_cooling*dt_cooling
  dt_cell(3) = cn_gravity*dt_gravity

#if MPI==1
  call mpi_allreduce(dt_cell  ,dt_array  ,3,mpi_real_dp,mpi_min_dp,world,ierror)  ! all in one block
!   call mpi_allreduce(dt_hydro_cell  ,dt_hydro  ,1,mpi_real_dp,mpi_min_dp,world,ierror)  ! hydro
!   call mpi_allreduce(dt_cooling_cell,dt_cooling,1,mpi_real_dp,mpi_min_dp,world,ierror)  ! cooling
!   call mpi_allreduce(dt_gravity_cell,dt_gravity,1,mpi_real_dp,mpi_min_dp,world,ierror)  ! gravity
#else
  dt_array = dt_cell
!   dt_hydro   = dt_hydro_cell
!   dt_cooling = dt_cooling_cell
!   dt_gravity = dt_gravity_cell
#endif

  ! global timestep
!   dt = min(dt_hydro,dt_cooling,dt_gravity)
  dt = minval(dt_array)

!   ! Get locations of limiting cells
!   ilim      = 0 ; jlim      = 0 ; klim      = 0 ; icount      = 0 ; limit      = 0
!   ilim_glob = 0 ; jlim_glob = 0 ; klim_glob = 0 ; icount_glob = 0 ; limit_glob = 0
! 
  if(master_cpu .and. (mod(it,nout_term_freq) == 0))then
! 
!      if(dt_hydro_cell == dt_hydro)then
!         icount(1:4) = icount(1:4) + 1
!         ilim  (1:4) = hlimi+nxlower(1)-1
!         jlim  (1:4) = hlimj+nxlower(2)-1
!         klim  (1:4) = hlimk+nxlower(3)-1
!      endif
! 
!      if(dt_cooling_cell == dt_cooling)then
!         icount(5) = icount(5) + 1
!         ilim  (5) = climi+nxlower(1)-1
!         jlim  (5) = climj+nxlower(2)-1
!         klim  (5) = climk+nxlower(3)-1
!      endif
! 
!      if(dt_gravity_cell == dt_gravity)then
!         icount(6) = icount(6) + 1
!         ilim  (6) = glimi+nxlower(1)-1
!         jlim  (6) = glimj+nxlower(2)-1
!         klim  (6) = glimk+nxlower(3)-1
!      endif
! 
!      if(dt_hydro_cell == dt)then
!         limit = limith
!         icount(7) = icount(7) + 1
!      elseif(dt_cooling_cell == dt)then
!         limit = 5
!         icount(7) = icount(7) + 1
!      elseif(dt_gravity_cell == dt)then
!         limit = 6
!         icount(7) = icount(7) + 1
!      endif
! 
! #if MPI==1
! 
!      ! Account for the fact that two cpus might hold the same limiting cell
!                   call mpi_allreduce(ilim(1:4),ilim_glob(1:4),4,mpi_integer,mpi_sum,world,ierror)
!      if(ndim > 1) call mpi_allreduce(jlim(1:4),jlim_glob(1:4),4,mpi_integer,mpi_sum,world,ierror)
!      if(ndim > 2) call mpi_allreduce(klim(1:4),klim_glob(1:4),4,mpi_integer,mpi_sum,world,ierror)
! 
!      call mpi_allreduce( limit, limit_glob,1,mpi_integer,mpi_sum,world,ierror)
!      call mpi_allreduce(icount,icount_glob,7,mpi_integer,mpi_sum,world,ierror)
! 
!      if(master_cpu)then
!         do i = 1,6
!            if(icount_glob(i) > 0)then
!               ilim_glob(i) =  ilim_glob(i) / icount_glob(i)
!               jlim_glob(i) =  jlim_glob(i) / icount_glob(i)
!               klim_glob(i) =  klim_glob(i) / icount_glob(i)
!            endif
!         enddo
!         limit_glob = limit_glob / icount_glob(7)
!      endif
! 
!      if(ndim < 2) jlim_glob = 1
!      if(ndim < 3) klim_glob = 1
! 
! #else
! 
!       ilim_glob = ilim
!       jlim_glob = jlim
!       klim_glob = klim
!      limit_glob = limit
! 
! #endif

!      if(master_cpu)then
!         write(terminal_outputs(3),202) ' dt limited at '//geom1//' =',ilim_glob(limit_glob), &
!                                           ', '//geom2//' =',jlim_glob(limit_glob), &
!                                           ', '//geom3//' =',klim_glob(limit_glob), &
!                                           ' by ',dt_limit(limit_glob)
        ioutput = 3
        write(terminal_outputs(ioutput),205) ' dt_hydro   = ',dt_array(1)
        if(cooling)then
           ioutput = ioutput + 1
           write(terminal_outputs(ioutput),205) ' dt_cooling = ',dt_array(2)
        endif
        if(gravity)then
           ioutput = ioutput + 1
           write(terminal_outputs(ioutput),205) ' dt_gravity = ',dt_array(3)
        endif
!      endif

  endif

  ! adjust timestep if end of simulation is reached
  if(dt > (tlim-t)) dt = tlim - t
  
  if(debug_l4) write(*,*) 'dt,dt_cell,dt_array',dt,dt_cell,dt_array !,dt_hydro_cell,dt_cooling,dt_cooling_cell,dt_gravity,dt_gravity_cell

  if(debug_l1) write(*,*) 'end   TIMESTEP',rank

  return

  202 format(a,i5,a,i5,a,i5,a,a)
  205 format(a,es13.5)

end subroutine timestep

! !###################################################################################################
! !###################################################################################################
! !###################################################################################################
! 
! !  Subroutine TIMESTEP_CELL:
! !
! !> Compute timestep in current cell.
! !!
! !! The dynamical timestep is either limited by gas speed or by the sound speed, whichever
! !! is the largest. The dynamical timestep is limited by the Courant number 'cn' set in the
! !! namelist.
! !!
! !! The cooling timestep is determined by the time it takes for the cell to radiate all of
! !! its energy. It is limited to a fraction of its value (usually 5%) by the 'tcc' variable
! !! in the namelist.
! !<
! subroutine timestep_cell(i,j,k,nv,ucons)
! 
!   use mod_thermodynamics
!   use mod_gridarrays    , only : x1,x2,x3,xn1,xn2
!   use mod_gravity       , only : gravity,gravity_type
!   use mod_variables
!   use mod_constants
!   use mod_timestep
!   use mod_grid
!   use mod_mpi           , only : rank
! 
!   implicit none
! 
!   integer                , intent(in) :: i,j,k,nv
!   real(dp), dimension(nv), intent(in) :: ucons
!   real(dp)                            :: cs,ws1,ws2,ws3,td1,td2,td3,td123,tc,tgrav
!   real(dp)                            :: temp,tlog,cooling_rate1,dx1,dx2,dx3,d,u,v,w,p
!  
!   if(debug_l3) write(*,*) 'begin TIMESTEP_CELL',rank
! 
!   
!   
!   if(debug_l3) write(*,*) 'end   TIMESTEP_CELL',rank
!   
!   return
!   
! end subroutine timestep_cell
! 
! !###################################################################################################
! !###################################################################################################
! !###################################################################################################
! 
! !  Subroutine REDUCE_TIMESTEP:
! !
! !> Reduce timestep computed by different cpus to find minimum in entire grid.
! !! The timestep is reduced over all nodes through the use of the MPI_ALLREDUCE routine.
! !<
! subroutine reduce_timestep
! 
!   use mod_thermodynamics, only : cooling,cn_cooling
!   use mod_gravity       , only : gravity,cn_gravity
!   use mod_variables
!   use mod_timestep
!   use mod_mpi
!   use mod_grid
! 
!   implicit none
!   
!   real(dp)                         :: dt_hydro,dt_cooling,dt_gravity
!   integer                          :: i,limit,limit_glob,ioutput
!   integer           , dimension(6) :: ilim,jlim,klim,ilim_glob,jlim_glob,klim_glob
!   integer           , dimension(7) :: icount,icount_glob
!   character (len=11), dimension(6) :: dt_limit
!   
!   if(debug_l1) write(*,*) 'begin REDUCE_TIMESTEP',rank
!   
!   dt_limit(1)    = geom1//' flow'
!   dt_limit(2)    = geom2//' flow'
!   dt_limit(3)    = geom3//' flow'
!   dt_limit(4)    = 'sound speed'
!   dt_limit(5)    = 'cooling'
!   dt_limit(6)    = 'gravity'
!   
!   ! local timestep
!   dt_hydro_cell   = cn_hydro  *dt_hydro_cell
!   dt_cooling_cell = cn_cooling*dt_cooling_cell
!   dt_gravity_cell = cn_gravity*dt_gravity_cell
! 
! #if MPI==1
!   call mpi_allreduce(dt_hydro_cell  ,dt_hydro  ,1,mpi_real_dp,mpi_min_dp,world,ierror)  ! hydro
!   call mpi_allreduce(dt_cooling_cell,dt_cooling,1,mpi_real_dp,mpi_min_dp,world,ierror)  ! cooling
!   call mpi_allreduce(dt_gravity_cell,dt_gravity,1,mpi_real_dp,mpi_min_dp,world,ierror)  ! gravity
! #else
!   dt_hydro   = dt_hydro_cell
!   dt_cooling = dt_cooling_cell
!   dt_gravity = dt_gravity_cell
! #endif
! 
!   ! global timestep
!   dt = min(dt_hydro,dt_cooling,dt_gravity)
! 
!   ! Get locations of limiting cells
!   ilim      = 0 ; jlim      = 0 ; klim      = 0 ; icount      = 0 ; limit      = 0
!   ilim_glob = 0 ; jlim_glob = 0 ; klim_glob = 0 ; icount_glob = 0 ; limit_glob = 0
! 
!   if(mod(it,nout_term_freq) == 0)then
! 
!      if(dt_hydro_cell == dt_hydro)then
!         icount(1:4) = icount(1:4) + 1
!         ilim  (1:4) = hlimi+nxlower(1)-1
!         jlim  (1:4) = hlimj+nxlower(2)-1
!         klim  (1:4) = hlimk+nxlower(3)-1
!      endif
! 
!      if(dt_cooling_cell == dt_cooling)then
!         icount(5) = icount(5) + 1
!         ilim  (5) = climi+nxlower(1)-1
!         jlim  (5) = climj+nxlower(2)-1
!         klim  (5) = climk+nxlower(3)-1
!      endif
! 
!      if(dt_gravity_cell == dt_gravity)then
!         icount(6) = icount(6) + 1
!         ilim  (6) = glimi+nxlower(1)-1
!         jlim  (6) = glimj+nxlower(2)-1
!         klim  (6) = glimk+nxlower(3)-1
!      endif
! 
!      if(dt_hydro_cell == dt)then
!         limit = limith
!         icount(7) = icount(7) + 1
!      elseif(dt_cooling_cell == dt)then
!         limit = 5
!         icount(7) = icount(7) + 1
!      elseif(dt_gravity_cell == dt)then
!         limit = 6
!         icount(7) = icount(7) + 1
!      endif
! 
! #if MPI==1
! 
!      ! Account for the fact that two cpus might hold the same limiting cell
!                   call mpi_allreduce(ilim(1:4),ilim_glob(1:4),4,mpi_integer,mpi_sum,world,ierror)
!      if(ndim > 1) call mpi_allreduce(jlim(1:4),jlim_glob(1:4),4,mpi_integer,mpi_sum,world,ierror)
!      if(ndim > 2) call mpi_allreduce(klim(1:4),klim_glob(1:4),4,mpi_integer,mpi_sum,world,ierror)
! 
!      call mpi_allreduce( limit, limit_glob,1,mpi_integer,mpi_sum,world,ierror)
!      call mpi_allreduce(icount,icount_glob,7,mpi_integer,mpi_sum,world,ierror)
! 
!      if(master_cpu)then
!         do i = 1,6
!            if(icount_glob(i) > 0)then
!               ilim_glob(i) =  ilim_glob(i) / icount_glob(i)
!               jlim_glob(i) =  jlim_glob(i) / icount_glob(i)
!               klim_glob(i) =  klim_glob(i) / icount_glob(i)
!            endif
!         enddo
!         limit_glob = limit_glob / icount_glob(7)
!      endif
! 
!      if(ndim < 2) jlim_glob = 1
!      if(ndim < 3) klim_glob = 1
! 
! #else
! 
!       ilim_glob = ilim
!       jlim_glob = jlim
!       klim_glob = klim
!      limit_glob = limit
! 
! #endif
! 
!      if(master_cpu)then
!         write(terminal_outputs(3),202) ' dt limited at '//geom1//' =',ilim_glob(limit_glob), &
!                                           ', '//geom2//' =',jlim_glob(limit_glob), &
!                                           ', '//geom3//' =',klim_glob(limit_glob), &
!                                           ' by ',dt_limit(limit_glob)
!         ioutput = 4
!         write(terminal_outputs(ioutput),205) ' dt_hydro   = ',dt_hydro
!         if(cooling)then
!            ioutput = ioutput + 1
!            write(terminal_outputs(ioutput),205) ' dt_cooling = ',dt_cooling
!         endif
!         if(gravity)then
!            ioutput = ioutput + 1
!            write(terminal_outputs(ioutput),205) ' dt_gravity = ',dt_gravity
!         endif
!      endif
! 
!   endif
! 
!   ! adjust timestep if end of simulation is reached
!   if(dt > (tlim-t)) dt = tlim - t
!   
!   if(debug_l4) write(*,*) dt,dt_hydro,dt_hydro_cell,dt_cooling,dt_cooling_cell,dt_gravity,dt_gravity_cell
! 
!   if(debug_l1) write(*,*) 'end   REDUCE_TIMESTEP',rank
! 
!   return
! 
!   202 format(a,i5,a,i5,a,i5,a,a)
!   205 format(a,es13.5)
! 
! end subroutine reduce_timestep
