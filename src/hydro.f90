!---------------------------------------------------------------------------------------------------
! File: hydro.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines hydro(), datacheck(), utop(), flux(), sources() and functions wave()
!! and av()
!!
!! This file contains the routines used to update all the cells in the computational medium by
!! one timestep. The hydro routine() computes interface values which are then fed into flux()
!! which calculates fluxes across the interface. Geometrical, cooling and gravitational sources
!! are included. Data is also checked for negative pressures and densities.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine HYDRO:
!
!> To compute first and second order fluxes accross cell faces.
!!
!! In the first order step, all quantities are piecewise constant inside each cell.
!! If a second order scheme is used, the first order step is only used to compute half
!! timestep predictions.
!!
!! In the second order step, quantity gradients are computed from the first order predictions
!! between neighbouring cells to improve the solution. In the case of spherical geometry
!! (geometry = 2), some flux offsets also have to be included.
!<
subroutine hydro

  use mod_thermodynamics
  use mod_gridarrays
  use mod_constants
  use mod_variables
  use mod_arrays
  use mod_grid
  use mod_mpi

  implicit none
  
  real(dp), dimension(nvar        ) :: var_l,var_r,flux
  real(dp), dimension(nvar,nsource) :: source
  real(dp), dimension(1:3         ) :: dx_l,dx_r
  real(dp)                          :: av,grad1,grad2,grad3
  integer                           :: i,j,k,h,n,s
  character(len=50) :: fname

  if(debug_l1) write(*,*) 'begin HYDRO',rank

  ! begin first order stage
  if(debug_l2) write(*,*) 'begin 1st order stage',rank
  
  call fill_ghost_cells(unew)
  
  call timestep
  
  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,n,h,s,var_l,var_r,flux,source,grad1,grad2,grad3,dx_l,dx_r)
  
  !$OMP WORKSHARE
  uold            = unew
  uint(:,1,:,:,:) = unew
  !$OMP END WORKSHARE

  ! now loop through whole grid
    
  !$OMP DO COLLAPSE(3)
  do k = 1-cdim(3),nxloc(3)+bdim(3)
     do j = 1-cdim(2),nxloc(2)+bdim(2)
        do i = 1-cdim(1),nxloc(1)+bdim(1)
        
           do n = 1,ndim
           
              do h = 1,nvar
                 var_l(h) = uold(ivar(h,n),i              ,j              ,k              )
                 var_r(h) = uold(ivar(h,n),i+identity(1,n),j+identity(2,n),k+identity(3,n))
              enddo
        
              ! compute flux by calling Riemann solver                      
              call compute_flux(var_l,var_r,flux)
              
              do h = 1,nvar
                 uint(ivar(h,n),1,i,j,k) = uint(ivar(h,n),1,i,j,k) - cell_area(ieven(n),i,j,k)*flux(h)*dt*half/cell_volume(i,j,k)
                 uint(ivar(h,n),1,i+identity(1,n),j+identity(2,n),k+identity(3,n)) = uint(ivar(h,n),1,i+identity(1,n),j+identity(2,n),k+identity(3,n)) + cell_area(iodd(n),i+identity(1,n),j+identity(2,n),k+identity(3,n))*flux(h)*dt*half/cell_volume(i+identity(1,n),j+identity(2,n),k+identity(3,n))
              enddo
              
           enddo
           
           call sources(i,j,k,uold(1:nvar,i,j,k),uold(1:nvar,i,j,k),uold(1:nvar,i,j,k),uold(1:nvar,i,j,k),source)
           
           do h = 1,nvar
              do s = 1,nsource
                 uint(h,1,i,j,k) = uint(h,1,i,j,k) + dt*source(h,s)*half
              enddo
           enddo
           
           call density_and_pressure_fix(uint(1:nvar,1,i,j,k))

        enddo
     enddo
  enddo
  !$OMP END DO
  
  if(debug_l2) write(*,*) 'end   1st order stage',rank
  
  call fill_ghost_cells(uint(:,1,:,:,:))
  
  if(debug_l2) write(*,*) 'begin 2nd order stage',rank
    
  !$OMP WORKSHARE
  uint(:,2,:,:,:) = uint(:,1,:,:,:)
  uint(:,3,:,:,:) = uint(:,1,:,:,:)
  !$OMP END WORKSHARE
  
  !$OMP DO COLLAPSE(3)
  do k = 1-bdim(3),nxloc(3)+bdim(3)
     do j = 1-bdim(2),nxloc(2)+bdim(2)
        do i = 1-bdim(1),nxloc(1)+bdim(1)

           do h = 1,nvar
              usrc(h,  i,j,k) = uint(h,1,i,j,k)
              grad1 = (uint(h,1,i  ,j,k) - uint(h,1,i-1,j,k)) / (xn1(i  ) - xn1(i-1))
              grad2 = (uint(h,1,i+1,j,k) - uint(h,1,i  ,j,k)) / (xn1(i+1) - xn1(i  ))
              grad3 = (uint(h,1,i+1,j,k) - uint(h,1,i-1,j,k)) / (xn1(i+1) - xn1(i-1))
              gradav(h,1,i,j,k) = av(slope_type,grad1,grad2,grad3)
              uint(h,2,i,j,k) = uint(h,1,i,j,k) + gradav(h,1,i,j,k)*(rnt(i) - xn1(i))
              if(geometry==3) usrc(h,i,j,k) = uint(h,1,i,j,k) + gradav(h,1,i,j,k)*(xh1(i) - xn1(i))
           enddo

        enddo
     enddo
  enddo
  !$OMP END DO
    
  if(ndim > 1)then
     !$OMP DO COLLAPSE(3)
     do k = 1-bdim(3),nxloc(3)+bdim(3)
        do j = 1-bdim(2),nxloc(2)+bdim(2)
           do i = 1-bdim(1),nxloc(1)+bdim(1)

              do h = 1,nvar
                 grad1 = (uint(h,2,i,j  ,k) - uint(h,2,i,j-1,k)) / (xn2(j  ) - xn2(j-1))
                 grad2 = (uint(h,2,i,j+1,k) - uint(h,2,i,j  ,k)) / (xn2(j+1) - xn2(j  ))
                 grad3 = (uint(h,2,i,j+1,k) - uint(h,2,i,j-1,k)) / (xn2(j+1) - xn2(j-1))
                 gradav(h,2,i,j,k) = av(slope_type,grad1,grad2,grad3)
                 uint(h,3,i,j,k) = uint(h,2,i,j,k) + gradav(h,2,i,j,k)*(xh2(j) - xn2(j))
                 if(geometry==2) usrc(h,i,j,k) = uint(h,2,i,j,k) + gradav(h,2,i,j,k)*(ths(j) - xn2(j))
              enddo

           enddo
        enddo
     enddo
     !$OMP END DO
  endif

  if(ndim > 2)then
     !$OMP DO COLLAPSE(3)
     do k = 1-bdim(3),nxloc(3)+bdim(3)
        do j = 1-bdim(2),nxloc(2)+bdim(2)
           do i = 1-bdim(1),nxloc(1)+bdim(1)

              do h = 1,nvar
                 grad1 = (uint(h,3,i,j,k  ) - uint(h,3,i,j,k-1)) / (xn3(k  ) - xn3(k-1))
                 grad2 = (uint(h,3,i,j,k+1) - uint(h,3,i,j,k  )) / (xn3(k+1) - xn3(k  ))
                 grad3 = (uint(h,3,i,j,k+1) - uint(h,3,i,j,k-1)) / (xn3(k+1) - xn3(k-1))
                 gradav(h,3,i,j,k) = av(slope_type,grad1,grad2,grad3)
              enddo

           enddo
        enddo
     enddo
     !$OMP END DO
  endif
  
  ! now loop through whole grid
  !$OMP DO COLLAPSE(3)
  do k = 1-adim(3),nxloc(3)
     do j = 1-adim(2),nxloc(2)
        do i = 1-adim(1),nxloc(1)
        
           dx_l(1) = x1 (i        ) - xn1(i)
           dx_r(1) = xn1(i+adim(1)) - x1 (i)
           dx_l(2) = x2 (j        ) - xn2(j)
           dx_r(2) = xn2(j+adim(2)) - x2 (j)
           dx_l(3) = x3 (k        ) - xn3(k)
           dx_r(3) = xn3(k+adim(3)) - x3 (k)
           
           do n = 1,ndim
           
              do h = 1,nvar
                 var_l(h) = uint(ivar(h,n),n,i              ,j              ,k              ) + gradav(ivar(h,n),n,i              ,j              ,k              )*dx_l(n)
                 var_r(h) = uint(ivar(h,n),n,i+identity(1,n),j+identity(2,n),k+identity(3,n)) - gradav(ivar(h,n),n,i+identity(1,n),j+identity(2,n),k+identity(3,n))*dx_r(n)                 
              enddo
              
              ! compute flux by calling Riemann solver                      
              call compute_flux(var_l,var_r,flux)
              
              do h = 1,nvar
                 unew(ivar(h,n),i              ,j              ,k              ) = unew(ivar(h,n),i              ,j              ,k              ) - cell_area(ieven(n),i              ,j              ,k              )*flux(h)*dt/cell_volume(i              ,j              ,k              )
                 unew(ivar(h,n),i+identity(1,n),j+identity(2,n),k+identity(3,n)) = unew(ivar(h,n),i+identity(1,n),j+identity(2,n),k+identity(3,n)) + cell_area(iodd (n),i+identity(1,n),j+identity(2,n),k+identity(3,n))*flux(h)*dt/cell_volume(i+identity(1,n),j+identity(2,n),k+identity(3,n))
              enddo
              
           enddo
           
           call sources(i,j,k,uint(1:nvar,1,i,j,k),uint(1:nvar,2,i,j,k),usrc(1:nvar,i,j,k),uold(1:nvar,i,j,k),source)

           do s = 1,nsource
              do h = 1,nvar
                 unew(h,i,j,k) = unew(h,i,j,k) + dt*source(h,s)
              enddo
           enddo
           
           call density_and_pressure_fix(unew(1:nvar,i,j,k))
        
        enddo
     enddo
  enddo
  !$OMP END DO
  
  !$OMP END PARALLEL
  
  if(debug_l2) write(*,*) 'end   2nd order stage',rank
  
  if(debug_l1) write(*,*) 'end   HYDRO',rank
  
  return
  
end subroutine hydro

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine DENSITY_AND_PRESSURE_FIX:
!
!> Checks the grid for negative densities and pressures before the first timestep,
!! between the first and second order stages, and at the end of each timestep.
!<
subroutine density_and_pressure_fix(ucons)

  use mod_thermodynamics, only : dcav,cavlim
  use mod_constants
  use mod_variables     , only : nvar,debug_l3
  use mod_mpi           , only : rank

  implicit none

  real(dp), dimension(1:nvar), intent(inout) :: ucons
  real(dp)                                   :: d,p,ke

  if(debug_l3) write(*,*) 'begin DENSITY_AND_PRESSURE_FIX_CELL',rank

  d = ucons(1)
  p = ucons(5)-half*(ucons(2)**2+ucons(3)**2+ucons(4)**2)/ucons(1)
  
  if(d < dcav)then
     write(*,*) d,dcav,' negative DENSITY',rank!,origin
     ! maintain the same velocity and pressure in cell whilst resetting density
     ucons(1) = dcav
     ucons(2) = ucons(2) / d * ucons(1)
     ucons(3) = ucons(3) / d * ucons(1)
     ucons(4) = ucons(4) / d * ucons(1)
  endif
  
  ! kinetic energy
  ke = half*(ucons(2)**2+ucons(3)**2+ucons(4)**2)/ucons(1)
  
  ! internal energy
  if(p < cavlim*ke)then
     write(*,*) p,ke,cavlim*ke,' negative PRESSURE',rank!,origin
     p = cavlim*ke
  endif
  
  ucons(5) = p + ke
  
  if(debug_l3) write(*,*) 'end   DENSITY_AND_PRESSURE_FIX_CELL',rank
  
  return
  
end subroutine density_and_pressure_fix

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine FLUX:
!
!> Selects Riemann solver to compute fluxes accross the interface.
!<
subroutine compute_flux(ucl,ucr,f)

  use mod_variables
  use mod_mpi      , only : rank

  implicit none

!   integer                  , intent(in ) :: ic
  real(dp), dimension(nvar), intent(in ) :: ucl,ucr
  real(dp), dimension(nvar), intent(out) :: f
  character(len=str)                     :: string

  if(debug_l3) write(*,*) 'begin FLUX',rank
  
  select case(isolver)

  case(isolver_exact)

     call solver_exact(ucl,ucr,f)

  case(isolver_roe)

     call solver_roe  (ucl,ucr,f)

  case(isolver_hllc)

     call solver_hllc (ucl,ucr,f)

  case default

     write(string,*) 'Unknown hydro solver:',isolver,solver
     call close_program(trim(string))

  end select

  if(debug_l3) write(*,*) 'end   FLUX',rank

  return

end subroutine compute_flux

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Function AV:
!
!> Gradient averaging for second order scheme. Preserves TVD and monotonicity.
!!
!! In namelist, set 'lim' to:
!!    - 0 : null     averaging
!!    - 1 : minmod   averaging
!!    - 2 : moncen   averaging
!!    - 3 : superbee averaging
!!    - 4 : falle    averaging
!<
function av(l,r1,r2,r3)

  use mod_constants
  use mod_variables, only : debug_l3,debug_l4
  use mod_mpi      , only : rank

  implicit none

  real(dp) :: r1,r2,r3,r4,av
  integer  :: l,i1

  if(debug_l3) write(*,*) 'begin AV',rank

  select case(l)

  case(0) ! No averaging

     av = zero

  case(1) ! Minmod

     if(abs(r1) > abs(r2))then
        av = r2
     else
        av = r1
     endif
     if(r1*r2 < zero)then
        av = zero
     endif

  case(2) ! Moncen

     r1 = two * r1
     r2 = two * r2
     if((abs(r1) < abs(r2)).and.(abs(r1) < abs(r3)))then
        av = r1
     elseif((abs(r2) < abs(r1)).and.(abs(r2) < abs(r3)))then
        av = r2
     else
        av = r3
     endif

     if((r1*r2 < zero).or.(r1*r3 < zero).or.(r2*r3 < zero))then
        av = zero
     endif

  case(3) ! Superbee
  
     if(abs(r1) > abs(r2))then
        r4 = half*r1
     else
        r4 = half*r2
     endif
     if((abs(r1) < abs(r2)).and.(abs(r1) < abs(r4)))then
        av = r1
     elseif((abs(r2) < abs(r1)).and.(abs(r2) < abs(r4)))then
        av = r2
     else
        av = r4
     endif
     if(r1*r2 < zero)then
        av = zero
     endif

  case(4) ! Falle
  
     i1 = 1
     if(r1*r2 < zero)then
        av = zero
     else
        r4 = (abs(r1)**i1) + (abs(r2)**i1)
        if(r4 == zero)then
           av = zero
        else
           av = ((abs(r1)**i1)*r2 + (abs(r2)**i1)*r1) / r4
        endif
     endif

  case(5) ! Arithmetic average
  
     if(r1*r2 < zero)then
        av = zero
     else
        av = half * (r1 + r2)
     endif
     
  case default

     call close_program('Wrong averaging function')

  end select
  
  if(debug_l4) write(*,*) l,r1,r2,r3,av

  if(debug_l3) write(*,*) 'end   AV',rank

end function av

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine SOURCES:
!
!> Computes geometrical, cooling and gravity source terms.
!!
!! The sources array(i,j) is composed following:
!!    - i : 1 to 5 for the primitive variables (density, velocity, pressure)
!!    - j : 1 for geometry, 2 for cooling, 3 for gravity
!!
!! Additional sources can either be added here or in the extras subroutine.
!<
subroutine sources(i,j,k,uc1,uc2,uc3,uc4,source)

  use mod_thermodynamics
  use mod_gridarrays    , only : xh1,rnt,b
  use mod_gravity
  use mod_variables
  use mod_constants
  use mod_grid
  use mod_mpi           , only : rank

  implicit none

  integer                          , intent(in ) :: i,j,k
  real(dp), dimension(nvar        ), intent(in ) :: uc1,uc2,uc3,uc4
  real(dp), dimension(nvar,nsource), intent(out) :: source
  integer                                        :: h
  real(dp)                                       :: temp,tlog,cooling_rate1,scalprod,cooling_rate2
  real(dp)                                       :: d1,u1,v1,w1,p1,d2,u2,v2,w2,p2
  real(dp)                                       :: d3,u3,v3,w3,p3,d4,u4,v4,w4,p4

  if(debug_l3) write(*,*) 'begin SOURCES',rank
  
  ! Convert to primitive variables  
  d1 = uc1(1)
  u1 = uc1(2)/uc1(1)
  v1 = uc1(3)/uc1(1)
  w1 = uc1(4)/uc1(1)
  p1 = g1*(uc1(5) - half*(uc1(2)*uc1(2)+uc1(3)*uc1(3)+uc1(4)*uc1(4))/uc1(1))
  
  d2 = uc2(1)
  u2 = uc2(2)/uc2(1)
  v2 = uc2(3)/uc2(1)
  w2 = uc2(4)/uc2(1)
  p2 = g1*(uc2(5) - half*(uc2(2)*uc2(2)+uc2(3)*uc2(3)+uc2(4)*uc2(4))/uc2(1))
  
  d3 = uc3(1)
  u3 = uc3(2)/uc3(1)
  v3 = uc3(3)/uc3(1)
  w3 = uc3(4)/uc3(1)
  p3 = g1*(uc3(5) - half*(uc3(2)*uc3(2)+uc3(3)*uc3(3)+uc3(4)*uc3(4))/uc3(1))
  
  d4 = uc4(1)
  u4 = uc4(2)/uc4(1)
  v4 = uc4(3)/uc4(1)
  w4 = uc4(4)/uc4(1)
  p4 = g1*(uc4(5) - half*(uc4(2)*uc4(2)+uc4(3)*uc4(3)+uc4(4)*uc4(4))/uc4(1))

!   write(*,*) 'got to here 1',switch,i,j,k
!   write(*,*)uc1
!   write(*,*)uc2
!   write(*,*)uc3
  
  source = zero

  ! Geometrical source terms
  if(geometry == 2)then
     select case(ndim)
     case(1)
        source(2,1) = two*p2/rnt(i)
     case(2)
        source(2,1) = (two*p2 + d2*v2*v2)/rnt(i)
        source(3,1) = -(d2*u2*v2)/rnt(i) + b(j)*p3/rnt(i)
     case(3)
        source(2,1) = (two*p2 + d2*(v2*v2 + w2*w2))/rnt(i)
        source(3,1) = -(d2*u2*v2)/rnt(i) + b(j)*(p3+d3*w3*w3)/rnt(i)
        source(4,1) = -(d2*u2*w2)/rnt(i) - b(j)*(d3*v3*w3)/rnt(i)
     end select
  endif

  if(geometry == 3)then
     select case(ndim)
     case(1)
        source(2,1) = p3/xh1(i)
     case(2)
        source(2,1) = p3/xh1(i)
     case(3)
        source(2,1) = (p3+d3*w3*w3)/xh1(i)
        source(4,1) = (-d3*u3*w3)/xh1(i)
     end select
  endif

  ! Radiative cooling
!   if(cooling .and. (.not.nocooling(i,j,k)))then
  if(cooling)then
!      select case(switch)
!      case(1)
     temp = p4/(d4*koverm)
     tlog = log10(temp)
     if((tlog > tclow).and.(tlog < tchigh))then
        source(nvar,2) = -d4*d4*(ten**cooling_rate1(tlog))*cfactor
     endif
!      case(2)
!         source(nvar,2) = source_in_mem
!      end select
  endif
  
!   write(*,*) 'got to here 2',switch,i,j,k
!   write(*,*)uc1
!   write(*,*)uc2
!   write(*,*)uc3
  

  ! Gravity source term
  if(gravity)then
     do h = 2,nvar-1
        source(h,3) = d1*gravity_force(h-1,i,j,k)
     enddo
     scalprod = u1*gravity_force(1,i,j,k) + v1*gravity_force(2,i,j,k) + w1*gravity_force(3,i,j,k)
     source(nvar,3) = d1*scalprod
  endif

  if(debug_l4) write(*,*) i,j,k,d1,u1,v1,w1,p1,d2,u2,v2,w2,p2,d3,u3,v3,w3,p3,d4,u4,v4,w4,p4,source!,source_in_mem

  if(debug_l3) write(*,*) 'end   SOURCES',rank

  return

end subroutine sources
