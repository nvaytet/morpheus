!---------------------------------------------------------------------------------------------------
! File: allocate_arrays.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines allocate_arrays(), deallocate_arrays() and allocate_output()
!!
!! This file contains the routines used to allocate and deallocate the variable arrays.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine ALLOCATE_ARRAYS:
!
!> Allocates grid, conservatives and promitive variable arrays.
!<
subroutine allocate_arrays

  use mod_gridarrays
  use mod_gravity   , only : gravity
  use mod_constants
  use mod_variables
  use mod_arrays
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer :: i,a1,a2,a3

  if(debug_l1) write(*,*) 'begin ALLOCATE_ARRAYS',rank

  adim = 0 ; bdim = 0 ; cdim = 0 ; edim = 0
  do i = 1,ndim
     adim(i) = 1
     bdim(i) = nghost-1
     cdim(i) = nghost
     edim(i) = 2
  enddo
  
  nmin1 = 1-nghost
  nmax1 = nxloc(1)+nghost
  nmin2 = 1
  nmax2 = 1
  nmin3 = 1
  nmax3 = 1
  a1 = 1 ; a2 = 0 ; a3 = 0

  if(ndim > 1)then
     nmin2 = 1-nghost
     nmax2 = nxloc(2)+nghost
     a2 = 1
  endif
  if(ndim > 2)then
     nmin3 = 1-nghost
     nmax3 = nxloc(3)+nghost
     a3 = 1
  endif

  allocate(unew(1:nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))
  allocate(uold(1:nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))
!   allocate(uint(1:nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))
!   allocate(uof1(1:nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))
!   allocate(uof2(1:nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))
  allocate(uint(1:nvar,1:3,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))
!   allocate(uof3(1:nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))
  allocate(usrc(1:nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))


  allocate(gradav(1:nvar,1:3,nmin1+a1:nmax1-a1,nmin2+a2:nmax2-a2,nmin3+a3:nmax3-a3))
!   allocate(gradav1(1:nvar,nmin1+a1:nmax1-a1,nmin2+a2:nmax2-a2,nmin3+a3:nmax3-a3))
!   allocate(gradav2(1:nvar,nmin1+a1:nmax1-a1,nmin2+a2:nmax2-a2,nmin3+a3:nmax3-a3))
!   allocate(gradav3(1:nvar,nmin1+a1:nmax1-a1,nmin2+a2:nmax2-a2,nmin3+a3:nmax3-a3))
!   gradav1 = zero ; gradav2 = zero ; gradav3 = zero
  gradav = zero

!   allocate(active_cell   (nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))
!   allocate(cooling_source(nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))

  allocate( x1(nmin1-a1:nmax1), x2(nmin2-a2:nmax2), x3(nmin3-a3:nmax3))
  allocate(xn1(nmin1   :nmax1),xn2(nmin2   :nmax2),xn3(nmin3   :nmax3))
  allocate(xh1(nmin1   :nmax1),xh2(nmin2   :nmax2),xh3(nmin3   :nmax3))
  allocate(rnt(nmin1   :nmax1),ths(nmin2   :nmax2),  b(nmin2   :nmax2))

  allocate(cell_volume(nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))
  allocate(cell_area(6,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3))

  allocate(bc1(nmin2:nmax2,nmin3:nmax3))
  allocate(bc2(nmin2:nmax2,nmin3:nmax3))
  allocate(bc3(nmin1:nmax1,nmin3:nmax3))
  allocate(bc4(nmin1:nmax1,nmin3:nmax3))
  allocate(bc5(nmin1:nmax1,nmin2:nmax2))
  allocate(bc6(nmin1:nmax1,nmin2:nmax2))

  allocate(inflow1(1:nvar,1:nghost,nmin2:nmax2,nmin3:nmax3))
  allocate(inflow2(1:nvar,1:nghost,nmin2:nmax2,nmin3:nmax3))
  allocate(inflow3(1:nvar,1:nghost,nmin1:nmax1,nmin3:nmax3))
  allocate(inflow4(1:nvar,1:nghost,nmin1:nmax1,nmin3:nmax3))
  allocate(inflow5(1:nvar,1:nghost,nmin1:nmax1,nmin2:nmax2))
  allocate(inflow6(1:nvar,1:nghost,nmin1:nmax1,nmin2:nmax2))

  if(gravity) call allocate_grav_arrays
  
  ! Initialise conservative arrays to avoid divisions by zero
  uold(1,:,:,:) = one
  uold(2,:,:,:) = zero
  uold(3,:,:,:) = zero
  uold(4,:,:,:) = zero
  uold(5,:,:,:) = one
  
  unew            = uold
  uint(:,1,:,:,:) = uold
  uint(:,2,:,:,:) = uold
  uint(:,3,:,:,:) = uold

  if(debug_l1) write(*,*) 'end   ALLOCATE_ARRAYS',rank

  return

end subroutine allocate_arrays

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine DEALLOCATE_ARRAYS:
!
!> Deallocates grid, conservatives and promitive variable arrays.
!<
subroutine deallocate_arrays

  use mod_gridarrays
  use mod_gravity   , only : gravity
  use mod_variables
  use mod_arrays
  use mod_mpi       , only : rank

  implicit none

  if(debug_l1) write(*,*) 'begin DEALLOCATE_ARRAYS',rank

!   deallocate(unew,uold,uint,uof1,uof2,uof3)
!   deallocate(gradav1,gradav2,gradav3)
  deallocate(unew,uold,uint)
  deallocate(gradav,usrc)
!   deallocate(cooling_source)
  deallocate(x1,x2,x3,xn1,xn2,xn3,xh1,xh2,xh3,rnt,ths,b)
  deallocate(bc1,bc2,bc3,bc4,bc5,bc6)
  deallocate(inflow1,inflow2,inflow3,inflow4,inflow5,inflow6)
  deallocate(cell_volume,cell_area)
  deallocate(ivar,terminal_outputs)
  if(gravity) call deallocate_grav_arrays

  if(debug_l1) write(*,*) 'end   DEALLOCATE_ARRAYS',rank

  return

end subroutine deallocate_arrays
