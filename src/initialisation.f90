!---------------------------------------------------------------------------------------------------
! File: initialisation.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines openfiles(), physics(), default_parameters() and check_boundaries()
!!
!! This file contains the routines which open the log files, sets all the physical constants and
!! code units, and initialises the simulation parameters to default values.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine INITIALISATION:
!
!> Opens log file and loads cooling curve.
!<
subroutine initialisation

  use mod_variables, only : master_cpu,debug_l1
  use mod_mpi      , only : nproc,ncpu,rank

  implicit none
  
  if(debug_l1) write(*,*) 'begin INITIALISATION'

#if MPI==1
  call mpi_initialisation
#else
  nproc = 1 ; ncpu = 1 ; rank = 0 ; master_cpu = .true.
  call print_welcome_message
#endif
  
  if(debug_l1) write(*,*) 'end   INITIALISATION',rank
  
  return
  
end subroutine initialisation

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine OPENFILES:
!
!> Opens log file and loads cooling curve.
!<
subroutine openfiles

  use mod_variables     , only : debug_l1
  use mod_thermodynamics, only : cooling
  use mod_mpi           , only : rank

  implicit none

  if(debug_l1) write(*,*) 'begin OPENFILES',rank

  ! load file containing radiative cooling data
  if(cooling) call read_rad_cooling_curve

  if(debug_l1) write(*,*) 'end   OPENFILES',rank

  return

end subroutine openfiles

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine PHYSICS:
!
!> Initialises all the physical constants and units needed by the code.
!<
subroutine physics

  use mod_thermodynamics
  use mod_variables     , only : debug_l1,nrestart,master_cpu
  use mod_constants
  use mod_units
  use mod_mpi

  implicit none

  if(debug_l1) write(*,*) 'begin PHYSICS',rank

  energy   = mass*(length/time)**2
  density  = mass/(length**3)
  pressure = mass/(length*time*time)
  velocity = length/time

  if(master_cpu)then
     write(*,*  )
     write(*,*  )'######## Code units (cgs) ########'
     write(*,209)' Time     =',time,    ' s         '
     write(*,209)' Mass     =',mass,    ' g         '
     write(*,209)' Length   =',length,  ' cm        '
     write(*,209)' Energy   =',energy,  ' erg       '
     write(*,209)' Density  =',density, ' g cm-3    '
     write(*,209)' Pressure =',pressure,' g cm-1 s-2'
     write(*,209)' Velocity =',velocity,' cm s-1    '
     write(*,*  )'##################################'
     write(*,*  )
  endif

  209 format(a11,es10.3,a11)

  ! physical constants
  pi      =  two*asin(one)
  c       =  2.997925e+10_dp * time/length
  c2      =  c*c
  eV      =  1.602200e-12_dp / energy
  grav    =  6.673000e-08_dp * mass*time*time/(length**3)
  hplanck =  6.626068e-27_dp * time/energy
  kb      =  1.380650e-16_dp * degree/energy
  uma     =  1.660538e-24_dp / mass
  a_R     =  7.564640e-15_dp * (degree**4)*(length**3)/energy
  Lsun    =  3.826800e+33_dp * time/energy
  Msun    =  1.989100e+33_dp / mass
  Rsun    =  6.955000e+10_dp / length
  au      =  1.495980e+13_dp / length
  pc      =  3.085678e+18_dp / length
  kpc     =  3.085678e+21_dp / length
  yr      =  3.155760e+07_dp / time
  Myrs    =  3.155760e+13_dp / time
  day     =  8.640000e+04_dp / time
  kms     =  1.000000e+05_dp * time/length
  koverm  =  kb/(mu*uma)     ! kb / average particle mass

  ! Negative pressure cavitation limit
  cavlim = ten**(-(dp+2))

  ! gamma combinations
  g1 = g - one
  g2 = g / g1
  g3 = half*(g+one)/g
  g4 = half*g1/g
  g5 = one / g4
  g6 = one / g
  g7 = two / g1
  g8 = g1 / (g+one)

  ! cooling parameter from units defined above
  cfactor = mass
  cfactor = cfactor*(time**3)
  cfactor = cfactor/(length**5)
  cfactor = cfactor/((mu*(uma*mass))**2)

  ! define negative density and cavitation pressure
  if(nrestart == 0)then
     dcav = (rhomin/mass)*(length**3)      ! cavitation limit in (g/cm3)
     pcav = koverm*(10.0_dp/degree)*dcav   ! low density gas at T = 10K
  endif

  if(debug_l1) write(*,*) 'end   PHYSICS',rank

  return

end subroutine physics

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine DEFAULT_PARAMETERS:
!
!> Sets default values for parameters tu ensure none have been forgotten in the namelists.
!<
subroutine default_parameters

  use mod_thermodynamics
  use mod_gravity
  use mod_variables
  use mod_constants
  use mod_units
  use mod_grid
  use mod_output
  use mod_mpi

  implicit none
  
  if(debug_l1) write(*,*) 'begin DEFAULT_PARAMETERS',rank

  ! Parameters
  nout_term_freq   =   10                  ! print status to terminal every x timesteps
  nrestart         =    0                  ! file number to restart from
  outputs_interval =    1                  ! 1=regular time intervals; 2=every N timesteps
  dt_bin_interval  =  100                  ! every N timesteps
  user_controlled  = .false.               ! pauses at the end of each timestep if .true.
  nout_bin_tot     =    5                  ! total number of binary outputs
  nout_ext_tot     =    1                  ! total number of extras outputs
  debug_level      =    0                  ! write extra information for debugging ? Levels 0 to 3
  debug_l1         = .false.               ! debug level 1
  debug_l2         = .false.               ! debug level 2
  debug_l3         = .false.               ! debug level 3
  debug_l4         = .false.               ! debug level 4
  liste            = 'list.nml'            ! namelist file holding simulation parameters
  libpath          = '../../lib'           ! library path
  the_pill         = 'blue'                ! The blue or the red pill ?

  ! Initilisation
  cn_hydro         =  2.00e-01_dp          ! hydro courant number
  cn_cooling       =  5.00e-02_dp          ! cooling courant number
  slope_type       =  1                    ! Slope limiter (0 = first order scheme)
  solver           = 'hllc'                ! type of hydro solver: 'exact', 'roe' or 'hllc'
  cooling          = .false.               ! include cooling?
  coolcurve        = 'Vaytetetal2007.clc'  ! cooling curve data file
  viscosity        = .false.               ! include viscosity?
  visc             =  0.1_dp
  g                =  1.666666666666667_dp ! gamma (ratio of specific heats)
  geometry         =  1                    ! grid geometry: cartesian=1, spherical=2, cylindrical=3
  boundc_1         =  1                    ! type of lower boundary condition in direction 1
  boundc_2         =  1                    ! type of upper boundary condition in direction 1
  boundc_3         =  1                    ! type of lower boundary condition in direction 2
  boundc_4         =  1                    ! type of upper boundary condition in direction 2
  boundc_5         =  1                    ! type of lower boundary condition in direction 3
  boundc_6         =  1                    ! type of upper boundary condition in direction 3
  same_bc          = .true.                ! one boundary has the same type all along its length
  nghost           =  3                    ! number of ghost cells for boundaries
  it               =  0                    ! starting timestep number
  tlim             =  one                  ! time limit
  rhomin           =  1.00e-20_dp          ! minimum density (in g/cm3)
  mu               =  one                  ! atomic number
  gravity          = .false.               ! include gravity?
  cn_gravity       =  1.00e-02_dp          ! gravity courant number
  gravity_eps      =  1.00e-06_dp          ! self-gravity epsilon for convergence
  gravity_precond  = .false.               ! self-gravity preconditionning
  gravity_type     =  0                    ! gravity_type
  gravity_npmass   =  1                    ! number of point masses for gravity

  ! Code units
  time             =  one                  ! code time        unit
  mass             =  one                  ! code mass        unit
  length           =  one                  ! code length      unit
  degree           =  one                  ! code temperature unit
  
  if(debug_l1) write(*,*) 'end   DEFAULT_PARAMETERS',rank

  return

end subroutine default_parameters

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine CHECK_BOUNDARIES:
!
!> Checks boundaries to see if the 'same_bc' speedup can be used or not.
!<
subroutine check_boundaries

  use mod_variables , only : debug_l1
  use mod_gridarrays
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer :: i,j,k,a

  if(debug_l1) write(*,*) 'begin CHECK_BOUNDARIES',rank

  a = bc1(1,1)
  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        if(bc1(j,k) .ne. a)then
           same_bc(1) = .false.
           exit
        endif
     enddo
  enddo

  a = bc2(1,1)
  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        if(bc2(j,k) .ne. a)then
           same_bc(2) = .false.
           exit
        endif
     enddo
  enddo

  if(ndim > 1)then
     a = bc3(1,1)
     do k = 1,nxloc(3)
        do i = 1,nxloc(1)
           if(bc3(i,k) .ne. a)then
              same_bc(3) = .false.
              exit
           endif
        enddo
     enddo

     a = bc4(1,1)
     do k = 1,nxloc(3)
        do i = 1,nxloc(1)
           if(bc4(i,k) .ne. a)then
              same_bc(4) = .false.
              exit
           endif
        enddo
     enddo
  endif

  if(ndim > 2)then
     a = bc5(1,1)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)
           if(bc5(i,j) .ne. a)then
              same_bc(5) = .false.
              exit
           endif
        enddo
     enddo

     a = bc6(1,1)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)
           if(bc6(i,j) .ne. a)then
              same_bc(6) = .false.
              exit
           endif
        enddo
     enddo
  endif

  if(debug_l1) write(*,*) 'end   CHECK_BOUNDARIES',rank

  return

end subroutine check_boundaries

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine DOMAIN_SETUP:
!
!> Performs simulation domain setup
!<
subroutine domain_setup(cen1,cen2,cen3,xfact1,xfact2,xfact3)

  use mod_variables
  use mod_gridarrays
  use mod_gravity   , only : gravity
  use mod_grid
  use mod_mpi

  implicit none

  real(dp) :: cen1,cen2,cen3,xfact1,xfact2,xfact3

  if(debug_l1) write(*,*) 'begin DOMAIN_SETUP',rank

  call safety_check
  call physics
  
  if(nrestart == 0)then
  
     ! two sources: geometry and radiative cooling
     nsource = 2
     if(gravity)then
        nsource = 3
     endif

#if MPI==1
     ! Perform domain decomposition
     call domain_decomposition(nproc,nx,ndim,ncpu,nxloc,rank)
#else
     nxloc = nx
#endif

     call allocate_arrays
     call grid_setup(cen1,cen2,cen3,xfact1,xfact2,xfact3)

  else
  
     call display_grid
     call cal_cell_volume
     call cal_cell_area
     
  endif
  
  if(debug_l1) write(*,*) 'end   DOMAIN_SETUP',rank
  
  return
  
end subroutine domain_setup

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine COMMON_SETUP1:
!
!> Performs simulation setup steps common to all problems
!<
subroutine common_setup1

  use mod_thermodynamics, only : cooling
  use mod_constants
  use mod_variables
  use mod_gridarrays
  use mod_gravity
  use mod_arrays
  use mod_grid
  use mod_mpi           , only : rank

  implicit none

  character(len=str) :: string
  integer :: i

  if(debug_l1) write(*,*) 'begin COMMON_SETUP1',rank
  
!   if(slope_type > 0)then
!      o = two
!   else
!      o = one
!   endif

  call extras_init
  call openfiles
  if(gravity_type == 4) call init_self_gravity

  ! Select hydro solver
  select case(solver)
  case('exact')
     isolver = isolver_exact
  case('roe')
     isolver = isolver_roe
  case('hllc')
     isolver = isolver_hllc
  case default
     write(string,*) 'Unknown hydro solver:',solver
     call close_program(trim(string))
  end select
  
  ! Ivar matrix for flux indices
  allocate(ivar(1:nvar,1:nvar))
  ivar(1,1) = 1 ; ivar(1,2) = 1 ; ivar(1,3) = 1
  ivar(2,1) = 2 ; ivar(2,2) = 3 ; ivar(2,3) = 4
  ivar(3,1) = 3 ; ivar(3,2) = 4 ; ivar(3,3) = 2
  ivar(4,1) = 4 ; ivar(4,2) = 2 ; ivar(4,3) = 3
  ivar(5,1) = 5 ; ivar(5,2) = 5 ; ivar(5,3) = 5
  
  identity = 0
  do i = 1,3
     identity(i,i) = 1
  enddo
  
  ! Initialise cyclic arrays
  do i = 1,3
     iodd (i) = 1+(i-1)*2
     ieven(i) = i*2
  enddo
  
  ! Allocate array for terminal outputs
  n_term_output = 3
  if(cooling) n_term_output = n_term_output + 1
  if(gravity)then
     n_term_output = n_term_output + 1
     if(gravity_type == 4) n_term_output = n_term_output + 1
  endif
  allocate(terminal_outputs(n_term_output))
  
!   ! Initialise cumulative radiated energy
!   if(nrestart == 0) cumul_erad = zero
  
!   ! Define active cells
!   active_cell = .true.
!   active_cell(1-nghost:          0,:,:) = .false.
!   active_cell(nxloc(1)+1  :nxloc(1)+nghost,:,:) = .false.
!   if(ndim > 1)then
!      active_cell(:,1-nghost:          0,:) = .false.
!      active_cell(:,nxloc(2)+1  :nxloc(2)+nghost,:) = .false.
!   endif
!   if(ndim > 2)then
!      active_cell(:,:,1-nghost:          0) = .false.
!      active_cell(:,:,nxloc(3)+1  :nxloc(3)+nghost) = .false.
!   endif

    
  if(debug_l1) write(*,*) 'end   COMMON_SETUP1',rank

  return

end subroutine common_setup1

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine COMMON_SETUP2:
!
!> Performs simulation setup steps common to all problems
!<
subroutine common_setup2

  use mod_constants
  use mod_variables
  use mod_arrays
  use mod_output
  use mod_mpi      , only : rank

  implicit none

  if(debug_l1) write(*,*) 'begin COMMON_SETUP2',rank
  
  ! copy unew values to uold
!   uold = unew
!   uint = unew
  
  call check_boundaries

  ! file dumping variables
  k_bin_output = 0
  k_ext_output = 0

  t_bin_output = zero
  t_ext_output = zero

  t_bin_step = (tlim-t_bin_output)/real(nout_bin_tot,dp)
  t_ext_step = (tlim-t_ext_output)/real(nout_ext_tot,dp)
  
  if(debug_l1) write(*,*) 'end   COMMON_SETUP2',rank

  return

end subroutine common_setup2

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine SET_DEBUG:
!
!> Sets debugging level.
!<
subroutine set_debug

  use mod_variables , only : debug_level,debug_l1,debug_l2,debug_l3,debug_l4,the_pill,master_cpu

  implicit none
  
  if(the_pill == 'red')then
     if(master_cpu) call white_rabbit
     debug_level = 4
  endif

  if(debug_level > 0) debug_l1 = .true.
  if(debug_level > 1) debug_l2 = .true.
  if(debug_level > 2) debug_l3 = .true.
  if(debug_level > 3) debug_l4 = .true.

  return

end subroutine set_debug

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine WHITE_RABBIT:
!
!> Follow the white rabbit.
!<
subroutine white_rabbit

  implicit none

  write(*,*) '                                          .--,_                     '
  write(*,*) '                                         / ,/ /                     '
  write(*,*) '                                        / // /                      '
  write(*,*) '                                       / // /                       '
  write(*,*) '                                     .''  '' (                      '
  write(*,*) '                                    /       \.-"""-._               '
  write(*,*) '                                   /     '' .    ''    `-.          '
  write(*,*) 'Follow the white rabbit...        (       .  ''      "   `.         '
  write(*,*) '                                   `-.-''       "       ''  ;       '
  write(*,*) '                                       `.''  "  .  .-''    " ;      '
  write(*,*) '                                        : .     .''          ;      '
  write(*,*) '                                        `.   '' :     ''   ''  ;    '
  write(*,*) '                                          )  _.". "     .  ";       '
  write(*,*) '                                        .''_.''   .''   ''  __.,`.  '
  write(*,*) '                                       ''"      ""''''---''`    "'' '
  
  return

end subroutine white_rabbit
