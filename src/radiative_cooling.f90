!---------------------------------------------------------------------------------------------------
! File: radiative_cooling.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutine read_rad_cooling_curve() and functions cooling_rate1() and cooling_rate2()
!!
!! This file contains the routines which calculates the cooling rate of the gas according to
!! its temperature.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine READ_RAD_COOLING_CURVE:
!
!> Reads in the data from the radiative cooling curve.
!<
subroutine read_rad_cooling_curve

  use mod_thermodynamics, only : logt,loge,ncool,coolcurve,tclow,tchigh,deltatc
  use mod_variables     , only : libpath,debug_l1,master_cpu
  use mod_mpi           , only : rank

  implicit none
  
  integer :: i

  if(debug_l1) write(*,*) 'begin READ_RAD_COOLING_CURVE',rank
  
  if(master_cpu) write(*,*) 'Loading cooling curve: ',coolcurve
  
  open(12,file=trim(libpath)//'/radiative_cooling/'//coolcurve,status='old')
  read(12,*) ncool
  allocate(logt(ncool),loge(ncool))
  do i = 1,ncool
     read(12,*) logt(i),loge(i)
  enddo
  close(12)
  tclow  = logt(1)
  tchigh = logt(ncool)
  
  ! Assume constant deltaT in temperature
  deltatc = logt(2) - logt(1)
  
  if(debug_l1) write(*,*) 'end   READ_RAD_COOLING_CURVE',rank
  
  return

end subroutine read_rad_cooling_curve

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Function COOLING_RATE1:
!
!> Computes the power index in the cooling curve according to the gas temperature T.
!! The routine first locates the two data points in the cooling curve between which T lies.
!! It then linearly interpolates between the two points to find the correct index.
!<
function cooling_rate1(x)

  use mod_variables     , only : debug_l3,debug_l4
  use mod_thermodynamics, only : logt,loge,ncool,tclow,tchigh,deltatc
  use mod_constants
  use mod_mpi           , only : rank

  implicit none

  real(dp) :: x1,x2,y1,y2,x,m,cooling_rate1
  integer  :: itemp

  if(debug_l3) write(*,*) 'begin COOLING_RATE1',rank

  if(x > tchigh)then
  
     ! Extrapolate with Bremmstrahlung power
     m = half
     x1 = logt(ncool)
     y1 = loge(ncool)
     cooling_rate1 = (m*(x-x1))+y1

  elseif(x < tclow)then

     cooling_rate1 = loge(1)

  else

     itemp = int(half + (x - tclow)/deltatc) + 1
     ! first order linear interpolation
     x1 = logt(itemp  )
     x2 = logt(itemp+1)
     y1 = loge(itemp  )
     y2 = loge(itemp+1)
     ! compute gradient
     m = (y2-y1)/(x2-x1)
     cooling_rate1 = (m*(x-x1))+y1
     
  endif

  if(debug_l4) write(*,*) x,cooling_rate1

  if(debug_l3) write(*,*) 'end   COOLING_RATE1',rank

end function cooling_rate1

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Function COOLING_RATE2:
!
!> Computes the power index in the cooling curve according to the gas temperature T.
!! The routine first locates the two data points in the cooling curve between which T lies.
!! It then linearly interpolates between the two points to find the correct index.
!<
function cooling_rate2(x)

  use mod_variables     , only : debug_l3,debug_l4
  use mod_thermodynamics, only : logt,loge,ncool
  use mod_constants
  use mod_mpi           , only : rank

  implicit none

  real(dp) :: x1,x2,y1,y2,x,m,cooling_rate2
  integer  :: i
  logical  :: q

  if(debug_l3) write(*,*) 'begin COOLING_RATE2',rank
  
  q = .true. ; m = zero
  cooling_rate2 = zero

  i = 1

  ! search through file and locate temp
  do while(q)
     if(x < logt(i)) then
        q = .false.
     endif
     if(x == logt(i)) then
        cooling_rate2 = loge(i)
        q = .false.
     else
        if(x > logt(i)) then
           if(x < logt(i+1)) then
              ! first order linear interpolation
              x1 = logt(i)
              x2 = logt(i+1)
              y1 = loge(i)
              y2 = loge(i+1)
              ! compute gradient
              m = (y2-y1)/(x2-x1)
              cooling_rate2 = (m*(x-x1))+y1
              q = .false.
           else
              i = i + 1
           endif
        endif
     endif
     ! ensure we are not trying to access elements outside of the array
     if(i > ncool) then
        q = .false.
     endif
  enddo
  
  if(debug_l4) write(*,*) x,cooling_rate2
  
  if(debug_l3) write(*,*) 'end   COOLING_RATE2',rank

end function cooling_rate2
