!---------------------------------------------------------------------------------------------------
! File: terminal_output.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines printing(), progressbar() and print_configuration().
!!
!! This file contains all the routines necessary to writing terminal outputs.
!! It contains the routines which write the code status to the terminal window.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine PRINT_WELCOME_MESSAGE:
!
!> Prints welcome banner.
!<
subroutine print_welcome_message

  implicit none

  write(*,*)'                                                                                 '
  write(*,*)'   _/    _/    _/_/    _/_/_/    _/_/_/    _/    _/  _/_/_/_/  _/    _/    _/_/_/'
  write(*,*)'   _/_/_/_/  _/    _/  _/    _/  _/    _/  _/    _/  _/        _/    _/  _/      '
  write(*,*)'   _/ _/ _/  _/    _/  _/_/_/    _/_/_/    _/_/_/_/  _/_/_/    _/    _/    _/_/  '
  write(*,*)'   _/    _/  _/    _/  _/    _/  _/        _/    _/  _/        _/    _/        _/'
  write(*,*)'   _/    _/    _/_/    _/    _/  _/        _/    _/  _/_/_/_/    _/_/    _/_/_/  '
  write(*,*)'                                                                                 '
  write(*,*)'    Manchester Omni-geometRical Program for Hydrodynamical EUlerian Simulations  '
  write(*,*)'                        Copyright (C) N. Vaytet & T. O''Brien                    '
  write(*,*)'                            The University of Manchester                         '
  write(*,*)'                                 Version 1.5 - 2013                              '
  write(*,*)'                                                                                 '
  
  return
  
end subroutine print_welcome_message

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine TERMINAL_OUTPUT:
!
!> Prints status to terminal: time, timestep dt. Also prints progress and fractions of time
!! spent by different subroutines.
!<
subroutine terminal_output

  use mod_variables
  use mod_output
  use mod_mpi      , only : rank

  implicit none

  real(dp)           :: previous
  character (len=20) :: string
  integer            :: i

  if(debug_l1) write(*,*) 'begin TERMINAL_OUTPUT',rank

  write(terminal_outputs(1),'(a,i8,a)')'**** Timestep: ',it,' ********************* MORPHEUS ***'
  
  do i = 1,n_term_output
     write(*,'(a)') trim(terminal_outputs(i))
  enddo

  write(*,*)

  previous = t_bin_output - t_bin_step
  write(string,'(a,i4,a)') ' Until output ',k_bin_output,':'
  call progressbar(string,(t-previous),(t_bin_output-previous))
  call progressbar(' Total progress   : ',t,tlim)

  write(*,'(a)') '**********************************************************'
  write(*,*)

  if(debug_l1) write(*,*) 'end   TERMINAL_OUTPUT',rank

  return

end subroutine terminal_output

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine PROGRESSBAR:
!
!> Creates a progress bar.
!<
subroutine progressbar(s1,t1,t2)

  use mod_precision
  use mod_variables, only : debug_l2
  use mod_mpi      , only : rank

  implicit none

  character (len=20), intent(in) :: s1
  real(dp)          , intent(in) :: t1,t2
  
  integer, parameter :: n = 34
  character (len=4)  :: s2
  character (len=n)  :: s3
  integer            :: i,nbar,iperc
  real(dp)           :: perc

  if(debug_l2) write(*,*) 'begin PROGRESSBAR',rank

   perc = t1/t2
  iperc = int(100.0_dp*perc)
  
  write(s2,'(i3,a1)') iperc,'%'

  s3(1:n) = '[                                ]'

  nbar = min(int(perc*real(n-1,dp)),n-2)

  if(nbar > 0)then
     do i = 1,nbar
        s3(1+i:1+i) = '='
     enddo
  endif

  write(*,'(a,a,a)') s1,s2,s3

  if(debug_l2) write(*,*) 'end   PROGRESSBAR',rank

  return

end subroutine progressbar

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine PRINT_CONFIGURATION:
!
!> Prints simulation configuration at the start of the run.
!<
subroutine print_configuration

  use mod_thermodynamics, only : cooling,viscosity
  use mod_gravity       , only : gravity,gravity_type
  use mod_variables
  use mod_constants     , only : g
  use mod_grid
  use mod_mpi

  implicit none

  if(debug_l2) write(*,*) 'begin PRINT_CONFIGURATION',rank

     write(*,*)
     write(*,'(a)'     ) ' ###########################################################'
     write(*,'(a)'     ) ' CONFIGURATION:'
     write(*,*)

     write(*,'(a)'     ) ' Reading namelist from:      '//trim(liste)

#if MPI==1
     write(*,'(a,i6)'  ) ' MPI:                        ON    nproc = ',nproc
#else
     write(*,'(a,i6)'  ) ' MPI:                        OFF'
#endif
  if(cooling)then
     write(*,'(a)'     ) ' Cooling:                    ON'
  else
     write(*,'(a)'     ) ' Cooling:                    OFF'
  endif

  if(gravity)then
     select case(gravity_type)
     case(1)
        write(*,'(a)'     ) ' Gravity: constant           ON'
     case(2)
        write(*,'(a)'     ) ' Gravity: Point mass origin  ON'
     case(3)
        write(*,'(a)'     ) ' Gravity: Point mass arbitr. ON'
     case(4)
        write(*,'(a)'     ) ' Gravity: Self-gravity       ON'
     end select
  else
     write(*,'(a)'     ) ' Gravity:                    OFF'
  endif

  if(viscosity)then
     write(*,'(a)'     ) ' Artificial viscosity:       ON'
  else
     write(*,'(a)'     ) ' Artificial viscosity:       OFF'
  endif

  if(nrestart > 0)then
     write(*,'(a)'     ) ' Restart:                    ON'
  else
     write(*,'(a)'     ) ' Restart:                    OFF'
  endif

  if(debug_level == 0)then
     write(*,'(a)'     ) ' Debug:                      OFF'
  else
     write(*,'(a,i1)'  ) ' Debug level:                ',debug_level
  endif

     write(*,'(a,i1)'  ) ' Number of dimensions:       ',ndim
     write(*,'(a)'     ) ' Grid geometry:              '//geom1//geom2//geom3
     write(*,'(a,f6.3)') ' Specific heats ratio:       ',g
     write(*,'(a,f6.3)') ' Courant number:             ',cn_hydro
  if(slope_type == 0)then
     write(*,'(a)'     ) ' Order of scheme:            1'
  else
     write(*,'(a)'     ) ' Order of scheme:            2'
     write(*,'(a,i1)'  ) ' Slope limiter:              ',slope_type
  endif
     write(*,'(a)'     ) ' Hydro solver:               '//solver
     write(*,'(a,i1)'  ) ' Number of ghost cells:      ',nghost

     write(*,'(a)'     ) ' ###########################################################'
     write(*,*)

  if(debug_l2) write(*,*) 'end   PRINT_CONFIGURATION',rank

  return

end subroutine print_configuration

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine DISPLAY_GRID:
!
!> Displays at the start of the run the domain decomposition amongst cpus in the case of MPI
!! simulation and the grid dimensions and geometry.
!<
subroutine display_grid

  use mod_gridarrays
  use mod_variables , only : str,debug_l2,master_cpu
  use mod_grid
  use mod_mpi

  implicit none

  character (len=5  ) :: st1,st2,st3
  character (len=str) :: string
  integer             :: i

  if(debug_l2) write(*,*) 'begin DISPLAY_GRID',rank

#if MPI==1

  if(master_cpu) write(*,'(a)') ' ################## Domain  decomposition ##################'

  do i = 0,nproc-1
     if(i == rank)then
        write(*,'(a12,i5)')  ' Rank     = ',rank
        write(*,213) '      nx1 = ',     nx(1),' -      nx2 = ',     nx(2),' -      nx3 = ',     nx(3)
        write(*,213) '   nxloc1 = ',  nxloc(1),' -   nxloc2 = ',  nxloc(2),' -   nxloc3 = ',  nxloc(3)
        write(*,213) '    ncpu1 = ',   ncpu(1),' -    ncpu2 = ',   ncpu(2),' -    ncpu3 = ',   ncpu(3)
        write(*,213) ' nxlower1 = ',nxlower(1),' - nxlower2 = ',nxlower(2),' - nxlower3 = ',nxlower(3)
        write(*,213) ' nxupper1 = ',nxupper(1),' - nxupper2 = ',nxupper(2),' - nxupper3 = ',nxupper(3)
        write(*,214) ' face   neighbours : ',face_neighbours
        write(*,214) ' edge   neighbours : ',edge_neighbours
        write(*,214) ' corner neighbours : ',corner_neighbours
        write(*,'(a)') ' ###########################################################'
     endif
     call mpi_barrier(world,ierror)
  enddo

  write(*,*)

  213 format(a,i5,a,i5,a,i5)
  214 format(a,12(i5))

#endif

  do i = 0,nproc-1
     if(i == rank)then

        write(st1   ,'(i5)') nxloc(1)
        write(st2   ,'(i5)') nxloc(2)
        write(st3   ,'(i5)') nxloc(3)
        write(string,'(a)' ) trim(adjustl(st1))//' x '//trim(adjustl(st2))//' x '//trim(adjustl(st3))

               write(*,217) ' ################## Local grid check ',rank,' ##################'
               write(*,216) ' Geometry: '//geom1//geom2//geom3//' | Number of dimensions: ',ndim,' | '//string
               write(*,215) ' lbox(1) = ',lbox(1)/real(ncpu(1),dp),' ,  x1(0) = ',x1(0),' ,  x1(nx) = ',x1(nxloc(1))
  if(ndim > 1) write(*,215) ' lbox(2) = ',lbox(2)/real(ncpu(2),dp),' ,  x2(0) = ',x2(0),' ,  x2(nx) = ',x2(nxloc(2))
  if(ndim > 2) write(*,215) ' lbox(3) = ',lbox(3)/real(ncpu(3),dp),' ,  x3(0) = ',x3(0),' ,  x3(nx) = ',x3(nxloc(3))
               write(*,'(a)') ' ###########################################################'
     endif
#if MPI==1
     call mpi_barrier(world,ierror)
#endif
  enddo

  write(*,*)

  215 format(a,es8.1,a,es8.1,a13,es8.1)
  216 format(a,i1,a)
  217 format(a,i5,a)

  if(debug_l2) write(*,*) 'end   DISPLAY_GRID',rank

  return

end subroutine display_grid
