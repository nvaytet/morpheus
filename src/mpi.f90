!---------------------------------------------------------------------------------------------------
! File: mpi.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines domain_decomposition(), primefactors(), mpi_comm_face1(), mpi_comm_face2(),
!! mpi_comm_face3(), mpi_comm_face4(), mpi_comm_face5(), mpi_comm_face6(), mpi_comm_corners_2D(),
!! mpi_comm_edges_3D(), mpi_comm_corners_3D(), mpi_initialisation()
!!
!! This file contains all the routines for splitting the computational volume evenly between
!! processors and for communication between processors.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine MPI_INITIALISATION:
!
!> Initialises MPI, variables types and reduction operations.
!<
subroutine mpi_initialisation

  use mod_precision
  use mod_variables, only : master_cpu
  use mod_mpi
  
  implicit none
  
  interface 
    function min_dp(in,inout,n,data_type)
    use mod_precision
    implicit none
      integer                :: n,i,data_type,min_dp
      real(dp), dimension(n) :: in,inout
    end function min_dp
    function max_dp(in,inout,n,data_type)
    use mod_precision
    implicit none
      integer                :: n,i,data_type,max_dp
      real(dp), dimension(n) :: in,inout
    end function max_dp
    function sum_dp(in,inout,n,data_type)
    use mod_precision
    implicit none
      integer                :: n,i,data_type,sum_dp
      real(dp), dimension(n) :: in,inout
    end function sum_dp
  end interface
  
  integer :: i

  call mpi_init(ierror)
  world = mpi_comm_world
  call mpi_comm_size(world,nproc,ierror)
  call mpi_comm_rank(world,rank ,ierror)
  call mpi_barrier(world,ierror)
  
  ! Create MPI variable type according to variable precision
  select case(dp)
  case(4)
     call mpi_type_contiguous(1,mpi_real4,mpi_real_dp,ierror)
     call mpi_type_commit(mpi_real_dp,ierror)
  case(8)
     call mpi_type_contiguous(1,mpi_real8,mpi_real_dp,ierror)
     call mpi_type_commit(mpi_real_dp,ierror)
  case(16)
     call mpi_type_contiguous(1,mpi_real16,mpi_real_dp,ierror) 
     call mpi_type_commit(mpi_real_dp,ierror)
  case default
     call close_program('Wrong real number precision')
  end select
  
  ! Create max and min operations for custom MPI data type
  call mpi_op_create(min_dp,.true.,mpi_min_dp,ierror)
  call mpi_op_create(max_dp,.true.,mpi_max_dp,ierror)
  call mpi_op_create(sum_dp,.true.,mpi_sum_dp,ierror)
  
  if(rank == 0)then
     master_cpu = .true.
  else
     master_cpu = .false.
  endif
  
  call mpi_barrier(world,ierror)
  
  if(master_cpu) call print_welcome_message

  do i = 0,nproc-1
     if(i == rank)then
        if(rank == 0)then
           write(*,'(a)') ' ###########################################################'
           write(*,'(a)') ' Initilisation of MPI :'
           write(*,*)
        endif
        write(*,*) 'World: i am rank',rank,' of size',nproc
     endif
     call mpi_barrier(world,ierror)
  enddo
  write(*,'(a)') ' ###########################################################'

  return

end subroutine mpi_initialisation

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Function MIN_DP:
!
!> Create custom minimum reduction function for MPI reduction with arbitrary precision numbers.
!<
function min_dp(in,inout,n,data_type)

  use mod_precision
  
  implicit none
   
  integer               , intent(in   ) :: n,data_type
  real(dp), dimension(n), intent(in   ) :: in
  real(dp), dimension(n), intent(inout) :: inout
  integer                               :: i,min_dp

  do i = 1,n
     inout(i) = min(inout(i),in(i))
  enddo

end function min_dp

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Function MAX_DP:
!
!> Create custom maximum reduction function for MPI reduction with arbitrary precision numbers.
!<
function max_dp(in,inout,n,data_type)

  use mod_precision
  
  implicit none
   
  integer               , intent(in   ) :: n,data_type
  real(dp), dimension(n), intent(in   ) :: in
  real(dp), dimension(n), intent(inout) :: inout
  integer                               :: i,max_dp

  do i = 1,n
     inout(i) = max(inout(i),in(i))
  enddo

end function max_dp

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Function SUM_DP:
!
!> Create custom sum reduction function for MPI reduction with arbitrary precision numbers.
!<
function sum_dp(in,inout,n,data_type)

  use mod_precision
  
  implicit none
   
  integer               , intent(in   ) :: n,data_type
  real(dp), dimension(n), intent(in   ) :: in
  real(dp), dimension(n), intent(inout) :: inout
  integer                               :: i,sum_dp

  do i = 1,n
     inout(i) = inout(i) + in(i)
  enddo

end function sum_dp

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine DOMAIN_DECOMPOSITION:
!
!> Distributes the computational volume among processors.
!! The routine explores all the possibilities and divides the volume in a way
!! which minimises the local grid face areas and maximises the local grid volume.
!! This aims to minimise communication loads between processors. The routine returns a
!! number of cpus in directions 1, 2 and 3. It also sets the values for the local grid
!! dimensions nxloc.
!<
subroutine domain_decomposition(nproc,nx,ndim,ncpu,nxloc,rank)

  use mod_precision
  use mod_variables, only : debug_l1
  use mod_constants, only : large_number

  implicit none

  integer, dimension(3)                :: ncpu,nx,nxloc
  integer, dimension(nproc/2)          :: factors
  integer, dimension(:,:), allocatable :: mat
  integer, dimension(:  ), allocatable :: c
  integer                              :: nproc,nf,ndim,nextra,ndiv,i,j,k,npos,jmem
  integer                              :: num1,num2,num3,rank
  real(dp)                             :: rmin,rlog,r12,r13,r23,rlog12,rlog13,rlog23

  if(debug_l1) write(*,*) 'begin DOMAIN_DECOMPOSITION',rank

  ncpu  = 1
  nxloc = 1

  if(ndim == 1)then
     ncpu(1) = nproc
     ndiv = nx(1)/nproc
     nextra = mod(nx(1),nproc)
     nxloc(1) = ndiv
     if(rank < nextra) nxloc(1) = nxloc(1) + 1
  endif


  if(ndim > 1)then

     call primefactors(nproc,factors,nf)
     ! calculate the number of possibilities
     npos = ndim**nf

     allocate(mat(nf,npos),c(nf))
     c = 1

     do j = 1,npos
        do i = 1,nf
           mat(i,j) = c(i)
        enddo
        c(nf) = c(nf) + 1
        if(j < npos)then
           do i = nf,1,-1
              if(c(i) > ndim)then
                 c(i  ) = 1
                 c(i-1) = c(i-1) + 1
              endif
           enddo
        endif
     enddo

  endif

 
  if(ndim == 2)then

     ! now compute ratios
     rmin = large_number
     do j = 1,npos
        num1 = 1 ; num2 = 1
        do i = 1,nf
           select case(mat(i,j))
           case(1)
              num1 = num1*factors(i)
           case(2)
              num2 = num2*factors(i)
           end select
        enddo
        r12 = (real(nx(1),dp) / real(nx(2),dp)) * (real(num2,dp) / real(num1,dp))
        rlog = abs(log10(r12))
        if(rlog < rmin)then
           rmin = rlog
           jmem = j
        endif
     enddo

     num1 = 1 ; num2 = 1
     do i = 1,nf
        select case(mat(i,jmem))
        case(1)
           num1 = num1*factors(i)
        case(2)
           num2 = num2*factors(i)
        end select
     enddo

     ncpu(1) = num1
     ncpu(2) = num2

     ndiv = nx(1)/ncpu(1)
     nextra = mod(nx(1),ncpu(1))
     nxloc(1) = ndiv
     k = mod(rank,ncpu(1))
     do i = 1,nextra
        if(k.eq.(i-1)) nxloc(1) = nxloc(1) + 1
     enddo

     ndiv = nx(2)/ncpu(2)
     nextra = mod(nx(2),ncpu(2))
     nxloc(2) = ndiv
     k = mod(rank,ncpu(1)*ncpu(2))
     do i = 1,nextra
        if(k.ge.((i-1)*ncpu(1)) .and. (k.lt.(i*ncpu(1)))) nxloc(2) = nxloc(2) + 1
     enddo

     deallocate(mat,c)

  endif


  if(ndim == 3)then

     ! now compute ratios
     rmin = large_number
     do j = 1,npos
        num1 = 1 ; num2 = 1 ; num3 = 1
        do i = 1,nf
           select case(mat(i,j))
           case(1)
              num1 = num1*factors(i)
           case(2)
              num2 = num2*factors(i)
           case(3)
              num3 = num3*factors(i)
           end select
        enddo
        r12 = (real(nx(1),dp) / real(nx(2),dp)) * (real(num2,dp) / real(num1,dp))
        r13 = (real(nx(1),dp) / real(nx(3),dp)) * (real(num3,dp) / real(num1,dp))
        r23 = (real(nx(2),dp) / real(nx(3),dp)) * (real(num3,dp) / real(num2,dp))
        rlog12 = abs(log10(r12))
        rlog13 = abs(log10(r13))
        rlog23 = abs(log10(r23))
        rlog = rlog12 + rlog13 + rlog23
        if(rlog < rmin)then
           rmin = rlog
           jmem = j
        endif
     enddo

     num1 = 1 ; num2 = 1 ; num3 = 1
     do i = 1,nf
        select case(mat(i,jmem))
        case(1)
           num1 = num1*factors(i)
        case(2)
           num2 = num2*factors(i)
        case(3)
           num3 = num3*factors(i)
        end select
     enddo

     ncpu(1) = num1
     ncpu(2) = num2
     ncpu(3) = num3

     ndiv = nx(1)/ncpu(1)
     nextra = mod(nx(1),ncpu(1))
     nxloc(1) = ndiv
     k = mod(rank,ncpu(1))
     do i = 1,nextra
        if(k.eq.(i-1)) nxloc(1) = nxloc(1) + 1
     enddo

     ndiv = nx(2)/ncpu(2)
     nextra = mod(nx(2),ncpu(2))
     nxloc(2) = ndiv
     k = mod(rank,ncpu(1)*ncpu(2))
     do i = 1,nextra
        if((k.ge.((i-1)*ncpu(1))) .and. (k.lt.(i*ncpu(1)))) nxloc(2) = nxloc(2) + 1
     enddo

     ndiv = nx(3)/ncpu(3)
     nextra = mod(nx(3),ncpu(3))
     nxloc(3) = ndiv
     k = int(rank/(ncpu(1)*ncpu(2)))
     do i = 1,nextra
        if(k.eq.(i-1)) nxloc(3) = nxloc(3) + 1
     enddo

     deallocate(mat,c)

  endif

  if(debug_l1) write(*,*) 'end   DOMAIN_DECOMPOSITION',rank

  return

end subroutine domain_decomposition

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine PRIMEFACTORS:
!
!> Decomposes the total number of cpus 'nproc' into prime factors.
!<
subroutine primefactors(num,factors,nf)

  use mod_variables, only : debug_l2
  use mod_mpi      , only : rank

  implicit none

  integer, intent(in)                      :: num
  integer, intent(out), dimension((num/2)) :: factors
  integer, intent(out)                     :: nf
  integer                                  :: i,n

  if(debug_l2) write(*,*) 'begin PRIMEFACTORS',rank

  i  = 2
  nf = 1
  n  = num
  do
     if(mod(n,i) == 0)then
        factors(nf) = i
        nf = nf + 1
        n = n/i
     else
        i = i + 1
     endif
     if(n == 1)then
        nf = nf - 1
        exit
     endif
  enddo

  if(debug_l2) write(*,*) 'end   PRIMEFACTORS',rank

  return

end subroutine primefactors

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine GRID_DECOMPOSITION:
!
!> Distributes the grid among cpus.
!<
subroutine grid_decomposition

  use mod_variables, only : debug_l1
  use mod_mpi
  use mod_grid

  implicit none
  
  integer , dimension(:,:), allocatable :: face_neighbours_glob
  integer                               :: k,nextra

  if(debug_l1) write(*,*) 'begin GRID_DECOMPOSITION',rank
  
  ! Distribute grid among cpus
  
  nextra     = mod(nx(1),ncpu(1))
  nxlower(1) = mod( int(nx(1)/ncpu(1))*rank+1 , (nx(1)-nextra) )
  if(nextra .ne. 0)then
     k = mod(rank,ncpu(1))
     if(k .le. nextra)then
        nxlower(1) = nxlower(1) + k
     else
        nxlower(1) = nxlower(1) + nextra
     endif
  endif
  nxupper(1) = nxlower(1) + nxloc(1) - 1

  if(ndim > 1)then
     nextra     = mod(nx(2),ncpu(2))
     nxlower(2) = mod( int(nx(2)/ncpu(2))*int(rank/ncpu(1))+1 , (nx(2)-nextra) )
     if(nextra .ne. 0)then
        k = int(mod(rank,ncpu(1)*ncpu(2)) / ncpu(1))
        if(k .le. nextra)then
           nxlower(2) = nxlower(2) + k
        else
           nxlower(2) = nxlower(2) + nextra
        endif
     endif
     nxupper(2) = nxlower(2) + nxloc(2) - 1
  endif

  if(ndim > 2)then
     nextra     = mod(nx(3),ncpu(3))
     nxlower(3) = mod( int(nx(3)/ncpu(3))*int(rank/(ncpu(1)*ncpu(2)))+1 , (nx(3)-nextra) )
     if(nextra .ne. 0)then
        k = int(rank/(ncpu(1)*ncpu(2)))
        if(k .le. nextra)then
           nxlower(3) = nxlower(3) + k
        else
           nxlower(3) = nxlower(3) + nextra
        endif
     endif
     nxupper(3) = nxlower(3) + nxloc(3) - 1
  endif
  
  
  ! Now find cpu neighbours
  allocate(face_neighbours_glob(6,0:nproc-1))

    face_neighbours = -1
    edge_neighbours = -1
  corner_neighbours = -1

  if(nxlower(1) .ne.    1 )then
     face_neighbours(1) = rank - 1
  else
     face_neighbours(1) = rank - 1 + ncpu(1)
  endif
  if(nxupper(1) .ne. nx(1))then
     face_neighbours(2) = rank + 1
  else
     face_neighbours(2) = rank + 1 - ncpu(1)
  endif

  if(ndim > 1)then
     if(nxlower(2) .ne.    1 )then
        face_neighbours(3) = rank - ncpu(1)
     else
        face_neighbours(3) = rank - ncpu(1)*(1 - ncpu(2))
     endif
     if(nxupper(2) .ne. nx(2))then
        face_neighbours(4) = rank + ncpu(1)
     else
        face_neighbours(4) = rank + ncpu(1)*(1 - ncpu(2))
     endif
  endif

  if(ndim > 2)then
     if(nxlower(3) .ne.    1 )then
        face_neighbours(5) = rank - ncpu(1)*ncpu(2)
     else
        face_neighbours(5) = rank - ncpu(1)*ncpu(2)*(1 - ncpu(3))
     endif
     if(nxupper(3) .ne. nx(3))then
        face_neighbours(6) = rank + ncpu(1)*ncpu(2)
     else
        face_neighbours(6) = rank + ncpu(1)*ncpu(2)*(1 - ncpu(3))
     endif
  endif

  call mpi_allgather(face_neighbours,6,mpi_integer,face_neighbours_glob,6,mpi_integer,world,ierror)

  if(ndim==2)then
     corner_neighbours(1) = face_neighbours_glob(1,face_neighbours(3))
     corner_neighbours(2) = face_neighbours_glob(1,face_neighbours(4))
     corner_neighbours(3) = face_neighbours_glob(2,face_neighbours(3))
     corner_neighbours(4) = face_neighbours_glob(2,face_neighbours(4))
  endif

  if(ndim==3)then
      edge_neighbours( 1) = face_neighbours_glob(1,face_neighbours(3))
      edge_neighbours( 2) = face_neighbours_glob(1,face_neighbours(4))
      edge_neighbours( 3) = face_neighbours_glob(2,face_neighbours(3))
      edge_neighbours( 4) = face_neighbours_glob(2,face_neighbours(4))
      edge_neighbours( 5) = face_neighbours_glob(3,face_neighbours(5))
      edge_neighbours( 6) = face_neighbours_glob(3,face_neighbours(6))
      edge_neighbours( 7) = face_neighbours_glob(4,face_neighbours(5))
      edge_neighbours( 8) = face_neighbours_glob(4,face_neighbours(6))
      edge_neighbours( 9) = face_neighbours_glob(1,face_neighbours(5))
      edge_neighbours(10) = face_neighbours_glob(2,face_neighbours(5))
      edge_neighbours(11) = face_neighbours_glob(1,face_neighbours(6))
      edge_neighbours(12) = face_neighbours_glob(2,face_neighbours(6))

     corner_neighbours(1) = face_neighbours_glob(1,face_neighbours_glob(3,face_neighbours(5)))
     corner_neighbours(2) = face_neighbours_glob(1,face_neighbours_glob(4,face_neighbours(5)))
     corner_neighbours(3) = face_neighbours_glob(2,face_neighbours_glob(3,face_neighbours(5)))
     corner_neighbours(4) = face_neighbours_glob(2,face_neighbours_glob(4,face_neighbours(5)))
     corner_neighbours(5) = face_neighbours_glob(1,face_neighbours_glob(3,face_neighbours(6)))
     corner_neighbours(6) = face_neighbours_glob(1,face_neighbours_glob(4,face_neighbours(6)))
     corner_neighbours(7) = face_neighbours_glob(2,face_neighbours_glob(3,face_neighbours(6)))
     corner_neighbours(8) = face_neighbours_glob(2,face_neighbours_glob(4,face_neighbours(6)))
  endif

  deallocate(face_neighbours_glob)
  
  call mpi_barrier(world,ierror)
  
  if(debug_l1) write(*,*) 'end   GRID_DECOMPOSITION',rank
  
  return
  
end subroutine grid_decomposition

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine MPI_COMMUNICATIONS:
!
!> Performs MPI communications to exchange information and store it in ghost cells.
!<
subroutine mpi_communications(array)

  use mod_variables, only : debug_l1,nvar
  use mod_mpi      , only : rank
  use mod_grid

  implicit none

  real(dp), dimension(nvar,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  
  if(debug_l1) write(*,*) 'begin MPI_COMMUNICATIONS',rank

  call mpi_comm_faces_1D(array,nvar)

  if(ndim > 1) call mpi_comm_faces_2D(array,nvar)

  if(ndim > 2) call mpi_comm_faces_3D(array,nvar)

  if(ndim == 2) call mpi_comm_corners_2D(array,nvar)

  if(ndim == 3)then
     call mpi_comm_edges_3D(array,nvar)
     call mpi_comm_corners_3D(array,nvar)
  endif

  if(debug_l1) write(*,*) 'end   MPI_COMMUNICATIONS',rank
  
  return
  
end subroutine mpi_communications

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine MPI_COMM_FACES_1D:
!
!> Sends the left face of thickness nghost to the left neighbour and receives the right ghost face
!! of thickness nghost from right neighbour using the MPI_SENDRECV routine.
!! Sends the right face of thickness nghost to the right neighbour and receives the left ghost face
!! of thickness nghost from left neighbour using the MPI_SENDRECV routine.
!<
subroutine mpi_comm_faces_1D(array,nv)

  use mod_variables, only : debug_l2
  use mod_mpi
  use mod_grid

  implicit none

  real(dp), dimension(nv,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp), dimension(:), allocatable                         :: send,recv
  integer                                                     :: i,j,k,h,m,array_size,nv

  if(debug_l2) write(*,*) 'begin MPI_COMM_FACES_1D',rank

  array_size = nxloc(2)*nxloc(3)*nv*nghost
  allocate(send(array_size),recv(array_size))
  
  m = 1
  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nghost
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,face_neighbours(1),rank,recv(1),array_size, &
                    mpi_real_dp,face_neighbours(2),face_neighbours(2),world,mpistat,ierror)

  m = 1
  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = nxloc(1)+1,nxloc(1)+nghost
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  m = 1
  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = nxloc(1)-nghost+1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,face_neighbours(2),rank,recv(1),array_size, &
                    mpi_real_dp,face_neighbours(1),face_neighbours(1),world,mpistat,ierror)

  m = 1
  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1-nghost,0
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  deallocate(send,recv)

  if(debug_l2) write(*,*) 'end   MPI_COMM_FACES_1D',rank

  return

end subroutine mpi_comm_faces_1D

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine MPI_COMM_FACES34:
!
!> Sends the front face of thickness nghost to the front neighbour and receives the back ghost face
!! of thickness nghost from back neighbour using the MPI_SENDRECV routine.
!! Sends the back face of thickness nghost to the back neighbour and receives the front ghost face
!! of thickness nghost from front neighbour using the MPI_SENDRECV routine.
!<
subroutine mpi_comm_faces_2D(array,nv)

  use mod_variables, only : debug_l2
  use mod_mpi
  use mod_grid

  implicit none

  real(dp), dimension(nv,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp), dimension(:), allocatable                         :: send,recv
  integer                                                     :: i,j,k,h,m,array_size,nv

  if(debug_l2) write(*,*) 'begin MPI_COMM_FACES_2D',rank

  array_size = nxloc(1)*nxloc(3)*nv*nghost
  allocate(send(array_size),recv(array_size))
  
  m = 1
  do k = 1,nxloc(3)
     do j = 1,nghost
        do i = 1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,face_neighbours(3),rank,recv(1),array_size, &
                    mpi_real_dp,face_neighbours(4),face_neighbours(4),world,mpistat,ierror)

  m = 1
  do k = 1,nxloc(3)
     do j = nxloc(2)+1,nxloc(2)+nghost
        do i = 1,nxloc(1)
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  m = 1
  do k = 1,nxloc(3)
     do j = nxloc(2)-nghost+1,nxloc(2)
        do i = 1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,face_neighbours(4),rank,recv(1),array_size, &
                    mpi_real_dp,face_neighbours(3),face_neighbours(3),world,mpistat,ierror)

  m = 1
  do k = 1,nxloc(3)
     do j = 1-nghost,0
        do i = 1,nxloc(1)
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  deallocate(send,recv)

  if(debug_l2) write(*,*) 'end   MPI_COMM_FACES_2D',rank

  return

end subroutine mpi_comm_faces_2D

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine MPI_COMM_FACES_3D:
!
!> Sends the bottom face of thickness nghost to the bottom neighbour and receives the top ghost face
!! of thickness nghost from top neighbour using the MPI_SENDRECV routine.
!! Sends the top face of thickness nghost to the top neighbour and receives the bottom ghost face
!! of thickness nghost from bottom neighbour using the MPI_SENDRECV routine.
!<
subroutine mpi_comm_faces_3D(array,nv)

  use mod_variables, only : debug_l2
  use mod_mpi
  use mod_grid

  implicit none

  real(dp), dimension(nv,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp), dimension(:), allocatable                         :: send,recv
  integer                                                     :: i,j,k,h,m,array_size,nv

  if(debug_l2) write(*,*) 'begin MPI_COMM_FACES_3D',rank

  array_size = nxloc(1)*nxloc(2)*nv*nghost
  allocate(send(array_size),recv(array_size))
  
  m = 1
  do k = 1,nghost
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,face_neighbours(5),rank,recv(1),array_size, &
                    mpi_real_dp,face_neighbours(6),face_neighbours(6),world,mpistat,ierror)

  m = 1
  do k = nxloc(3)+1,nxloc(3)+nghost
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  m = 1
  do k = nxloc(3)-nghost+1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,face_neighbours(6),rank,recv(1),array_size, &
                    mpi_real_dp,face_neighbours(5),face_neighbours(5),world,mpistat,ierror)

  m = 1
  do k = 1-nghost,0
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  deallocate(send,recv)

  if(debug_l2) write(*,*) 'end   MPI_COMM_FACES_3D',rank

  return

end subroutine mpi_comm_faces_3D

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine MPI_COMM_CORNERS_2D:
!
!> Sends and receives all four ghost corners in 2D grid.
!<
subroutine mpi_comm_corners_2D(array,nv)

  use mod_variables, only : debug_l2
  use mod_mpi
  use mod_grid

  implicit none

  real(dp), dimension(nv,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp), dimension(:), allocatable                         :: send,recv
  integer                                                     :: i,j,h,m,array_size,nv

  if(debug_l2) write(*,*) 'begin MPI_COMM_CORNERS_2D',rank

  array_size = nv*(nghost**2)
  allocate(send(array_size),recv(array_size))

  ! 1 - Send lower left corner and receive upper right corner
  m = 1
  do j = 1,nghost
     do i = 1,nghost
        do h = 1,nv
           send(m) = array(h,i,j,1)
           m = m + 1
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,corner_neighbours(1),rank,recv(1),array_size, &
                    mpi_real_dp,corner_neighbours(4),corner_neighbours(4),world,mpistat,ierror)

  m = 1
  do j = nxloc(2)+1,nxloc(2)+nghost
     do i = nxloc(1)+1,nxloc(1)+nghost
        do h = 1,nv
           array(h,i,j,1) = recv(m)
           m = m + 1
        enddo
     enddo
  enddo

  ! 2 - Send upper left corner and receive lower right corner
  m = 1
  do j = nxloc(2)-nghost+1,nxloc(2)
     do i = 1,nghost
        do h = 1,nv
           send(m) = array(h,i,j,1)
           m = m + 1
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,corner_neighbours(2),rank,recv(1),array_size, &
                    mpi_real_dp,corner_neighbours(3),corner_neighbours(3),world,mpistat,ierror)

  m = 1
  do j = 1-nghost,0
     do i = nxloc(1)+1,nxloc(1)+nghost
        do h = 1,nv
           array(h,i,j,1) = recv(m)
           m = m + 1
        enddo
     enddo
  enddo

  ! 3 - Send lower right corner and receive upper left corner
  m = 1
  do j = 1,nghost
     do i = nxloc(1)-nghost+1,nxloc(1)
        do h = 1,nv
           send(m) = array(h,i,j,1)
           m = m + 1
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,corner_neighbours(3),rank,recv(1),array_size, &
                    mpi_real_dp,corner_neighbours(2),corner_neighbours(2),world,mpistat,ierror)

  m = 1
  do j = nxloc(2)+1,nxloc(2)+nghost
     do i = 1-nghost,0
        do h = 1,nv
           array(h,i,j,1) = recv(m)
           m = m + 1
        enddo
     enddo
  enddo

  ! 4 - Send upper right corner and receive lower left corner
  m = 1
  do j = nxloc(2)-nghost+1,nxloc(2)
     do i = nxloc(1)-nghost+1,nxloc(1)
        do h = 1,nv
           send(m) = array(h,i,j,1)
           m = m + 1
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,corner_neighbours(4),rank,recv(1),array_size, &
                    mpi_real_dp,corner_neighbours(1),corner_neighbours(1),world,mpistat,ierror)

  m = 1
  do j = 1-nghost,0
     do i = 1-nghost,0
        do h = 1,nv
           array(h,i,j,1) = recv(m)
           m = m + 1
        enddo
     enddo
  enddo

  deallocate(send,recv)

  if(debug_l2) write(*,*) 'end   MPI_COMM_CORNERS_2D',rank

  return

end subroutine mpi_comm_corners_2D

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine MPI_COMM_EDGES_3D:
!
!> Sends and receives all twelve ghost edges in 3D grid.
!<
subroutine mpi_comm_edges_3D(array,nv)

  use mod_variables, only : debug_l2
  use mod_mpi
  use mod_grid

  implicit none

  real(dp), dimension(nv,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp), dimension(:), allocatable                         :: send,recv
  integer                                                     :: i,j,k,h,m,array_size,nv

  if(debug_l2) write(*,*) 'begin MPI_COMM_EDGES_3D',rank

  array_size = nxloc(3)*nv*(nghost**2)
  allocate(send(array_size),recv(array_size))

  ! 1 - Send lower front left corner and receive upper back right corner
  m = 1
  do k = 1,nxloc(3)
     do j = 1,nghost
        do i = 1,nghost
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,edge_neighbours(1),rank,recv(1),array_size, &
                    mpi_real_dp,edge_neighbours(4),edge_neighbours(4),world,mpistat,ierror)

  m = 1
  do k = 1,nxloc(3)
     do j = nxloc(2)+1,nxloc(2)+nghost
        do i = nxloc(1)+1,nxloc(1)+nghost
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 2 - Send lower back left corner and receive upper front right corner
  m = 1
  do k = 1,nxloc(3)
     do j = nxloc(2)-nghost+1,nxloc(2)
        do i = 1,nghost
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,edge_neighbours(2),rank,recv(1),array_size, &
                    mpi_real_dp,edge_neighbours(3),edge_neighbours(3),world,mpistat,ierror)

  m = 1
  do k = 1,nxloc(3)
     do j = 1-nghost,0
        do i = nxloc(1)+1,nxloc(1)+nghost
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 3 - Send lower front right corner and receive upper back left corner
  m = 1
  do k = 1,nxloc(3)
     do j = 1,nghost
        do i = nxloc(1)-nghost+1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,edge_neighbours(3),rank,recv(1),array_size, &
                    mpi_real_dp,edge_neighbours(2),edge_neighbours(2),world,mpistat,ierror)

  m = 1
  do k = 1,nxloc(3)
     do j = nxloc(2)+1,nxloc(2)+nghost
        do i = 1-nghost,0
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 4 - Send lower back right corner and receive upper front left corner
  m = 1
  do k = 1,nxloc(3)
     do j = nxloc(2)-nghost+1,nxloc(2)
        do i = nxloc(1)-nghost+1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,edge_neighbours(4),rank,recv(1),array_size, &
                    mpi_real_dp,edge_neighbours(1),edge_neighbours(1),world,mpistat,ierror)

  m = 1
  do k = 1,nxloc(3)
     do j = 1-nghost,0
        do i = 1-nghost,0
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  deallocate(send,recv)
  array_size = nxloc(1)*nv*(nghost**2)
  allocate(send(array_size),recv(array_size))

  ! 5 - Send upper front left corner and receive lower back right corner
  m = 1
  do k = 1,nghost
     do j = 1,nghost
        do i = 1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,edge_neighbours(5),rank,recv(1),array_size, &
                    mpi_real_dp,edge_neighbours(8),edge_neighbours(8),world,mpistat,ierror)

  m = 1
  do k = nxloc(3)+1,nxloc(3)+nghost
     do j = nxloc(2)+1,nxloc(2)+nghost
        do i = 1,nxloc(1)
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 6 - Send upper front right corner and receive lower back left corner
  m = 1
  do k = nxloc(3)-nghost+1,nxloc(3)
     do j = 1,nghost
        do i = 1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,edge_neighbours(6),rank,recv(1),array_size, &
                    mpi_real_dp,edge_neighbours(7),edge_neighbours(7),world,mpistat,ierror)

  m = 1
  do k = 1-nghost,0
     do j = nxloc(2)+1,nxloc(2)+nghost
        do i = 1,nxloc(1)
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 7 - Send lower back left corner and receive upper front right corner
  m = 1
  do k = 1,nghost
     do j = nxloc(2)-nghost+1,nxloc(2)
        do i = 1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,edge_neighbours(7),rank,recv(1),array_size, &
                    mpi_real_dp,edge_neighbours(6),edge_neighbours(6),world,mpistat,ierror)

  m = 1
  do k = nxloc(3)+1,nxloc(3)+nghost
     do j = 1-nghost,0
        do i = 1,nxloc(1)
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 8 - Send lower back right corner and receive upper front left corner
  m = 1
  do k = nxloc(3)-nghost+1,nxloc(3)
     do j = nxloc(2)-nghost+1,nxloc(2)
        do i = 1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,edge_neighbours(8),rank,recv(1),array_size, &
                    mpi_real_dp,edge_neighbours(5),edge_neighbours(5),world,mpistat,ierror)

  m = 1
  do k = 1-nghost,0
     do j = 1-nghost,0
        do i = 1,nxloc(1)
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  deallocate(send,recv)
  array_size = nxloc(2)*nv*(nghost**2)
  allocate(send(array_size),recv(array_size))

  ! 9 - Send upper front left corner and receive lower back right corner
  m = 1
  do k = 1,nghost
     do j = 1,nxloc(2)
        do i = 1,nghost
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,edge_neighbours(9),rank,recv(1),array_size, &
                    mpi_real_dp,edge_neighbours(12),edge_neighbours(12),world,mpistat,ierror)

  m = 1
  do k = nxloc(3)+1,nxloc(3)+nghost
     do j = 1,nxloc(2)
        do i = nxloc(1)+1,nxloc(1)+nghost
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 10 - Send upper front right corner and receive lower back left corner
  m = 1
  do k = 1,nghost
     do j = 1,nxloc(2)
        do i = nxloc(1)-nghost+1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,edge_neighbours(10),rank,recv(1),array_size, &
                    mpi_real_dp,edge_neighbours(11),edge_neighbours(11),world,mpistat,ierror)

  m = 1
  do k = nxloc(3)+1,nxloc(3)+nghost
     do j = 1,nxloc(2)
        do i = 1-nghost,0
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 11 - Send lower back left corner and receive upper front right corner
  m = 1
  do k = nxloc(3)-nghost+1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nghost
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,edge_neighbours(11),rank,recv(1),array_size, &
                    mpi_real_dp,edge_neighbours(10),edge_neighbours(10),world,mpistat,ierror)

  m = 1
  do k = 1-nghost,0
     do j = 1,nxloc(2)
        do i = nxloc(1)+1,nxloc(1)+nghost
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 12 - Send lower back right corner and receive upper front left corner
  m = 1
  do k = nxloc(3)-nghost+1,nxloc(3)
     do j = 1,nxloc(2)
        do i = nxloc(1)-nghost+1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,edge_neighbours(12),rank,recv(1),array_size, &
                    mpi_real_dp,edge_neighbours(9),edge_neighbours(9),world,mpistat,ierror)

  m = 1
  do k = 1-nghost,0
     do j = 1,nxloc(2)
        do i = 1-nghost,0
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  deallocate(send,recv)

  if(debug_l2) write(*,*) 'end   MPI_COMM_EDGES_3D',rank

  return

end subroutine mpi_comm_edges_3D

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine MPI_COMM_CORNERS_3D:
!
!> Sends and receives all eight ghost corners in 3D grid.
!<
subroutine mpi_comm_corners_3D(array,nv)

  use mod_variables, only : debug_l2
  use mod_mpi
  use mod_grid

  implicit none

  real(dp), dimension(nv,nmin1:nmax1,nmin2:nmax2,nmin3:nmax3) :: array
  real(dp), dimension(:), allocatable                         :: send,recv
  integer                                                     :: i,j,k,h,m,array_size,nv

  if(debug_l2) write(*,*) 'begin MPI_COMM_CORNERS_3D',rank

  array_size = nv*(nghost**ndim)
  allocate(send(array_size),recv(array_size))

  ! 1 - Send lower front left corner and receive upper back right corner
  m = 1
  do k = 1,nghost
     do j = 1,nghost
        do i = 1,nghost
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,corner_neighbours(1),rank,recv(1),array_size, &
                    mpi_real_dp,corner_neighbours(8),corner_neighbours(8),world,mpistat,ierror)

  m = 1
  do k = nxloc(3)+1,nxloc(3)+nghost
     do j = nxloc(2)+1,nxloc(2)+nghost
        do i = nxloc(1)+1,nxloc(1)+nghost
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 2 - Send lower back left corner and receive upper front right corner
  m = 1
  do k = 1,nghost
     do j = nxloc(2)-nghost+1,nxloc(2)
        do i = 1,nghost
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,corner_neighbours(2),rank,recv(1),array_size, &
                    mpi_real_dp,corner_neighbours(7),corner_neighbours(7),world,mpistat,ierror)

  m = 1
  do k = nxloc(3)+1,nxloc(3)+nghost
     do j = 1-nghost,0
        do i = nxloc(1)+1,nxloc(1)+nghost
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 3 - Send lower front right corner and receive upper back left corner
  m = 1
  do k = 1,nghost
     do j = 1,nghost
        do i = nxloc(1)-nghost+1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,corner_neighbours(3),rank,recv(1),array_size, &
                    mpi_real_dp,corner_neighbours(6),corner_neighbours(6),world,mpistat,ierror)

  m = 1
  do k = nxloc(3)+1,nxloc(3)+nghost
     do j = nxloc(2)+1,nxloc(2)+nghost
        do i = 1-nghost,0
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 4 - Send lower back right corner and receive upper front left corner
  m = 1
  do k = 1,nghost
     do j = nxloc(2)-nghost+1,nxloc(2)
        do i = nxloc(1)-nghost+1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,corner_neighbours(4),rank,recv(1),array_size, &
                    mpi_real_dp,corner_neighbours(5),corner_neighbours(5),world,mpistat,ierror)

  m = 1
  do k = nxloc(3)+1,nxloc(3)+nghost
     do j = 1-nghost,0
        do i = 1-nghost,0
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 5 - Send upper front left corner and receive lower back right corner
  m = 1
  do k = nxloc(3)-nghost+1,nxloc(3)
     do j = 1,nghost
        do i = 1,nghost
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,corner_neighbours(5),rank,recv(1),array_size, &
                    mpi_real_dp,corner_neighbours(4),corner_neighbours(4),world,mpistat,ierror)

  m = 1
  do k = 1-nghost,0
     do j = nxloc(2)+1,nxloc(2)+nghost
        do i = nxloc(1)+1,nxloc(1)+nghost
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 6 - Send upper back left corner and receive lower front right corner
  m = 1
  do k = nxloc(3)-nghost+1,nxloc(3)
     do j = nxloc(2)-nghost+1,nxloc(2)
        do i = 1,nghost
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,corner_neighbours(6),rank,recv(1),array_size, &
                    mpi_real_dp,corner_neighbours(3),corner_neighbours(3),world,mpistat,ierror)

  m = 1
  do k = 1-nghost,0
     do j = 1-nghost,0
        do i = nxloc(1)+1,nxloc(1)+nghost
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 7 - Send upper front right corner and receive lower back left corner
  m = 1
  do k = nxloc(3)-nghost+1,nxloc(3)
     do j = 1,nghost
        do i = nxloc(1)-nghost+1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,corner_neighbours(7),rank,recv(1),array_size, &
                    mpi_real_dp,corner_neighbours(2),corner_neighbours(2),world,mpistat,ierror)

  m = 1
  do k = 1-nghost,0
     do j = nxloc(2)+1,nxloc(2)+nghost
        do i = 1-nghost,0
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  ! 8 - Send upper back right corner and receive lower front left corner
  m = 1
  do k = nxloc(3)-nghost+1,nxloc(3)
     do j = nxloc(2)-nghost+1,nxloc(2)
        do i = nxloc(1)-nghost+1,nxloc(1)
           do h = 1,nv
              send(m) = array(h,i,j,k)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  call mpi_sendrecv(send(1),array_size,mpi_real_dp,corner_neighbours(8),rank,recv(1),array_size, &
                    mpi_real_dp,corner_neighbours(1),corner_neighbours(1),world,mpistat,ierror)

  m = 1
  do k = 1-nghost,0
     do j = 1-nghost,0
        do i = 1-nghost,0
           do h = 1,nv
              array(h,i,j,k) = recv(m)
              m = m + 1
           enddo
        enddo
     enddo
  enddo

  deallocate(send,recv)

  if(debug_l2) write(*,*) 'end   MPI_COMM_CORNERS_3D',rank

  return

end subroutine mpi_comm_corners_3D
