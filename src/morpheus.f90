!---------------------------------------------------------------------------------------------------
! File: morpheus.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains main program MORPHEUS and subroutines openfiles(), close_program()
!!
!! This file contains the main program and the routine used to cleanly exit the program and close
!! the log files.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Program MORPHEUS:
!
!> Main program
!!
!! 3D MPI-OPENMP Eulerian second-order Godunov hydrodynamic simulation code in cartesian, 
!! spherical and cylindrical coordinates.
!!
!! Copyright: Neil Vaytet & Tim O'Brien (07/2013).
!!
!! - calls the SETUP routine to initiate the computational volume
!! - initiates time loop 
!! - calls the HYDRO routine to compute the timestep, fluxes between cells and updates
!!   the quantities in the cells
!! - increases time by dt
!! - calls the DUMP routine to write datacubes
!<
program morpheus

  use mod_variables
  use mod_constants
!   use mod_timestep , only : dt,dt_old
  use mod_gravity  , only : gravity
  use mod_output
  use mod_mpi

  implicit none
  
  namelist/PARAMS/nout_term_freq,nrestart,debug_level,the_pill,user_controlled,nout_bin_tot,nout_ext_tot

  call initialisation

  call default_parameters

  ! read params namelist
  open  (10,file=trim(liste))
  rewind(10                 )
  read  (10,nml=PARAMS      )
  close (10                 )

  call set_debug

  if(debug_l1) write(*,*) 'begin MORPHEUS',rank

  if(nrestart > 0)then
     call restart
  else
     call setup
  endif

  ! print configuration
  if(master_cpu) call print_configuration

  if(nrestart == 0) call output_control   ! write inital data snapshot before computation starts
  
  ! Compute first timestep
!   call initial_timestep
  
  if(master_cpu)then
     write(*,*)
     write(*,*) 'STARTING COMPUTATIONAL LOOP'
     write(*,*)
  endif

  do while(t < tlim)

     it = it + 1
     
!      dt_old = dt

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% HYDRO ROUTINES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!

     call extras                              ! call extra subroutines
     
!      call timestep

     if(gravity) call calc_grav_force         ! calculate gravity

     call hydro                               ! calculate hydro evolution for dt

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%!

     t = t + dt!_old

     call output_control(it)

     if(master_cpu)then
        if(mod(it,nout_term_freq)==0)then
           write(terminal_outputs(2),'(a,es13.5,a,es13.5)')' Time:',t,',  Timestep:',dt
           call terminal_output
        endif
     endif

     if(user_controlled .and. master_cpu) read(*,*)

  enddo

  if(master_cpu)then
     write(*,*) 'Time,tlim',t,tlim
     write(*,*) 'Total number of timesteps:',it
  endif

  ! final frame dump
  t = t + epsilon(t)
  call output_control(it)

  call close_program('Time limit reached NORMALLY')

  if(debug_l1) write(*,*) 'end   MORPHEUS',rank

end program morpheus

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine CLOSE_PROGRAM:
!
!> Closes program cleanly. Calls routine DEALLOCATE_ARRAYS, finalises MPI,
!! closes log file.
!<
subroutine close_program(message)

  use mod_variables, only : debug_l1
  use mod_mpi

  implicit none

  character (len=*) :: message

  if(debug_l1) write(*,*) 'begin CLOSE_PROGRAM',rank

  write(*,*)
  write(*,'(a)'   ) ' ###########################################################'
  write(*,'(a,i6)') ' Call to the CLOSE_PROGRAM routine by cpu ',rank
  write(*,'(a,a )') ' Message: ',message
  write(*,'(a   )') ' Exiting...'
  write(*,'(a   )') ' ###########################################################'

  call extras_end

  call deallocate_arrays

#if MPI==1
  call mpi_finalize(ierror)
#endif

  stop

  if(debug_l1) write(*,*) 'end CLOSE_PROGRAM',rank

  return

end subroutine close_program
