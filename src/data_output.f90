!---------------------------------------------------------------------------------------------------
! File: data_output.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines output_control(), output_binary_data(), restart(), generate_dirname()
!! and generate_fname()
!!
!! This file contains all the routines necessary to data writing.
!! It contains the routines which write data cubes and read restart data.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine OUTPUT_CONTROL:
!
!> Controls cube and screen outputs.
!<
subroutine output_control

  use mod_variables
  use mod_output
  use mod_mpi      , only : rank

  implicit none

  if(debug_l1) write(*,*) 'begin OUTPUT_CONTROL',rank
  
  ! output cube of data to a file
  select case(outputs_interval)
  case(1)
     if(t >= t_bin_output)then
        t_bin_output = t_bin_output + t_bin_step
        call output_binary_data(k_bin_output)
        k_bin_output = k_bin_output + 1
     endif
  case(2)
     if(mod(it,dt_bin_interval) == 0)then
        call output_binary_data(k_bin_output)
        k_bin_output = k_bin_output + 1
     endif
  end select

!   ! output cube of data to a file every nitoutput timesteps
!   if(mod(it,nout_bin_freq) == 0)then
!      call output_binary_data(9999)
!   endif

  ! extras output
  if(t >= t_ext_output)then
     t_ext_output = t_ext_output + t_ext_step
     call output_extras_data
     k_ext_output = k_ext_output + 1
  endif

  if(debug_l1) write(*,*) 'end   OUTPUT_CONTROL',rank

  return

end subroutine output_control

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine OUTPUT_BINARY_DATA:
!
!> Writes a binary file containing all variables necessary to plotting and restart.
!<
subroutine output_binary_data(output_number)

  use mod_thermodynamics
  use mod_gridarrays
  use mod_variables
  use mod_constants
  use mod_gravity
  use mod_output
  use mod_arrays
  use mod_units
  use mod_grid
  use mod_mpi

  implicit none

  character (len=str) :: fname,dirname,filecmd,filename
  integer             :: output_number,a1,a2,a3,id

  if(debug_l1) write(*,*) 'begin OUTPUT_BINARY_DATA',rank

  ! generate filename  
  call generate_dirname(output_number,dirname)
  
 filecmd = 'mkdir -p '//trim(dirname)
 call system(trim(filecmd))
  
! #if USEMPI==1
!   call mpi_barrier(world,ierror)
! #endif
  
  call generate_fname(output_number,rank,fname)
  
  filename = trim(dirname)//'/'//trim(fname)

  a1 = 1 ; a2 = 0 ; a3 = 0
  if(ndim > 1) a2 = 1
  if(ndim > 2) a3 = 1
  
  id = 300+rank
  
  ! write data cube
  if(master_cpu) write(*,*)
  write(*,*) 'writing binary data to ',trim(filename)
  
  open (id,file=trim(filename),status='unknown',form='unformatted')
  ! write file header
  write(id) ndim
  write(id) nvar
  write(id) n_term_output
  write(id) nproc
  write(id) ncpu
  write(id) it
  write(id) geometry
  write(id) t
!   write(id) tlim
  write(id) time
  write(id) mass
  write(id) length
  write(id) degree
  write(id) cooling
  write(id) viscosity
  write(id) gravity
  write(id) same_bc
  write(id) cn_hydro
  write(id) cn_cooling
  write(id) visc
  write(id) g
  write(id) mu
  write(id) dcav
  write(id) pcav
!   write(id) cumul_erad
  write(id) slope_type
  write(id) solver
  write(id) nsource
  write(id) geom1
  write(id) geom2
  write(id) geom3
  write(id) coolcurve
  write(id) t_bin_output
  write(id) t_bin_step
  write(id) t_ext_output
  write(id) t_ext_step
  write(id) output_number
  write(id) k_ext_output
  ! write data
  write(id) nx
  write(id) nxloc
  write(id) nxlower
  write(id) nxupper
  write(id) nghost
  write(id) nmin1
  write(id) nmax1
  write(id) nmin2
  write(id) nmax2
  write(id) nmin3
  write(id) nmax3
  write(id) face_neighbours
  write(id) edge_neighbours
  write(id) corner_neighbours
  write(id) xgcen
  write(id) lbox
!   write(id)  x1glob(1-a1:nx(1))
!   write(id)  x2glob(1-a2:nx(2))
!   write(id)  x3glob(1-a3:nx(3))
!   write(id) xn1glob(1   :nx(1))
!   write(id) xn2glob(1   :nx(2))
!   write(id) xn3glob(1   :nx(3))
  write(id)  x1!(1-a1:nxloc(1))
  write(id)  x2!(1-a2:nxloc(2))
  write(id)  x3!(1-a3:nxloc(3))
  write(id) xn1!(1   :nxloc(1))
  write(id) xn2!(1   :nxloc(2))
  write(id) xn3!(1   :nxloc(3))
  write(id) xh1!(1   :nxloc(1))
  write(id) xh2!(1   :nxloc(2))
  write(id) xh3!(1   :nxloc(3))
  write(id) rnt!(1   :nxloc(1))
  write(id) ths!(1   :nxloc(2))
  write(id) b  !(1   :nxloc(3))
  write(id) bc1
  write(id) bc2
  write(id) bc3
  write(id) bc4
  write(id) bc5
  write(id) bc6
  write(id) inflow1
  write(id) inflow2
  write(id) inflow3
  write(id) inflow4
  write(id) inflow5
  write(id) inflow6
  write(id) unew
  close(id)
  
  if(master_cpu) write(*,*)

  if(debug_l1) write(*,*) 'end   OUTPUT_BINARY_DATA',rank

  return

end subroutine output_binary_data

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine RESTART:
!
!> Reads binary file and restarts simulation from there. Routine replaces SETUP in the case
!! where the variable 'resume' in the namelist is set to .true.
!<
subroutine restart

  use mod_thermodynamics
  use mod_gridarrays
  use mod_variables
  use mod_constants
  use mod_gravity
  use mod_arrays
  use mod_output
  use mod_units
  use mod_grid
  use mod_mpi

  implicit none

  integer             :: dum,a1,a2,a3,id
  character (len=str) :: fname,dirname,filename
  real(dp) :: rho1,rho2,u1,u2,p1,p2,t1,t2
  
  namelist/INIT/cn_hydro,cn_cooling,visc,slope_type,solver,cooling,coolcurve,viscosity, &
                gravity,g,nx,nproc,lbox,tlim,rho1,rho2,u1,u2,p1,p2,t1,t2,ndim,geometry, &
                rhomin,boundc_1,boundc_2,boundc_3,boundc_4,boundc_5,boundc_6,gravity_type

  if(debug_l1) write(*,*) 'begin RESTART',rank

  ! generate filename  
  call generate_dirname(nrestart,dirname)
  
  call generate_fname(nrestart,rank,fname)
  
  filename = trim(dirname)//'/'//trim(fname)

  write(*,*)'Restarting from: ',filename

  a1 = 1 ; a2 = 0 ; a3 = 0
  if(ndim > 1) a2 = 1
  if(ndim > 2) a3 = 1
  
  id = 400+rank
  
  open (id,file=trim(filename),status='old',form='unformatted')
  ! read file header
  read (id) ndim
  read (id) dum
  read (id) n_term_output
  read (id) nproc
  read (id) ncpu
  read (id) it
  read (id) geometry
  read (id) t
!   read (id) tlim
  read (id) time
  read (id) mass
  read (id) length
  read (id) degree
  read (id) cooling
  read (id) viscosity
  read (id) gravity
  read (id) same_bc
  read (id) cn_hydro
  read (id) cn_cooling
  read (id) visc
  read (id) g
  read (id) mu
  read (id) dcav
  read (id) pcav
!   read (id) cumul_erad
  read (id) slope_type
  read (id) solver
  read (id) nsource
  read (id) geom1
  read (id) geom2
  read (id) geom3
  read (id) coolcurve
  read (id) t_bin_output
  read (id) t_bin_step
  read (id) t_ext_output
  read (id) t_ext_step
  read (id) k_bin_output
  read (id) k_ext_output
  read (id) nx
  read (id) nxloc
  read (id) nxlower
  read (id) nxupper
  read (id) nghost
  read (id) nmin1
  read (id) nmax1
  read (id) nmin2
  read (id) nmax2
  read (id) nmin3
  read (id) nmax3
  
  call allocate_arrays
  
  ! read data
  read (id) face_neighbours
  read (id) edge_neighbours
  read (id) corner_neighbours
  read (id) xgcen
  read (id) lbox
!   read (id)  x1glob(1-a1:nx(1))
!   read (id)  x2glob(1-a2:nx(2))
!   read (id)  x3glob(1-a3:nx(3))
!   read (id) xn1glob(1   :nx(1))
!   read (id) xn2glob(1   :nx(2))
!   read (id) xn3glob(1   :nx(3))
  read (id) x1 !(1-a1:nxloc(1))
  read (id) x2 !(1-a2:nxloc(2))
  read (id) x3 !(1-a3:nxloc(3))
  read (id) xn1!(1   :nxloc(1))
  read (id) xn2!(1   :nxloc(2))
  read (id) xn3!(1   :nxloc(3))
  read (id) xh1!(1   :nxloc(1))
  read (id) xh2!(1   :nxloc(2))
  read (id) xh3!(1   :nxloc(3))
  read (id) rnt!(1   :nxloc(1))
  read (id) ths!(1   :nxloc(2))
  read (id) b  !(1   :nxloc(3))
  read (id) bc1
  read (id) bc2
  read (id) bc3
  read (id) bc4
  read (id) bc5
  read (id) bc6
  read (id) inflow1
  read (id) inflow2
  read (id) inflow3
  read (id) inflow4
  read (id) inflow5
  read (id) inflow6
  read (id) unew
  close(id)
  
  id = 500+rank
  open  (id,file=trim(liste))
  rewind(id                 )
  read  (id,nml=INIT        )
  close (id                 )
  
  call domain_setup(zero,zero,zero,one,one,one)
  call common_setup1

  k_bin_output = k_bin_output + 1

#if MPI==1
  call mpi_barrier(world,ierror)
#endif

  if(debug_l1) write(*,*) 'end   RESTART',rank

  return

end subroutine restart

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine GENERATE_DIRNAME:
!
!> Generates a directory name for dumping datacubes.
!<
subroutine generate_dirname(iout,dirname)

  use mod_variables, only : str,debug_l2
  use mod_mpi      , only : rank

  implicit none

  character (len=str) :: dirname
  integer             :: iout
  
  if(debug_l2) write(*,*) 'begin GENERATE_DIRNAME',rank

!   WRITE (ans, '(a, i3.3)') chr1, val1
!   
!   write(scube,'(a4,i1)') '0000',kcube
!   
!   if(kcube <  10)then
!      write(scube,'(a4,i1)') '0000',kcube
!   elseif((kcube >= 10).and.(kcube < 100))then
!      write(scube,'(a3,i2)') '000',kcube
!   elseif((kcube >= 100).and.(kcube < 1000))then
!      write(scube,'(a2,i3)') '00',kcube
!   elseif((kcube >= 1000).and.(kcube < 10000))then
!      write(scube,'(a1,i4)') '0',kcube
!   else
!      write(scube,'(i5)') kcube
!   endif
! 
!   dirname = 'output-'//scube
  
  write(dirname,'(a7,i5.5)') 'output-',iout
  
  if(debug_l2) write(*,*) 'end   GENERATE_DIRNAME',rank

  return

end subroutine generate_dirname

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine GENERATE_FNAME:
!
!> Generates a filename for dumping datacube to binary file.
!<
subroutine generate_fname(iout,irank,fname)

  use mod_variables, only : str,debug_l2
  use mod_mpi      , only : rank

  implicit none

!   character (len=4  ) :: scube
!   character (len=6  ) :: srank
  character (len=str) :: fname
  integer             :: iout,irank

  if(debug_l2) write(*,*) 'begin GENERATE_FNAME',rank

!   if(kcube <  10)then
!      write(scube,'(a4,i1)') '0000',kcube
!   elseif((kcube >= 10).and.(kcube < 100))then
!      write(scube,'(a3,i2)') '000',kcube
!   elseif((kcube >= 100).and.(kcube < 1000))then
!      write(scube,'(a2,i3)') '00',kcube
!   elseif((kcube >= 1000).and.(kcube < 10000))then
!      write(scube,'(a1,i4)') '0',kcube
!   else
!      write(scube,'(i5)') kcube
!   endif
  
!   if( kcube <  10)                  write(scube,'(a3,i1)') '000',kcube
!   if((kcube >= 10).and.(kcube < 100)) write(scube,'(a2,i2)') '00',kcube
!   if((kcube >= 100).and.(kcube < 1000)) write(scube,'(a1,i3)') '0',kcube
!   if((kcube >= 1000).and.(kcube < 10000)) write(scube,   '(i4)')    kcube
! 
!   if( krank <  10)                  write(srank,'(a,i1)') '00000',krank
!   if((krank >= 10).and.(krank < 100)) write(srank,'(a,i2)') '0000',krank
!   if((krank >= 100).and.(krank < 1000)) write(srank,'(a,i3)') '000',krank
!   if((krank >= 1000).and.(krank < 10000)) write(srank,'(a,i4)') '00',krank
!   if((krank >= 10000).and.(krank < 100000)) write(srank,'(a,i5)') '0',krank
!   if((krank >= 100000).and.(krank < 1000000)) write(srank,   '(i6)')   krank
! 
!   fname = trim(adjustl(root))//'-'//scube//'-'//srank//'.bin'

  write(fname,'(a7,i5.5,a1,i6.6,a4)') 'output-',iout,'-',irank,'.bin'
  
  if(debug_l2) write(*,*) 'end   GENERATE_FNAME',rank

  return

end subroutine generate_fname
