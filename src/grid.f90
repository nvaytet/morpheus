!---------------------------------------------------------------------------------------------------
! File: grid.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines safety_check(), grid_setup(), display_grid(), cal_cell_volume()
!! and cal_cell_area()
!!
!! This file contains the routines used to set up the grid and the lower and upper bounds for the
!! variable arrays.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine SAFETY_CHECK:
!
!> Checks for wrong values in nx, profiles, slices and constant values according to ndim.
!<
subroutine safety_check

  use mod_variables, only : debug_l1
  use mod_grid
  use mod_output
  use mod_mpi

  implicit none

  if(debug_l1) write(*,*) 'begin SAFETY_CHECK',rank

  ! override grid dimensions for safety
  if(ndim == 1)then
     nx(2) = 1
     nx(3) = 1
  endif
  if(ndim == 2)then
     nx(3) = 1
  endif

  if(debug_l1) write(*,*) 'end   SAFETY_CHECK',rank

  return

end subroutine safety_check

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine GRID_SETUP:
!
!> Sets up the grid structure: dimensions, lower and upper limts, geometry, x1, x2 and
!! x3 coordinate arrays and computes the cell areas and volumes.
!<
subroutine grid_setup(cen1,cen2,cen3,xfact1,xfact2,xfact3)

  use mod_gridarrays
  use mod_variables
  use mod_constants
  use mod_grid
  use mod_mpi

  implicit none

  real(dp)                            :: xfact1,xfact2,xfact3,dx1,dx2,dx3
  real(dp)                            :: cen1,cen2,cen3
!   real(dp), dimension(:), allocatable :: xh1glob,xh2glob,xh3glob
  real(dp), dimension(:), allocatable :: rntglob,bglob,thsglob
  real(dp), dimension(:), allocatable :: x1glob,x2glob,x3glob
  real(dp), dimension(:), allocatable :: xn1glob,xn2glob,xn3glob
  real(dp), dimension(:), allocatable :: xh1glob,xh2glob,xh3glob
  integer                             :: nminglob1,nmaxglob1,nminglob2,nmaxglob2,nminglob3,nmaxglob3
  integer                             :: i,j,k,n
  integer                             :: a1,a2,a3
  
  if(debug_l1) write(*,*) 'begin GRID_SETUP',rank

  if(lbox(2) < zero) lbox(2) = pi*(-lbox(2))
  if(lbox(3) < zero) lbox(3) = pi*(-lbox(3))

  xgcen = zero
  xgcen(1) = cen1*lbox(1)                ! grid centre in direction 1
  if(ndim > 1) xgcen(2) = cen2*lbox(2)   ! grid centre in direction 2
  if(ndim > 2) xgcen(3) = cen3*lbox(3)   ! grid centre in direction 3
  
  geom1 = '-' ; geom2 = '-' ; geom3 = '-'
  if(geometry == 1)then
                  geom1 = 'x'
     if(ndim > 1) geom2 = 'y'
     if(ndim > 2) geom3 = 'z'
  elseif(geometry == 2)then
                  geom1 = 'r'
     if(ndim > 1) geom2 = 't'
     if(ndim > 2) geom3 = 'p'
  elseif(geometry == 3)then
                  geom1 = 'r'
     if(ndim > 1) geom2 = 'z'
     if(ndim > 2) geom3 = 'p'
  else
     call close_program('Bad geometry in SETUP')
  endif

  nminglob1 = 1-nghost
  nmaxglob1 = nx(1)+nghost
  nminglob2 = 1
  nmaxglob2 = 1
  nminglob3 = 1
  nmaxglob3 = 1
  a1 = 1 ; a2 = 0 ; a3 = 0

  if(ndim > 1)then
     nminglob2 = 1-nghost
     nmaxglob2 = nx(2)+nghost
     a2 = 1
  endif
  if(ndim > 2)then
     nminglob3 = 1-nghost
     nmaxglob3 = nx(3)+nghost
     a3 = 1
  endif

  allocate( x1glob(nminglob1-a1:nmaxglob1))
  allocate( x2glob(nminglob2-a2:nmaxglob2))
  allocate( x3glob(nminglob3-a3:nmaxglob3))
  allocate(xn1glob(nminglob1   :nmaxglob1))
  allocate(xn2glob(nminglob2   :nmaxglob2))
  allocate(xn3glob(nminglob3   :nmaxglob3))
  allocate(xh1glob(nminglob1   :nmaxglob1))
  allocate(xh2glob(nminglob2   :nmaxglob2))
  allocate(xh3glob(nminglob3   :nmaxglob3))
  allocate(rntglob(nminglob1   :nmaxglob1))
  allocate(thsglob(nminglob2   :nmaxglob2))
  allocate(  bglob(nminglob2   :nmaxglob2))

   x1 = zero ;  x2 = zero ;  x3 = zero
  xn1 = zero ; xn2 = zero ; xn3 = zero
  xh1 = zero ; xh2 = zero ; xh3 = zero

   x1glob = zero ;  x2glob = zero ;  x3glob = zero
  xn1glob = zero ; xn2glob = zero ; xn3glob = zero
  xh1glob = zero ; xh2glob = zero ; xh3glob = zero

  ! Setup positions of grid interfaces
  x1glob(0) = zero
  dx1 = one
  do i = 1,nx(1)
     x1glob(i) = x1glob(i-1) + dx1
     dx1 = dx1*xfact1
  enddo
  x1glob = x1glob * lbox(1) / x1glob(nx(1))

  dx1 = x1glob(1) - x1glob(0)
  do n = 1,nghost
     x1glob(-n) = x1glob(1-n) - dx1
  enddo
  dx1 = x1glob(nx(1)) - x1glob(nx(1)-1)
  do n = nx(1)+1,nx(1)+nghost
     x1glob(n) = x1glob(n-1) + dx1
  enddo

  x1glob = x1glob - xgcen(1)
  
  if(ndim > 1)then
  
     x2glob(0) = zero
     dx2 = one
     do j = 1,nx(2)
        x2glob(j) = x2glob(j-1) + dx2
        dx2 = dx2*xfact2
     enddo
     x2glob = x2glob * lbox(2) / x2glob(nx(2))

     dx2 = x2glob(1) - x2glob(0)
     do n = 1,nghost
        x2glob(-n) = x2glob(1-n) - dx2
     enddo
     dx2 = x2glob(nx(2)) - x2glob(nx(2)-1)
     do n = nx(2)+1,nx(2)+nghost
        x2glob(n) = x2glob(n-1) + dx2
     enddo

     x2glob = x2glob - xgcen(2)

  endif
  
  if(ndim > 2)then
  
     x3glob(0) = zero
     dx3 = one
     do k = 1,nx(3)
        x3glob(k) = x3glob(k-1) + dx3
        dx3 = dx3*xfact3
     enddo
     x3glob = x3glob * lbox(3) / x3glob(nx(3))

     dx3 = x3glob(1) - x3glob(0)
     do n = 1,nghost
        x3glob(-n) = x3glob(1-n) - dx3
     enddo
     dx3 = x3glob(nx(3)) - x3glob(nx(3)-1)
     do n = nx(3)+1,nx(3)+nghost
        x3glob(n) = x3glob(n-1) + dx3
     enddo

     x3glob = x3glob - xgcen(3)

  endif

  ! radii of nodal positions of cells
  do i = -nghost+1,nx(1)+nghost
     if(geometry == 1)then
        xn1glob(i) = half*(x1glob(i)+x1glob(i-1))
        rntglob(i) = xn1glob(i)
     elseif(geometry == 2)then
        xn1glob(i) = (three/four)*(x1glob(i)**4-x1glob(i-1)**4)/(x1glob(i)**3-x1glob(i-1)**3)
        rntglob(i) = (two/three)*(x1glob(i)**3-x1glob(i-1)**3)/(x1glob(i)**2-x1glob(i-1)**2)
     elseif(geometry == 3)then
        xn1glob(i) = (two/three)*(x1glob(i)**3-x1glob(i-1)**3)/(x1glob(i)**2-x1glob(i-1)**2)
        rntglob(i) = xn1glob(i)
     endif
     xh1glob(i) = half*(x1glob(i)+x1glob(i-1))
  enddo
  if(ndim > 1)then
     do j = -nghost+1,nx(2)+nghost
        if(geometry == 1)then
           xn2glob(j) = half*(x2glob(j)+x2glob(j-1))
           bglob(j)   = zero
           thsglob(j) = xn2glob(j)
        elseif(geometry == 2)then
           xn2glob(j) = half*(x2glob(j)+x2glob(j-1))+(half*(x2glob(j)-x2glob(j-1))*(cos(x2glob(j-1))+ &
                        cos(x2glob(j)))+sin(x2glob(j-1))-sin(x2glob(j)))/(cos(x2glob(j))-cos(x2glob(j-1)))
           bglob(j)   = (sin(x2glob(j))-sin(x2glob(j-1)))/(cos(x2glob(j-1))-cos(x2glob(j)))
           thsglob(j) = half*(x2glob(j)+x2glob(j-1))+(half*(x2glob(j)-x2glob(j-1))*(sin(x2glob(j-1))+ &
                        sin(x2glob(j)))+cos(x2glob(j))-cos(x2glob(j-1)))/(sin(x2glob(j))-sin(x2glob(j-1)))
        elseif(geometry == 3)then
           xn2glob(j) = half*(x2glob(j)+x2glob(j-1))
           bglob(j)   = zero
           thsglob(j) = xn2glob(j)
        endif
        xh2glob(j) = half*(x2glob(j)+x2glob(j-1))
     enddo
  endif
  if(ndim > 2)then
     do k = -nghost+1,nx(3)+nghost
        if(geometry == 1)then
           xn3glob(k) = half*(x3glob(k)+x3glob(k-1))
        elseif(geometry == 2)then
           xn3glob(k) = half*(x3glob(k)+x3glob(k-1))
        elseif(geometry == 3)then
           xn3glob(k) = half*(x3glob(k)+x3glob(k-1))
        endif
        xh3glob(k) = half*(x3glob(k)+x3glob(k-1))
     enddo
  endif

  ! Distribute grid cells among processors

  nxlower = 1
  nxupper = nx
  
    face_neighbours = -1
    edge_neighbours = -1
  corner_neighbours = -1

#if MPI==1
  call grid_decomposition
#endif

   x1(-nghost  :nxloc(1)+nghost) =  x1glob(nxlower(1)-nghost-1:nxupper(1)+nghost)
  xn1(-nghost+1:nxloc(1)+nghost) = xn1glob(nxlower(1)-nghost  :nxupper(1)+nghost)
  xh1(-nghost+1:nxloc(1)+nghost) = xh1glob(nxlower(1)-nghost  :nxupper(1)+nghost)
  rnt(-nghost+1:nxloc(1)+nghost) = rntglob(nxlower(1)-nghost  :nxupper(1)+nghost)

  if(ndim > 1)then
      x2(-nghost  :nxloc(2)+nghost) =  x2glob(nxlower(2)-nghost-1:nxupper(2)+nghost)
     xn2(-nghost+1:nxloc(2)+nghost) = xn2glob(nxlower(2)-nghost  :nxupper(2)+nghost)
     xh2(-nghost+1:nxloc(2)+nghost) = xh2glob(nxlower(2)-nghost  :nxupper(2)+nghost)
       b(-nghost+1:nxloc(2)+nghost) =   bglob(nxlower(2)-nghost  :nxupper(2)+nghost)
     ths(-nghost+1:nxloc(2)+nghost) = thsglob(nxlower(2)-nghost  :nxupper(2)+nghost)
  endif

  if(ndim > 2)then
      x3(-nghost  :nxloc(3)+nghost) =  x3glob(nxlower(3)-nghost-1:nxupper(3)+nghost)
     xn3(-nghost+1:nxloc(3)+nghost) = xn3glob(nxlower(3)-nghost  :nxupper(3)+nghost)
     xh3(-nghost+1:nxloc(3)+nghost) = xh3glob(nxlower(3)-nghost  :nxupper(3)+nghost)
  endif

  call display_grid
  call cal_cell_volume
  call cal_cell_area

  deallocate(x1glob,x2glob,x3glob)
  deallocate(xn1glob,xn2glob,xn3glob)
  deallocate(xh1glob,xh2glob,xh3glob)
  deallocate(rntglob,thsglob,  bglob)

  if(debug_l1) write(*,*) 'end   GRID_SETUP',rank

  return

end subroutine grid_setup

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine CAL_CELL_VOLUME:
!
!> Computes the cell volumes in the entire grid.
!<
subroutine cal_cell_volume

  use mod_gridarrays, only : x1,x2,x3,cell_volume
  use mod_variables , only : debug_l1,cdim
  use mod_constants
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer :: i,j,k

  if(debug_l1) write(*,*) 'begin CAL_CELL_VOLUME',rank

  cell_volume = one

  select case(geometry)

  case(1)

     do k = 1-cdim(3),nxloc(3)+cdim(3)
        do j = 1-cdim(2),nxloc(2)+cdim(2)
           do i = 1-cdim(1),nxloc(1)+cdim(1)

              select case(ndim)
              case(1)
                 cell_volume(i,j,k) =  x1(i)-x1(i-1)
              case(2)
                 cell_volume(i,j,k) = (x1(i)-x1(i-1))*(x2(j)-x2(j-1))
              case(3)
                 cell_volume(i,j,k) = (x1(i)-x1(i-1))*(x2(j)-x2(j-1))*(x3(k)-x3(k-1))
              end select

           enddo
        enddo
     enddo

  case(2)

     do k = 1-cdim(3),nxloc(3)+cdim(3)
        do j = 1-cdim(2),nxloc(2)+cdim(2)
           do i = 1-cdim(1),nxloc(1)+cdim(1)

              select case(ndim)
              case(1)
                 cell_volume(i,j,k) = (four/three)*pi*((x1(i)*x1(i)*x1(i))-(x1(i-1)*x1(i-1)*x1(i-1)))
              case(2)
                 cell_volume(i,j,k) = (two/three)*pi*((x1(i)*x1(i)*x1(i))-(x1(i-1)*x1(i-1)*x1(i-1)))*(cos(x2(j-1))-cos(x2(j)))
              case(3)
                 cell_volume(i,j,k) = (one/three)*(x3(k)-x3(k-1))*((x1(i)*x1(i)*x1(i))-(x1(i-1)*x1(i-1)*x1(i-1)))*(cos(x2(j-1))-cos(x2(j)))
              end select

           enddo
        enddo
     enddo

  case(3)

     do k = 1-cdim(3),nxloc(3)+cdim(3)
        do j = 1-cdim(2),nxloc(2)+cdim(2)
           do i = 1-cdim(1),nxloc(1)+cdim(1)

              select case(ndim)
              case(1)
                 cell_volume(i,j,k) = pi*((x1(i)*x1(i))-(x1(i-1)*x1(i-1)))
              case(2)
                 cell_volume(i,j,k) = pi*((x1(i)*x1(i))-(x1(i-1)*x1(i-1)))*(x2(j)-x2(j-1))
              case(3)
                 cell_volume(i,j,k) = half*((x1(i)*x1(i))-(x1(i-1)*x1(i-1)))*(x2(j)-x2(j-1))*(x3(k)-x3(k-1))
              end select

           enddo
        enddo
     enddo

  end select

  if(debug_l1) write(*,*) 'end   CAL_CELL_VOLUME',rank

  return

end subroutine cal_cell_volume

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine CAL_CELL_AREA:
!
!> Computes the cell areas in the entire grid.
!<
subroutine cal_cell_area

  use mod_gridarrays, only : x1,x2,x3,cell_area
  use mod_variables , only : debug_l1,cdim
  use mod_constants
  use mod_grid
  use mod_mpi       , only : rank

  implicit none

  integer :: i,j,k

  if(debug_l1) write(*,*) 'begin CAL_CELL_AREA',rank

  cell_area = zero

  select case(geometry)

  case(1)

     do k = 1-cdim(3),nxloc(3)+cdim(3)
        do j = 1-cdim(2),nxloc(2)+cdim(2)
           do i = 1-cdim(1),nxloc(1)+cdim(1)

              select case(ndim)
              case(1)
                 cell_area(1,i,j,k) = one
                 cell_area(2,i,j,k) = one
              case(2)
                 cell_area(1,i,j,k) = x2(j)-x2(j-1)
                 cell_area(2,i,j,k) = x2(j)-x2(j-1)
                 cell_area(3,i,j,k) = x1(i)-x1(i-1)
                 cell_area(4,i,j,k) = x1(i)-x1(i-1)
              case(3)
                 cell_area(1,i,j,k) = (x2(j)-x2(j-1))*(x3(k)-x3(k-1))
                 cell_area(2,i,j,k) = (x2(j)-x2(j-1))*(x3(k)-x3(k-1))
                 cell_area(3,i,j,k) = (x1(i)-x1(i-1))*(x3(k)-x3(k-1))
                 cell_area(4,i,j,k) = (x1(i)-x1(i-1))*(x3(k)-x3(k-1))
                 cell_area(5,i,j,k) = (x1(i)-x1(i-1))*(x2(j)-x2(j-1))
                 cell_area(6,i,j,k) = (x1(i)-x1(i-1))*(x2(j)-x2(j-1))
              end select

           enddo
        enddo
     enddo

  case(2)

     do k = 1-cdim(3),nxloc(3)+cdim(3)
        do j = 1-cdim(2),nxloc(2)+cdim(2)
           do i = 1-cdim(1),nxloc(1)+cdim(1)

              select case(ndim)
              case(1)
                 cell_area(1,i,j,k) = four*pi*(x1(i-1)*x1(i-1))
                 cell_area(2,i,j,k) = four*pi*(x1(i  )*x1(i  ))
              case(2)
                 cell_area(1,i,j,k) = two*pi*(x1(i-1)*x1(i-1))*(cos(x2(j-1))-cos(x2(j)))
                 cell_area(2,i,j,k) = two*pi*(x1(i  )*x1(i  ))*(cos(x2(j-1))-cos(x2(j)))
                 cell_area(3,i,j,k) = pi*((x1(i)*x1(i))-(x1(i-1)*x1(i-1)))*sin(x2(j-1))
                 cell_area(4,i,j,k) = pi*((x1(i)*x1(i))-(x1(i-1)*x1(i-1)))*sin(x2(j  ))
              case(3)
                 cell_area(1,i,j,k) = (x1(i-1)*x1(i-1))*(cos(x2(j-1))-cos(x2(j)))*(x3(k)-x3(k-1))
                 cell_area(2,i,j,k) = (x1(i  )*x1(i  ))*(cos(x2(j-1))-cos(x2(j)))*(x3(k)-x3(k-1))
                 cell_area(3,i,j,k) = half*((x1(i)*x1(i))-(x1(i-1)*x1(i-1)))*sin(x2(j-1))*(x3(k)-x3(k-1))
                 cell_area(4,i,j,k) = half*((x1(i)*x1(i))-(x1(i-1)*x1(i-1)))*sin(x2(j  ))*(x3(k)-x3(k-1))
                 cell_area(5,i,j,k) = half*((x1(i)*x1(i))-(x1(i-1)*x1(i-1)))*(x2(j)-x2(j-1))
                 cell_area(6,i,j,k) = half*((x1(i)*x1(i))-(x1(i-1)*x1(i-1)))*(x2(j)-x2(j-1))
              end select

           enddo
        enddo
     enddo

  case(3)

     do k = 1-cdim(3),nxloc(3)+cdim(3)
        do j = 1-cdim(2),nxloc(2)+cdim(2)
           do i = 1-cdim(1),nxloc(1)+cdim(1)

              select case(ndim)
              case(1)
                 cell_area(1,i,j,k) = two*pi*x1(i-1)
                 cell_area(2,i,j,k) = two*pi*x1(i  )
              case(2)
                 cell_area(1,i,j,k) = two*pi*x1(i-1)*(x2(j)-x2(j-1))
                 cell_area(2,i,j,k) = two*pi*x1(i  )*(x2(j)-x2(j-1))
                 cell_area(3,i,j,k) = pi*((x1(i)*x1(i))-(x1(i-1)*x1(i-1)))
                 cell_area(4,i,j,k) = pi*((x1(i)*x1(i))-(x1(i-1)*x1(i-1)))
              case(3)
                 cell_area(1,i,j,k) = x1(i-1)*(x2(j)-x2(j-1))*(x3(k)-x3(k-1))
                 cell_area(2,i,j,k) = x1(i  )*(x2(j)-x2(j-1))*(x3(k)-x3(k-1))
                 cell_area(3,i,j,k) = half*((x1(i)*x1(i))-(x1(i-1)*x1(i-1)))*(x3(k)-x3(k-1))
                 cell_area(4,i,j,k) = half*((x1(i)*x1(i))-(x1(i-1)*x1(i-1)))*(x3(k)-x3(k-1))
                 cell_area(5,i,j,k) = (x1(i)-x1(i-1))*(x2(j)-x2(j-1))
                 cell_area(6,i,j,k) = (x1(i)-x1(i-1))*(x2(j)-x2(j-1))
              end select

           enddo
        enddo
     enddo

  end select

  if(debug_l1) write(*,*) 'end   CAL_CELL_AREA',rank

  return

end subroutine cal_cell_area
