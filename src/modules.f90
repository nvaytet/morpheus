!---------------------------------------------------------------------------------------------------
! File: modules.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains modules modprecision, variables, arrays, modmpi, modnompi, grid, gridarrays,
!! thermodynamics, modgravity, dump, units, time_monitor
!!
!! This file contains all the modules which hold all the global variables.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module MODPRECISION:
!
!> Sets the single and double precision in the code. Note that for the real-time screen
!! dumps, PGPLOT uses single precision only.
!<
module mod_precision
  implicit none
  integer, parameter :: dp = 8 !< Double precision parameter
end module mod_precision

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module VARIABLES:
!
!> Contains the global simulation parameters.
!<
module mod_variables
  use mod_precision
  implicit none
  integer                                        :: nout_term_freq   !< Print terminal outputs every N timesteps
  integer                                        :: n_term_output    !< Number of terminal outputs
  integer                                        :: nrestart
  integer                                        :: nsource          !< Number of sources (geometrical, cooling, gravity, etc...)
  integer                                        :: slope_type       !< Slope limiter: 0 = first order
  integer                                        :: it
  integer                                        :: isolver          !< Hydro solver identifier
  integer, parameter                             :: isolver_exact=1  !< Exact hydro solver identifier
  integer, parameter                             :: isolver_roe  =2  !< Roe   hydro solver identifier
  integer, parameter                             :: isolver_hllc =3  !< HLLC  hydro solver identifier
  integer                                        :: debug_level      !< Ranges from 0 to 3
  integer, dimension(:,:), allocatable           :: ivar             !< Cyclic array for hydro fluxes
  integer, dimension(3,3)                        :: identity
  integer, dimension(3  )                        :: iodd
  integer, dimension(3  )                        :: ieven
  integer, dimension(3  )                        :: adim
  integer, dimension(3  )                        :: bdim
  integer, dimension(3  )                        :: cdim
  integer, dimension(3  )                        :: edim
  real(dp)                                       :: t                !< Time
  real(dp)                                       :: cn_hydro         !< Courant number for limiting hydro timestep
!   real(dp)                                       :: o                !< Real value for reconstruction order (= 1.0 or 2.0)
  real(dp)                                       :: dt
  real(dp)                                       :: tlim             !< Time limit for simulation
  logical                                        :: debug_l1         !< Debug level 1 if .true.
  logical                                        :: debug_l2         !< Debug level 2 if .true.
  logical                                        :: debug_l3         !< Debug level 3 if .true.
  logical                                        :: debug_l4         !< Debug level 4 if .true.
  logical                                        :: user_controlled  !< Pauses at the end of each timestep if .true.
  logical                                        :: master_cpu       !< .true. if cpu rank = 0
  integer , parameter                            :: nvar =   5       !< Number of conservative variables
  integer                                        :: nvar_tot         !< Total number of variables
  integer , parameter                            :: str  = 200       !< Standard length of strings
  character (len=str)                            :: liste            !< Name of namelist file
  character (len=str)                            :: libpath          !< Path for colortables and radiative cooling libraries
  character (len=5  )                            :: solver           !< Type of hydro solver: 'exact', 'roe' or 'hllc'
  character (len=4  )                            :: the_pill         !< Blue or red ? Choose the red pill to see how deep does the rabbit hole go
  character (len=str), dimension(:), allocatable :: terminal_outputs !< Array to hold terminal outputs
end module mod_variables

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module ARRAYS:
!
!> Contains the global conservative and primitive variable arrays.
!<
module mod_arrays
  use mod_precision
  implicit none
  real(dp), dimension(:,:,:,:  ), allocatable :: uold            !< Conservative variables array (old)
  real(dp), dimension(:,:,:,:  ), allocatable :: unew            !< Conservative variables array (new)
  real(dp), dimension(:,:,:,:,:), allocatable :: uint            !< Conservative variables array (intermediate)
  real(dp), dimension(:,:,:,:  ), allocatable :: usrc            !< Primitive variables array at \f$ r = r_{\theta} \f$
  real(dp), dimension(:,:,:,:,:), allocatable :: gradav         !< Average gradient of cell in direction 1
!   real(dp), dimension(:,:,:    ), allocatable :: cooling_source  !< Array to conserve radiative cooling source
!   logical , dimension(:,:,:    ), allocatable :: active_cell     !< Array which holds regions where hydro is solved
end module mod_arrays

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module CONSTANTS:
!
!> Contains all the physical constants and the code units.
!<
module mod_constants
  use mod_precision
  implicit none
  real(dp)            :: pi                          !< \f$ \pi \f$
  real(dp)            :: c                           !< Speed of light \f$ c \f$
  real(dp)            :: c2                          !< Square of the speed of light \f$ c^{2} \f$
  real(dp)            :: eV                          !< Electron volt \f$ eV \f$
  real(dp)            :: grav                        !< Gravitational constant \f$ G \f$
  real(dp)            :: hplanck                     !< Planck constant \f$ h \f$
  real(dp)            :: kb                          !< Boltzmann constant \f$ k_{B} \f$
  real(dp)            :: uma                         !< Atomic mass unit \f$ u \f$
  real(dp)            :: a_r                         !< Stefan-Boltzmann constant \f$ a_{R} \f$
  real(dp)            :: Lsun                        !< Solar luminosity \f$ L_{\odot} \f$
  real(dp)            :: Msun                        !< Solar mass \f$ M_{\odot} \f$
  real(dp)            :: Rsun                        !< Solar radius \f$ R_{\odot} \f$
  real(dp)            :: pc                          !< Parsec
  real(dp)            :: kpc                         !< Kiloparsec
  real(dp)            :: Myrs                        !< Mega years
  real(dp)            :: mu                          !< Atomic number
  real(dp)            :: yr                          !< Year
  real(dp)            :: au                          !< Astronomical unit
  real(dp)            :: kms                         !< Kilometre
  real(dp)            :: day                         !< Day
  real(dp)            :: koverm                      !< Bolztmann's constant over average atomic mass \f$ k_{B} / m_{\mathrm{av}}\f$
  real(dp)            :: g                           !< Ratio of specific heats \f$ \gamma = C_{P} / C_{V} \f$
  real(dp)            :: g1                          !< \f$ \gamma - 1 \f$
  real(dp)            :: g2                          !< \f$ \displaystyle \frac{\gamma}{\gamma - 1} \f$
  real(dp)            :: g3                          !< \f$ \displaystyle \frac{\gamma + 1}{2\gamma} \f$
  real(dp)            :: g4                          !< \f$ \displaystyle \frac{\gamma-1}{2\gamma} \f$
  real(dp)            :: g5                          !< \f$ \displaystyle \frac{2\gamma}{\gamma-1} \f$
  real(dp)            :: g6                          !< \f$ \displaystyle \frac{1}{\gamma} \f$
  real(dp)            :: g7                          !< \f$ \displaystyle \frac{2}{\gamma-1} \f$
  real(dp)            :: g8                          !< \f$ \displaystyle \frac{\gamma-1}{\gamma+1} \f$
  real(dp), parameter :: zero         = 0.00e+00_dp  !<  0
  real(dp), parameter :: one          = 1.00e+00_dp  !<  1
  real(dp), parameter :: two          = 2.00e+00_dp  !<  2
  real(dp), parameter :: three        = 3.00e+00_dp  !<  3
  real(dp), parameter :: four         = 4.00e+00_dp  !<  4
  real(dp), parameter :: five         = 5.00e+00_dp  !<  5
  real(dp), parameter :: six          = 6.00e+00_dp  !<  6
  real(dp), parameter :: seven        = 7.00e+00_dp  !<  7
  real(dp), parameter :: eight        = 8.00e+00_dp  !<  8
  real(dp), parameter :: nine         = 9.00e+00_dp  !<  9
  real(dp), parameter :: ten          = 1.00e+01_dp  !< 10
  real(dp), parameter :: half         = 0.50e+00_dp  !< 1/2
  real(dp), parameter :: quater       = 0.25e+00_dp  !< 1/4
  real(dp), parameter :: large_number = 1.00e+35_dp  !< A very large number
  real(dp), parameter :: small_number = 1.00e-35_dp  !< A very small number
end module mod_constants

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module MODGRAVITY:
!
!> Contains all the variables used for the treatment of gravity.
!<
module mod_gravity
  use mod_precision
  implicit none
  real(dp), dimension(:,:,:,:), allocatable :: gravity_force    !< Gravitational force
  real(dp), dimension(:,:,:  ), allocatable :: gravity_phi      !< Gravitational potential
  real(dp), dimension(:,:,:  ), allocatable :: gravity_phi_old  !< Old gravitational potential
  real(dp), dimension(:,:    ), allocatable :: gravity_params   !< Gravity parameters
  integer                                   :: gravity_type     !< Type of gravity: 1=constant field, 2=point masses, 3=self gravity
  integer                                   :: gravity_npmass   !< Number of gravitational point masses
  integer                                   :: nitetot_grav
  logical                                   :: gravity          !< Include gravity if .true.
  logical                                   :: isolated_system  !< Isolated boundary conditions if .true.
  logical                                   :: gravity_precond  !< Use matrix preconditionning if .true.
  real(dp)                                  :: cn_gravity       !< Courant number for gravity timestep
  real(dp)                                  :: gravity_eps      !< Convergence criterion for conjugate gradient
end module mod_gravity

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module GRID:
!
!> Contains all the parameters of the grid including its dimensions and geometry.
!<
module mod_grid
  use mod_precision
  implicit none
  integer                :: ndim         !< Number of dimensions (1, 2 or 3)
  integer                :: geometry     !< Grid geometry: 1=cartesian, 2=spherical, 3=cylindrical
  integer                :: nghost       !< Number of ghost cells
  integer                :: nmin1        !< Lower dimension 1 of variable arrays
  integer                :: nmin2        !< Lower dimension 2 of variable arrays
  integer                :: nmin3        !< Lower dimension 3 of variable arrays
  integer                :: nmax1        !< Upper dimension 1 of variable arrays
  integer                :: nmax2        !< Upper dimension 2 of variable arrays
  integer                :: nmax3        !< Upper dimension 3 of variable arrays
  integer                :: boundc_1     !< Type of lower boundary condition in direction 1
  integer                :: boundc_2     !< Type of upper boundary condition in direction 1
  integer                :: boundc_3     !< Type of lower boundary condition in direction 2
  integer                :: boundc_4     !< Type of upper boundary condition in direction 2
  integer                :: boundc_5     !< Type of lower boundary condition in direction 3
  integer                :: boundc_6     !< Type of upper boundary condition in direction 3
  character (len=1)      :: geom1        !< Coordinate in direction 1 \f$(x, r \mathrm{or} r)\f$
  character (len=1)      :: geom2        !< Coordinate in direction 2 \f$(y, \theta \mathrm{or} \phi)\f$
  character (len=1)      :: geom3        !< Coordinate in direction 3 \f$(z, \phi \mathrm{or} z)\f$
  integer , dimension(3) :: nx           !< Total number of cells in directions 1, 2 and 3
  integer , dimension(3) :: nxloc        !< Local number of cells in directions 1, 2 and 3
  integer , dimension(3) :: nxlower      !< Lower number of cells which processor holds
  integer , dimension(3) :: nxupper      !< Upper number of cells which processor holds
  real(dp), dimension(3) :: xgcen        !< Geometrical center of the grid
  real(dp), dimension(3) :: lbox         !< Size of the computational volume
  logical , dimension(6) :: same_bc      !< The type of boundary condition is constant across the face if .true.
end module mod_grid

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module GRIDARRAYS:
!
!> Contains all the coordinate and boundary condition arrays.
!<
module mod_gridarrays
  use mod_precision
  implicit none
  real(dp), dimension(:      ), allocatable :: x1           !< Interface coordinate array direction 1
  real(dp), dimension(:      ), allocatable :: x2           !< Interface coordinate array direction 2
  real(dp), dimension(:      ), allocatable :: x3           !< Interface coordinate array direction 3
!   real(dp), dimension(:      ), allocatable :: x1glob       !< Global interface coordinate array direction 1
!   real(dp), dimension(:      ), allocatable :: x2glob       !< Global interface coordinate array direction 2
!   real(dp), dimension(:      ), allocatable :: x3glob       !< Global interface coordinate array direction 3
!   real(dp), dimension(:      ), allocatable :: xn1glob      !< Global nodal coordinate array direction 1
!   real(dp), dimension(:      ), allocatable :: xn2glob      !< Global nodal coordinate array direction 2
!   real(dp), dimension(:      ), allocatable :: xn3glob      !< Global nodal coordinate array direction 3
  real(dp), dimension(:      ), allocatable :: xn1          !< Nodal coordinate array direction 1
  real(dp), dimension(:      ), allocatable :: xn2          !< Nodal coordinate array direction 2
  real(dp), dimension(:      ), allocatable :: xn3          !< Nodal coordinate array direction 3
  real(dp), dimension(:      ), allocatable :: xh1          !< Median coordinate array direction 1
  real(dp), dimension(:      ), allocatable :: xh2          !< Median coordinate array direction 2
  real(dp), dimension(:      ), allocatable :: xh3          !< Median coordinate array direction 3
  real(dp), dimension(:      ), allocatable :: rnt          !< \f$ \theta_{\star} \f$ coordinate array
  real(dp), dimension(:      ), allocatable :: ths          !< \f$ \theta_{\star} \f$ coordinate array
  real(dp), dimension(:      ), allocatable :: b            !< \f$ \beta \f$ coordinate array
  integer , dimension(:,:    ), allocatable :: bc1          !< Type of boundary condition 1
  integer , dimension(:,:    ), allocatable :: bc2          !< Type of boundary condition 2
  integer , dimension(:,:    ), allocatable :: bc3          !< Type of boundary condition 3
  integer , dimension(:,:    ), allocatable :: bc4          !< Type of boundary condition 4
  integer , dimension(:,:    ), allocatable :: bc5          !< Type of boundary condition 5
  integer , dimension(:,:    ), allocatable :: bc6          !< Type of boundary condition 6
  real(dp), dimension(:,:,:,:), allocatable :: inflow1      !< Array which holds the inflow boundary condition 1
  real(dp), dimension(:,:,:,:), allocatable :: inflow2      !< Array which holds the inflow boundary condition 2
  real(dp), dimension(:,:,:,:), allocatable :: inflow3      !< Array which holds the inflow boundary condition 3
  real(dp), dimension(:,:,:,:), allocatable :: inflow4      !< Array which holds the inflow boundary condition 4
  real(dp), dimension(:,:,:,:), allocatable :: inflow5      !< Array which holds the inflow boundary condition 5
  real(dp), dimension(:,:,:,:), allocatable :: inflow6      !< Array which holds the inflow boundary condition 6
  real(dp), dimension(:,:,:,:), allocatable :: cell_area    !< Array which holds the face areas of all cells
  real(dp), dimension(:,:,:  ), allocatable :: cell_volume  !< Array which holds the volume of all cells
end module mod_gridarrays

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module MODMPI:
!
!> Contains all the MPI variables and the 'mpi.h' header file.
!<
module mod_mpi
  implicit none
#if MPI==1
  include "mpif.h"                                          ! Include MPI header file
  integer                             :: mpi_real_dp        !< MPI variable type accounting for variable precision
  integer                             :: mpi_min_dp         !< Minimum reduction function
  integer                             :: mpi_max_dp         !< Maximum reduction function
  integer                             :: mpi_sum_dp         !< Sum reduction function
  integer                             :: ierror             !< MPI error variable
  integer                             :: world              !< MPI world variable
  integer, dimension(mpi_status_size) :: mpistat            !< Array for MPI status
#endif
  integer                             :: nproc              !< Total number of processors
  integer                             :: rank               !< Rank ID of processor
  integer, dimension(              3) :: ncpu               !< Number of cpus in directions 1, 2 and 3
  integer, dimension(              6) :: face_neighbours    !< Arrays to hold neighbouring processors
  integer, dimension(              8) :: corner_neighbours  !< Arrays to hold neighbouring processors
  integer, dimension(             12) :: edge_neighbours    !< Arrays to hold neighbouring edge processors
end module mod_mpi

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module OUTPUT:
!
!> Contains all the variables used for writing results to disk.
!<
module mod_output
  use mod_precision
  use mod_variables, only : str
  implicit none
  real(dp) :: t_bin_output             !< Time at which next cube dump will occur
  real(dp) :: t_bin_step               !< Time interval between cube dumps
  real(dp) :: t_ext_output             !< Time at which next extras dump will occur
  real(dp) :: t_ext_step               !< Time interval between extras dumps
  integer  :: dt_bin_interval          !< Dump a data cube every N timesteps (other outputs)
  integer  :: k_bin_output             !< Cube dump number
  integer  :: k_ext_output             !< Extras dump number
  integer  :: nout_bin_tot             !< Total number of cube dumps
  integer  :: nout_ext_tot             !< Total number of extras dumps
  integer  :: outputs_interval         !< 1=regular in time; 2=every N timesteps
end module mod_output

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module RADIATIVE_TRANSFER:
!
!> Contains all the variables for radiative transfer.
!! The radiative transfer model is the explicit reduced speed of light scheme.
!<
module mod_radiative_transfer
  use mod_precision
  implicit none
  logical :: radiation
  integer :: ngroups
end module mod_radiative_transfer

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module THERMODYNAMICS:
!
!> Contains all the variables which govern the gas physics.
!<
module mod_thermodynamics
  use mod_precision
  use mod_variables, only : str
  implicit none
  real(dp), dimension(:), allocatable :: logt        !< Array to hold the temperatures from the cooling curve
  real(dp), dimension(:), allocatable :: loge        !< Array to hold the cooling rates from the cooling curve
  real(dp)                            :: cfactor     !< Scaling factor use in radiative cooling
  real(dp)                            :: visc        !< Artificial viscosity diffusion factor
  real(dp)                            :: cavlim      !< Cavitation limit (fraction of gas kinetic energy)
  real(dp)                            :: pcav        !< Cavitation pressure limit
  real(dp)                            :: dcav        !< Cavitation density limit
  real(dp)                            :: rhomin      !< Minimum density
  real(dp)                            :: pext        !< Pressure of external medium
  real(dp)                            :: dext        !< Density of external medium
  real(dp)                            :: tclow       !< Lower temperature limit for cooling curve
  real(dp)                            :: tchigh      !< Upper temperature limit for cooling curve
  real(dp)                            :: deltatc     !< Regular logT step in temperature for cooling curve
  real(dp)                            :: cn_cooling  !< Cooling courant number
  logical                             :: cooling     !< Include radiative cooling if .true.
  logical                             :: viscosity   !< Include artificial viscosity if .true.
  integer                             :: ncool       !< Number of points in the cooling curve
  character (len=str)                 :: coolcurve   !< Name of file containing cooling curve
end module mod_thermodynamics

! !###################################################################################################
! !###################################################################################################
! !###################################################################################################
! 
! !  Module TIMESTEP:
! !
! !> Contains the variables for timestep computation.
! !<
! module mod_timestep
!   use mod_precision
!   implicit none
!   integer  :: hlimi
!   integer  :: hlimj
!   integer  :: hlimk
!   integer  :: climi
!   integer  :: climj
!   integer  :: climk
!   integer  :: glimi
!   integer  :: glimj
!   integer  :: glimk
!   integer  :: limith
!   real(dp) :: dt               !< Timestep
! end module mod_timestep

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module UNITS:
!
!> Contains all the physical constants and the code units.
!<
module mod_units
  use mod_precision
  implicit none
  real(dp) :: length    !< Length      unit
  real(dp) :: mass      !< Mass        unit
  real(dp) :: time      !< Time        unit
  real(dp) :: energy    !< Energy      unit
  real(dp) :: density   !< Density     unit
  real(dp) :: pressure  !< Pressure    unit
  real(dp) :: velocity  !< Velocity    unit
  real(dp) :: degree    !< Temperature unit
end module mod_units
