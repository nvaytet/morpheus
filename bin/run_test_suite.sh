#!/bin/bash

#######################################################################
#
# Script to run the MORPHEUS test suite
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
#
# Usage:
#   ./run_test_suite.sh
#    This is the default usage: no MPI, no OPENMP, using
#    gnuplot visualization.
#
# Run the suite in parallel (on 4 cpus):
#   ./run_test_suite.sh -p 4
#
# Run the suite using OPENMP (4 threads):
#   ./run_test_suite.sh -o 4
#
# Run the suite without deleting results:
#   ./run_test_suite.sh -r
#
# Run the suite using python visualization:
#   ./run_test_suite.sh -y
#
# ---------------------------------------------------------------------
#  Morpheus is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  
#  Morpheus is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------
#######################################################################


#######################################################################
# Determine if test suite is to be run using MPI or OPENMP
#######################################################################
MPI=0;
NCPU=1;
OPENMP=0;
DELDATA=true;
USEPYTHON=0;
SELECTTEST=false;
VERBOSE=false;
while getopts "o:p:t:ryv" OPTION; do
   case $OPTION in
      o)
         OPENMP=1;
         NTHREADS=$OPTARG;
      ;;
      p)
         MPI=1;
         NCPU=$OPTARG;
      ;;
      r)
         DELDATA=false;
      ;;
      t)
         SELECTTEST=true;
         TESTNUMBER=$OPTARG;
      ;;
      y)
         USEPYTHON=1;
      ;;
      v)
         VERBOSE=true;
      ;;
   esac
done

#######################################################################
# Setup paths and commands
#######################################################################
BIN_DIRECTORY=$(pwd);

length=${#BIN_DIRECTORY};
icut=`expr $length - 4`;

BASE_DIRECTORY="${BIN_DIRECTORY:0:${icut}}";

TEST_DIRECTORY="${BASE_DIRECTORY}/test";
VISU_DIR_FORTRAN="${BASE_DIRECTORY}/vis/fortran";
VISU_DIR_PYTHON="${BASE_DIRECTORY}/vis/python";
DELETE_SOURCES="rm -f setup.o extras.o";
RETURN_TO_BIN="cd ${BIN_DIRECTORY}";
LOGFILE="${TEST_DIRECTORY}/test_suite.log";
echo -n > ${LOGFILE};

if [ ${MPI} -eq 1 ]; then
   RUN_TEST="mpirun -np ${NCPU} ${BIN_DIRECTORY}/morpheus";
else
   RUN_TEST="${BIN_DIRECTORY}/morpheus";
fi

#######################################################################
# Set environment variables
#######################################################################
export PYTHONPATH=$PYTHONPATH:${VISU_DIR_PYTHON};
export OMP_NUM_THREADS=${NTHREADS};

#######################################################################
# Print start banner
#######################################################################
echo "#####################################";
echo "#####################################" >> ${LOGFILE};
echo "Running MORPHEUS automatic test suite";
echo "Running MORPHEUS automatic test suite" >> ${LOGFILE};
echo "#####################################";
echo "#####################################" >> ${LOGFILE};

#######################################################################
# List of tests
#######################################################################
testlist[0]="sod-tube";
testlist[1]="sedov1d-spherical";
testlist[2]="primakoff";
testlist[3]="sedov2d-cartesian";
testlist[4]="sedov2d-spherical";
testlist[5]="sedov3d-cartesian";
testlist[6]="sedov3d-cylindrical";
testlist[7]="implosion";
testlist[8]="explosion";
testlist[9]="noh";
testlist[10]="kelvin-helmholtz";
testlist[11]="rayleigh-taylor";

ntests=${#testlist[@]};
ntestsfull=$ntests;

#######################################################################
# Select particular test if this was asked by user
#######################################################################
if $SELECTTEST ; then

   # Split test selection with commas
   s1=$(echo $TESTNUMBER | sed 's/,/ /'g);
   testsegs=( $s1 );
   nseg=${#testsegs[@]};
   
   # Search for dashes in individual segments
   ntests=0;
   for ((n=0;n<$nseg;n++)); do
      dashsearch=$(echo ${testsegs[n]} | grep '-');
      if [ ${#dashsearch} -gt 0 ] ; then
         istart=$(echo ${testsegs[n]} | cut -d '-' -f1);
         iend=$(echo ${testsegs[n]} | cut -d '-' -f2);
         is=$((istart - 1));
         ie=$((iend - 1));
         iep1=$(($ie + 1));
         for ((j=$is;j<$iep1;j++)); do
            if [ ${j} -ge 0 ] && [ ${j} -lt $ntestsfull ] ; then
               testnum[${ntests}]=$j;
               ntests=$((ntests + 1));
            else
               echo "Selected test ${j} does not exist! Ignoring test";
               echo "Selected test ${j} does not exist! Ignoring test" >> ${LOGFILE};
            fi
         done
      else
         # No dash, just include test in list
         j=$((${testsegs[n]} - 1));
         if [ ${j} -ge 0 ] && [ ${j} -lt $ntestsfull ] ; then
            testnum[${ntests}]=$j;
            ntests=$((ntests + 1));
         else
            echo "Selected test ${j} does not exist! Ignoring test";
            echo "Selected test ${j} does not exist! Ignoring test" >> ${LOGFILE};
         fi
      fi
   done

else

   # Include all tests by default
   for ((n=0;n<$ntests;n++)); do
      testnum[n]=$n;
   done
   
fi

#######################################################################
# Write list of tests
#######################################################################
echo "Will perform the following tests:";
echo "Will perform the following tests:" >> ${LOGFILE};
for ((i=0;i<$ntests;i++)); do
   n=${testnum[i]};
   j=$(($i + 1));
   if [ $ntests -gt 9 ] && [ $j -lt 10 ] ; then
      echo " [ ${j}] ${testlist[n]}";
      echo " [ ${j}] ${testlist[n]}" >> ${LOGFILE};
   else
      echo " [${j}] ${testlist[n]}";
      echo " [${j}] ${testlist[n]}" >> ${LOGFILE};
   fi
done
echo "-------------------------------------";
echo "-------------------------------------" >> ${LOGFILE};

#######################################################################
# Prepare visualization software
#######################################################################
echo "Compiling visualization software";
echo "Compiling visualization software" >> ${LOGFILE};
cd ${VISU_DIR_FORTRAN};
if ${VERBOSE} ; then
   make clean |& tee -a ${LOGFILE};
   make |& tee -a ${LOGFILE};
else
   make clean &>> ${LOGFILE};
   make &>> ${LOGFILE};
fi

#######################################################################
# Clean bin directory
#######################################################################
echo "Initial cleanup";
echo "Initial cleanup" >> ${LOGFILE};
cd ${BIN_DIRECTORY};
if ${VERBOSE} ; then
   make clean |& tee -a ${LOGFILE};
else
   make clean &>> ${LOGFILE};
fi
echo "-------------------------------------";
echo "-------------------------------------" >> ${LOGFILE};

#######################################################################
# Loop through all tests
#######################################################################
itest=0;
for ((i=0;i<$ntests;i++)); do

   n=${testnum[i]};
   itest=$(($itest + 1));
   echo "Test ${itest}/${ntests}: ${testlist[n]}";
   echo "Test ${itest}/${ntests}: ${testlist[n]}" >> ${LOGFILE};
   echo "Compiling source";
   echo "Compiling source" >> ${LOGFILE};
   if ${VERBOSE} ; then
      make PROBLEM=${TEST_DIRECTORY}/${testlist[n]} MPI=${MPI} OPENMP=${OPENMP} -j ${NCPU} |& tee -a ${LOGFILE};
   else
      make PROBLEM=${TEST_DIRECTORY}/${testlist[n]} MPI=${MPI} OPENMP=${OPENMP} -j ${NCPU} &>> ${LOGFILE};
   fi
   cd ${TEST_DIRECTORY}/${testlist[n]};
   rm -Rf output-*;
   echo "Running test";
   echo "Running test" >> ${LOGFILE};
   if ${VERBOSE} ; then
      { time ${RUN_TEST}; } |& tee -a ${testlist[n]}.log ${LOGFILE};
   else
      { time ${RUN_TEST}; } &> ${testlist[n]}.log;
      cat ${testlist[n]}.log >> ${LOGFILE};
   fi
   echo "Plotting results";
   echo "Plotting results" >> ${LOGFILE};
   if ${VERBOSE} ; then
      ./plot_${testlist[n]}.sh ${USEPYTHON} |& tee -a ${LOGFILE};
   else
      ./plot_${testlist[n]}.sh ${USEPYTHON} &>> ${LOGFILE};
   fi
   $RETURN_TO_BIN;
   $DELETE_SOURCES;
   echo "-------------------------------------";
   echo "-------------------------------------" >> ${LOGFILE};

done

#######################################################################
# Generate pdf document with test results
#######################################################################
echo "Compiling latex document";
echo "Compiling latex document" >> ${LOGFILE};
cd ${TEST_DIRECTORY};
latexfile="test_results";
echo "\documentclass[12pt]{article}" > ${latexfile}.tex;
echo "\usepackage{graphicx}" >> ${latexfile}.tex;
echo "\usepackage[colorlinks=true,linkcolor=blue]{hyperref}" >> ${latexfile}.tex;
echo "\topmargin -1.3in" >> ${latexfile}.tex;
echo "\textheight 10.1in" >> ${latexfile}.tex;
echo "\oddsidemargin -0.7in" >> ${latexfile}.tex;
echo "\evensidemargin -0.7in" >> ${latexfile}.tex;
echo "\textwidth 7.7in" >> ${latexfile}.tex;
echo >> ${latexfile}.tex;
echo "\title{MORPHEUS test suite}" >> ${latexfile}.tex;
echo "\date{\today}" >> ${latexfile}.tex;
echo "\author{${USER}}" >> ${latexfile}.tex;
echo >> ${latexfile}.tex;
echo "\begin{document}" >> ${latexfile}.tex;
echo >> ${latexfile}.tex;
echo "\maketitle" >> ${latexfile}.tex;
echo >> ${latexfile}.tex;
echo "\begin{table}[ht]" >> ${latexfile}.tex;
echo "\centering" >> ${latexfile}.tex;
echo "\caption{Test run summary using ${NCPU} processors}" >> ${latexfile}.tex;
echo "\begin{tabular}{|r|l|l|l|}" >> ${latexfile}.tex;
echo "\hline" >> ${latexfile}.tex;
echo "~ & Test name & Real time & User time \\\\" >> ${latexfile}.tex;
echo "\hline" >> ${latexfile}.tex;
for ((i=0;i<$ntests;i++)); do
   n=${testnum[i]};
   testrealtime=$(tail ${TEST_DIRECTORY}/${testlist[n]}/${testlist[n]}.log | grep real | cut -d 'l' -f2);
   testusertime=$(tail ${TEST_DIRECTORY}/${testlist[n]}/${testlist[n]}.log | grep user | cut -d 'r' -f2);
   itest=$(($i + 1));
   echo $itest "& \hyperref[fig-${testlist[n]}]{${testlist[n]}} &" ${testrealtime} "&" ${testusertime} "\\\\" >> ${latexfile}.tex;
done
echo "\hline" >> ${latexfile}.tex;
echo "\end{tabular}" >> ${latexfile}.tex;
echo "\end{table}" >> ${latexfile}.tex;
echo "\clearpage" >> ${latexfile}.tex;
echo >> ${latexfile}.tex;

for ((i=0;i<$ntests;i++)); do
   n=${testnum[i]};
   echo "\begin{figure}" >> ${latexfile}.tex;
   echo "\centering" >> ${latexfile}.tex;
   echo "\includegraphics[width=0.9\textwidth,height=0.9\textheight,keepaspectratio]{${TEST_DIRECTORY}/${testlist[n]}/${testlist[n]}.pdf}" >> ${latexfile}.tex;
   echo "\caption{${testlist[n]} test}" >> ${latexfile}.tex;
   echo "\label{fig-${testlist[n]}}" >> ${latexfile}.tex;
   echo "\end{figure}" >> ${latexfile}.tex;
   echo "\clearpage" >> ${latexfile}.tex;
   echo >> ${latexfile}.tex;
done
echo "\end{document}" >> ${latexfile}.tex;
if ${VERBOSE} ; then
   pdflatex ${latexfile} |& tee -a ${LOGFILE};
   pdflatex ${latexfile} |& tee -a ${LOGFILE};
   pdflatex ${latexfile} |& tee -a ${LOGFILE};
else
   pdflatex ${latexfile} &>> ${LOGFILE};
   pdflatex ${latexfile} &>> ${LOGFILE};
   pdflatex ${latexfile} &>> ${LOGFILE};
fi
rm -f ${latexfile}.log ${latexfile}.aux ${latexfile}.out ${latexfile}.tex;

#######################################################################
# Clean up
#######################################################################
if ${DELDATA} ; then
   for ((i=0;i<$ntests;i++)); do
      n=${testnum[i]};
      cd ${TEST_DIRECTORY}/${testlist[n]};
      rm -rf output-* ${testlist[n]}.log ${testlist[n]}.pdf;
   done
fi
cd $VISU_DIR_FORTRAN;
if ${VERBOSE} ; then
   make clean |& tee -a ${LOGFILE};
else
   make clean &>> ${LOGFILE};
fi
cd ${VISU_DIR_PYTHON};
rm -f *.pyc;
$RETURN_TO_BIN;
if ${VERBOSE} ; then
   make clean |& tee -a ${LOGFILE};
else
   make clean &>> ${LOGFILE};
fi
rm morpheus;
