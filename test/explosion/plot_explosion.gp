#---------------------------------------------------------------------
# File: plot_explosion.gp
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

reset
set view map

file1 = 'morpheus-0001/morpheus-0001-dxy.txt'
file2 = 'morpheus-0002/morpheus-0002-dxy.txt'
file3 = 'morpheus-0003/morpheus-0003-dxy.txt'
file4 = 'morpheus-0004/morpheus-0004-dxy.txt'
file5 = 'morpheus-0005/morpheus-0005-dxy.txt'
file6 = 'morpheus-0006/morpheus-0006-dxy.txt'

set xlabel 'Distance x (cm)'
set ylabel 'Distance y (cm)' offset 2.0

set xtics 0.0,0.5,1.5
set ytics 0.0,0.5,1.5 rotate by 90

set cbrange[0.08:0.21]

set term post enh color
set output 'explosion.ps'

set multiplot layout 2, 3

splot file1 with image
splot file2 with image
splot file3 with image
splot file4 with image
splot file5 with image
splot file6 with image

unset multiplot

unset output
