#---------------------------------------------------------------------
# File: plot_primakoff.gp
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

reset

file1 = 'output-00001/output-00001-dxz.txt'
file2 = 'output-00001/output-00001-vxz.txt'
file3 = 'output-00001/output-00001-pxz.txt'
file4 = 'output-00001/output-00001-txz.txt'
file5 = 'primakoff_solution.txt'

set xlabel 'Distance x (cm)'

unset key

set term post enh color
set output 'primakoff.ps'

u_T = `head -n 22 output-00001/output-00001-inf.txt | tail -n 1`
u_M = `head -n 24 output-00001/output-00001-inf.txt | tail -n 1`
u_L = `head -n 26 output-00001/output-00001-inf.txt | tail -n 1`
u_K = `head -n 28 output-00001/output-00001-inf.txt | tail -n 1`

set xrange[0.0:1.0e15]

set multiplot layout 2, 2

set ylabel 'Density (g/cm3)'
plot file1 u (($1)*u_L):(($3)*u_M/u_L**3) lt 6 lc 0, file5 u 1:2 w l lc 1 lt 1 lw 2
set ylabel 'Velocity (cm/s)'
plot file2 u (($1)*u_L):(($3)*u_L/u_T) lt 6 lc 0, file5 u 1:3 w l lc 1 lt 1 lw 2
set ylabel 'Pressure (g/cm/s2)'
plot file3 u (($1)*u_L):(($3)*u_M/(u_L*u_T**2)) lt 6 lc 0, file5 u 1:4 w l lc 1 lt 1 lw 2
set ylabel 'Temperature (K)'
plot file4 u (($1)*u_L):(($3)*u_K) lt 6 lc 0, file5 u 1:5 w l lc 1 lt 1 lw 2

unset multiplot

unset output
