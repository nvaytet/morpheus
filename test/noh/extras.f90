!---------------------------------------------------------------------------------------------------
! File: extras.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines extras_init(), extras() and extras_end()
!!
!! This file contains all the routines for the user to add extra operations specific to his
!! simulation. The routine extras_init() is called once at the start of the run and is used to
!! initialise all the extra variables. The extras() routine is called at every timestep before the
!! hydro computations. The extras_end() subroutine is call at the end of the program, if data files
!! need writing or closing.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine EXTRAS_INIT:
!
!> Initialise extra variables needed specific to the run.
!<
subroutine extras_init

  implicit none

  return

end subroutine extras_init

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine EXTRAS:
!
!> Additional subroutines specific to the run.
!<
subroutine extras

  use mod_gridarrays
  use mod_grid
  use mod_constants
  use mod_variables

  implicit none
  
  integer  :: i,j,k
  real(dp) :: r,angle,p1,u1,rho1
  
  p1 = 1.0e-06_dp
  u1 = one
  
  do k = nmin3,nmax3
     do j = nmin2,nmax2
        do i = 1,nghost
           r = sqrt(xn1(nxloc(1)+i)**2 + xn2(j)**2)
           rho1 = one + t / r
           angle = atan2(xn2(j),xn1(nxloc(1)+i))
           inflow2(1,i,j,k) =  rho1
           inflow2(2,i,j,k) = -u1*rho1*cos(angle)
           inflow2(3,i,j,k) = -u1*rho1*sin(angle)
           inflow2(4,i,j,k) =  zero
           inflow2(5,i,j,k) =  half*rho1*u1*u1 + p1/g1
        enddo
     enddo
  enddo

  do k = nmin3,nmax3
     do i = nmin1,nmax1
        do j = 1,nghost
           r = sqrt(xn1(i)**2 + xn2(nxloc(2)+j)**2)
           rho1 = one + t / r
           angle = atan2(xn2(nxloc(2)+j),xn1(i))
           inflow4(1,j,i,k) =  rho1
           inflow4(2,j,i,k) = -u1*rho1*cos(angle)
           inflow4(3,j,i,k) = -u1*rho1*sin(angle)
           inflow4(4,j,i,k) =  zero
           inflow4(5,j,i,k) =  half*rho1*u1*u1 + p1/g1
        enddo
     enddo
  enddo
  
  return

end subroutine extras

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine OUTPUT_EXTRAS_DATA:
!
!> Additional subroutines specific to the run.
!<
subroutine output_extras_data

  implicit none

  return

end subroutine output_extras_data

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine EXTRAS_END:
!
!> Finalise additional variables, routines, files, ...
!<
subroutine extras_end

  implicit none

  return

end subroutine extras_end
