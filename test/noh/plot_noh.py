#---------------------------------------------------------------------
# File: plot_noh.py
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

import read_morpheus_data
from pylab import *

fig = matplotlib.pyplot.figure()
ratio = 2.0
sizex = 15.0
fig.set_size_inches(sizex,ratio*sizex)

nruns = 3

ipanl = 0

for irun in range(nruns):
    
    data = read_morpheus_data.MorpheusData(ifile=irun+1)
    rho  = transpose(data.get_slice_12(type='rho',cut3=0))
    u    = transpose(data.get_slice_12(type='u',cut3=0))
    ux   = data.get_slice_12(type='u1',cut3=0)
    uy   = data.get_slice_12(type='u2',cut3=0)
    p    = transpose(data.get_slice_12(type='p',cut3=0))

    t = data.t[0]
    time = "%.2f" % t
    
    [nx,ny] = shape(rho)
    
    iskip = 3
    nn = (nx/iskip+1)*(ny/iskip+1)
    radi = zeros([nn])
    dens = zeros([nn])
    velo = zeros([nn])
    pres = zeros([nn])
    
    n = 0
    for i in range(0,nx,iskip):
        for j in range(0,ny,iskip):
            x = data.xn1glob[i]
            y = data.xn2glob[j]
            radi[n] = sqrt(x**2 + y**2)
            dens[n] = rho[i,j]
            theta = arctan2(y,x)
            velo[n] = ux[i,j]*cos(theta) + uy[i,j]*sin(theta)
            pres[n] = p[i,j]
            n = n + 1
    
    nz = 101
    rmax = sqrt(data.xn1glob[nx-1]**2 + data.xn2glob[ny-1]**2)
    r_solution = linspace(0.0,rmax,nz)
    d_solution = zeros([nz])
    u_solution = zeros([nz])
    p_solution = zeros([nz])
    rshock = t / 3.0    
    for i in range(nz):
        if r_solution[i] > rshock:
            d_solution[i] = 1.0 + t / r_solution[i]
            u_solution[i] = -1.0
            p_solution[i] = 0.0
        else:
            d_solution[i] = 16.0
            u_solution[i] = 0.0
            p_solution[i] = 16.0/3.0

    dmap = subplot(6,3,irun+1)
    dmap.imshow(rho,origin='lower',extent=[data.x1glob[0],data.x1glob[data.nx[0]],data.x2glob[0],data.x2glob[data.nx[1]]],interpolation='None',cmap='jet')
    dmap.set_xlabel('Distance x')
    dmap.set_ylabel('Distance y')
    dmap.text(0.96,0.91,'t = '+time+'s',color='w',ha='right',va='bottom',transform = dmap.transAxes)
    
    dprof = subplot(6,3,irun+4)
    dprof.plot(radi[:n],dens[:n],'o',markeredgecolor='b',markerfacecolor='None')
    dprof.plot(r_solution,d_solution,color='r')
    dprof.set_xlabel('Distance r')
    dprof.set_ylabel('Density')
    
    umap = subplot(6,3,irun+7)
    umap.imshow(u,origin='lower',extent=[data.x1glob[0],data.x1glob[data.nx[0]],data.x2glob[0],data.x2glob[data.nx[1]]],interpolation='None',cmap='jet')
    umap.set_xlabel('Distance x')
    umap.set_ylabel('Distance y')
    umap.text(0.96,0.91,'t = '+time+'s',color='w',ha='right',va='bottom',transform = umap.transAxes)
    
    uprof = subplot(6,3,irun+10)
    uprof.plot(radi[:n],velo[:n],'o',markeredgecolor='b',markerfacecolor='None')
    uprof.plot(r_solution,u_solution,color='r')
    uprof.set_xlabel('Distance r')
    uprof.set_ylabel('Velocity')
    
    pmap = subplot(6,3,irun+13)
    pmap.imshow(p,origin='lower',extent=[data.x1glob[0],data.x1glob[data.nx[0]],data.x2glob[0],data.x2glob[data.nx[1]]],interpolation='None',cmap='jet')
    pmap.set_xlabel('Distance x')
    pmap.set_ylabel('Distance y')
    pmap.text(0.96,0.91,'t = '+time+'s',color='w',ha='right',va='bottom',transform = pmap.transAxes)
    
    pprof = subplot(6,3,irun+16)
    pprof.plot(radi[:n],pres[:n],'o',markeredgecolor='b',markerfacecolor='None')
    pprof.plot(r_solution,p_solution,color='r')
    pprof.set_xlabel('Distance r')
    pprof.set_ylabel('Pressure')
    
savefig('noh.pdf',bbox_inches='tight')
