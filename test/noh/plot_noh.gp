#---------------------------------------------------------------------
# File: plot_noh.gp
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

reset
set view map

file1 = 'output-00001/output-00001-dxy.txt'
file2 = 'output-00001/output-00001-vxy.txt'
file3 = 'output-00001/output-00001-pxy.txt'
file4 = 'output-00002/output-00002-dxy.txt'
file5 = 'output-00002/output-00002-vxy.txt'
file6 = 'output-00002/output-00002-pxy.txt'
file7 = 'output-00003/output-00003-dxy.txt'
file8 = 'output-00003/output-00003-vxy.txt'
file9 = 'output-00003/output-00003-pxy.txt'

t1 = `head -n 14 output-00001/output-00001-inf.txt | tail -n 1`
t2 = `head -n 14 output-00002/output-00002-inf.txt | tail -n 1`
t3 = `head -n 14 output-00003/output-00003-inf.txt | tail -n 1`

set xrange[0:1.5]
rs1 = t1 / 3.0
rs2 = t2 / 3.0
rs3 = t3 / 3.0
d1(x) = x>rs1 ? 1.0 + t1 / x : 16.0
d2(x) = x>rs2 ? 1.0 + t2 / x : 16.0
d3(x) = x>rs3 ? 1.0 + t3 / x : 16.0
u1(x) = x>rs1 ? -1.0 : 0.0
u2(x) = x>rs2 ? -1.0 : 0.0
u3(x) = x>rs3 ? -1.0 : 0.0
p1(x) = x>rs1 ? 0.0 : 16.0/3.0
p2(x) = x>rs2 ? 0.0 : 16.0/3.0
p3(x) = x>rs3 ? 0.0 : 16.0/3.0

set xlabel 'Distance x'
unset key

set xtics 0.0,0.5,1.0

set term post enh color portrait font 'Helvetica,8'
set output 'noh.ps'

set multiplot layout 6, 3
set size square

set ylabel 'Distance y' offset 2.0
set ytics 0.0,0.5,1.0 rotate by 90
set xrange[0:1.0]
splot file1 with image
splot file4 with image
splot file7 with image

set ylabel 'Density' offset 2.0
set ytics 0.0,5.0,20.0 rotate by 90
set xrange[0:1.5]
plot file1 u (sqrt(($1)**2+($2)**2)):3 every 20 pt 6 lc 0, d1(x) w l lc 1 lt 1
plot file4 u (sqrt(($1)**2+($2)**2)):3 every 20 pt 6 lc 0, d2(x) w l lc 1 lt 1
plot file7 u (sqrt(($1)**2+($2)**2)):3 every 20 pt 6 lc 0, d3(x) w l lc 1 lt 1

set ylabel 'Distance y' offset 2.0
set ytics 0.0,0.5,1.0 rotate by 90
set xrange[0:1.0]
splot file2 with image
splot file5 with image
splot file8 with image

set ylabel 'Velocity' offset 2.0
set xrange[0:1.5]
set ytics -1.0,0.5,0.0 rotate by 90
plot file2 u (sqrt(($1)**2+($2)**2)):(-($3)) every 20 pt 6 lc 0, u1(x) w l lc 1 lt 1
plot file5 u (sqrt(($1)**2+($2)**2)):(-($3)) every 20 pt 6 lc 0, u2(x) w l lc 1 lt 1
plot file8 u (sqrt(($1)**2+($2)**2)):(-($3)) every 20 pt 6 lc 0, u3(x) w l lc 1 lt 1

set ylabel 'Distance y' offset 2.0
set ytics 0.0,0.5,1.0 rotate by 90
set xrange[0:1.0]
splot file3 with image
splot file6 with image
splot file9 with image

set ylabel 'Pressure' offset 2.0
set xrange[0:1.5]
set ytics 0.0,2.0,8.0 rotate by 90
plot file3 u (sqrt(($1)**2+($2)**2)):3 every 20 pt 6 lc 0, p1(x) w l lc 1 lt 1
plot file6 u (sqrt(($1)**2+($2)**2)):3 every 20 pt 6 lc 0, p2(x) w l lc 1 lt 1
plot file9 u (sqrt(($1)**2+($2)**2)):3 every 20 pt 6 lc 0, p3(x) w l lc 1 lt 1

unset multiplot

unset output
