!---------------------------------------------------------------------------------------------------
! File: setup.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines setup() and physics()
!!
!! This file contains the routines which set up the initial conditions of the problem.
!! The namelist of parameters is read in, the grid is set up and the initial state of the variables
!! is given. The physical constants and code units are also defined.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine SETUP:
!
!> Initialises all the variables, arrays, grid, etc... for the simulation.
!! The routine reads the 'INIT' parameters from the namelist.
!! It then calls a 'safety_check' to ensure consistency of dimensions and dumping
!! variables.
!! It calls the 'domain_decomposition' to evenly distribute computational volume among the
!! nodes.
!! It initialises the physical constants and units through the call to 'physics'.
!! It sets up the array dimensions and the grid.
!! It then sets the initial conditions for the conservative variables in the grid which is
!! specific to each simulation.
!! The boundary conditions are then set.
!! Finally the time intervals between cube/image dumps are set.
!<
subroutine setup

  use mod_thermodynamics
  use mod_gridarrays
  use mod_variables
  use mod_constants
  use mod_gravity
  use mod_arrays
  use mod_output
  use mod_units
  use mod_grid
  use mod_mpi

  implicit none

  integer  :: h,i,j,k,x_r,y_r,z_r,nmax
  real(dp) :: rho1,rho2,u1,u2,p1,p2,t1,t2
  real(dp) :: xfact1,xfact2,xfact3,cen1,cen2,cen3
  real(dp) :: rad,dist,fract,xinpt,yinpt,zinpt,rhoav,uav,dist1,pert_vx,pert_vy,pert,l

  namelist/INIT/cn_hydro,cn_cooling,visc,slope_type,solver,cooling,coolcurve,viscosity, &
                gravity,g,nx,nproc,lbox,tlim,rho1,rho2,u1,u2,p1,p2,t1,t2,ndim,geometry, &
                rhomin,boundc_1,boundc_2,boundc_3,boundc_4,boundc_5,boundc_6,gravity_type

  namelist/CODEUNITS/time,mass,length,degree

  if(debug_l1) write(*,*) 'begin SETUP'

  ! read init namelist
  if(debug_l1) write(*,*) 'Reading setup parameters from namelist'
  open  (35,file=trim(liste))
  rewind(35                 )
  read  (35,nml=INIT        )
  read  (35,nml=CODEUNITS   )
  close (35                 )

  ! grid setup
  cen1 = half  ! grid direction 1 centre
  cen2 = half  ! grid direction 2 centre
  cen3 = half  ! grid direction 3 centre

  xfact1 = one ! size increase factor from one cell to the next in direction 1
  xfact2 = one ! size increase factor from one cell to the next in direction 2
  xfact3 = one ! size increase factor from one cell to the next in direction 3

  call domain_setup(cen1,cen2,cen3,xfact1,xfact2,xfact3)

  ! Initialise start time
  t = zero

  call common_setup1

  ! Setup medium
  if(debug_l1) write(*,*) 'Initialising gas values in grid medium'

  pert = 0.05_dp
  l    = half*lbox(1)
  rad  = 0.25_dp
  nmax = 20

  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)

           pert_vx = zero ! pert*sin(two*pi*xn1(i)/l)
           pert_vy = pert*sin(two*pi*xn1(i)/l)
!           pert_vx = pert*(rand(0)-half)
!           pert_vy = pert*(rand(0)-half)

           unew(1,i,j,k) = rho1
           unew(2,i,j,k) = unew(1,i,j,k)*u1*(one+pert_vx)
           unew(3,i,j,k) = unew(1,i,j,k)*pert_vy
           unew(4,i,j,k) = zero
           unew(5,i,j,k) = half*(unew(2,i,j,k)*unew(2,i,j,k)+unew(3,i,j,k)*unew(3,i,j,k)+unew(4,i,j,k)*unew(4,i,j,k))/unew(1,i,j,k) + p1/g1

        enddo
     enddo
  enddo

  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)

           pert_vx = zero ! pert*sin(two*pi*xn1(i)/l)
           pert_vy = pert*sin(two*pi*xn1(i)/l)

           if(abs(xn2(j)) < 1.2_dp*rad)then
              fract = zero
              do y_r = 1,nmax
                 yinpt = x2(j-1) + ((real(y_r,dp)-half)*(one/real(nmax,dp)))*(x2(j)-x2(j-1))
                 if(abs(yinpt) <= rad) fract = fract + (one/(real(nmax,dp)))
              enddo
              if(fract > zero)then
                 if(fract >= one)then
                    unew(1,i,j,k) = rho2
                    unew(2,i,j,k) = unew(1,i,j,k)*u2*(one+pert_vx)
                    unew(3,i,j,k) = unew(1,i,j,k)*pert_vy
                    unew(4,i,j,k) = zero
                    unew(5,i,j,k) = half*(unew(2,i,j,k)*unew(2,i,j,k)+unew(3,i,j,k)*unew(3,i,j,k)+unew(4,i,j,k)*unew(4,i,j,k))/unew(1,i,j,k) + p1/g1
                 else
                    rhoav = fract*rho2 + (one-fract)*rho1
                    uav   = fract*u2   + (one-fract)*u1
                    unew(1,i,j,k) = rhoav
                    unew(2,i,j,k) = unew(1,i,j,k)*uav*(one+pert_vx)
                    unew(3,i,j,k) = unew(1,i,j,k)*pert_vy
                    unew(4,i,j,k) = zero
                    unew(5,i,j,k) = half*(unew(2,i,j,k)*unew(2,i,j,k)+unew(3,i,j,k)*unew(3,i,j,k)+unew(4,i,j,k)*unew(4,i,j,k))/unew(1,i,j,k) + p1/g1
                 endif
              endif
           endif

        enddo
     enddo
  enddo

  ! Setup boundary conditions:
  !    1 : free-flow
  !    2 : reflexive
  !    3 : periodic
  !    4 : inflow
  !
  bc1 = boundc_1   ! left   boundary condition (lower x)
  bc2 = boundc_2   ! right  boundary condition (upper x)
  bc3 = boundc_3   ! bottom boundary condition (lower y)
  bc4 = boundc_4   ! top    boundary condition (upper y)
  bc5 = boundc_5   ! front  boundary condition (lower z)
  bc6 = boundc_6   ! back   boundary condition (upper z)

  call common_setup2

  if(debug_l1) write(*,*) 'end   SETUP',rank

  return

end subroutine setup
