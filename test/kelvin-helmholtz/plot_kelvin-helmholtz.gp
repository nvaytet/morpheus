#---------------------------------------------------------------------
# File: plot_kelvin-helmholtz.gp
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

reset
set view map

file1 = 'output-00001/output-00001-dxy.txt'
file2 = 'output-00002/output-00002-dxy.txt'
file3 = 'output-00003/output-00003-dxy.txt'
file4 = 'output-00004/output-00004-dxy.txt'
file5 = 'output-00005/output-00005-dxy.txt'
file6 = 'output-00006/output-00006-dxy.txt'

set xlabel 'Distance x (cm)'
set ylabel 'Distance y (cm)' offset 2.0

set xtics -0.5,0.5,0.5
set ytics -0.4,0.2,0.4 rotate by 90

set term post enh color portrait
set output 'kelvin-helmholtz.ps'

set multiplot layout 3, 2

splot file1 with image
splot file2 with image
splot file3 with image
splot file4 with image
splot file5 with image
splot file6 with image

unset multiplot

unset output
