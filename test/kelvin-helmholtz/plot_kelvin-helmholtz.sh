#!/bin/bash

USEPYTHON=$1;

testname="kelvin-helmholtz";

if [ ${USEPYTHON} -eq 1 ] ; then

    python plot_${testname}.py;
    
else

    ../../vis/fortran/visualize 1 6;
    gnuplot plot_${testname}.gp;
    ps2pdf ${testname}.ps;

fi

exit;