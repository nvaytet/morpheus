#---------------------------------------------------------------------
# File: plot_sedovxyz.py
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

import read_morpheus_data
from pylab import *

fig = matplotlib.pyplot.figure()
ratio = 1.0
sizex = 12.0
fig.set_size_inches(sizex,ratio*sizex)

nruns = 6

for n in range(nruns):

    data = read_morpheus_data.MorpheusData(ifile=n+1)
    rho  = transpose(data.get_slice_12(type='rho',cut3=0))
    t    = data.t[0]
    time = "%.2f" % t

    density = subplot(3,2,n+1)
    density.imshow(rho,origin='lower',extent=[data.x1glob[0],data.x1glob[data.nx[0]],data.x2glob[0],data.x2glob[data.nx[1]]],interpolation='None',cmap='jet')
    density.set_xlabel('Distance x (cm)')
    density.set_ylabel('Distance y (cm)')
    density.text(0.96,0.91,'t = '+time+'s',color='w',ha='right',va='bottom',transform = density.transAxes)
    
savefig('kelvin-helmholtz.pdf',bbox_inches='tight')
