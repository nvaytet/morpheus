#---------------------------------------------------------------------
# File: plot_sedovxyz.py
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

reset
set view map

file1 = 'output-00001/output-00001-dxy.txt'
file2 = 'output-00001/output-00001-vxy.txt'
file3 = 'output-00001/output-00001-pxy.txt'
file4 = 'output-00002/output-00002-dxy.txt'
file5 = 'output-00002/output-00002-vxy.txt'
file6 = 'output-00002/output-00002-pxy.txt'
file7 = 'output-00003/output-00003-dxy.txt'
file8 = 'output-00003/output-00003-vxy.txt'
file9 = 'output-00003/output-00003-pxy.txt'

set xlabel 'Distance x (cm)'
set ylabel 'Distance y (cm)' offset 2.0

set xtics -0.4,0.2,0.4
set ytics -0.5,0.5,0.5 rotate by 90

set term post enh color portrait font 'Helvetica,10'
set output 'sedov2d-cartesian.ps'

set multiplot layout 3,3

set label at  0.0,0.85 'Density' center
splot file1 with image
unset label
set label at  0.0,0.85 'Velocity' center
splot file2 with image
unset label
set label at  0.0,0.85 'Pressure' center
splot file3 with image
unset label
set label at  0.0,0.85 'Density' center
splot file4 with image
unset label
set label at  0.0,0.85 'Velocity' center
splot file5 with image
unset label
set label at  0.0,0.85 'Pressure' center
splot file6 with image
unset label
set label at  0.0,0.85 'Density' center
splot file7 with image
unset label
set label at  0.0,0.85 'Velocity' center
splot file8 with image
unset label
set label at  0.0,0.85 'Pressure' center
splot file9 with image

unset multiplot

unset output
