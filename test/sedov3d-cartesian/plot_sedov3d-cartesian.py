#---------------------------------------------------------------------
# File: plot_sedovxyz.py
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

import read_morpheus_data
from pylab import *

fig = matplotlib.pyplot.figure()
ratio = 2.0
sizex = 12.0
cbfrac = 0.065
fig.set_size_inches(sizex,ratio*sizex)

dmin = 0.08
dmax = 2.90

nruns = 2

ipanl = 0

for n in range(nruns):
    
    data_slice = read_morpheus_data.ReadTextFile(ifile=n+1,type='dxy',ext='.txt')
    rho = data_slice.data
    
    [ny,nx] = shape(rho)
    
    leftright = zeros([ny,nx])
    topbottom = zeros([ny,nx])
    
    for i in range(nx):
        for j in range(ny):
            leftright[j,i] = rho[j,i]/rho[j,nx-i-1]
            topbottom[j,i] = rho[j,i]/rho[ny-j-1,i]

    lrmin = amin(leftright)
    lrmax = amax(leftright)
    tbmin = amin(topbottom)
    tbmax = amax(topbottom)

    ipanl = ipanl + 1
    density = subplot(6,3,ipanl)
    dax = density.imshow(rho,origin='lower',extent=[data_slice.min1,data_slice.max1,data_slice.min2,data_slice.max2],vmin=dmin,vmax=dmax,interpolation='None',cmap='jet')
    dbar = fig.colorbar(dax,fraction=cbfrac,ticks=[dmin,0.5*(dmin+dmax),dmax])
    density.set_xlabel('Distance x (cm)')
    density.set_ylabel('Distance y (cm)')
    dbar.ax.text(1.2,0.75,'Density',va='center',ha='left',rotation=90)
    
    ipanl = ipanl + 1
    symmlr = subplot(6,3,ipanl)
    lrax = symmlr.imshow(leftright,origin='lower',extent=[data_slice.min1,data_slice.max1,data_slice.min2,data_slice.max2],interpolation='None',cmap='RdBu')
    lrbar = fig.colorbar(lrax,fraction=cbfrac,ticks=[lrmin,0.5*(lrmin+lrmax),lrmax])
    symmlr.set_xlabel('Distance x (cm)')
    symmlr.set_ylabel('Distance y (cm)')
    lrbar.ax.text(1.2,0.75,'Left-right symm.',va='center',ha='left',rotation=90)
    
    ipanl = ipanl + 1
    symmtp = subplot(6,3,ipanl)
    tbax = symmtp.imshow(topbottom,origin='lower',extent=[data_slice.min1,data_slice.max1,data_slice.min2,data_slice.max2],interpolation='None',cmap='RdBu')
    tbbar = fig.colorbar(tbax,fraction=cbfrac,ticks=[tbmin,0.5*(tbmin+tbmax),tbmax])
    symmtp.set_xlabel('Distance x (cm)')
    symmtp.set_ylabel('Distance y (cm)')
    tbbar.ax.text(1.2,0.75,'Top-bottom symm.',va='center',ha='left',rotation=90)
    
    
    data_slice = read_morpheus_data.ReadTextFile(ifile=n+1,type='dxz',ext='.txt')
    rho = data_slice.data
    
    [ny,nx] = shape(rho)
    
    leftright = zeros([ny,nx])
    topbottom = zeros([ny,nx])
    
    for i in range(nx):
        for j in range(ny):
            leftright[j,i] = rho[j,i]/rho[j,nx-i-1]
            topbottom[j,i] = rho[j,i]/rho[ny-j-1,i]

    lrmin = amin(leftright)
    lrmax = amax(leftright)
    tbmin = amin(topbottom)
    tbmax = amax(topbottom)

    ipanl = ipanl + 1
    density = subplot(6,3,ipanl)
    dax = density.imshow(rho,origin='lower',extent=[data_slice.min1,data_slice.max1,data_slice.min2,data_slice.max2],vmin=dmin,vmax=dmax,interpolation='None',cmap='jet')
    dbar = fig.colorbar(dax,fraction=cbfrac,ticks=[dmin,0.5*(dmin+dmax),dmax])
    density.set_xlabel('Distance x (cm)')
    density.set_ylabel('Distance z (cm)')
    dbar.ax.text(1.2,0.75,'Density',va='center',ha='left',rotation=90)
    
    ipanl = ipanl + 1
    symmlr = subplot(6,3,ipanl)
    lrax = symmlr.imshow(leftright,origin='lower',extent=[data_slice.min1,data_slice.max1,data_slice.min2,data_slice.max2],interpolation='None',cmap='RdBu')
    lrbar = fig.colorbar(lrax,fraction=cbfrac,ticks=[lrmin,0.5*(lrmin+lrmax),lrmax])
    symmlr.set_xlabel('Distance x (cm)')
    symmlr.set_ylabel('Distance z (cm)')
    lrbar.ax.text(1.2,0.75,'Left-right symm.',va='center',ha='left',rotation=90)
    
    ipanl = ipanl + 1
    symmtp = subplot(6,3,ipanl)
    tbax = symmtp.imshow(topbottom,origin='lower',extent=[data_slice.min1,data_slice.max1,data_slice.min2,data_slice.max2],interpolation='None',cmap='RdBu')
    tbbar = fig.colorbar(tbax,fraction=cbfrac,ticks=[tbmin,0.5*(tbmin+tbmax),tbmax])
    symmtp.set_xlabel('Distance x (cm)')
    symmtp.set_ylabel('Distance z (cm)')
    tbbar.ax.text(1.2,0.75,'Top-bottom symm.',va='center',ha='left',rotation=90)
    
    
    data_slice = read_morpheus_data.ReadTextFile(ifile=n+1,type='dyz',ext='.txt')
    rho = transpose(data_slice.data)
    
    [ny,nx] = shape(rho)
    
    leftright = zeros([ny,nx])
    topbottom = zeros([ny,nx])
    
    for i in range(nx):
        for j in range(ny):
            leftright[j,i] = rho[j,i]/rho[j,nx-i-1]
            topbottom[j,i] = rho[j,i]/rho[ny-j-1,i]

    lrmin = amin(leftright)
    lrmax = amax(leftright)
    tbmin = amin(topbottom)
    tbmax = amax(topbottom)

    ipanl = ipanl + 1
    density = subplot(6,3,ipanl)
    dax = density.imshow(rho,origin='lower',extent=[data_slice.min2,data_slice.max2,data_slice.min1,data_slice.max1],vmin=dmin,vmax=dmax,interpolation='None',cmap='jet')
    dbar = fig.colorbar(dax,fraction=cbfrac,ticks=[dmin,0.5*(dmin+dmax),dmax])
    density.set_xlabel('Distance z (cm)')
    density.set_ylabel('Distance y (cm)')
    dbar.ax.text(1.2,0.75,'Density',va='center',ha='left',rotation=90)
    
    ipanl = ipanl + 1
    symmlr = subplot(6,3,ipanl)
    lrax = symmlr.imshow(leftright,origin='lower',extent=[data_slice.min2,data_slice.max2,data_slice.min1,data_slice.max1],interpolation='None',cmap='RdBu')
    lrbar = fig.colorbar(lrax,fraction=cbfrac,ticks=[lrmin,0.5*(lrmin+lrmax),lrmax])
    symmlr.set_xlabel('Distance z (cm)')
    symmlr.set_ylabel('Distance y (cm)')
    lrbar.ax.text(1.2,0.75,'Left-right symm.',va='center',ha='left',rotation=90)
    
    ipanl = ipanl + 1
    symmtp = subplot(6,3,ipanl)
    tbax = symmtp.imshow(topbottom,origin='lower',extent=[data_slice.min2,data_slice.max2,data_slice.min1,data_slice.max1],interpolation='None',cmap='RdBu')
    tbbar = fig.colorbar(tbax,fraction=cbfrac,ticks=[tbmin,0.5*(tbmin+tbmax),tbmax])
    symmtp.set_xlabel('Distance z (cm)')
    symmtp.set_ylabel('Distance y (cm)')
    tbbar.ax.text(1.2,0.75,'Top-bottom symm.',va='center',ha='left',rotation=90)
    
    
tight_layout(pad=1.5,w_pad=1.0,h_pad=0.5)
savefig('sedov3d-cartesian.pdf')
