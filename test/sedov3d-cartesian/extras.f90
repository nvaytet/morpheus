!---------------------------------------------------------------------------------------------------
! File: extras.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines extras_init(), extras(), output_extras_data() and extras_end()
!!
!! This file contains all the routines for the user to add extra operations specific to his
!! simulation. The routine extras_init() is called once at the start of the run and is used to
!! initialise all the extra variables. The extras() routine is called at every timestep before the
!! hydro computations. The extras_end() subroutine is call at the end of the program, if data files
!! need writing or closing.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine EXTRAS_INIT:
!
!> Initialise extra variables needed specific to the run.
!<
subroutine extras_init

  implicit none

  return

end subroutine extras_init

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine EXTRAS:
!
!> Additional subroutines specific to the run.
!<
subroutine extras

  implicit none

  return

end subroutine extras

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine OUTPUT_EXTRAS_DATA:
!
!> Additional subroutines specific to the run.
!<
subroutine output_extras_data

  implicit none

  return

end subroutine output_extras_data

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine EXTRAS_END:
!
!> Finalise additional variables, routines, files, ...
!<
subroutine extras_end

  implicit none

  return

end subroutine extras_end
