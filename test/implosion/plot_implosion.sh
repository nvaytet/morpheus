#!/bin/bash

USEPYTHON=$1;

testname="implosion";

../../vis/fortran/visualize 1 3;

if [ ${USEPYTHON} -eq 1 ] ; then

    python plot_${testname}.py;
    
else

    gnuplot plot_${testname}.gp;
    ps2pdf ${testname}.ps;

fi

exit;