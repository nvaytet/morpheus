#---------------------------------------------------------------------
# File: plot_sedovxyz.py
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

import read_morpheus_data
from pylab import *

fig = matplotlib.pyplot.figure()
ratio = 1.4
sizex = 12.0
cbfrac = 0.0453
fig.set_size_inches(sizex,ratio*sizex)

#dmin = 0.08
#dmax = 2.90

nruns = 3

ipanl = 0

for n in range(nruns):
    
    data_slice = read_morpheus_data.ReadTextFile(ifile=n+1,type='dxy',ext='.txt')
    
    rho = data_slice.data
    
    [ny,nx] = shape(rho)
    
    symmetry = zeros([ny,nx])
    
    for i in range(nx):
        for j in range(ny):
            symmetry[j,i] = rho[j,i]/rho[i,j]

    dmin = amin(rho)
    dmax = amax(rho)
    smin = amin(symmetry)
    smax = amax(symmetry)

    ipanl = ipanl + 1
    density = subplot(3,2,ipanl)
    dax = density.imshow(rho,origin='lower',extent=[data_slice.min1,data_slice.max1,data_slice.min2,data_slice.max2,],vmin=dmin,vmax=dmax,interpolation='None',cmap='jet')
    dbar = fig.colorbar(dax,fraction=cbfrac,ticks=[dmin,0.5*(dmin+dmax),dmax])
    density.set_xlabel('Distance x (cm)')
    density.set_ylabel('Distance y (cm)')
    density.text(0.15,0.31,'Density',va='bottom',ha='center')
    
    ipanl = ipanl + 1
    symm = subplot(3,2,ipanl)
    sax = symm.imshow(symmetry,origin='lower',extent=[data_slice.min1,data_slice.max1,data_slice.min2,data_slice.max2,],interpolation='None',cmap='RdBu')
    sbar = fig.colorbar(sax,fraction=cbfrac,ticks=[smin,0.5*(smin+smax),smax])
    symm.set_xlabel('Distance x (cm)')
    symm.set_ylabel('Distance y (cm)')
    symm.text(0.15,0.31,'Symmetry error',va='bottom',ha='center')
    
tight_layout(pad=1.0,w_pad=1.0,h_pad=0.1)
savefig('implosion.pdf')
