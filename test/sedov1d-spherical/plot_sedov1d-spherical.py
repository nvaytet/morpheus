#---------------------------------------------------------------------
# File: plot_primakoff.py
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

import read_morpheus_data
from pylab import *

fig = matplotlib.pyplot.figure()

ratio = 0.75
sizex = 12.0

fig.set_size_inches(sizex,ratio*sizex)

# constants
const = read_morpheus_data.constants()
  
# Read data
var = read_morpheus_data.MorpheusData(ifile=1)

# Load analytical solution
solution = loadtxt('sedov1d-spherical_solution.txt')

# set up data arrays
x   = var.xn1glob * var.length
rho = var.get_slice_1(type='rho') * var.mass / (var.length**3)
u   = var.get_slice_1(type='u1') * var.length / var.time
p   = var.get_slice_1(type='p') * var.mass / (var.length * var.time**2)
T   = var.get_slice_1(type='T') * var.degree

xmin = 0.0 ; xmax = 2.0e+16

density = subplot(221)
density.plot(x,rho,'o',markerfacecolor='none',markeredgecolor='black')
density.plot(solution[:,0],solution[:,1],color='red')
density.set_xlim([xmin,xmax])
density.set_xlabel('Distance (cm)')
density.set_ylabel('Density (g cm$^{-3}$)')

velocity = subplot(222)
velocity.plot(x,u,'o',markerfacecolor='none',markeredgecolor='black')
velocity.plot(solution[:,0],solution[:,2],color='red')
velocity.set_xlim([xmin,xmax])
velocity.set_xlabel('Distance (cm)')
velocity.set_ylabel('Velocity (cm s$^{-1}$)')

pressure = subplot(223)
pressure.plot(x,p,'o',markerfacecolor='none',markeredgecolor='black')
pressure.plot(solution[:,0],solution[:,3],color='red')
pressure.set_xlim([xmin,xmax])
pressure.set_xlabel('Distance (cm)')
pressure.set_ylabel('Pressure (g cm$^{-1}$ s$^{-2}$)')

temperature = subplot(224)
temperature.semilogy(x,T,'o',markerfacecolor='none',markeredgecolor='black')
temperature.semilogy(solution[:,0],solution[:,4],color='red')
temperature.set_xlim([xmin,xmax])
temperature.set_ylim([0.0,6.0e13])
temperature.set_xlabel('Distance (cm)')
temperature.set_ylabel('Temperature (K)')

savefig('sedov1d-spherical.pdf',bbox_inches='tight')
