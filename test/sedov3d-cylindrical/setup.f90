!---------------------------------------------------------------------------------------------------
! File: setup.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines setup() and physics()
!!
!! This file contains the routines which set up the initial conditions of the problem.
!! The namelist of parameters is read in, the grid is set up and the initial state of the variables
!! is given. The physical constants and code units are also defined.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine SETUP:
!
!> Initialises all the variables, arrays, grid, etc... for the simulation.
!! The routine reads the 'INIT' parameters from the namelist.
!! It then calls a 'safety_check' to ensure consistency of dimensions and dumping
!! variables.
!! It calls the 'domain_decomposition' to evenly distribute computational volume among the
!! nodes.
!! It initialises the physical constants and units through the call to 'physics'.
!! It sets up the array dimensions and the grid.
!! It then sets the initial conditions for the conservative variables in the grid which is
!! specific to each simulation.
!! The boundary conditions are then set.
!! Finally the time intervals between cube/image dumps are set.
!<
subroutine setup

  use mod_thermodynamics
  use mod_gridarrays
  use mod_variables
  use mod_constants
  use mod_gravity
  use mod_arrays
  use mod_output
  use mod_units
  use mod_grid
  use mod_mpi

  implicit none

  integer  :: h,i,j,k,x_r,y_r,z_r
  real(dp) :: rho1,rho2,u1,u2,p1,p2,t1,t2
  real(dp) :: xfact1,xfact2,xfact3,cen1,cen2,cen3
  real(dp) :: rad

  namelist/INIT/cn_hydro,cn_cooling,visc,slope_type,solver,cooling,coolcurve,viscosity, &
                gravity,g,nx,nproc,lbox,tlim,rho1,rho2,u1,u2,p1,p2,t1,t2,ndim,geometry, &
                rhomin,boundc_1,boundc_2,boundc_3,boundc_4,boundc_5,boundc_6,gravity_type

  namelist/CODEUNITS/time,mass,length,degree

  if(debug_l1) write(*,*) 'begin SETUP',rank

  ! read init namelist
  if(debug_l1) write(*,*) 'Reading setup parameters from namelist'
  open  (35,file=trim(liste))
  rewind(35                 )
  read  (35,nml=INIT        )
  read  (35,nml=CODEUNITS   )
  close (35                 )

  ! grid setup
  cen1 = zero  ! grid direction 1 centre
  cen2 = zero  ! grid direction 2 centre
  cen3 = zero  ! grid direction 3 centre

  xfact1 = one ! size increase factor from one cell to the next in direction 1
  xfact2 = one ! size increase factor from one cell to the next in direction 2
  xfact3 = one ! size increase factor from one cell to the next in direction 3

  call domain_setup(cen1,cen2,cen3,xfact1,xfact2,xfact3)

  ! Initialise start time
  t = zero

  call common_setup1

  ! Setup medium
  if(debug_l1) write(*,*) 'Initialising gas values in grid medium'

  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)
           unew(1,i,j,k) = rho1
           unew(2,i,j,k) = zero
           unew(3,i,j,k) = zero
           unew(4,i,j,k) = zero
           unew(5,i,j,k) = p1/g1
        enddo
     enddo
  enddo

  rad  = 0.1_dp

  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)
           if(x1(i) <= rad) unew(5,i,j,k) = p2/g1
        enddo
     enddo
  enddo

  ! Setup boundary conditions:
  !    1 : free-flow
  !    2 : reflexive
  !    3 : periodic
  !    4 : inflow
  !
  bc1 = boundc_1   ! left   boundary condition (lower x)
  bc2 = boundc_2   ! right  boundary condition (upper x)
  bc3 = boundc_3   ! bottom boundary condition (lower y)
  bc4 = boundc_4   ! top    boundary condition (upper y)
  bc5 = boundc_5   ! front  boundary condition (lower z)
  bc6 = boundc_6   ! back   boundary condition (upper z)

  call common_setup2

  if(debug_l1) write(*,*) 'end   SETUP',rank

  return

end subroutine setup
