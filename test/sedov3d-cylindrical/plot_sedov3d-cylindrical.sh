#!/bin/bash

USEPYTHON=$1;

testname="sedov3d-cylindrical";

if [ ${USEPYTHON} -eq 1 ] ; then

    python plot_${testname}.py;
    
else

    ../../vis/fortran/visualize 1 3 scal=3 ypos=0.0;
    gnuplot plot_${testname}.gp;
    ps2pdf ${testname}.ps;

fi

exit;