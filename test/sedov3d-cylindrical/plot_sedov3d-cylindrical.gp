#---------------------------------------------------------------------
# File: plot_sedovrzp.gp
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

reset
set view map

file1 = 'output-00001/output-00001-dxy.txt'
file2 = 'output-00001/output-00001-dxz.txt'
file3 = 'output-00002/output-00002-dxy.txt'
file4 = 'output-00002/output-00002-dxz.txt'
file5 = 'output-00003/output-00003-dxy.txt'
file6 = 'output-00003/output-00003-dxz.txt'

set xlabel 'Distance x (cm)'
#set ylabel 'Distance y (cm)' offset 2.0

set xtics 0.0,0.1,0.5
set ytics 0.0,0.1,0.5 rotate by 90
set size square

set cbrange[0.08:1.20]

set term post enh color portrait
set output 'sedov3d-cylindrical.ps'

set multiplot layout 3, 2

set ylabel 'Distance y (cm)' offset 2.0
splot file1 with image
set ylabel 'Distance z (cm)' offset 2.0
splot file2 with image
set ylabel 'Distance y (cm)' offset 2.0
splot file3 with image
set ylabel 'Distance z (cm)' offset 2.0
splot file4 with image
set ylabel 'Distance y (cm)' offset 2.0
splot file5 with image
set ylabel 'Distance z (cm)' offset 2.0
splot file6 with image

unset multiplot

unset output
