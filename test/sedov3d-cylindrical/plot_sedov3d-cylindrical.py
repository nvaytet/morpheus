#---------------------------------------------------------------------
# File: plot_sedovxyz.py
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

import read_morpheus_data
from pylab import *
from matplotlib.collections import PolyCollection

fig = matplotlib.pyplot.figure()
ratio = 1.4
sizex = 12.0
fig.set_size_inches(sizex,ratio*sizex)

#dmin = 0.08
#dmax = 1.20

nruns = 3

ipanl = 0

for n in range(nruns):
    
    data = read_morpheus_data.MorpheusData(ifile=n+1)
    
    [polygons1,polydata1,xmin1,xmax1,ymin1,ymax1] = data.get_polygon_slice_13(type='rho',cut2=0)
    p1 = PolyCollection(polygons1, array=polydata1, cmap=matplotlib.cm.jet, edgecolors='None')
    [polygons2,polydata2,xmin2,xmax2,ymin2,ymax2] = data.get_polygon_slice_12(type='rho',cut3=0)
    p2 = PolyCollection(polygons2, array=polydata2, cmap=matplotlib.cm.jet, edgecolors='None')
    
    ipanl = ipanl + 1
    ax1 = subplot(3,2,ipanl)
    ax1.add_collection(p1)
    ax1.set_xlim([xmin1,xmax1])
    ax1.set_ylim([ymin1,ymax1])
    ax1.set_xlabel('Distance x (cm)')
    ax1.set_ylabel('Distance y (cm)')
    ax1.set_aspect('equal')
    
    ipanl = ipanl + 1
    ax2 = subplot(3,2,ipanl)
    ax2.add_collection(p2)
    ax2.set_xlim([xmin2,xmax2])
    ax2.set_ylim([ymin2,ymax2])
    ax2.set_xlabel('Distance x (cm)')
    ax2.set_ylabel('Distance z (cm)')
    ax2.set_aspect('equal')
    
tight_layout(pad=0.2,w_pad=1.0,h_pad=0.5)
savefig('sedov3d-cylindrical.pdf')
