!---------------------------------------------------------------------------------------------------
! File: setup.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Contains subroutines setup() and physics()
!!
!! This file contains the routines which set up the initial conditions of the problem.
!! The namelist of parameters is read in, the grid is set up and the initial state of the variables
!! is given. The physical constants and code units are also defined.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine SETUP:
!
!> Initialises all the variables, arrays, grid, etc... for the simulation.
!! The routine reads the 'INIT' parameters from the namelist.
!! It then calls a 'safety_check' to ensure consistency of dimensions and dumping
!! variables.
!! It calls the 'domain_decomposition' to evenly distribute computational volume among the
!! nodes.
!! It initialises the physical constants and units through the call to 'physics'.
!! It sets up the array dimensions and the grid.
!! It then sets the initial conditions for the conservative variables in the grid which is
!! specific to each simulation.
!! The boundary conditions are then set.
!! Finally the time intervals between cube/image dumps are set.
!<
subroutine setup

  use mod_thermodynamics
  use mod_gridarrays
  use mod_variables
  use mod_constants
  use mod_gravity
  use mod_arrays
  use mod_output
  use mod_units
  use mod_grid
  use mod_mpi

  implicit none

  integer  :: h,i,j,k,x_r,y_r,z_r,nmax,nb_case_loc,nb_case
  real(dp) :: rho1,rho2,u1,u2,p1,p2,t1,t2,xm,ym,zm,a,cc,m
  real(dp) :: xfact1,xfact2,xfact3,cen1,cen2,cen3
  real(dp) :: rad,dist,fract,xinpt,yinpt,zinpt,pav,dist1,offsetx,offsety,offsetz
  real(dp), dimension(3) :: dx_from_m

  namelist/INIT/cn_hydro,cn_cooling,visc,slope_type,solver,cooling,coolcurve,viscosity, &
                gravity,g,nx,nproc,lbox,tlim,rho1,rho2,u1,u2,p1,p2,t1,t2,ndim,geometry, &
                rhomin,boundc_1,boundc_2,boundc_3,boundc_4,boundc_5,boundc_6,gravity_type

  namelist/CODEUNITS/time,mass,length,degree

  if(debug_l1) write(*,*) 'begin SETUP'

  ! read init namelist
  if(debug_l1) write(*,*) 'Reading setup parameters from namelist'
  open  (35,file=trim(liste))
  rewind(35                 )
  read  (35,nml=INIT        )
  read  (35,nml=CODEUNITS   )
  close (35                 )

  ! grid setup
  cen1 = zero  ! grid direction 1 centre
  cen2 = zero  ! grid direction 2 centre
  cen3 = zero  ! grid direction 3 centre

  xfact1 = one ! size increase factor from one cell to the next in direction 1
  xfact2 = one ! size increase factor from one cell to the next in direction 2
  xfact3 = one ! size increase factor from one cell to the next in direction 3

  call common_setup(cen1,cen2,cen3,xfact1,xfact2,xfact3)

  ! Initialise start time
  t = zero

  ! Initialise cumulative radiated energy
  cumul_erad = zero

  a  = 2.50e+05_dp
  cc = 1.25e+05_dp
  m  = one*msun
  xm = 5.00e+05_dp
  ym = 5.00e+05_dp
  zm = 5.00e+05_dp

  ! Setup medium
  if(debug_l1) write(*,*) 'Initialising gas values in grid medium'

  un = zero

  nb_case_loc = 0
  nb_case     = 0

  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)

           dx_from_m(1) = abs(xn1(i)-xm)
           dx_from_m(2) = abs(xn2(j)-ym)
           dx_from_m(3) = abs(xn3(k)-zm)

           if(dx_from_m(1)**2/a**2+(dx_from_m(2))**2/a**2+(dx_from_m(3))**2/a**2 < one)then

              nb_case_loc = nb_case_loc + 1  

           endif

        enddo
     enddo
  enddo
  
  nb_case = nb_case_loc

  do k = 1,nxloc(3)
     do j = 1,nxloc(2)
        do i = 1,nxloc(1)

           dx_from_m(1) = abs(xn1(i)-xm)
           dx_from_m(2) = abs(xn2(j)-ym)
           dx_from_m(3) = abs(xn3(k)-zm)

           if(dx_from_m(1)**2/a**2+(dx_from_m(2))**2/a**2+(dx_from_m(3))**2/a**2 < one)then

              un (1,i,j,k) = m/(nb_case*cell_volume(i,j,k))

           else

              un (1,i,j,k) = rhomin

           endif

        enddo
     enddo
  enddo

  un(5,:,:,:) = p2/g1

  isolated_system = .true.

  ! Setup boundary conditions:
  !    1 : free-flow
  !    2 : reflexive
  !    3 : periodic
  !    4 : inflow
  !
  bc1 = boundc_1   ! left   boundary condition (lower x)
  bc2 = boundc_2   ! right  boundary condition (upper x)
  bc3 = boundc_3   ! bottom boundary condition (lower y)
  bc4 = boundc_4   ! top    boundary condition (upper y)
  bc5 = boundc_5   ! front  boundary condition (lower z)
  bc6 = boundc_6   ! back   boundary condition (upper z)

  call check_boundaries

  ! Define special regions (if any) where hydro and cooling do not limit dt
  nohydro   = .true.
  nocooling = .false.

  ! copy un values to uo
  uo = un

  ! file dumping variables
  kcub = 0
  kext = 0

  tcubdump = zero
  textdump = zero

  tcubstep = (tlim-tcubdump)/real(ncub,dp)
  textstep = (tlim-textdump)/real(next,dp)

  if(debug_l1) write(*,*) 'end SETUP'

  return

end subroutine setup
