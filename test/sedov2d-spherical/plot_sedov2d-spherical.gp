#---------------------------------------------------------------------
# File: plot_sedovrtp.gp
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

reset
set view map

file1 = 'output-00001/output-00001-dxz.txt'
file2 = 'output-00001/output-00001-vxz.txt'
file3 = 'output-00001/output-00001-pxz.txt'
file4 = 'output-00002/output-00002-dxz.txt'
file5 = 'output-00002/output-00002-vxz.txt'
file6 = 'output-00002/output-00002-pxz.txt'
file7 = 'output-00003/output-00003-dxz.txt'
file8 = 'output-00003/output-00003-vxz.txt'
file9 = 'output-00003/output-00003-pxz.txt'

unset key

set xlabel 'Distance x (cm)'
set ylabel 'Distance z (cm)'

set yrange[-0.35:0.5]

set xtics 0.0,0.2,0.6
#set ytics -0.5,0.5,0.5 rotate by 90

set term post enh color portrait font 'Helvetica,10'
set output 'sedov2d-spherical.ps'

set multiplot layout 3,3

set cbrange[0.08:1.20]
set label at  0.25,0.58 'Density' center
splot file1 with image
unset label
set cbrange[0.0:0.5]
set label at  0.25,0.58 'Velocity' center
splot file2 with image
unset label
set cbrange[0.0:0.3]
set label at  0.25,0.58 'Pressure' center
splot file3 with image
unset label
set cbrange[0.08:1.20]
set label at  0.25,0.58 'Density' center
splot file4 with image
unset label
set cbrange[0.0:0.5]
set label at  0.25,0.58 'Velocity' center
splot file5 with image
unset label
set cbrange[0.0:0.3]
set label at  0.25,0.58 'Pressure' center
splot file6 with image
unset label
set cbrange[0.08:1.20]
set label at  0.25,0.58 'Density' center
splot file7 with image
unset label
set cbrange[0.0:0.5]
set label at  0.25,0.58 'Velocity' center
splot file8 with image
unset label
set cbrange[0.0:0.3]
set label at  0.25,0.58 'Pressure' center
splot file9 with image

unset multiplot

unset output
