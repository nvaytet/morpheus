#---------------------------------------------------------------------
# File: plot_sedovxyz.py
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

import read_morpheus_data
from pylab import *
from matplotlib.collections import PolyCollection

fig = matplotlib.pyplot.figure()
ratio = 0.49
sizex = 12.0
fig.set_size_inches(sizex,ratio*sizex)

dmin = 0.08
dmax = 1.20

nruns = 3

for n in range(nruns):
    
    data = read_morpheus_data.MorpheusData(ifile=n+1)
    
    [polygons,polydata,xmin,xmax,ymin,ymax] = data.get_polygon_slice_12(type='rho',cut3=0)
        
    p = PolyCollection(polygons, array=polydata, cmap=matplotlib.cm.jet, edgecolors='None')

    density = subplot(1,3,n+1)
    density.add_collection(p)
    
    density.set_xlim([xmin,xmax])
    density.set_ylim([ymin,ymax])
    density.set_xlabel('Distance x (cm)')
    density.set_ylabel('Distance z (cm)')
    
tight_layout(pad=0.2,w_pad=1.0,h_pad=0.5)
savefig('sedov2d-spherical.pdf')
