! Name list file for SOD SHOCK TUBE test
!---------------------------------------------------------------------
! File: list.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------



&PARAMS
debug_level    =  0                 ! write extra information for debugging ?
nrestart       =  0                 ! file number to restart from
nout_bin_tot   =  1                 ! total number of cube   dumps
nout_ext_tot   =  1                 ! total number of extras dumps
nout_term_freq =  1                 ! print status to terminal every x timesteps
&END


&INIT
cn_hydro    =  0.20e0               ! courant number
slope_type  =  1                    ! slope limiter
solver      = 'hllc'                ! type of hydro solver: 'exact', 'roe' or 'hlle'
g           =  1.4                  ! gamma (ratio of specific heats)
ndim        =  1                    ! number of dimensions
geometry    =  1                    ! grid geometry: cartesian=1, spherical=2, cylindrical=3
nx(1)       =  512                  ! number of cells in direction 1
nx(2)       =  1                    ! number of cells in direction 2
nx(3)       =  1                    ! number of cells in direction 3
lbox(1)     =  1.0e0                ! size of x domain
lbox(2)     =  0.0e0                ! size of y domain
lbox(3)     =  0.0e0                ! size of z domain
boundc_1    =  1                    ! type of lower boundary condition in direction 1
boundc_2    =  1                    ! type of upper boundary condition in direction 1
boundc_3    =  1                    ! type of lower boundary condition in direction 2
boundc_4    =  1                    ! type of upper boundary condition in direction 2
boundc_5    =  1                    ! type of lower boundary condition in direction 3
boundc_6    =  1                    ! type of upper boundary condition in direction 3
tlim        =  0.2                  ! time limit
rhomin      =  1.0e-10              ! minimum density (in g/cm3)
rho1        =  1.0                  ! density     1
rho2        =  0.125                ! density     2
u1          =  0.0                  ! velocity    1
u2          =  0.0                  ! velocity    2
p1          =  1.0                  ! pressure    1
p2          =  0.1                  ! pressure    2
t1          =  0.0                  ! temperature 1
t2          =  0.0                  ! temperature 2
&END


&CODEUNITS
time        =  1.0                  ! code time        unit
mass        =  1.0                  ! code mass        unit
length      =  1.0                  ! code length      unit
degree      =  1.0                  ! code temperature unit
&END
