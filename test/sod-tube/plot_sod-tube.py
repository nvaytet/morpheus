#---------------------------------------------------------------------
# File: plot_sod.py
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

import read_morpheus_data
from pylab import *

fig = matplotlib.pyplot.figure()

ratio = 0.70
sizex = 12.0

fig.set_size_inches(sizex,ratio*sizex)
  
# Read data
var = read_morpheus_data.MorpheusData(ifile=1)

# Load analytical solution
solution = loadtxt('sod-tube_solution.txt')

# set up data arrays
nx = var.nx[0]
ny = var.nx[1]
nz = var.nx[2]
x  = var.xn1glob

rho = var.get_slice_1(type='rho')
u   = var.get_slice_1(type='u1')
p   = var.get_slice_1(type='p')
T   = var.get_slice_1(type='T')

density = subplot(221)
density.plot(x,rho,'o',markerfacecolor='none',markeredgecolor='black')
density.plot(solution[:,0],solution[:,1],color='red')
density.set_xlim([-0.5,0.5])
density.set_xlabel('Distance (cm)')
density.set_ylabel('Density (g cm$^{-3}$)')

velocity = subplot(222)
velocity.plot(x,u,'o',markerfacecolor='none',markeredgecolor='black')
velocity.plot(solution[:,0],solution[:,2],color='red')
velocity.set_xlim([-0.5,0.5])
velocity.set_xlabel('Distance (cm)')
velocity.set_ylabel('Velocity (cm s$^{-1}$)')

pressure = subplot(223)
pressure.plot(x,p,'o',markerfacecolor='none',markeredgecolor='black')
pressure.plot(solution[:,0],solution[:,3],color='red')
pressure.set_xlim([-0.5,0.5])
pressure.set_xlabel('Distance (cm)')
pressure.set_ylabel('Pressure (g cm$^{-1}$ s$^{-2}$)')

temperature = subplot(224)
temperature.plot(x,T,'o',markerfacecolor='none',markeredgecolor='black')
temperature.plot(solution[:,0],solution[:,4],color='red')
temperature.set_xlim([-0.5,0.5])
temperature.set_xlabel('Distance (cm)')
temperature.set_ylabel('Temperature (K)')

savefig('sod-tube.pdf',bbox_inches='tight')
