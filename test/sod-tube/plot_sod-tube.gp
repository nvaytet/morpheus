#---------------------------------------------------------------------
# File: plot_sod.gp
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

reset
set view map

file1 = 'output-00001/output-00001-dxy.txt'
file2 = 'output-00001/output-00001-vxy.txt'
file3 = 'output-00001/output-00001-pxy.txt'
file4 = 'output-00001/output-00001-txy.txt'
file5 = 'sod-tube_solution.txt'

set xlabel 'Distance x (cm)'

set xtics -0.4,0.2,0.4
set ytics  0.0,0.2,1.0 rotate by 90

unset key

set term post enh color
set output 'sod-tube.ps'

set multiplot layout 2, 2

set ylabel 'Density (g/cm3)' offset 2.0
plot file1 u 1:3 lt 6 lc 0, file5 u 1:2 w l lc 1 lt 1 lw 2
set ylabel 'Velocity (cm/s)' offset 2.0
plot file2 u 1:3 lt 6 lc 0, file5 u 1:3 w l lc 1 lt 1 lw 2
set ylabel 'Pressure (g/cm/s2)' offset 2.0
plot file3 u 1:3 lt 6 lc 0, file5 u 1:4 w l lc 1 lt 1 lw 2
set ylabel 'Temperature (K)' offset 2.0
set ytics  8.0e-09,2.0e-09,1.4e-08 rotate by 90
set format y "%7.1E"
plot file4 u 1:3 lt 6 lc 0, file5 u 1:5 w l lc 1 lt 1 lw 2

unset multiplot

unset output
