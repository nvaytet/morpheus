#---------------------------------------------------------------------
# File: plot_rayleigh-taylor.gp
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

reset
set view map
unset key

file1 = 'output-00001/output-00001-dxy.txt'
file2 = 'output-00001/output-00001-dxz.txt'
file3 = 'output-00001/output-00001-dyz.txt'
file4 = 'output-00002/output-00002-dxy.txt'
file5 = 'output-00002/output-00002-dxz.txt'
file6 = 'output-00002/output-00002-dyz.txt'
file7 = 'output-00003/output-00003-dxy.txt'
file8 = 'output-00003/output-00003-dxz.txt'
file9 = 'output-00003/output-00003-dyz.txt'

set xlabel 'Distance x (cm)'
set ylabel 'Distance y (cm)' offset 2.0

set xtics -0.2,0.2,0.2

set term post enh color portrait
set output 'rayleigh-taylor.ps'

set multiplot layout 3, 3

set ytics -0.2,0.2,0.2 rotate by 90
set size 0.3,0.23
set origin 0.0,0.74
splot file1 with image
set ytics -0.5,0.5,0.5 rotate by 90
set size 0.3,0.45
set origin 0.35,0.63
splot file2 with image
set ytics -0.5,0.5,0.5 rotate by 90
set size 0.3,0.45
set origin 0.70,0.63
splot file3 with image
set ytics -0.2,0.2,0.2 rotate by 90
set size 0.3,0.23
set origin 0.0,0.385
splot file4 with image
set ytics -0.5,0.5,0.5 rotate by 90
set size 0.3,0.45
set origin 0.35,0.275
splot file5 with image
set ytics -0.5,0.5,0.5 rotate by 90
set size 0.3,0.45
set origin 0.70,0.275
splot file6 with image
set ytics -0.2,0.2,0.2 rotate by 90
set size 0.3,0.23
set origin 0.0,0.03
splot file7 with image
set ytics -0.5,0.5,0.5 rotate by 90
set size 0.3,0.45
set origin 0.35,-0.08
splot file8 with image
set ytics -0.5,0.5,0.5 rotate by 90
set size 0.3,0.45
set origin 0.70,-0.08
splot file9 with image

unset multiplot

unset output
