#---------------------------------------------------------------------
# File: plot_sedovxyz.py
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

import read_morpheus_data
from pylab import *

fig = matplotlib.pyplot.figure()
ratio = 1.80
sizex = 12.0
fig.set_size_inches(sizex,ratio*sizex)

nruns = 3

ipanl = 0

for n in range(nruns):
    
    data_slice1 = read_morpheus_data.ReadTextFile(ifile=n+1,type='dxy',ext='.txt')
    data_slice2 = read_morpheus_data.ReadTextFile(ifile=n+1,type='dxz',ext='.txt')
    data_slice3 = read_morpheus_data.ReadTextFile(ifile=n+1,type='dyz',ext='.txt')
    
    rho1 = data_slice1.data
    rho2 = data_slice2.data
    rho3 = data_slice3.data

    ipanl = ipanl + 1
    density1 = subplot(3,3,ipanl)
    density1.imshow(rho1,origin='lower',extent=[data_slice1.min1,data_slice1.max1,data_slice1.min2,data_slice1.max2],interpolation='None',cmap='jet')
    density1.set_xlabel('Distance x (cm)')
    density1.set_ylabel('Distance y (cm)')
    
    ipanl = ipanl + 1
    density2 = subplot(3,3,ipanl)
    density2.imshow(rho2,origin='lower',extent=[data_slice2.min1,data_slice2.max1,data_slice2.min2,data_slice2.max2],interpolation='None',cmap='jet')
    density2.set_xlabel('Distance x (cm)')
    density2.set_ylabel('Distance z (cm)')
    
    ipanl = ipanl + 1
    density3 = subplot(3,3,ipanl)
    density3.imshow(rho3,origin='lower',extent=[data_slice3.min1,data_slice3.max1,data_slice3.min2,data_slice3.max2],interpolation='None',cmap='jet')
    density3.set_xlabel('Distance y (cm)')
    density3.set_ylabel('Distance z (cm)')
    
savefig('rayleigh-taylor.pdf',bbox_inches='tight')
