#---------------------------------------------------------------------
# File: read_morpheus_data.py
#
# This file is part of Morpheus.
# Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
# 
# Morpheus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Morpheus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
#---------------------------------------------------------------------

import numpy as np
import math
import fortranfile as ff

## Reading routine for Morpheus Outputs
# 
# ifile: number of the output
# form: if 'hdf' then read hdf5 else read binary
# opt: if 1 read the corresponding array
# opt[ 0]: rho
# opt[ 1]: ux
# opt[ 2]: uy
# opt[ 3]: uz
# opt[ 4]: u
# opt[ 5]: P
# opt[ 6]: T
# opt[ 7]: E
# opt[ 8]: ke
# opt[ 9]: te
# opt[10]: u vectors
#
# verbose :  mode 
#        
# endian : character, optional
# Specify the endian-ness of the file.  Possible values are
# '>', '<', '@' and '='.  See the documentation of Python's
# struct module for their meanings.  The deafult is '>' (native
# byte order)
# header_prec : character, optional
# Specify the precision used for the record headers.  Possible
# values are 'h', 'i', 'l' and 'q' with their meanings from
# Python's struct module.  The default is 'i' (the system's
# default integer).
#
# dir : directory where the data are stored (Default: current directory)
#

########################################################################
# Define functions
########################################################################

def atan3(y,x):
    result = math.atan2(y,x)
    if result < 0.0:
        result = 2.0*math.pi + result
    return result

########################################################################

def GenerateFilename(use_rank=True,ifile=1,type='',rank=0,ext='.bin',dir='.'):

    def str_suffix(n,length=6):
        fewzero = ''
        for i in range(length-len(str(n))):
            fewzero = fewzero + '0'
        return fewzero+str(n)
    
    filename = dir+'/output-'+str_suffix(ifile,length=5)+'/output-'+str_suffix(ifile,length=5)
    if len(type) > 0:
        filename = filename + '-' + type
    if use_rank:
        filename = filename + '-' + str_suffix(rank,length=6)
        
    filename = filename + ext
    
    return filename

########################################################################
# Define classes
########################################################################

class MorpheusData:
    
    def __init__(self,ifile=1,opt=[1,1,1,1,1,1,1,1,1,1,1],endian='@',header_prec='i',dir='.'):

        # Read first output header to get basic info
        filename = GenerateFilename(ifile=ifile,dir=dir,rank=0)
        
        header = BinHeader(filename,endian=endian,header_prec=header_prec)

        self.ndim         = header.ndim
        self.nvar         = header.nvar
        self.nproc        = header.nproc
        self.ncpu         = header.ncpu
        self.it           = header.it
        self.geometry     = header.geometry
        self.t            = header.t
        self.tlim         = header.tlim
        self.time         = header.time
        self.mass         = header.mass
        self.length       = header.length
        self.degree       = header.degree
        self.cooling      = header.cooling
        self.viscosity    = header.viscosity
        self.gravity      = header.gravity
        self.same_bc      = header.same_bc
        self.cn_hydro     = header.cn_hydro
        self.cn_cooling   = header.cn_cooling
        self.visc         = header.visc
        self.g            = header.g
        self.mu           = header.mu
        self.dcav         = header.dcav
        self.pcav         = header.pcav
        #self.cumul_erad   = header.cumul_erad
        self.slope_type   = header.slope_type
        self.solver       = header.solver
        self.nsource      = header.nsource
        self.geom1        = header.geom1
        self.geom2        = header.geom2
        self.geom3        = header.geom3
        self.coolcurve    = header.coolcurve
        self.k_bin_output = header.k_bin_output
        self.nx           = header.nx
        self.nghost       = header.nghost
        self.xgcen        = header.xgcen
        self.lbox         = header.lbox
        self.x1glob       = header.x1glob
        self.x2glob       = header.x2glob
        self.x3glob       = header.x3glob
        self.xn1glob      = header.xn1glob
        self.xn2glob      = header.xn2glob
        self.xn3glob      = header.xn3glob

        del(header)
        
        const = constants(self.mass,self.length,self.time,self.degree,self.mu)

        #Define variable arrays
        self.un = np.zeros((self.nvar,self.nx[0],self.nx[1],self.nx[2]))

        if opt[0]==1:
            self.rho = np.zeros((self.nx[0],self.nx[1],self.nx[2]))
        if opt[1]==1:
            self.u1  = np.zeros((self.nx[0],self.nx[1],self.nx[2]))
        if opt[2]==1:
            self.u2  = np.zeros((self.nx[0],self.nx[1],self.nx[2]))
        if opt[3]==1:
            self.u3  = np.zeros((self.nx[0],self.nx[1],self.nx[2]))
        if opt[4]==1:
            self.u   = np.zeros((self.nx[0],self.nx[1],self.nx[2]))
        if opt[5]==1:
            self.p   = np.zeros((self.nx[0],self.nx[1],self.nx[2]))
        if opt[6]==1:
            self.T   = np.zeros((self.nx[0],self.nx[1],self.nx[2]))
        if opt[7]==1:
            self.e   = np.zeros((self.nx[0],self.nx[1],self.nx[2]))
        if opt[8]==1:
            self.ke  = np.zeros((self.nx[0],self.nx[1],self.nx[2]))
        if opt[9]==1:
            self.te  = np.zeros((self.nx[0],self.nx[1],self.nx[2]))


        # Now loop over cpus
        for rank in range(self.nproc):

            filename = GenerateFilename(ifile=ifile,dir=dir,rank=rank)

            data = BinFile(filename,endian=endian,header_prec=header_prec)

            self.un[0:self.nvar,data.nxlower[0]-1:data.nxupper[0],data.nxlower[1]-1:data.nxupper[1],data.nxlower[2]-1:data.nxupper[2]] = data.un[0:self.nvar,0:data.nxloc[0],0:data.nxloc[1],0:data.nxloc[2]]

        # Fill global arrays
        for i in range(self.nx[0]):
            for j in range(self.nx[1]):
                for k in range(self.nx[2]):

                    if opt[0]==1:
                        self.rho[i,j,k] = self.un[0,i,j,k]

                    if opt[1]==1:
                        self.u1 [i,j,k] = self.un[1,i,j,k]/self.un[0,i,j,k]

                    if opt[2]==1:
                        self.u2 [i,j,k] = self.un[2,i,j,k]/self.un[0,i,j,k]

                    if opt[3]==1:
                        self.u3 [i,j,k] = self.un[3,i,j,k]/self.un[0,i,j,k]

                    if opt[4]==1:
                        self.u  [i,j,k] = math.sqrt((self.un[1,i,j,k]*self.un[1,i,j,k])+(self.un[2,i,j,k]*self.un[2,i,j,k])+(self.un[3,i,j,k]*self.un[3,i,j,k]))/self.un[0,i,j,k]

                    if opt[8]==1:
                        self.ke [i,j,k] = 0.5 * ( (self.un[1,i,j,k]*self.un[1,i,j,k])+(self.un[2,i,j,k]*self.un[2,i,j,k])+(self.un[3,i,j,k]*self.un[3,i,j,k]))/self.un[0,i,j,k]

                    if opt[5]==1:
                        self.p  [i,j,k] = (data.g-1.0) * (self.un[4,i,j,k] - 0.5 * ( (self.un[1,i,j,k]*self.un[1,i,j,k])+(self.un[2,i,j,k]*self.un[2,i,j,k])+(self.un[3,i,j,k]*self.un[3,i,j,k]))/self.un[0,i,j,k])

                    if opt[9]==1:
                        self.te [i,j,k] = self.un[4,i,j,k] - 0.5 * ( (self.un[1,i,j,k]*self.un[1,i,j,k])+(self.un[2,i,j,k]*self.un[2,i,j,k])+(self.un[3,i,j,k]*self.un[3,i,j,k]))/self.un[0,i,j,k]

                    if opt[6]==1:
                        self.T  [i,j,k] = (self.un[4,i,j,k] - 0.5 * ( (self.un[1,i,j,k]*self.un[1,i,j,k])+(self.un[2,i,j,k]*self.un[2,i,j,k])+(self.un[3,i,j,k]*self.un[3,i,j,k]))/self.un[0,i,j,k] ) * (self.g - 1.0) / (const.koverm * self.un[0,i,j,k])

                    if opt[7]==1:
                        self.e  [i,j,k] = self.un[4,i,j,k]

        del(data,self.un)


    def get_slice_1(self,type='rho',cut2=0,cut3=0):
        if type=='rho':
            slice=self.rho[:,cut2,cut3]
        elif type=='u1':
            slice=self.u1 [:,cut2,cut3]
        elif type=='u2':
            slice=self.u2 [:,cut2,cut3]
        elif type=='u3':
            slice=self.u3 [:,cut2,cut3]
        elif type=='u':
            slice=self.u  [:,cut2,cut3]
        elif type=='p':
            slice=self.p  [:,cut2,cut3]
        elif type=='T':
            slice=self.T  [:,cut2,cut3]
        elif type=='e':
            slice=self.e  [:,cut2,cut3]
        elif type=='ke':
            slice=self.ke [:,cut2,cut3]
        elif type=='te':
            slice=self.te [:,cut2,cut3]

        return slice

    def get_slice_2(self,type='rho',cut1=0,cut3=0):
        if type=='rho':
            slice=self.rho[cut1,:,cut3]
        elif type=='u1':
            slice=self.u1 [cut1,:,cut3]
        elif type=='u2':
            slice=self.u2 [cut1,:,cut3]
        elif type=='u3':
            slice=self.u3 [cut1,:,cut3]
        elif type=='u':
            slice=self.u  [cut1,:,cut3]
        elif type=='p':
            slice=self.p  [cut1,:,cut3]
        elif type=='T':
            slice=self.T  [cut1,:,cut3]
        elif type=='e':
            slice=self.e  [cut1,:,cut3]
        elif type=='ke':
            slice=self.ke [cut1,:,cut3]
        elif type=='te':
            slice=self.te [cut1,:,cut3]

        return slice

    def get_slice_3(self,type='rho',cut1=0,cut2=0):
        if type=='rho':
            slice=self.rho[cut1,cut2,:]
        elif type=='u1':
            slice=self.u1 [cut1,cut2,:]
        elif type=='u2':
            slice=self.u2 [cut1,cut2,:]
        elif type=='u3':
            slice=self.u3 [cut1,cut2,:]
        elif type=='u':
            slice=self.u  [cut1,cut2,:]
        elif type=='p':
            slice=self.p  [cut1,cut2,:]
        elif type=='T':
            slice=self.T  [cut1,cut2,:]
        elif type=='e':
            slice=self.e  [cut1,cut2,:]
        elif type=='ke':
            slice=self.ke [cut1,cut2,:]
        elif type=='te':
            slice=self.te [cut1,cut2,:]

        return slice

    def get_slice_12(self,type='rho',cut3=0):
        if type=='rho':
            slice=self.rho[:,:,cut3]
        elif type=='u1':
            slice=self.u1 [:,:,cut3]
        elif type=='u2':
            slice=self.u2 [:,:,cut3]
        elif type=='u3':
            slice=self.u3 [:,:,cut3]
        elif type=='u':
            slice=self.u  [:,:,cut3]
        elif type=='p':
            slice=self.p  [:,:,cut3]
        elif type=='T':
            slice=self.T  [:,:,cut3]
        elif type=='e':
            slice=self.e  [:,:,cut3]
        elif type=='ke':
            slice=self.ke [:,:,cut3]
        elif type=='te':
            slice=self.te [:,:,cut3]

        return slice

    def get_slice_23(self,type='rho',cut1=0):
        if type=='rho':
            slice=self.rho[cut1,:,:]
        elif type=='u1':
            slice=self.u1 [cut1,:,:]
        elif type=='u2':
            slice=self.u2 [cut1,:,:]
        elif type=='u3':
            slice=self.u3 [cut1,:,:]
        elif type=='u':
            slice=self.u  [cut1,:,:]
        elif type=='p':
            slice=self.p  [cut1,:,:]
        elif type=='T':
            slice=self.T  [cut1,:,:]
        elif type=='e':
            slice=self.e  [cut1,:,:]
        elif type=='ke':
            slice=self.ke [cut1,:,:]
        elif type=='te':
            slice=self.te [cut1,:,:]

        return slice

    def get_slice_13(self,type='rho',cut2=0):
        if type=='rho':
            slice=self.rho[:,cut2,:]
        elif type=='u1':
            slice=self.u1 [:,cut2,:]
        elif type=='u2':
            slice=self.u2 [:,cut2,:]
        elif type=='u3':
            slice=self.u3 [:,cut2,:]
        elif type=='u':
            slice=self.u  [:,cut2,:]
        elif type=='p':
            slice=self.p  [:,cut2,:]
        elif type=='T':
            slice=self.T  [:,cut2,:]
        elif type=='e':
            slice=self.e  [:,cut2,:]
        elif type=='ke':
            slice=self.ke [:,cut2,:]
        elif type=='te':
            slice=self.te [:,cut2,:]

        return slice
    
    
    
    def get_polygon_slice_12(self,type='rho',cut3=0):
        
        xmin =  1.0e+30
        xmax = -1.0e+30
        ymin =  1.0e+30
        ymax = -1.0e+30
        
        polygons = np.zeros([self.nx[0]*self.nx[1],4,2])
        polydata = np.zeros([self.nx[0]*self.nx[1]])
        n = 0
        for j in range(self.nx[1]):
            for i in range(self.nx[0]):
                if self.geometry == 1:
                    polygons[n,0,0] = self.x1glob[i  ]
                    polygons[n,1,0] = self.x1glob[i+1]
                    polygons[n,2,0] = self.x1glob[i+1]
                    polygons[n,3,0] = self.x1glob[i  ]
                    polygons[n,0,1] = self.x2glob[j  ]
                    polygons[n,1,1] = self.x2glob[j  ]
                    polygons[n,2,1] = self.x2glob[j+1]
                    polygons[n,3,1] = self.x2glob[j+1]
                elif self.geometry == 2:
                    polygons[n,0,0] = self.x1glob[i  ]*math.sin(self.x2glob[j  ])
                    polygons[n,1,0] = self.x1glob[i+1]*math.sin(self.x2glob[j  ])
                    polygons[n,2,0] = self.x1glob[i+1]*math.sin(self.x2glob[j+1])
                    polygons[n,3,0] = self.x1glob[i  ]*math.sin(self.x2glob[j+1])
                    polygons[n,0,1] = self.x1glob[i  ]*math.cos(self.x2glob[j  ])
                    polygons[n,1,1] = self.x1glob[i+1]*math.cos(self.x2glob[j  ])
                    polygons[n,2,1] = self.x1glob[i+1]*math.cos(self.x2glob[j+1])
                    polygons[n,3,1] = self.x1glob[i  ]*math.cos(self.x2glob[j+1])
                elif self.geometry == 3:
                    polygons[n,0,0] = self.x1glob[i  ]
                    polygons[n,1,0] = self.x1glob[i+1]
                    polygons[n,2,0] = self.x1glob[i+1]
                    polygons[n,3,0] = self.x1glob[i  ]
                    polygons[n,0,1] = self.x2glob[j  ]
                    polygons[n,1,1] = self.x2glob[j  ]
                    polygons[n,2,1] = self.x2glob[j+1]
                    polygons[n,3,1] = self.x2glob[j+1]
                xmin = min(xmin,polygons[n,0,0],polygons[n,1,0],polygons[n,2,0],polygons[n,3,0])
                xmax = max(xmax,polygons[n,0,0],polygons[n,1,0],polygons[n,2,0],polygons[n,3,0])
                ymin = min(ymin,polygons[n,0,1],polygons[n,1,1],polygons[n,2,1],polygons[n,3,1])
                ymax = max(ymax,polygons[n,0,1],polygons[n,1,1],polygons[n,2,1],polygons[n,3,1])
                if type=='rho':
                    polydata[n] = self.rho[i,j,cut3]
                elif type=='u1':
                    polydata[n] = self.u1 [i,j,cut3]
                elif type=='u2':
                    polydata[n] = self.u2 [i,j,cut3]
                elif type=='u3':
                    polydata[n] = self.u3 [i,j,cut3]
                elif type=='u':
                    polydata[n] = self.u  [i,j,cut3]
                elif type=='p':
                    polydata[n] = self.p  [i,j,cut3]
                elif type=='T':
                    polydata[n] = self.T  [i,j,cut3]
                elif type=='e':
                    polydata[n] = self.e  [i,j,cut3]
                elif type=='ne':
                    polydata[n] = self.ke [i,j,cut3]
                elif type=='te':
                    polydata[n] = self.te [i,j,cut3]
                    
                n = n + 1
                    
        return [polygons,polydata,xmin,xmax,ymin,ymax]
    
    def get_polygon_slice_23(self,type='rho',cut1=0):
        
        xmin =  1.0e+30
        xmax = -1.0e+30
        ymin =  1.0e+30
        ymax = -1.0e+30
        
        polygons = np.zeros([self.nx[1]*self.nx[2],4,2])
        polydata = np.zeros([self.nx[1]*self.nx[2]])
        n = 0
        for k in range(self.nx[2]):
            for j in range(self.nx[1]):
                polygons[n,0,0] = self.x2glob[j  ]
                polygons[n,1,0] = self.x2glob[j+1]
                polygons[n,2,0] = self.x2glob[j+1]
                polygons[n,3,0] = self.x2glob[j  ]
                polygons[n,0,1] = self.x3glob[k  ]
                polygons[n,1,1] = self.x3glob[k  ]
                polygons[n,2,1] = self.x3glob[k+1]
                polygons[n,3,1] = self.x3glob[k+1]
                xmin = min(xmin,polygons[n,0,0],polygons[n,1,0],polygons[n,2,0],polygons[n,3,0])
                xmax = max(xmax,polygons[n,0,0],polygons[n,1,0],polygons[n,2,0],polygons[n,3,0])
                ymin = min(ymin,polygons[n,0,1],polygons[n,1,1],polygons[n,2,1],polygons[n,3,1])
                ymax = max(ymax,polygons[n,0,1],polygons[n,1,1],polygons[n,2,1],polygons[n,3,1])
                if type=='rho':
                    polydata[n] = self.rho[cut1,j,k]
                elif type=='u1':
                    polydata[n] = self.u1 [cut1,j,k]
                elif type=='u2':
                    polydata[n] = self.u2 [cut1,j,k]
                elif type=='u3':
                    polydata[n] = self.u3 [cut1,j,k]
                elif type=='u':
                    polydata[n] = self.u  [cut1,j,k]
                elif type=='p':
                    polydata[n] = self.p  [cut1,j,k]
                elif type=='T':
                    polydata[n] = self.T  [cut1,j,k]
                elif type=='e':
                    polydata[n] = self.e  [cut1,j,k]
                elif type=='ke':
                    polydata[n] = self.ke [cut1,j,k]
                elif type=='te':
                    polydata[n] = self.te [cut1,j,k]
                    
                n = n + 1
                                        
        return [polygons,polydata,xmin,xmax,ymin,ymax]
        
    def get_polygon_slice_13(self,type='rho',cut2=0):
        
        xmin =  1.0e+30
        xmax = -1.0e+30
        ymin =  1.0e+30
        ymax = -1.0e+30
        
        polygons = np.zeros([self.nx[0]*self.nx[2],4,2])
        polydata = np.zeros([self.nx[0]*self.nx[2]])
        n = 0
        for k in range(self.nx[2]):
            for i in range(self.nx[0]):
                if self.geometry == 1:
                    polygons[n,0,0] = self.x1glob[i  ]
                    polygons[n,1,0] = self.x1glob[i+1]
                    polygons[n,2,0] = self.x1glob[i+1]
                    polygons[n,3,0] = self.x1glob[i  ]
                    polygons[n,0,1] = self.x3glob[k  ]
                    polygons[n,1,1] = self.x3glob[k  ]
                    polygons[n,2,1] = self.x3glob[k+1]
                    polygons[n,3,1] = self.x3glob[k+1]
                elif self.geometry == 2:
                    polygons[n,0,0] = self.x1glob[i  ]*math.sin(self.x2glob[cut2])*math.cos(self.x3glob[k  ])
                    polygons[n,1,0] = self.x1glob[i+1]*math.sin(self.x2glob[cut2])*math.cos(self.x3glob[k  ])
                    polygons[n,2,0] = self.x1glob[i+1]*math.sin(self.x2glob[cut2])*math.cos(self.x3glob[k+1])
                    polygons[n,3,0] = self.x1glob[i  ]*math.sin(self.x2glob[cut2])*math.cos(self.x3glob[k+1])
                    polygons[n,0,1] = self.x1glob[i  ]*math.sin(self.x2glob[cut2])*math.sin(self.x3glob[k  ])
                    polygons[n,1,1] = self.x1glob[i+1]*math.sin(self.x2glob[cut2])*math.sin(self.x3glob[k  ])
                    polygons[n,2,1] = self.x1glob[i+1]*math.sin(self.x2glob[cut2])*math.sin(self.x3glob[k+1])
                    polygons[n,3,1] = self.x1glob[i  ]*math.sin(self.x2glob[cut2])*math.sin(self.x3glob[k+1])
                elif self.geometry == 3:
                    polygons[n,0,0] = self.x1glob[i  ]*math.cos(self.x3glob[k  ])
                    polygons[n,1,0] = self.x1glob[i+1]*math.cos(self.x3glob[k  ])
                    polygons[n,2,0] = self.x1glob[i+1]*math.cos(self.x3glob[k+1])
                    polygons[n,3,0] = self.x1glob[i  ]*math.cos(self.x3glob[k+1])
                    polygons[n,0,1] = self.x1glob[i  ]*math.sin(self.x3glob[k  ])
                    polygons[n,1,1] = self.x1glob[i+1]*math.sin(self.x3glob[k  ])
                    polygons[n,2,1] = self.x1glob[i+1]*math.sin(self.x3glob[k+1])
                    polygons[n,3,1] = self.x1glob[i  ]*math.sin(self.x3glob[k+1])
                xmin = min(xmin,polygons[n,0,0],polygons[n,1,0],polygons[n,2,0],polygons[n,3,0])
                xmax = max(xmax,polygons[n,0,0],polygons[n,1,0],polygons[n,2,0],polygons[n,3,0])
                ymin = min(ymin,polygons[n,0,1],polygons[n,1,1],polygons[n,2,1],polygons[n,3,1])
                ymax = max(ymax,polygons[n,0,1],polygons[n,1,1],polygons[n,2,1],polygons[n,3,1])
                if type=='rho':
                    polydata[n] = self.rho[i,cut2,k]
                elif type=='u1':
                    polydata[n] = self.u1 [i,cut2,k]
                elif type=='u2':
                    polydata[n] = self.u2 [i,cut2,k]
                elif type=='u3':
                    polydata[n] = self.u3 [i,cut2,k]
                elif type=='u':
                    polydata[n] = self.u  [i,cut2,k]
                elif type=='p':
                    polydata[n] = self.p  [i,cut2,k]
                elif type=='T':
                    polydata[n] = self.T  [i,cut2,k]
                elif type=='e':
                    polydata[n] = self.e  [i,cut2,k]
                elif type=='ke':
                    polydata[n] = self.ke [i,cut2,k]
                elif type=='te':
                    polydata[n] = self.te [i,cut2,k]
                    
                n = n + 1
                                        
        return [polygons,polydata,xmin,xmax,ymin,ymax]
    
    def str_suffix(n,length=6):
        fewzero=''
        for i in range(length-len(str(n))):
            fewzero=fewzero+'0'
        return fewzero+str(n)
        
        
## Read header of a binary output
#
# can change endian and header_prec see MorpheusData
# ! it is not possible to read only one field if all the fields before it are not read (binary output)
class BinHeader:

    def __init__(self,filename='output-00000-000000.bin',endian='@', header_prec='i'):

        # Open file
        f = ff.FortranFile(filename,endian=endian,header_prec=header_prec)

        # Read morpheus data
        self.ndim              = f.readInts('i')
        self.nvar              = f.readInts('i')
        self.noutput           = f.readInts('i')
        self.nproc             = f.readInts('i')
        self.ncpu              = f.readInts('i')
        self.it                = f.readInts('i')
        self.geometry          = f.readInts('i')
        self.t                 = f.readReals('d')
        self.tlim              = f.readReals('d')
        self.time              = f.readReals('d')
        self.mass              = f.readReals('d')
        self.length            = f.readReals('d')
        self.degree            = f.readReals('d')
        self.cooling           = f.readInts('i')
        self.viscosity         = f.readInts('i')
        self.gravity           = f.readInts('i')
        self.same_bc           = f.readInts('i')
        self.cn_hydro          = f.readReals('d')
        self.cn_cooling        = f.readReals('d')
        self.visc              = f.readReals('d')
        self.g                 = f.readReals('d')
        self.mu                = f.readReals('d')
        self.dcav              = f.readReals('d')
        self.pcav              = f.readReals('d')
        #self.cumul_erad        = f.readReals('d')
        self.slope_type        = f.readInts('i')
        self.solver            = f.readString()
        self.nsource           = f.readInts('i')
        self.geom1             = f.readType('1s')
        self.geom2             = f.readType('1s')
        self.geom3             = f.readType('1s')
        self.coolcurve         = f.readString()
        self.t_bin_output      = f.readReals('d')
        self.t_bin_step        = f.readReals('d')
        self.t_ext_output      = f.readReals('d')
        self.t_ext_step        = f.readReals('d')
        self.k_bin_output      = f.readInts('i')
        self.k_ext_output      = f.readInts('i')
        self.nx                = f.readInts('i')
        self.nxloc             = f.readInts('i')
        self.nxlower           = f.readInts('i')
        self.nxupper           = f.readInts('i')
        self.nghost            = f.readInts('i')
        self.face_neighbours   = f.readInts('i')
        self.edge_neighbours   = f.readInts('i')
        self.corner_neighbours = f.readInts('i')
        self.xgcen             = f.readReals('d')
        self.lbox              = f.readReals('d')
        self.x1glob            = f.readReals('d')
        self.x2glob            = f.readReals('d')
        self.x3glob            = f.readReals('d')
        self.xn1glob           = f.readReals('d')
        self.xn2glob           = f.readReals('d')
        self.xn3glob           = f.readReals('d')
  
        f.close()


## Read a binary output of a single cpu
#
# can change endian and header_prec see MorpheusData
# ! it is not possible to read only one field if all the fields before it are not read (binary output)
class BinFile:

    def __init__(self,filename='output-00000-000000.bin',endian='@', header_prec='i'):

        # Open file
        f = ff.FortranFile(filename,endian=endian,header_prec=header_prec)

        # Read morpheus data
        self.ndim              = f.readInts('i')
        self.nvar              = f.readInts('i')
        self.noutput           = f.readInts('i')
        self.nproc             = f.readInts('i')
        self.ncpu              = f.readInts('i')
        self.it                = f.readInts('i')
        self.geometry          = f.readInts('i')
        self.t                 = f.readReals('d')
        self.tlim              = f.readReals('d')
        self.time              = f.readReals('d')
        self.mass              = f.readReals('d')
        self.length            = f.readReals('d')
        self.degree            = f.readReals('d')
        self.cooling           = f.readInts('i')
        self.viscosity         = f.readInts('i')
        self.gravity           = f.readInts('i')
        self.same_bc           = f.readInts('i')
        self.cn_hydro          = f.readReals('d')
        self.cn_cooling        = f.readReals('d')
        self.visc              = f.readReals('d')
        self.g                 = f.readReals('d')
        self.mu                = f.readReals('d')
        self.dcav              = f.readReals('d')
        self.pcav              = f.readReals('d')
        #self.cumul_erad        = f.readReals('d')
        self.slope_type        = f.readInts('i')
        self.solver            = f.readString()
        self.nsource           = f.readInts('i')
        self.geom1             = f.readType('1s')
        self.geom2             = f.readType('1s')
        self.geom3             = f.readType('1s')
        self.coolcurve         = f.readString()
        self.t_bin_output      = f.readReals('d')
        self.t_bin_step        = f.readReals('d')
        self.t_ext_output      = f.readReals('d')
        self.t_ext_step        = f.readReals('d')
        self.k_bin_output      = f.readInts('i')
        self.k_ext_output      = f.readInts('i')
        self.nx                = f.readInts('i')
        self.nxloc             = f.readInts('i')
        self.nxlower           = f.readInts('i')
        self.nxupper           = f.readInts('i')
        self.nghost            = f.readInts('i')
        self.face_neighbours   = f.readInts('i')
        self.edge_neighbours   = f.readInts('i')
        self.corner_neighbours = f.readInts('i')
        self.xgcen             = f.readReals('d')
        self.lbox              = f.readReals('d')
        self.x1glob            = f.readReals('d')
        self.x2glob            = f.readReals('d')
        self.x3glob            = f.readReals('d')
        self.xn1glob           = f.readReals('d')
        self.xn2glob           = f.readReals('d')
        self.xn3glob           = f.readReals('d')
        self.x1                = f.readReals('d')
        self.x2                = f.readReals('d')
        self.x3                = f.readReals('d')
        self.xn1               = f.readReals('d')
        self.xn2               = f.readReals('d')
        self.xn3               = f.readReals('d')
        self.xh1               = f.readReals('d')
        self.xh2               = f.readReals('d')
        self.xh3               = f.readReals('d')
        self.rnt               = f.readReals('d')
        self.ths               = f.readReals('d')
        self.b                 = f.readReals('d')
        self.bc1               = f.readInts('i')
        self.bc2               = f.readInts('i')
        self.bc3               = f.readInts('i')
        self.bc4               = f.readInts('i')
        self.bc5               = f.readInts('i')
        self.bc6               = f.readInts('i')
        self.inflow1           = f.readReals('d')
        self.inflow2           = f.readReals('d')
        self.inflow3           = f.readReals('d')
        self.inflow4           = f.readReals('d')
        self.inflow5           = f.readReals('d')
        self.inflow6           = f.readReals('d')
        
        shape = (self.nxloc[2],self.nxloc[1],self.nxloc[0],self.nvar)
        self.un = np.transpose((f.readReals('d')).reshape(shape),(3,2,1,0))
  
        f.close()


class constants:

    def __init__(self,mass=1.0,length=1.0,time=1.0,degree=1.0,mu=1.0):
        
        # Units
        energy   = mass*(length/time)**2
        density  = mass/(length**3)
        pressure = mass/(length*time*time)
        velocity = length/time
        # Physical constants
        self.c       =  2.997925e+10 * time/length   
        self.eV      =  1.602200e-12 / energy
        self.grav    =  6.673000e-08 * mass*time*time/(length**3)
        self.hplanck =  6.626068e-27 * time/energy
        self.kb      =  1.380650e-16 * degree/energy
        self.uma     =  1.660531e-24 / mass
        self.a_R     =  7.564640e-15 * (degree**4)*(length**3)/energy
        self.Lsun    =  3.826800e+33 * time/energy
        self.Msun    =  1.989100e+33 / mass
        self.Rsun    =  6.955000e+10 / length
        self.au      =  1.495980e+13 / length
        self.pc      =  3.085678e+18 / length
        self.kpc     =  1.000000e+03 * self.pc
        self.mpc     =  1.000000e+06 * self.pc
        self.year    =  3.155760e+07 / time
        self.kyrs    =  1.000000e+03 * self.year
        self.myrs    =  1.000000e+06 * self.year
        self.gyrs    =  1.000000e+09 * self.year
        self.day     =  8.640000e+04 / time
        self.ms      =  1.000000e+02 * time/length
        self.kms     =  1.000000e+03 * self.ms
        self.koverm  =  self.kb/(mu*self.uma)
        self.large_number = 1.0e+30




class ExtractSlices:

    def __init__(self,ifile=1,scaling=1.0,xslice=0.0,yslice=0.0,zslice=0.0,endian='@',header_prec='i',dir='.'):

        # Read first output header to get basic info
        filename = GenerateFilename(ifile=ifile,dir=dir,rank=0)
        
        header = BinHeader(filename,endian=endian,header_prec=header_prec)
        
        self.time   = header.time
        self.mass   = header.mass
        self.length = header.length
        self.degree = header.degree
        self.mu     = header.mu
        
        const = constants(self.mass,self.length,self.time,self.degree,self.mu)

        # Prepare slices
        slicexy = False ; slicexz = False ; sliceyz = False
        if header.geometry == 1:
            if header.ndim > 1:
                slicexy = True
            if header.ndim > 2:
                slicexz = True
                sliceyz = True
        else:
            if header.ndim > 1:
                slicexz = True
            if header.ndim > 2:
                slicexy = True
                sliceyz = True

        # Compute dimensions
        a1 = 0 ; a2 = 1 ; a3 = 1
        if header.ndim > 1:
            a2 = 0
        if header.ndim > 2:
            a3 = 0
            
        if header.geometry == 1: # Cartesian
     
            self.xmin_slice = header.x1glob[0]
            self.xmax_slice = header.x1glob[header.nx[0]-a1]
            self.ymin_slice = header.x2glob[0]
            self.ymax_slice = header.x2glob[header.nx[1]-a2]
            self.zmin_slice = header.x3glob[0]
            self.zmax_slice = header.x3glob[header.nx[2]-a3]

            xslice = xslice * (self.xmax_slice - self.xmin_slice) + self.xmin_slice
            yslice = yslice * (self.ymax_slice - self.ymin_slice) + self.ymin_slice
            zslice = zslice * (self.zmax_slice - self.zmin_slice) + self.zmin_slice

            dx = (self.xmax_slice-self.xmin_slice)/float(header.nx[0])
            if header.ndim > 1:
                dx = min(dx,(self.ymax_slice-self.ymin_slice)/float(header.nx[1]))
            if header.ndim > 2:
                dx = min(dx,(self.zmax_slice-self.zmin_slice)/float(header.nx[2]))

            dx = dx / scaling

            self.nxslice = int((self.xmax_slice - self.xmin_slice)/dx)
            self.nyslice = 1 ; self.nzslice = 1
            if header.ndim > 1:
                self.nyslice = int((self.ymax_slice - self.ymin_slice)/dx)
            if header.ndim > 2:
                self.nzslice = int((self.zmax_slice - self.zmin_slice)/dx)
                
        elif header.geometry == 2: # Spherical
  
            dx = (header.x1glob[header.nx[0]]-header.x1glob[0]) / float(header.nx[0]) / scaling
     
            self.xmin_slice =  const.large_number
            self.xmax_slice = -const.large_number
            self.ymin_slice =  const.large_number
            self.ymax_slice = -const.large_number
            self.zmin_slice =  const.large_number
            self.zmax_slice = -const.large_number

            for i in range(header.nx[0]+1-a1):
                for j in range(header.nx[1]+1-a2):
                    for k in range(header.nx[2]+1-a3):
                        x = header.x1glob[i]*math.sin(header.x2glob[j])*math.cos(header.x3glob[k])
                        y = header.x1glob[i]*math.sin(header.x2glob[j])*math.sin(header.x3glob[k])
                        z = header.x1glob[i]*math.cos(header.x2glob[j])
                        self.xmin_slice = min(self.xmin_slice,x)
                        self.xmax_slice = max(self.xmax_slice,x)
                        self.ymin_slice = min(self.ymin_slice,y)
                        self.ymax_slice = max(self.ymax_slice,y)
                        self.zmin_slice = min(self.zmin_slice,z)
                        self.zmax_slice = max(self.zmax_slice,z)
     
            xslice = xslice * (self.xmax_slice - self.xmin_slice) + self.xmin_slice
            yslice = yslice * (self.ymax_slice - self.ymin_slice) + self.ymin_slice
            zslice = zslice * (self.zmax_slice - self.zmin_slice) + self.zmin_slice

            self.nxslice = int((self.xmax_slice - self.xmin_slice)/dx)
            self.nyslice = 1 ; self.nzslice = 1
            if header.ndim > 1:
                self.nzslice = int((self.zmax_slice - self.zmin_slice)/dx)
            if header.ndim > 2:
                self.nyslice = int((self.ymax_slice - self.ymin_slice)/dx)
                
        elif header.geometry == 3: # Cylindrical
    
            dx = (header.x1glob[header.nx[0]]-header.x1glob[0]) / float(header.nx[0]) / scaling

            self.xmin_slice =  const.large_number
            self.xmax_slice = -const.large_number
            self.ymin_slice =  const.large_number
            self.ymax_slice = -const.large_number
            self.zmin_slice =  const.large_number
            self.zmax_slice = -const.large_number
            
            for i in range(header.nx[0]+1-a1):
                for j in range(header.nx[1]+1-a2):
                    for k in range(header.nx[2]+1-a3):

                        x = header.x1glob[i]*math.cos(header.x3glob[k])
                        y = header.x1glob[i]*math.sin(header.x3glob[k])
                        z = header.x2glob[j]
                        self.xmin_slice = min(self.xmin_slice,x)
                        self.xmax_slice = max(self.xmax_slice,x)
                        self.ymin_slice = min(self.ymin_slice,y)
                        self.ymax_slice = max(self.ymax_slice,y)
                        self.zmin_slice = min(self.zmin_slice,z)
                        self.zmax_slice = max(self.zmax_slice,z)
     
            xslice = xslice * (self.xmax_slice - self.xmin_slice) + self.xmin_slice
            yslice = yslice * (self.ymax_slice - self.ymin_slice) + self.ymin_slice
            zslice = zslice * (self.zmax_slice - self.zmin_slice) + self.zmin_slice

            self.nxslice = int((self.xmax_slice - self.xmin_slice)/dx)
            self.nyslice = 1 ; self.nzslice = 1
            if header.ndim > 1:
                self.nzslice = int((self.zmax_slice - self.zmin_slice)/dx)
            if header.ndim > 2:
                self.nyslice = int((self.ymax_slice - self.ymin_slice)/dx)
     

        self.dslicexy = np.zeros([self.nxslice,self.nyslice])
        self.dslicexz = np.zeros([self.nxslice,self.nzslice])
        self.dsliceyz = np.zeros([self.nyslice,self.nzslice])
        self.pslicexy = np.zeros([self.nxslice,self.nyslice])
        self.pslicexz = np.zeros([self.nxslice,self.nzslice])
        self.psliceyz = np.zeros([self.nyslice,self.nzslice])
        self.vslicexy = np.zeros([self.nxslice,self.nyslice])
        self.vslicexz = np.zeros([self.nxslice,self.nzslice])
        self.vsliceyz = np.zeros([self.nyslice,self.nzslice])
        self.tslicexy = np.zeros([self.nxslice,self.nyslice])
        self.tslicexz = np.zeros([self.nxslice,self.nzslice])
        self.tsliceyz = np.zeros([self.nyslice,self.nzslice])
        
        self.x1slice  = np.zeros([self.nxslice+1])
        self.x2slice  = np.zeros([self.nyslice+1])
        self.x3slice  = np.zeros([self.nzslice+1])
        self.xn1slice = np.zeros([self.nxslice  ])
        self.xn2slice = np.zeros([self.nyslice  ])
        self.xn3slice = np.zeros([self.nzslice  ])

        for i in range(self.nxslice):
            for j in range(self.nyslice):
                self.dslicexy[i,j] = -const.large_number
                self.pslicexy[i,j] = -const.large_number
                self.vslicexy[i,j] = -const.large_number
                self.tslicexy[i,j] = -const.large_number
                
        for i in range(self.nxslice):
            for k in range(self.nzslice):
                self.dslicexz[i,k] = -const.large_number
                self.pslicexz[i,k] = -const.large_number
                self.vslicexz[i,k] = -const.large_number
                self.tslicexz[i,k] = -const.large_number
                
        for j in range(self.nyslice):
            for k in range(self.nzslice):
                self.dsliceyz[j,k] = -const.large_number
                self.psliceyz[j,k] = -const.large_number
                self.vsliceyz[j,k] = -const.large_number
                self.tsliceyz[j,k] = -const.large_number

        dxslice = (self.xmax_slice-self.xmin_slice)/float(self.nxslice)

        self.x1slice[0] = self.xmin_slice
        self.x2slice[0] = self.ymin_slice
        self.x3slice[0] = self.zmin_slice

        for i in range(self.nxslice):
            self.x1slice[i+1]  = self.x1slice[i] + dxslice
            self.xn1slice[i] = 0.5 * (self.x1slice[i-1] + self.x1slice[i])

        for j in range(self.nyslice):
            self.x2slice[j+1]  = self.x2slice[j] + dxslice
            self.xn2slice[j] = 0.5 * (self.x2slice[j-1] + self.x2slice[j])
            
        for k in range(self.nzslice):
            self.x3slice[k+1]  = self.x3slice[k-1] + dxslice
            self.xn3slice[k] = 0.5 * (self.x3slice[k-1] + self.x3slice[k])
        
        
        
        
            
        # Now loop through cpus, read data and extract slices
        for rank in range(header.nproc):

            filename = GenerateFilename(ifile=ifile,dir=dir,rank=rank)

            data = BinFile(filename,endian=endian,header_prec=header_prec)
            
            if sliceyz:
                for j in range(self.nyslice):
                    for k in range(self.nzslice):
                        
                        icell_found = False
                        jcell_found = False
                        kcell_found = False
           
                        # Convert slice coordinate to spherical coordinates
                        if data.geometry == 1:
                            x1 = xslice
                            x2 = self.xn2slice[j]
                            x3 = self.xn3slice[k]
                        elif data.geometry == 2:
                            x1 = math.sqrt(xslice*xslice + self.xn2slice[j]*self.xn2slice[j] + self.xn3slice[k]*self.xn3slice[k])
                            x2 = atan3(math.sqrt(xslice*xslice+self.xn2slice[j]*self.xn2slice[j]),self.xn3slice[k])
                            x3 = atan3(self.xn2slice[j],xslice)
                        elif data.geometry == 3:
                            x1 = math.sqrt(xslice*xslice + self.xn2slice[j]*self.xn2slice[j])
                            x2 = self.xn3slice[k]
                            x3 = atan3(self.xn2slice[j],xslice)
           
                        # Locate direction 1 index for current image pixel
                        for n in range(data.nxloc[0]):
                            dx1 = x1 - data.x1[n  ]
                            dx2 = x1 - data.x1[n+1]
                            dx0 = dx1 * dx2
                            if dx0 <= 0.0:
                                icell = n
                                icell_found = True
                                break
           
                        # Locate direction 2 index for current image pixel
                        for n in range(data.nxloc[1]):
                            dx1 = x2 - data.x2[n  ]
                            dx2 = x2 - data.x2[n+1]
                            dx0 = dx1 * dx2
                            if dx0 <= 0.0:
                                jcell = n
                                jcell_found = True
                                break
                                
                        # Locate direction 3 index for current image pixel
                        if data.ndim > 2:
                            for n in range(data.nxloc[2]):
                                dx1 = x3 - data.x3[n  ]
                                dx2 = x3 - data.x3[n+1]
                                dx0 = dx1 * dx2
                                if dx0 <= 0.0:
                                    kcell = n
                                    kcell_found = True
                                    break
                        else:
                            kcell = 0
                            kcell_found = True
           
                        if icell_found and jcell_found and kcell_found:
                            self.dsliceyz[j,k] = data.un[0,icell,jcell,kcell]
                            self.psliceyz[j,k] = (data.g-1.0)*(data.un[4,icell,jcell,kcell]-0.5*( (data.un[1,icell,jcell,kcell]*data.un[1,icell,jcell,kcell])+(data.un[2,icell,jcell,kcell]*data.un[2,icell,jcell,kcell])+(data.un[3,icell,jcell,kcell]*data.un[3,icell,jcell,kcell]))/data.un[0,icell,jcell,kcell])
                            self.vsliceyz[j,k] = math.sqrt((data.un[1,icell,jcell,kcell]*data.un[1,icell,jcell,kcell])+(data.un[2,icell,jcell,kcell]*data.un[2,icell,jcell,kcell])+(data.un[3,icell,jcell,kcell]*data.un[3,icell,jcell,kcell]))/data.un[0,icell,jcell,kcell]
                            self.tsliceyz[j,k] = data.un[4,icell,jcell,kcell] - 0.5 * ( (data.un[1,icell,jcell,kcell]*data.un[1,icell,jcell,kcell])+(data.un[2,icell,jcell,kcell]*data.un[2,icell,jcell,kcell])+(data.un[3,icell,jcell,kcell]*data.un[3,icell,jcell,kcell]))/data.un[0,icell,jcell,kcell] / (const.koverm * data.un[0,icell,jcell,kcell])
  
  
            if slicexz:
                for i in range(self.nxslice):
                    for k in range(self.nzslice):
                        
                        icell_found = False
                        jcell_found = False
                        kcell_found = False
                        
                        if data.geometry == 1:
                            x1 = self.xn1slice[i]
                            x2 = yslice
                            x3 = self.xn3slice[k]
                        elif data.geometry == 2:
                            x1 = math.sqrt(self.xn1slice[i]*self.xn1slice[i] + yslice*yslice + self.xn3slice[k]*self.xn3slice[k])
                            x2 = atan3(math.sqrt(self.xn1slice[i]*self.xn1slice[i]+yslice*yslice),self.xn3slice[k])
                            x3 = atan3(yslice,self.xn1slice[i])
                        elif data.geometry == 3:
                            x1 = math.sqrt(self.xn1slice[i]*self.xn1slice[i] + yslice*yslice)
                            x2 = self.xn3slice[k]
                            x3 = atan3(yslice,self.xn1slice[i])
                            
                        # Locate direction 1 index for current image pixel
                        for n in range(data.nxloc[0]):
                            dx1 = x1 - data.x1[n  ]
                            dx2 = x1 - data.x1[n+1]
                            dx0 = dx1 * dx2
                            if dx0 <= 0.0:
                                icell = n
                                icell_found = True
                                break
           
                        # Locate direction 2 index for current image pixel
                        for n in range(data.nxloc[1]):
                            dx1 = x2 - data.x2[n  ]
                            dx2 = x2 - data.x2[n+1]
                            dx0 = dx1 * dx2
                            if dx0 <= 0.0:
                                jcell = n
                                jcell_found = True
                                break
                                
                        # Locate direction 3 index for current image pixel
                        if data.ndim > 2:
                            for n in range(data.nxloc[2]):
                                dx1 = x3 - data.x3[n  ]
                                dx2 = x3 - data.x3[n+1]
                                dx0 = dx1 * dx2
                                if dx0 <= 0.0:
                                    kcell = n
                                    kcell_found = True
                                    break
                        else:
                            kcell = 0
                            kcell_found = True
           
                        if icell_found and jcell_found and kcell_found:
                            self.dslicexz[i,k] = data.un[0,icell,jcell,kcell]
                            self.pslicexz[i,k] = (data.g-1.0)*(data.un[4,icell,jcell,kcell]-0.5*( (data.un[1,icell,jcell,kcell]*data.un[1,icell,jcell,kcell])+(data.un[2,icell,jcell,kcell]*data.un[2,icell,jcell,kcell])+(data.un[3,icell,jcell,kcell]*data.un[3,icell,jcell,kcell]))/data.un[0,icell,jcell,kcell])
                            self.vslicexz[i,k] = math.sqrt((data.un[1,icell,jcell,kcell]*data.un[1,icell,jcell,kcell])+(data.un[2,icell,jcell,kcell]*data.un[2,icell,jcell,kcell])+(data.un[3,icell,jcell,kcell]*data.un[3,icell,jcell,kcell]))/data.un[0,icell,jcell,kcell]
                            self.tslicexz[i,k] = data.un[4,icell,jcell,kcell] - 0.5 * ( (data.un[1,icell,jcell,kcell]*data.un[1,icell,jcell,kcell])+(data.un[2,icell,jcell,kcell]*data.un[2,icell,jcell,kcell])+(data.un[3,icell,jcell,kcell]*data.un[3,icell,jcell,kcell]))/data.un[0,icell,jcell,kcell] / (const.koverm * data.un[0,icell,jcell,kcell])
           
  
            if slicexy:
                for i in range(self.nxslice):
                    for j in range(self.nyslice):
                        
                        icell_found = False
                        jcell_found = False
                        kcell_found = False
                        
                        if data.geometry == 1:
                            x1 = self.xn1slice[i]
                            x2 = self.xn2slice[j]
                            x3 = zslice
                        elif data.geometry == 2:
                            x1 = math.sqrt(self.xn1slice[i]*self.xn1slice[i] + self.xn2slice[j]*self.xn2slice[j] + zslice*zslice)
                            x2 = atan3(math.sqrt(self.xn1slice[i]*self.xn1slice[i]+self.xn2slice[j]*self.xn2slice[j]),zslice)
                            x3 = atan3(self.xn2slice[j],self.xn1slice[i])
                        elif data.geometry == 3:
                            x1 = math.sqrt(self.xn1slice[i]*self.xn1slice[i] + self.xn2slice[j]*self.xn2slice[j])
                            x2 = zslice
                            x3 = atan3(self.xn2slice[j],self.xn1slice[i])
                            
                        # Locate direction 1 index for current image pixel
                        for n in range(data.nxloc[0]):
                            dx1 = x1 - data.x1[n  ]
                            dx2 = x1 - data.x1[n+1]
                            dx0 = dx1 * dx2
                            if dx0 <= 0.0:
                                icell = n
                                icell_found = True
                                break
           
                        # Locate direction 2 index for current image pixel
                        for n in range(data.nxloc[1]):
                            dx1 = x2 - data.x2[n  ]
                            dx2 = x2 - data.x2[n+1]
                            dx0 = dx1 * dx2
                            if dx0 <= 0.0:
                                jcell = n
                                jcell_found = True
                                break
                                
                        # Locate direction 3 index for current image pixel
                        if data.ndim > 2:
                            for n in range(data.nxloc[2]):
                                dx1 = x3 - data.x3[n  ]
                                dx2 = x3 - data.x3[n+1]
                                dx0 = dx1 * dx2
                                if dx0 <= 0.0:
                                    kcell = n
                                    kcell_found = True
                                    break
                        else:
                            kcell = 0
                            kcell_found = True
           
                        if icell_found and jcell_found and kcell_found:
                            self.dslicexy[i,j] = data.un[0,icell,jcell,kcell]
                            self.pslicexy[i,j] = (data.g-1.0)*(data.un[4,icell,jcell,kcell]-0.5*( (data.un[1,icell,jcell,kcell]*data.un[1,icell,jcell,kcell])+(data.un[2,icell,jcell,kcell]*data.un[2,icell,jcell,kcell])+(data.un[3,icell,jcell,kcell]*data.un[3,icell,jcell,kcell]))/data.un[0,icell,jcell,kcell])
                            self.vslicexy[i,j] = math.sqrt((data.un[1,icell,jcell,kcell]*data.un[1,icell,jcell,kcell])+(data.un[2,icell,jcell,kcell]*data.un[2,icell,jcell,kcell])+(data.un[3,icell,jcell,kcell]*data.un[3,icell,jcell,kcell]))/data.un[0,icell,jcell,kcell]
                            self.tslicexy[i,j] = data.un[4,icell,jcell,kcell] - 0.5 * ( (data.un[1,icell,jcell,kcell]*data.un[1,icell,jcell,kcell])+(data.un[2,icell,jcell,kcell]*data.un[2,icell,jcell,kcell])+(data.un[3,icell,jcell,kcell]*data.un[3,icell,jcell,kcell]))/data.un[0,icell,jcell,kcell] / (const.koverm * data.un[0,icell,jcell,kcell])
           
            


class ReadTextFile:

    def __init__(self,ifile=1,dir='.',type='dxy',ext='.txt'):
       
        filename = GenerateFilename(ifile=ifile,dir=dir,type=type,use_rank=False,ext=ext)
        
        f = open(filename).readlines()
        
        [dummy,sn1,sn2,smin1,smax1,smin2,smax2] = str.split(f[0])
        
        self.n1   = int  (sn1  )
        self.n2   = int  (sn2  )
        self.min1 = float(smin1)
        self.max1 = float(smax1)
        self.min2 = float(smin2)
        self.max2 = float(smax2)
        
        rest = f[1:]
        arr  = []
                
        for line in rest:
            if len(line) > 1:
                strings = str.split(line)
                arr.append([float(strings[0]),float(strings[1]),float(strings[2])])
        
        arrnew = np.array(arr)
        
        self.data = np.zeros([self.n2,self.n1])
        
        for j in range(self.n2):
            for i in range(self.n1):
                self.data[j,i] = arrnew[self.n1*j+i,2]
