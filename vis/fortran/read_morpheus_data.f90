!---------------------------------------------------------------------------------------------------
! File: read_morpheus_data.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Program to read morpheus fortran binary outputs.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module MORPHEUS_DATA_STRUCTURE
!
!> Definition of the morpheus data structure.
!<
module morpheus_data_structure
  implicit none
  integer, parameter :: dp  = 8
  integer, parameter :: str = 200
  
  type :: morpheus_data
  
      character (len=1)                         :: geom1,geom2,geom3
      character (len=5  )                       :: solver
      character (len=str)                       :: coolcurve
      
      logical                                   :: cooling,viscosity,gravity
      logical , dimension(6)                    :: same_bc
  
      integer                                   :: ndim,nvar,noutput,nproc,nghost,geometry
      integer                                   :: nmin1,nmax1,nmin2,nmax2,nmin3,nmax3
      integer                                   :: it,slope_type,nsource,k_bin_output,k_ext_output
      integer, dimension(       3)              :: ncpu,nx,nxloc,nxlower,nxupper
      integer, dimension(       6)              :: face_neighbours
      integer, dimension(       8)              :: corner_neighbours
      integer, dimension(      12)              :: edge_neighbours
      integer, dimension(:,:     ), allocatable :: bc1,bc2,bc3,bc4,bc5,bc6
      
      real(dp)                                  :: time,mass,length,degree
      real(dp)                                  :: t,cn_hydro,cn_cooling,visc,mu
      real(dp)                                  :: g,dcav,pcav!,cumul_erad
      real(dp)                                  :: t_bin_output,t_bin_step,t_ext_output,t_ext_step
      real(dp), dimension(      3)              :: xgcen,lbox
!       real(dp), dimension(:      ), allocatable :: x1glob,x2glob,x3glob,xn1glob,xn2glob,xn3glob
      real(dp), dimension(:      ), allocatable :: x1,x2,x3,xn1,xn2,xn3,xh1,xh2,xh3,rnt,ths,b
      real(dp), dimension(:,:,:,:), allocatable :: inflow1,inflow2,inflow3,inflow4,inflow5,inflow6
      real(dp), dimension(:,:,:  ), allocatable :: density,pressure,vel_mag,temperature
      real(dp), dimension(:,:,:,:), allocatable :: un,velocity
      
  end type morpheus_data
end module morpheus_data_structure

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Module CONSTANTS
!
!> Definition of physical constants.
!<
module constants
  use morpheus_data_structure, only : dp
  implicit none
  ! cgs units
  real(dp) :: gram
  real(dp) :: centimetre
  real(dp) :: second
  real(dp) :: kelvin
  real(dp) :: dyne
  real(dp) :: erg
  ! physical constants
  real(dp) :: pi
  real(dp) :: c
  real(dp) :: ev
  real(dp) :: grav
  real(dp) :: hplanck
  real(dp) :: kb
  real(dp) :: uma
  real(dp) :: a_r
  ! useful units
  real(dp) :: density
  real(dp) :: energy
  real(dp) :: velocity
  real(dp) :: pressure
  real(dp) :: ms
  real(dp) :: kms
  real(dp) :: day
  real(dp) :: year
  real(dp) :: kyrs
  real(dp) :: myrs
  real(dp) :: gyrs
  real(dp) :: msun
  real(dp) :: lsun
  real(dp) :: rsun
  real(dp) :: pc
  real(dp) :: kpc
  real(dp) :: mpc
  real(dp) :: au
  ! useful constants
  real(dp), parameter :: zero         =  0.0_dp
  real(dp), parameter :: one          =  1.0_dp
  real(dp), parameter :: two          =  2.0_dp
  real(dp), parameter :: three        =  3.0_dp
  real(dp), parameter :: four         =  4.0_dp
  real(dp), parameter :: five         =  5.0_dp
  real(dp), parameter :: six          =  6.0_dp
  real(dp), parameter :: seven        =  7.0_dp
  real(dp), parameter :: eight        =  8.0_dp
  real(dp), parameter :: nine         =  9.0_dp
  real(dp), parameter :: ten          = 10.0_dp
  real(dp), parameter :: half         =  0.5_dp
  real(dp), parameter :: large_number = 1.0e+30_dp
  real(dp), parameter :: small_number = 1.0e-30_dp
end module constants

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine READ_MORPHEUS_DATA_FULL
!
!> Subroutine to read morpheus data outputs.
!<
subroutine read_morpheus_data_full(ifile,var)

  use morpheus_data_structure

  implicit none

  integer             :: i,ifile,a1,a2,a3
  integer             :: nx,ny,nz,nx_loc,ny_loc,nz_loc
  character (len=str) :: fname
  type(morpheus_data) :: var,var_header,var_cpu
  
  call generate_fname(ifile,0,fname)

  write(*,*) 'Reading epoch:',ifile

  ! Read file header to gather simulation properties  
  call read_morpheus_header(fname,var_header)
  
  var%ndim         = var_header%ndim
  var%nvar         = var_header%nvar
  var%noutput      = var_header%noutput
  var%nproc        = var_header%nproc
  var%ncpu         = var_header%ncpu
  var%it           = var_header%it
  var%geometry     = var_header%geometry
  var%t            = var_header%t
!   var%tlim         = var_header%tlim
  var%time         = var_header%time
  var%mass         = var_header%mass
  var%length       = var_header%length
  var%degree       = var_header%degree
  var%cooling      = var_header%cooling
  var%viscosity    = var_header%viscosity
  var%gravity      = var_header%gravity
  var%same_bc      = var_header%same_bc
  var%cn_hydro     = var_header%cn_hydro
  var%cn_cooling   = var_header%cn_cooling
  var%visc         = var_header%visc
  var%g            = var_header%g
  var%dcav         = var_header%dcav
  var%pcav         = var_header%pcav
!   var%cumul_erad   = var_header%cumul_erad
  var%coolcurve    = var_header%coolcurve
  var%slope_type   = var_header%slope_type
  var%solver       = var_header%solver
  var%nsource      = var_header%nsource
  var%geom1        = var_header%geom1
  var%geom2        = var_header%geom2
  var%geom3        = var_header%geom3
  var%t_bin_output = var_header%t_bin_output
  var%t_bin_step   = var_header%t_bin_step
  var%k_bin_output = var_header%k_bin_output
  var%nx           = var_header%nx
  var%nxloc        = var_header%nxloc
  var%nxlower      = var_header%nxlower
  var%nxupper      = var_header%nxupper
  var%nghost       = var_header%nghost
  
  a1 = 1 ; a2 = 0 ; a3 = 0
  if(var%ndim > 1) a2 = 1
  if(var%ndim > 2) a3 = 1

  ! Allocate variables
  allocate(var%un         (1:var%nvar,1:var%nx(1),1:var%nx(2),1:var%nx(3)))
  allocate(var%density    (           1:var%nx(1),1:var%nx(2),1:var%nx(3)))
  allocate(var%pressure   (           1:var%nx(1),1:var%nx(2),1:var%nx(3)))
  allocate(var%temperature(           1:var%nx(1),1:var%nx(2),1:var%nx(3)))
  allocate(var%vel_mag    (           1:var%nx(1),1:var%nx(2),1:var%nx(3)))
  allocate(var%velocity   (1:3       ,1:var%nx(1),1:var%nx(2),1:var%nx(3)))
  allocate(var%x1 (1-a1:var%nx(1)),var%x2 (1-a2:var%nx(2)),var%x3 (1-a3:var%nx(3)))
  allocate(var%xn1(1   :var%nx(1)),var%xn2(1   :var%nx(2)),var%xn3(1   :var%nx(3)))
  allocate(var%xh1(1   :var%nx(1)),var%xh2(1   :var%nx(2)),var%xh3(1   :var%nx(3)))
  allocate(var%rnt(1   :var%nx(1)),var%ths(1   :var%nx(2)),var%b  (1   :var%nx(3)))
  
  ! Now loop through all the cpu outputs
  do i = 0,var%nproc-1

     call generate_fname(ifile,i,fname)

     ! Read single cpu data
     call read_morpheus_data(fname,var_cpu)

     nx_loc = var_cpu%nxloc(1)
     ny_loc = var_cpu%nxloc(2)
     nz_loc = var_cpu%nxloc(3)
  
     ! Fill global arrays ================================================
     ! Coordinates
     var%x1 (var_cpu%nxlower(1)-1:var_cpu%nxupper(1)) = var_cpu%x1 (0:var_cpu%nxloc(1))
     var%xn1(var_cpu%nxlower(1)  :var_cpu%nxupper(1)) = var_cpu%xn1(1:var_cpu%nxloc(1))
     var%x2 (var_cpu%nxlower(2)-1:var_cpu%nxupper(2)) = var_cpu%x2 (0:var_cpu%nxloc(2))
     var%xn2(var_cpu%nxlower(2)  :var_cpu%nxupper(2)) = var_cpu%xn2(1:var_cpu%nxloc(2))
     var%x3 (var_cpu%nxlower(3)-1:var_cpu%nxupper(3)) = var_cpu%x3 (0:var_cpu%nxloc(3))
     var%xn3(var_cpu%nxlower(3)  :var_cpu%nxupper(3)) = var_cpu%xn3(1:var_cpu%nxloc(3))
          
     ! Variables
     var%density    (    var_cpu%nxlower(1):var_cpu%nxupper(1), &
                         var_cpu%nxlower(2):var_cpu%nxupper(2), &
                         var_cpu%nxlower(3):var_cpu%nxupper(3)) = var_cpu%density    (    1:var_cpu%nxloc(1),1:var_cpu%nxloc(2),1:var_cpu%nxloc(3))
                 
     var%pressure   (    var_cpu%nxlower(1):var_cpu%nxupper(1), &
                         var_cpu%nxlower(2):var_cpu%nxupper(2), &
                         var_cpu%nxlower(3):var_cpu%nxupper(3)) = var_cpu%pressure   (    1:var_cpu%nxloc(1),1:var_cpu%nxloc(2),1:var_cpu%nxloc(3))
                 
     var%temperature(    var_cpu%nxlower(1):var_cpu%nxupper(1), &
                         var_cpu%nxlower(2):var_cpu%nxupper(2), &
                         var_cpu%nxlower(3):var_cpu%nxupper(3)) = var_cpu%temperature(    1:var_cpu%nxloc(1),1:var_cpu%nxloc(2),1:var_cpu%nxloc(3))
                 
     var%vel_mag    (    var_cpu%nxlower(1):var_cpu%nxupper(1), &
                         var_cpu%nxlower(2):var_cpu%nxupper(2), &
                         var_cpu%nxlower(3):var_cpu%nxupper(3)) = var_cpu%vel_mag    (    1:var_cpu%nxloc(1),1:var_cpu%nxloc(2),1:var_cpu%nxloc(3))
                 
     var%velocity   (1:3,var_cpu%nxlower(1):var_cpu%nxupper(1), &
                         var_cpu%nxlower(2):var_cpu%nxupper(2), &
                         var_cpu%nxlower(3):var_cpu%nxupper(3)) = var_cpu%velocity   (1:3,1:var_cpu%nxloc(1),1:var_cpu%nxloc(2),1:var_cpu%nxloc(3))
     
     call deallocate_morpheus_data(var_cpu)

  enddo

  return

end subroutine read_morpheus_data_full

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine READ_MORPHEUS_HEADER
!
!> Subroutine to read morpheus output header.
!<
subroutine read_morpheus_header(fname,var)

  use morpheus_data_structure
  
  implicit none

  character(len=str)  :: fname
  type(morpheus_data) :: var

  open (51,file=trim(fname),form='unformatted')
  read (51) var%ndim
  read (51) var%nvar
  read (51) var%noutput
  read (51) var%nproc
  read (51) var%ncpu
  read (51) var%it
  read (51) var%geometry
  read (51) var%t
!   read (51) var%tlim
  read (51) var%time
  read (51) var%mass
  read (51) var%length
  read (51) var%degree
  read (51) var%cooling
  read (51) var%viscosity
  read (51) var%gravity
  read (51) var%same_bc
  read (51) var%cn_hydro
  read (51) var%cn_cooling
  read (51) var%visc
  read (51) var%g
  read (51) var%mu
  read (51) var%dcav
  read (51) var%pcav
!   read (51) var%cumul_erad
  read (51) var%slope_type
  read (51) var%solver
  read (51) var%nsource
  read (51) var%geom1
  read (51) var%geom2
  read (51) var%geom3
  read (51) var%coolcurve
  read (51) var%t_bin_output
  read (51) var%t_bin_step
  read (51) var%t_ext_output
  read (51) var%t_ext_step
  read (51) var%k_bin_output
  read (51) var%k_ext_output
  read (51) var%nx
  read (51) var%nxloc
  read (51) var%nxlower
  read (51) var%nxupper
  read (51) var%nghost
  read (51) var%nmin1
  read (51) var%nmax1
  read (51) var%nmin2
  read (51) var%nmax2
  read (51) var%nmin3
  read (51) var%nmax3
  
  close(51)
    
  return

end subroutine read_morpheus_header

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine READ_MORPHEUS_DATA
!
!> Subroutine to read morpheus output data of a single cpu.
!<
subroutine read_morpheus_data(fname,var)

  use morpheus_data_structure
  use constants
  
  implicit none

  ! Input variables
  character(len=str ) :: fname
  type(morpheus_data) :: var
  
  ! Local variables
  integer :: i,j,k,a1,a2,a3
  
  open (51,file=trim(fname),form='unformatted')
  read (51) var%ndim
  read (51) var%nvar
  read (51) var%noutput
  read (51) var%nproc
  read (51) var%ncpu
  read (51) var%it
  read (51) var%geometry
  read (51) var%t
!   read (51) var%tlim
  read (51) var%time
  read (51) var%mass
  read (51) var%length
  read (51) var%degree
  read (51) var%cooling
  read (51) var%viscosity
  read (51) var%gravity
  read (51) var%same_bc
  read (51) var%cn_hydro
  read (51) var%cn_cooling
  read (51) var%visc
  read (51) var%g
  read (51) var%mu
  read (51) var%dcav
  read (51) var%pcav
!   read (51) var%cumul_erad
  read (51) var%slope_type
  read (51) var%solver
  read (51) var%nsource
  read (51) var%geom1
  read (51) var%geom2
  read (51) var%geom3
  read (51) var%coolcurve
  read (51) var%t_bin_output
  read (51) var%t_bin_step
  read (51) var%t_ext_output
  read (51) var%t_ext_step
  read (51) var%k_bin_output
  read (51) var%k_ext_output
  read (51) var%nx
  read (51) var%nxloc
  read (51) var%nxlower
  read (51) var%nxupper
  read (51) var%nghost
  read (51) var%nmin1
  read (51) var%nmax1
  read (51) var%nmin2
  read (51) var%nmax2
  read (51) var%nmin3
  read (51) var%nmax3
  
  call physical_constants(var%length,var%mass,var%time,var%degree)
  
  a1 = 1 ; a2 = 0 ; a3 = 0
  if(var%ndim > 1) a2 = 1
  if(var%ndim > 2) a3 = 1

  allocate(var%un         (1:var%nvar,var%nmin1:var%nmax1,var%nmin2:var%nmax2,var%nmin3:var%nmax3))
  allocate(var%density    (           var%nmin1:var%nmax1,var%nmin2:var%nmax2,var%nmin3:var%nmax3))
  allocate(var%pressure   (           var%nmin1:var%nmax1,var%nmin2:var%nmax2,var%nmin3:var%nmax3))
  allocate(var%temperature(           var%nmin1:var%nmax1,var%nmin2:var%nmax2,var%nmin3:var%nmax3))
  allocate(var%vel_mag    (           var%nmin1:var%nmax1,var%nmin2:var%nmax2,var%nmin3:var%nmax3))
  allocate(var%velocity   (1:3       ,var%nmin1:var%nmax1,var%nmin2:var%nmax2,var%nmin3:var%nmax3))
!   allocate(var%x1glob(1-a1:var%nx(1)),var%x2glob(1-a2:var%nx(2)),var%x3glob(1-a3:var%nx(3)))
  allocate(var%x1 (var%nmin1-a1:var%nmax1),var%x2 (var%nmin2-a2:var%nmax2),var%x3 (var%nmin3-a3:var%nmax3))
  allocate(var%xn1(var%nmin1   :var%nmax1),var%xn2(var%nmin2   :var%nmax2),var%xn3(var%nmin3   :var%nmax3))
  allocate(var%xh1(var%nmin1   :var%nmax1),var%xh2(var%nmin2   :var%nmax2),var%xh3(var%nmin3   :var%nmax3))
  allocate(var%rnt(var%nmin1   :var%nmax1),var%ths(var%nmin2   :var%nmax2),var%b  (var%nmin2   :var%nmax2))
  allocate(var%bc1(var%nmin2:var%nmax2,var%nmin3:var%nmax3))
  allocate(var%bc2(var%nmin2:var%nmax2,var%nmin3:var%nmax3))
  allocate(var%bc3(var%nmin1:var%nmax1,var%nmin3:var%nmax3))
  allocate(var%bc4(var%nmin1:var%nmax1,var%nmin3:var%nmax3))
  allocate(var%bc5(var%nmin1:var%nmax1,var%nmin2:var%nmax2))
  allocate(var%bc6(var%nmin1:var%nmax1,var%nmin2:var%nmax2))
  allocate(var%inflow1(1:var%nvar,1:var%nghost,var%nmin2:var%nmax2,var%nmin3:var%nmax3))
  allocate(var%inflow2(1:var%nvar,1:var%nghost,var%nmin2:var%nmax2,var%nmin3:var%nmax3))
  allocate(var%inflow3(1:var%nvar,1:var%nghost,var%nmin1:var%nmax1,var%nmin3:var%nmax3))
  allocate(var%inflow4(1:var%nvar,1:var%nghost,var%nmin1:var%nmax1,var%nmin3:var%nmax3))
  allocate(var%inflow5(1:var%nvar,1:var%nghost,var%nmin1:var%nmax1,var%nmin2:var%nmax2))
  allocate(var%inflow6(1:var%nvar,1:var%nghost,var%nmin1:var%nmax1,var%nmin2:var%nmax2))

  ! read data
  read (51) var%face_neighbours
  read (51) var%edge_neighbours
  read (51) var%corner_neighbours
  read (51) var%xgcen
  read (51) var%lbox
!   read (51) var%x1glob
!   read (51) var%x2glob
!   read (51) var%x3glob
!   read (51) var%xn1glob
!   read (51) var%xn2glob
!   read (51) var%xn3glob
  read (51) var%x1
  read (51) var%x2
  read (51) var%x3
  read (51) var%xn1
  read (51) var%xn2
  read (51) var%xn3
  read (51) var%xh1
  read (51) var%xh2
  read (51) var%xh3
  read (51) var%rnt
  read (51) var%ths
  read (51) var%b
  read (51) var%bc1
  read (51) var%bc2
  read (51) var%bc3
  read (51) var%bc4
  read (51) var%bc5
  read (51) var%bc6
  read (51) var%inflow1
  read (51) var%inflow2
  read (51) var%inflow3
  read (51) var%inflow4
  read (51) var%inflow5
  read (51) var%inflow6
  read (51) var%un
  close(51)
  
  do k = 1,var%nxloc(3)
     do j = 1,var%nxloc(2)
        do i = 1,var%nxloc(1)
        
           ! Density
           var%density    (  i,j,k) = var%un(1,i,j,k)
           ! Velocity
           var%velocity   (1,i,j,k) = var%un(2,i,j,k) / var%un(1,i,j,k)
           var%velocity   (2,i,j,k) = var%un(3,i,j,k) / var%un(1,i,j,k)
           var%velocity   (3,i,j,k) = var%un(4,i,j,k) / var%un(1,i,j,k)
           var%vel_mag    (  i,j,k) = sqrt(var%velocity(1,i,j,k)*var%velocity(1,i,j,k) + &
                                           var%velocity(2,i,j,k)*var%velocity(2,i,j,k) + &
                                           var%velocity(3,i,j,k)*var%velocity(3,i,j,k))
           ! Pressure
           var%pressure   (  i,j,k) = (var%g-one) * (var%un(5,i,j,k) - half*var%un(1,i,j,k)*var%vel_mag(i,j,k)*var%vel_mag(i,j,k))
           ! Temperature
           var%temperature(  i,j,k) = var%pressure(i,j,k) / (kb/(var%mu*uma) * var%density(i,j,k))
           
        enddo
     enddo
  enddo
  
  return
     
end subroutine read_morpheus_data

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine GENERATE_FNAME:
!
!> Generates the name of binary output to read from
!<
subroutine generate_fname(kout,krank,fname)

  use morpheus_data_structure, only : str

  implicit none

  character (len=str) :: fname
  integer             :: kout,krank
  
  write(fname,'(a7,i5.5,a8,i5.5,a1,i6.6,a4)') 'output-',kout,'/output-',kout,'-',krank,'.bin'

!   if( kout <  10)                 write(scube,'(a3,i1)') '000',kout
!   if((kout >= 10).and.(kout < 100)) write(scube,'(a2,i2)') '00',kout
!   if((kout >= 100).and.(kout < 1000)) write(scube,'(a1,i3)') '0',kout
!   if((kout >= 1000).and.(kout < 10000)) write(scube,   '(i4)')    kout
! 
!   dirname = trim(adjustl(root))//'-'//scube
!   
!   if( kout <  10)                 write(scube,'(a3,i1)') '000',kout
!   if((kout >= 10).and.(kout < 100)) write(scube,'(a2,i2)') '00',kout
!   if((kout >= 100).and.(kout < 1000)) write(scube,'(a1,i3)') '0',kout
!   if((kout >= 1000).and.(kout < 10000)) write(scube,   '(i4)')    kout
! 
!   if( krank <  10)                  write(srank,'(a,i1)') '00000',krank
!   if((krank >= 10).and.(krank < 100)) write(srank,'(a,i2)') '0000',krank
!   if((krank >= 100).and.(krank < 1000)) write(srank,'(a,i3)') '000',krank
!   if((krank >= 1000).and.(krank < 10000)) write(srank,'(a,i4)') '00',krank
!   if((krank >= 10000).and.(krank < 100000)) write(srank,'(a,i5)') '0',krank
!   if((krank >= 100000).and.(krank < 1000000)) write(srank,   '(i6)')   krank
! 
!   fname = trim(dirname)//'/'//trim(adjustl(root))//'-'//scube//'-'//srank//'.bin'
  
  return

end subroutine generate_fname

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine DEALLOCATE_morpheus_DATA
!
!> Subroutine to free memory used by morpheus data
!! structure.
!<
subroutine deallocate_morpheus_data(var)

  use morpheus_data_structure

  implicit none

  type(morpheus_data) :: var

  deallocate(var%un)
  deallocate(var%density    )
  deallocate(var%pressure   )
  deallocate(var%temperature)
  deallocate(var%vel_mag    )
  deallocate(var%velocity   )
!   deallocate(var%x1glob,var%x2glob,var%x3glob)
  deallocate(var%x1 ,var%x2 ,var%x3 )
  deallocate(var%xn1,var%xn2,var%xn3)
  deallocate(var%xh1,var%xh2,var%xh3)
  deallocate(var%rnt,var%ths,var%b  )
  deallocate(var%bc1,var%bc2,var%bc3,var%bc4,var%bc5,var%bc6)
  deallocate(var%inflow1,var%inflow2,var%inflow3,var%inflow4,var%inflow5,var%inflow6)
    
  return

end subroutine deallocate_morpheus_data

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine PHYSICAL_CONSTANTS
!
!> Initialise physical constants
!<
subroutine physical_constants(length,mass,time,degree)

  use constants
  
  real(dp) :: length,mass,time,degree
  
  ! Units
  energy   = mass*(length/time)**2
  density  = mass/(length**3)
  pressure = mass/(length*time*time)
  velocity = length/time
  
  ! Physical constants
  c       =  2.997925e+10_dp * time/length   
  eV      =  1.602200e-12_dp / energy
  grav    =  6.673000e-08_dp * mass*time*time/(length**3)
  hplanck =  6.626068e-27_dp * time/energy
  kb      =  1.380650e-16_dp * degree/energy
  uma     =  1.660531e-24_dp / mass
  a_R     =  7.564640e-15_dp * (degree**4)*(length**3)/energy
  Lsun    =  3.826800e+33_dp * time/energy
  Msun    =  1.989100e+33_dp / mass
  Rsun    =  6.955000e+10_dp / length
  au      =  1.495980e+13_dp / length
  pc      =  3.085678e+18_dp / length
  kpc     =  1.000000e+03_dp * pc
  mpc     =  1.000000e+06_dp * pc
  year    =  3.155760e+07_dp / time
  kyrs    =  1.000000e+03_dp * year
  myrs    =  1.000000e+06_dp * year
  gyrs    =  1.000000e+09_dp * year
  day     =  8.640000e+04_dp / time
  ms      =  1.000000e+02_dp * time/length
  kms     =  1.000000e+03_dp * ms

  return
  
end subroutine physical_constants
