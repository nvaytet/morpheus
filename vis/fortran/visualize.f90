!---------------------------------------------------------------------------------------------------
! File: setup.f90
!
! This file is part of Morpheus.
! Copyright (C) N. Vaytet & T. O'Brien (2008-2013)
! 
! Morpheus is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! Morpheus is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with Morpheus.  If not, see <http://www.gnu.org/licenses/>.
!---------------------------------------------------------------------------------------------------

!> \file
!! Program to extract slices through morpheus results and create VTK files.
!<

!###################################################################################################
!###################################################################################################
!###################################################################################################

!> Module PLOTPARAMETERS:
!<
module plotparameters
  use morpheus_data_structure, only : dp,str
  integer                               :: nxslice,nyslice,nzslice,ncolors
  real(dp)                              :: dmin,dmax,vmin,vmax,pmin,pmax,tmin,tmax
  real(dp)                              :: xslice,yslice,zslice,dxslice,scaling
  real(dp)                              :: xslice_pos,yslice_pos,zslice_pos
  real(dp)                              :: xmin_slice,ymin_slice,zmin_slice
  real(dp)                              :: xmax_slice,ymax_slice,zmax_slice
  real(dp)                              :: xslice_lower,yslice_lower,zslice_lower
  real(dp)                              :: xslice_upper,yslice_upper,zslice_upper
  real(dp)                              :: dminsliceyz,dmaxsliceyz,vminsliceyz,vmaxsliceyz
  real(dp)                              :: pminsliceyz,pmaxsliceyz,tminsliceyz,tmaxsliceyz
  real(dp)                              :: dminslicexz,dmaxslicexz,vminslicexz,vmaxslicexz
  real(dp)                              :: pminslicexz,pmaxslicexz,tminslicexz,tmaxslicexz
  real(dp)                              :: dminslicexy,dmaxslicexy,vminslicexy,vmaxslicexy
  real(dp)                              :: pminslicexy,pmaxslicexy,tminslicexy,tmaxslicexy
  integer , dimension(:  ), allocatable :: red,green,blue
  real(dp), dimension(:,:), allocatable :: dslicexy,dslicexz,dsliceyz
  real(dp), dimension(:,:), allocatable :: pslicexy,pslicexz,psliceyz
  real(dp), dimension(:,:), allocatable :: vslicexy,vslicexz,vsliceyz
  real(dp), dimension(:,:), allocatable :: tslicexy,tslicexz,tsliceyz
  real(dp), dimension(:  ), allocatable ::  x1slice, x2slice, x3slice
  real(dp), dimension(:  ), allocatable :: xn1slice,xn2slice,xn3slice
  logical                               :: d_log,v_log,p_log,t_log,auto_adjust
  character(len=str)                    :: colormap,libpath
end module plotparameters

!###################################################################################################
!###################################################################################################
!###################################################################################################

!> Program VISUALIZE:
!!
!! Plot 2D ppm images, VTK files and txt slices from reading MORPHEUS data.
!!
!! Compile with Makefile
!!
!<
program visualize

  use constants
  use plotparameters
  use morpheus_data_structure

  implicit none

  integer             :: i,j,k,narg,fstart,fend,fincr,ifile,rank,i1,i2,i3
  character(len=str)  :: fname,args,value
  character(len=4  )  :: option
  logical             :: writetxt,writeppm,writevtk,writeinf,writecub,fend_not_defined
  type(morpheus_data) :: var_cpu


! ################################## INPUT PARAMETERS ######################################
  writetxt    = .true.      ! create txt files ?
  writeppm    = .true.      ! create ppm image ?
  writevtk    = .false.     ! create VTK file  ?
  writeinf    = .true.      ! create information file ?
  writecub    = .false.     ! create full ascii data cube?
  fstart      = 1           ! starting output number
  fend        = 1           ! ending output number
  fincr       = 1           ! output increment looping
  d_log       = .false.     ! plot log of density     in image and profile dumps ?
  v_log       = .false.     ! plot log of velocity    in image and profile dumps ?
  p_log       = .false.     ! plot log of pressure    in image and profile dumps ?
  t_log       = .false.     ! plot log of temperature in image and profile dumps ?
  auto_adjust = .true.      ! auto-adjust y axes when plotting profiles to screen ?
  dmin        = 0.1_dp      ! lower color scaling limit for density     image and profile dumps
  dmax        = 2.0_dp      ! upper color scaling limit for density     image and profile dumps
  vmin        = 0.0_dp      ! lower color scaling limit for velocity    image and profile dumps
  vmax        = 0.5_dp      ! upper color scaling limit for velocity    image and profile dumps
  pmin        = 0.1_dp      ! lower color scaling limit for pressure    image and profile dumps
  pmax        = 1.0_dp      ! upper color scaling limit for pressure    image and profile dumps
  tmin        = 0.0_dp      ! lower color scaling limit for temperature image and profile dumps
  tmax        = 1.0_dp      ! upper color scaling limit for temperature image and profile dumps
  scaling     = 1.0_dp      ! image size scaling of ppm images
  colormap    = 'jet'   ! colormap for ppm images
  libpath     = '../../lib/colormaps/'
  xslice_pos  = 0.5_dp
  yslice_pos  = 0.5_dp
  zslice_pos  = 0.5_dp
! ##########################################################################################


  narg = iargc()
  
  if(narg < 1)then
     write(*,*) 'Missing arguments'
     write(*,*) 'Usage: ./visualize nsta (nend incr -scal=3 dlog=T)'
     stop
  endif
  
  fend_not_defined = .true.
  
  do i = 1,narg
     call getarg(i,args)
     i1 = index(args,'-')
     i2 = index(args,'=')
     if(i2 == 0)then
        if(i == 1)then
           read(args,*) fstart
        elseif(i == 2)then
           read(args,*) fend
           fend_not_defined = .false.
        elseif(i == 3)then
           read(args,*) fincr
        else
           write(*,*) 'Invalid argument number:',i
           write(*,*) 'Valid format is: option=arg'
           stop
        endif
     else
        i3 = len(trim(args))
        option = trim(adjustl(args(i1+1:i2-1)))
        value  = args(i2+1:i3)
        select case(option)
        case('nsta')
           read(value,*) fstart
        case('nend')
           read(value,*) fend
           fend_not_defined = .false.
        case('incr')
           read(value,*) fincr
        case('wtxt')
           read(value,*) writetxt
        case('wppm')
           read(value,*) writeppm
        case('wvtk')
           read(value,*) writevtk
        case('winf')
           read(value,*) writeinf
        case('wcub')
           read(value,*) writecub
        case('dlog')
           read(value,*) d_log
        case('vlog')
           read(value,*) v_log
        case('plog')
           read(value,*) p_log
        case('tlog')
           read(value,*) t_log
        case('adju')
           read(value,*) auto_adjust
        case('dmin')
           read(value,*) dmin
        case('dmax')
           read(value,*) dmax
        case('vmin')
           read(value,*) vmin
        case('vmax')
           read(value,*) vmax
        case('pmin')
           read(value,*) pmin
        case('pmax')
           read(value,*) pmax
        case('tmin')
           read(value,*) tmin
        case('tmax')
           read(value,*) tmax
        case('scal')
           read(value,*) scaling
        case('cmap')
           read(value,*) colormap
        case('xpos')
           read(value,*) xslice_pos
        case('ypos')
           read(value,*) yslice_pos
        case('zpos')
           read(value,*) zslice_pos
        case default
           write(*,*) 'WARNING: ignoring invalid option: '//option
        end select
     endif
     
  enddo
  
  if(fend_not_defined) fend = fstart
  
  if(writeppm) call load_colormap
  
  do ifile = fstart,fend,fincr
  
     write(*,*) 'Reading epoch:',ifile
  
     ! Read first file to gather simulation properties
     call generate_filename(.true.,.false.,ifile,0,'---','.bin ',fname)
     call read_morpheus_data(fname,var_cpu)
     
!      do i = -1,var_cpu%nx(1)+2
!      write(*,*) i,var_cpu%density(i,1,1)
!      enddo
     
     
     call initialise_physics(var_cpu%mass,var_cpu%length,var_cpu%time,var_cpu%degree,var_cpu%mu)
     
     if(writetxt .or. writeppm)then
        call prepare_slices(var_cpu)
        call extract_slices(var_cpu)
     endif
     
     if(writevtk) call write_vtk(ifile,0,var_cpu)
     if(writeinf) call write_inf(ifile,var_cpu)
     if(writecub) call write_cub(ifile,0,var_cpu)
     
     call deallocate_morpheus_data(var_cpu)
     
     ! Now loop through remaining cpus
     do rank = 1,var_cpu%nproc-1
     
        call generate_filename(.true.,.false.,ifile,rank,'---','.bin ',fname)
        call read_morpheus_data(fname,var_cpu)
        
        if(writetxt .or. writeppm) call extract_slices(var_cpu)
        if(writevtk) call write_vtk(ifile,rank,var_cpu)
        if(writecub) call write_cub(ifile,rank,var_cpu)
        
        call deallocate_morpheus_data(var_cpu)
        
     enddo

     call scan_data_slices
     
     if(writetxt) call write_txt(ifile,rank)
     if(writeppm) call write_ppm(ifile,rank)
     
     if(writetxt .or. writeppm) call free_slice_memory
     
  enddo

  stop

end program visualize

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine LOAD_COLORMAP:
!
!> Loads colormap
!<
subroutine load_colormap

  use plotparameters

  implicit none
  
  integer  :: i
  real(dp) :: rd,gn,bl,gradient,offset

  if(colormap == 'default')then
  
     ! Create default colormap
     ncolors = 255
     allocate(red(0:ncolors),green(0:ncolors),blue(0:ncolors))
     gradient = 0.0103_dp ; offset = 0.0318_dp
     do i = 0,ncolors
        red  (i) = min(max(nint(ncolors*(gradient*real(i-1,dp) + offset       )),0),ncolors)
        green(i) = min(max(nint(ncolors*(gradient*real(i-1,dp) + offset-1.0_dp)),0),ncolors)
        blue (i) = min(max(nint(ncolors*(gradient*real(i-1,dp) + offset-2.0_dp)),0),ncolors)
     enddo
     
  else
  
     open(16,file=trim(libpath)//trim(colormap),status='old',form='formatted')
     read(16,*) ncolors
     allocate(red(0:ncolors),green(0:ncolors),blue(0:ncolors))
     do i = 0,ncolors
        read(16,*) rd,gn,bl
        red  (i) = nint(ncolors*rd)
        green(i) = nint(ncolors*gn)
        blue (i) = nint(ncolors*bl)
     enddo
     close(16)
     
  endif

  return
  
end subroutine load_colormap

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine GENERATE_FILENAME:
!
!> Generates the name of either binary morpheus output to read from, or ppm image slice, or
!! VTK files to write to.
!<
subroutine generate_filename(use_rank,use_dir,ifile,rank,dir,ext,fname)

  use morpheus_data_structure, only : str

  implicit none

  logical             :: use_rank,use_dir
  integer             :: ifile,rank
  character (len=3  ) :: dir
!   character (len=4  ) :: sfile
  character (len=5  ) :: ext
!   character (len=6  ) :: srank
  character (len=str) :: fname
  
!   write(fname,'(a7,i5.5,a8,i5.5,a1,i6.6,a4)') 'output-',iout,'/output-',iout,'-',irank,'.bin'
  
  write(fname,'(a7,i5.5,a8,i5.5)') 'output-',ifile,'/output-',ifile

!   if( ifile <  10)                  write(sfile,'(a3,i1)') '000',ifile
!   if((ifile >= 10).and.(ifile < 100)) write(sfile,'(a2,i2)') '00',ifile
!   if((ifile >= 100).and.(ifile < 1000)) write(sfile,'(a1,i3)') '0',ifile
!   if((ifile >= 1000).and.(ifile < 10000)) write(sfile,   '(i4)')    ifile
! 
!   fname = trim(root)//'-'//sfile
!   
!   if( ifile <  10)                  write(sfile,'(a3,i1)') '000',ifile
!   if((ifile >= 10).and.(ifile < 100)) write(sfile,'(a2,i2)') '00',ifile
!   if((ifile >= 100).and.(ifile < 1000)) write(sfile,'(a1,i3)') '0',ifile
!   if((ifile >= 1000).and.(ifile < 10000)) write(sfile,   '(i4)')    ifile
!   
!   fname = trim(fname)//'/'//trim(root)//'-'//sfile
  
  if(use_dir) fname = trim(fname)//'-'//dir

  if(use_rank) write(fname,'(a,a1,i6.6)') trim(fname),'-',rank
!      if( rank <  10)                 write(srank,'(a,i1)') '00000',rank
!      if((rank >= 10).and.(rank < 100)) write(srank,'(a,i2)') '0000',rank
!      if((rank >= 100).and.(rank < 1000)) write(srank,'(a,i3)') '000',rank
!      if((rank >= 1000).and.(rank < 10000)) write(srank,'(a,i4)') '00',rank
!      if((rank >= 10000).and.(rank < 100000)) write(srank,'(a,i5)') '0',rank
!      if((rank >= 100000).and.(rank < 1000000)) write(srank,   '(i6)')   rank
!      fname = trim(fname)//'-'//srank
!   endif

  fname = trim(fname)//trim(ext)
  
  return

end subroutine generate_filename

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine INITIALISE_PHYSICS:
!
!> Initialise physical constants and units
!<
subroutine initialise_physics(mass,length,time,degree,mu)

  use constants
  
  implicit none
  
  real(dp) :: mass,length,time,degree,mu
  
  energy   = mass*(length/time)**2
  density  = mass/(length**3)
  pressure = mass/(length*time*time)
  velocity = length/time
  ! Physical constants
  pi      =  two*asin(one)
  c       =  2.997925e+10_dp * time/length   
  eV      =  1.602200e-12_dp / energy
  grav    =  6.673000e-08_dp * mass*time*time/(length**3)
  hplanck =  6.626068e-27_dp * time/energy
  kb      =  1.380650e-16_dp * degree/energy
  uma     =  1.660531e-24_dp / mass
  a_R     =  7.564640e-15_dp * (degree**4)*(length**3)/energy
  Lsun    =  3.826800e+33_dp * time/energy
  Msun    =  1.989100e+33_dp / mass
  Rsun    =  6.955000e+10_dp / length
  au      =  1.495980e+13_dp / length
  pc      =  3.085678e+18_dp / length
  kpc     =  1.000000e+03_dp * pc
  mpc     =  1.000000e+06_dp * pc
  year    =  3.155760e+07_dp / time
  kyrs    =  1.000000e+03_dp * year
  myrs    =  1.000000e+06_dp * year
  gyrs    =  1.000000e+09_dp * year
  day     =  8.640000e+04_dp / time
  ms      =  1.000000e+02_dp * time/length
  kms     =  1.000000e+03_dp * ms
  
  return
  
end subroutine initialise_physics

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine PREPARE_SLICES:
!
!> Prepare slice arrays
!<
subroutine prepare_slices(var)

  use constants
  use plotparameters
  use morpheus_data_structure

  implicit none
  
  integer             :: i,j,k
  type(morpheus_data) :: var
  
  call cal_dims(var)
    
  allocate(dslicexy(1:nxslice,1:nyslice),dslicexz(1:nxslice,1:nzslice),dsliceyz(1:nyslice,1:nzslice))
  allocate(pslicexy(1:nxslice,1:nyslice),pslicexz(1:nxslice,1:nzslice),psliceyz(1:nyslice,1:nzslice))
  allocate(vslicexy(1:nxslice,1:nyslice),vslicexz(1:nxslice,1:nzslice),vsliceyz(1:nyslice,1:nzslice))
  allocate(tslicexy(1:nxslice,1:nyslice),tslicexz(1:nxslice,1:nzslice),tsliceyz(1:nyslice,1:nzslice))
  allocate( x1slice(0:nxslice          ), x2slice(0:nyslice          ), x3slice(0:nzslice          ))
  allocate(xn1slice(1:nxslice          ),xn2slice(1:nyslice          ),xn3slice(1:nzslice          ))
  
  dslicexy = -large_number ; dslicexz = -large_number ; dsliceyz = -large_number
  pslicexy = -large_number ; pslicexz = -large_number ; psliceyz = -large_number
  vslicexy = -large_number ; vslicexz = -large_number ; vsliceyz = -large_number
  tslicexy = -large_number ; tslicexz = -large_number ; tsliceyz = -large_number
  
  dxslice = (xmax_slice-xmin_slice)/real(nxslice,dp)
  
  x1slice(0) = xmin_slice
  x2slice(0) = ymin_slice
  x3slice(0) = zmin_slice
  
  do i = 1,nxslice
      x1slice(i) = x1slice(i-1) + dxslice
     xn1slice(i) = half * (x1slice(i-1) + x1slice(i))
  enddo
  
  do j = 1,nyslice
      x2slice(j) = x2slice(j-1) + dxslice
     xn2slice(j) = half * (x2slice(j-1) + x2slice(j))
  enddo
  
  do k = 1,nzslice
      x3slice(k) = x3slice(k-1) + dxslice
     xn3slice(k) = half * (x3slice(k-1) + x3slice(k))
  enddo
    
  return
  
end subroutine prepare_slices

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine CAL_DIMS:
!
!> Computes dimensions of arrays for plotting images or profiles to screen taking into account
!! the geometry.
!<
subroutine cal_dims(var)

  use constants
  use plotparameters
  use morpheus_data_structure

  implicit none

  integer             :: i,j,k,a1,a2,a3
  real(dp)            :: x,y,z,dx
  type(morpheus_data) :: var
  
  a1 = 1 ; a2 = 0 ; a3 = 0
  if(var%ndim > 1) a2 = 1
  if(var%ndim > 2) a3 = 1
  
  select case(var%geometry)

  case(1) ! Cartesian
  
     xmin_slice = -var%xgcen(1)
     xmax_slice = var%lbox(1)-var%xgcen(1)
     ymin_slice = -var%xgcen(2)
     ymax_slice = var%lbox(2)-var%xgcen(2)
     zmin_slice = -var%xgcen(3)
     zmax_slice = var%lbox(3)-var%xgcen(3)
     
!      write(*,*) xmin_slice,xmax_slice,ymin_slice,ymax_slice,zmin_slice,zmax_slice
     
!   
!      xmin_slice =  large_number
!      xmax_slice = -large_number
!      ymin_slice =  large_number
!      ymax_slice = -large_number
!      zmin_slice =  large_number
!      zmax_slice = -large_number
! 
!      !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,x,y,z) &
!      !$OMP REDUCTION(MIN:xmin_slice) REDUCTION(MIN:ymin_slice) REDUCTION(MIN:zmin_slice) &
!      !$OMP REDUCTION(MAX:xmax_slice) REDUCTION(MAX:ymax_slice) REDUCTION(MAX:zmax_slice)
!      !$OMP DO COLLAPSE(3)
!      do k = 1-a3,var%nx(3)
!         do j = 1-a2,var%nx(2)
!            do i = 1-a1,var%nx(1)
!               select case(var%ndim)
!               case(1)
!                  x = var%x1glob(i)
!                  y = zero
!                  z = zero
!               case(2)
!                  x = var%x1glob(i)*sin(var%x2glob(j))
!                  y = zero
!                  z = var%x1glob(i)*cos(var%x2glob(j))
!               case(3)
!                  x = var%x1glob(i)*sin(var%x2glob(j))*cos(var%x3glob(k))
!                  y = var%x1glob(i)*sin(var%x2glob(j))*sin(var%x3glob(k))
!                  z = var%x1glob(i)*cos(var%x2glob(j))
!               end select
!               xmin_slice = min(xmin_slice,x)
!               xmax_slice = max(xmax_slice,x)
!               ymin_slice = min(ymin_slice,y)
!               ymax_slice = max(ymax_slice,y)
!               zmin_slice = min(zmin_slice,z)
!               zmax_slice = max(zmax_slice,z)
!            enddo
!         enddo
!      enddo
!      !$OMP END DO
!      !$OMP END PARALLEL
!      
!      xmin_slice = var%x1glob(1-a1)
!      xmax_slice = var%x1glob(var%nx(1))
!      ymin_slice = var%x2glob(1-a2)
!      ymax_slice = var%x2glob(var%nx(2))
!      zmin_slice = var%x3glob(1-a3)
!      zmax_slice = var%x3glob(var%nx(3))
     
     xslice = xslice_pos * (xmax_slice - xmin_slice) + xmin_slice
     yslice = yslice_pos * (ymax_slice - ymin_slice) + ymin_slice
     zslice = zslice_pos * (zmax_slice - zmin_slice) + zmin_slice
     
     dx = (xmax_slice-xmin_slice)/real(var%nx(1),dp)
     if(var%ndim > 1) dx = min(dx,(ymax_slice-ymin_slice)/real(var%nx(2),dp))
     if(var%ndim > 2) dx = min(dx,(zmax_slice-zmin_slice)/real(var%nx(3),dp))
     
     dx = dx / scaling
     
     nxslice = max(int((xmax_slice - xmin_slice)/dx),1)
     nyslice = max(int((ymax_slice - ymin_slice)/dx),1)
     nzslice = max(int((zmax_slice - zmin_slice)/dx),1)
  
  case(2) ! Spherical
  
     xmin_slice = -var%xgcen(1)
     xmax_slice = var%lbox(1)-var%xgcen(1)
     ymin_slice = -var%xgcen(2)
     ymax_slice = var%lbox(2)-var%xgcen(2)
     zmin_slice = -var%xgcen(3)
     zmax_slice = var%lbox(3)-var%xgcen(3)
  
!      dx = (var%x1glob(var%nx(1))-var%x1glob(0)) / real(var%nx(1),dp) / scaling
     
!      xmin_slice =  large_number
!      xmax_slice = -large_number
!      ymin_slice =  large_number
!      ymax_slice = -large_number
!      zmin_slice =  large_number
!      zmax_slice = -large_number
! 
!      !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,x,y,z) &
!      !$OMP REDUCTION(MIN:xmin_slice) REDUCTION(MIN:ymin_slice) REDUCTION(MIN:zmin_slice) &
!      !$OMP REDUCTION(MAX:xmax_slice) REDUCTION(MAX:ymax_slice) REDUCTION(MAX:zmax_slice)
!      !$OMP DO COLLAPSE(3)
!      do k = 1-a3,var%nx(3)
!         do j = 1-a2,var%nx(2)
!            do i = 1-a1,var%nx(1)
!               select case(var%ndim)
!               case(1)
!                  x = var%x1glob(i)
!                  y = zero
!                  z = zero
!               case(2)
!                  x = var%x1glob(i)*sin(var%x2glob(j))
!                  y = zero
!                  z = var%x1glob(i)*cos(var%x2glob(j))
!               case(3)
!                  x = var%x1glob(i)*sin(var%x2glob(j))*cos(var%x3glob(k))
!                  y = var%x1glob(i)*sin(var%x2glob(j))*sin(var%x3glob(k))
!                  z = var%x1glob(i)*cos(var%x2glob(j))
!               end select
!               xmin_slice = min(xmin_slice,x)
!               xmax_slice = max(xmax_slice,x)
!               ymin_slice = min(ymin_slice,y)
!               ymax_slice = max(ymax_slice,y)
!               zmin_slice = min(zmin_slice,z)
!               zmax_slice = max(zmax_slice,z)
!            enddo
!         enddo
!      enddo
!      !$OMP END DO
!      !$OMP END PARALLEL
     
     dx = (xmax_slice-xmin_slice)/real(var%nx(1),dp)
     
     xslice = xslice_pos * (xmax_slice - xmin_slice) + xmin_slice
     yslice = yslice_pos * (ymax_slice - ymin_slice) + ymin_slice
     zslice = zslice_pos * (zmax_slice - zmin_slice) + zmin_slice

     nxslice = max(int((xmax_slice - xmin_slice)/dx),1)
     nyslice = max(int((ymax_slice - ymin_slice)/dx),1)
     nzslice = max(int((zmax_slice - zmin_slice)/dx),1)
     
  case(3) ! Cylindrical
  
     xmin_slice = -var%xgcen(1)
     xmax_slice = var%lbox(1)-var%xgcen(1)
     ymin_slice = -var%xgcen(2)
     ymax_slice = var%lbox(2)-var%xgcen(2)
     zmin_slice = -var%xgcen(3)
     zmax_slice = var%lbox(3)-var%xgcen(3)
  
!      dx = (var%x1glob(var%nx(1))-var%x1glob(0)) / real(var%nx(1),dp)
!      if(var%ndim > 1) dx = min(dx,(var%x2glob(var%nx(2))-var%x2glob(0))/real(var%nx(2),dp))
!      
!      dx = dx / scaling
!      
!      xmin_slice =  large_number
!      xmax_slice = -large_number
!      ymin_slice =  large_number
!      ymax_slice = -large_number
!      zmin_slice =  large_number
!      zmax_slice = -large_number
! 
!      !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,x,y,z) &
!      !$OMP REDUCTION(MIN:xmin_slice) REDUCTION(MIN:ymin_slice) REDUCTION(MIN:zmin_slice) &
!      !$OMP REDUCTION(MAX:xmax_slice) REDUCTION(MAX:ymax_slice) REDUCTION(MAX:zmax_slice)
!      !$OMP DO COLLAPSE(3)
!      do k = 1-a3,var%nx(3)
!         do j = 1-a2,var%nx(2)
!            do i = 1-a1,var%nx(1)
!               select case(var%ndim)
!               case(1)
!                  x = var%x1glob(i)
!                  y = zero
!                  z = zero
!               case(2)
!                  x = var%x1glob(i)
!                  y = zero
!                  z = var%x2glob(j)
!               case(3)
!                  x = var%x1glob(i)*cos(var%x3glob(k))
!                  y = var%x1glob(i)*sin(var%x3glob(k))
!                  z = var%x2glob(j)
!               end select
!               xmin_slice = min(xmin_slice,x)
!               xmax_slice = max(xmax_slice,x)
!               ymin_slice = min(ymin_slice,y)
!               ymax_slice = max(ymax_slice,y)
!               zmin_slice = min(zmin_slice,z)
!               zmax_slice = max(zmax_slice,z)
!            enddo
!         enddo
!      enddo
!      !$OMP END DO
!      !$OMP END PARALLEL
     
     dx = (xmax_slice-xmin_slice)/real(var%nx(1),dp)
     
     xslice = xslice_pos * (xmax_slice - xmin_slice) + xmin_slice
     yslice = yslice_pos * (ymax_slice - ymin_slice) + ymin_slice
     zslice = zslice_pos * (zmax_slice - zmin_slice) + zmin_slice
     
     nxslice = max(int((xmax_slice - xmin_slice)/dx),1)
     nyslice = max(int((ymax_slice - ymin_slice)/dx),1)
     nzslice = max(int((zmax_slice - zmin_slice)/dx),1)
     
  case default

     write(*,*) 'Wrong geometry in cal_dims'

  end select
  
  return

end subroutine cal_dims

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine EXTRACT_SLICES:
!
!> Extract cartesian slices from 2D-3D data.
!<
subroutine extract_slices(var)

  use constants
  use plotparameters
  use morpheus_data_structure

  implicit none

  integer             :: i,j,k,n,a1,a2,a3
  integer             :: icell,jcell,kcell
  real(dp)            :: x1,x2,x3,dx0,dx1,dx2,atan3
  logical             :: icell_found,jcell_found,kcell_found
  type(morpheus_data) :: var
  
  a1 = 1 ; a2 = 0 ; a3 = 0
  if(var%ndim > 1) a2 = 1
  if(var%ndim > 2) a3 = 1
  
  ! xslice
  
  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(j,k,n,x1,x2,x3,dx0,dx1,dx2,icell,jcell,kcell,&
  !$OMP icell_found,jcell_found,kcell_found)
  !$OMP DO COLLAPSE(2)
  do k = 1,nzslice
     do j = 1,nyslice
        
        icell_found = .false.
        jcell_found = .false.
        kcell_found = .false.
        
        ! Convert slice coordinate to spherical coordinates
        select case(var%geometry)
        case(1)
           x1 = xslice
           x2 = xn2slice(j)
           x3 = xn3slice(k)
        case(2)
           x1 = sqrt(xslice*xslice + xn2slice(j)*xn2slice(j) + xn3slice(k)*xn3slice(k))
           x2 = atan3(sqrt(xslice*xslice+xn2slice(j)*xn2slice(j)),xn3slice(k))
           x3 = atan3(xn2slice(j),xslice)
        case(3)
           x1 = sqrt(xslice*xslice + xn2slice(j)*xn2slice(j))
           x2 = xn3slice(k)
           x3 = atan3(xn2slice(j),xslice)
        end select
        
        call extract_cell(var%ndim,var%nxloc(1),var%nxloc(2),var%nxloc(3),a1,a2,a3,&
                          var%x1(1-a1:var%nxloc(1)),var%x2(1-a2:var%nxloc(2)),var%x3(1-a3:var%nxloc(3)),&
                          x1,x2,x3,icell,jcell,kcell,icell_found,jcell_found,kcell_found)
        
        if(icell_found .and. jcell_found .and. kcell_found)then
           dsliceyz(j,k) = var%density    (icell,jcell,kcell)
           psliceyz(j,k) = var%pressure   (icell,jcell,kcell)
           vsliceyz(j,k) = var%vel_mag    (icell,jcell,kcell)
           tsliceyz(j,k) = var%temperature(icell,jcell,kcell)
        endif
        
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL
       
  ! yslice
  
  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k,n,x1,x2,x3,dx0,dx1,dx2,icell,jcell,kcell,&
  !$OMP icell_found,jcell_found,kcell_found)
  !$OMP DO COLLAPSE(2)
  do k = 1,nzslice
     do i = 1,nxslice
        
        icell_found = .false.
        jcell_found = .false.
        kcell_found = .false.
        
        ! Convert slice coordinate to spherical coordinates
        select case(var%geometry)
        case(1)
           x1 = xn1slice(i)
           x2 = yslice
           x3 = xn3slice(k)
        case(2)
           x1 = sqrt(xn1slice(i)*xn1slice(i) + yslice*yslice + xn3slice(k)*xn3slice(k))
           x2 = atan3(sqrt(xn1slice(i)*xn1slice(i)+yslice*yslice),xn3slice(k))
           x3 = atan3(yslice,xn1slice(i))
        case(3)
           x1 = sqrt(xn1slice(i)*xn1slice(i) + yslice*yslice)
           x2 = xn3slice(k)
           x3 = atan3(yslice,xn1slice(i))
        end select
        
        call extract_cell(var%ndim,var%nxloc(1),var%nxloc(2),var%nxloc(3),a1,a2,a3,&
                          var%x1(1-a1:var%nxloc(1)),var%x2(1-a2:var%nxloc(2)),var%x3(1-a3:var%nxloc(3)),&
                          x1,x2,x3,icell,jcell,kcell,icell_found,jcell_found,kcell_found)
        
        if(icell_found .and. jcell_found .and. kcell_found)then
           dslicexz(i,k) = var%density    (icell,jcell,kcell)
           pslicexz(i,k) = var%pressure   (icell,jcell,kcell)
           vslicexz(i,k) = var%vel_mag    (icell,jcell,kcell)
           tslicexz(i,k) = var%temperature(icell,jcell,kcell)
        endif
        
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL
  
  ! zslice
  
  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,n,x1,x2,x3,dx0,dx1,dx2,icell,jcell,kcell,&
  !$OMP icell_found,jcell_found,kcell_found)
  !$OMP DO COLLAPSE(2)
  do j = 1,nyslice
     do i = 1,nxslice
        
        icell_found = .false.
        jcell_found = .false.
        kcell_found = .false.
        
        ! Convert slice coordinate to spherical coordinates
        select case(var%geometry)
        case(1)
           x1 = xn1slice(i)
           x2 = xn2slice(j)
           x3 = zslice
        case(2)
           x1 = sqrt(xn1slice(i)*xn1slice(i) + xn2slice(j)*xn2slice(j) + zslice*zslice)
           x2 = atan3(sqrt(xn1slice(i)*xn1slice(i)+xn2slice(j)*xn2slice(j)),zslice)
           x3 = atan3(xn2slice(j),xn1slice(i))
        case(3)
           x1 = sqrt(xn1slice(i)*xn1slice(i) + xn2slice(j)*xn2slice(j))
           x2 = zslice
           x3 = atan3(xn2slice(j),xn1slice(i))
        end select
        
        call extract_cell(var%ndim,var%nxloc(1),var%nxloc(2),var%nxloc(3),a1,a2,a3,&
                          var%x1(1-a1:var%nxloc(1)),var%x2(1-a2:var%nxloc(2)),var%x3(1-a3:var%nxloc(3)),&
                          x1,x2,x3,icell,jcell,kcell,icell_found,jcell_found,kcell_found)
        
        if(icell_found .and. jcell_found .and. kcell_found)then
           dslicexy(i,j) = var%density    (icell,jcell,kcell)
           pslicexy(i,j) = var%pressure   (icell,jcell,kcell)
           vslicexy(i,j) = var%vel_mag    (icell,jcell,kcell)
           tslicexy(i,j) = var%temperature(icell,jcell,kcell)
        endif
        
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL
     
  return

end subroutine extract_slices

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine EXTRACT_CELL:
!
!> Extract data from cell.
!<
subroutine extract_cell(ndim,n1,n2,n3,a1,a2,a3,xarr1,xarr2,xarr3,x1,x2,x3,icell,jcell,kcell,icell_found,jcell_found,kcell_found)

  use morpheus_data_structure, only : dp
  use constants              , only : zero

  implicit none
  
  integer                     , intent(in ) :: ndim,n1,n2,n3,a1,a2,a3
  real(dp), dimension(1-a1:n1), intent(in ) :: xarr1
  real(dp), dimension(1-a1:n2), intent(in ) :: xarr2
  real(dp), dimension(1-a1:n3), intent(in ) :: xarr3
  real(dp)                    , intent(in ) :: x1,x2,x3
  integer                     , intent(out) :: icell,jcell,kcell
  logical                     , intent(out) :: icell_found,jcell_found,kcell_found
  
  integer  :: n
  real(dp) :: dx0,dx1,dx2
  
        
  ! Locate direction 1 index for current image pixel
  do n = 1,n1
     dx1 = x1 - xarr1(n-1)
     dx2 = x1 - xarr1(n  )
     dx0 = dx1 * dx2
     if(dx0 .le. zero)then
        icell = n
        icell_found = .true.
        exit
     endif
  enddo
  
  ! Locate direction 2 index for current image pixel
  if(ndim > 1)then
     do n = 1,n2
        dx1 = x2 - xarr2(n-1)
        dx2 = x2 - xarr2(n  )
        dx0 = dx1 * dx2
        if(dx0 .le. zero)then
           jcell = n
           jcell_found = .true.
           exit
        endif
     enddo
  else
     jcell = 1
     jcell_found = .true.
  endif
  
  ! Locate direction 3 index for current image pixel
  if(ndim > 2)then
     do n = 1,n3
        dx1 = x3 - xarr3(n-1)
        dx2 = x3 - xarr3(n  )
        dx0 = dx1 * dx2
        if(dx0 .le. zero)then
           kcell = n
           kcell_found = .true.
           exit
        endif
     enddo
  else
     kcell = 1
     kcell_found = .true.
  endif
  
  return
  
end subroutine extract_cell

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine SCAN_DATA_SLICES:
!
!> Scans the data to find minimum and maximum values.
!<
subroutine scan_data_slices

  use constants
  use plotparameters

  implicit none
  
  integer :: i,j,k

  dminsliceyz =  large_number ; dminslicexz =  large_number ; dminslicexy =  large_number
  dmaxsliceyz = -large_number ; dmaxslicexz = -large_number ; dmaxslicexy = -large_number
  vminsliceyz =  large_number ; vminslicexz =  large_number ; vminslicexy =  large_number
  vmaxsliceyz = -large_number ; vmaxslicexz = -large_number ; vmaxslicexy = -large_number
  pminsliceyz =  large_number ; pminslicexz =  large_number ; pminslicexy =  large_number
  pmaxsliceyz = -large_number ; pmaxslicexz = -large_number ; pmaxslicexy = -large_number
  tminsliceyz =  large_number ; tminslicexz =  large_number ; tminslicexy =  large_number
  tmaxsliceyz = -large_number ; tmaxslicexz = -large_number ; tmaxslicexy = -large_number
  
  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(k,j) &
  !$OMP REDUCTION(MIN:dminsliceyz) REDUCTION(MIN:pminsliceyz) REDUCTION(MIN:vminsliceyz) REDUCTION(MIN:tminsliceyz) &
  !$OMP REDUCTION(MAX:dmaxsliceyz) REDUCTION(MAX:pmaxsliceyz) REDUCTION(MAX:vmaxsliceyz) REDUCTION(MAX:tmaxsliceyz)
  !$OMP DO COLLAPSE(2)
  do k = 1,nzslice
     do j = 1,nyslice
        if(d_log)then
           if(dsliceyz(j,k) > zero)then
              dsliceyz(j,k) = log10(dsliceyz(j,k))
              if(dsliceyz(j,k) < dminsliceyz) dminsliceyz = dsliceyz(j,k)
              if(dsliceyz(j,k) > dmaxsliceyz) dmaxsliceyz = dsliceyz(j,k)
           else
              dsliceyz(j,k) = -100.0_dp
           endif
        else
           if(dsliceyz(j,k) > -one)then
              if(dsliceyz(j,k) < dminsliceyz) dminsliceyz = dsliceyz(j,k)
              if(dsliceyz(j,k) > dmaxsliceyz) dmaxsliceyz = dsliceyz(j,k)
           endif
        endif
        if(p_log)then
           if(psliceyz(j,k) > zero)then
              psliceyz(j,k) = log10(psliceyz(j,k))
              if(psliceyz(j,k) < pminsliceyz) pminsliceyz = psliceyz(j,k)
              if(psliceyz(j,k) > pmaxsliceyz) pmaxsliceyz = psliceyz(j,k)
           else
              psliceyz(j,k) = -100.0_dp
           endif
        else
           if(psliceyz(j,k) > -one)then
              if(psliceyz(j,k) < pminsliceyz) pminsliceyz = psliceyz(j,k)
              if(psliceyz(j,k) > pmaxsliceyz) pmaxsliceyz = psliceyz(j,k)
           endif
        endif
        if(v_log)then
           if(vsliceyz(j,k) > zero)then
              vsliceyz(j,k) = log10(vsliceyz(j,k))
              if(vsliceyz(j,k) < vminsliceyz) vminsliceyz = vsliceyz(j,k)
              if(vsliceyz(j,k) > vmaxsliceyz) vmaxsliceyz = vsliceyz(j,k)
           else
              vsliceyz(j,k) = -100.0_dp
           endif
        else
           if(vsliceyz(j,k) > -one)then
              if(vsliceyz(j,k) < vminsliceyz) vminsliceyz = vsliceyz(j,k)
              if(vsliceyz(j,k) > vmaxsliceyz) vmaxsliceyz = vsliceyz(j,k)
           endif
        endif
        if(t_log)then
           if(tsliceyz(j,k) > zero)then
              tsliceyz(j,k) = log10(tsliceyz(j,k))
              if(tsliceyz(j,k) < tminsliceyz) tminsliceyz = tsliceyz(j,k)
              if(tsliceyz(j,k) > tmaxsliceyz) tmaxsliceyz = tsliceyz(j,k)
           else
              tsliceyz(j,k) = -100.0_dp
           endif
        else
           if(tsliceyz(j,k) > -one)then
              if(tsliceyz(j,k) < tminsliceyz) tminsliceyz = tsliceyz(j,k)
              if(tsliceyz(j,k) > tmaxsliceyz) tmaxsliceyz = tsliceyz(j,k)
           endif
        endif
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,k) &
  !$OMP REDUCTION(MIN:dminslicexz) REDUCTION(MIN:pminslicexz) REDUCTION(MIN:vminslicexz) REDUCTION(MIN:tminslicexz) &
  !$OMP REDUCTION(MAX:dmaxslicexz) REDUCTION(MAX:pmaxslicexz) REDUCTION(MAX:vmaxslicexz) REDUCTION(MAX:tmaxslicexz)
  !$OMP DO COLLAPSE(2)
  do k = 1,nzslice
     do i = 1,nxslice
        if(d_log)then
           if(dslicexz(i,k) > zero)then
              dslicexz(i,k) = log10(dslicexz(i,k))
              if(dslicexz(i,k) < dminslicexz) dminslicexz = dslicexz(i,k)
              if(dslicexz(i,k) > dmaxslicexz) dmaxslicexz = dslicexz(i,k)
           else
              dslicexz(i,k) = -100.0_dp
           endif
        else
           if(dslicexz(i,k) > -one)then
              if(dslicexz(i,k) < dminslicexz) dminslicexz = dslicexz(i,k)
              if(dslicexz(i,k) > dmaxslicexz) dmaxslicexz = dslicexz(i,k)
           endif
        endif
        if(p_log)then
           if(pslicexz(i,k) > zero)then
              pslicexz(i,k) = log10(pslicexz(i,k))
              if(pslicexz(i,k) < pminslicexz) pminslicexz = pslicexz(i,k)
              if(pslicexz(i,k) > pmaxslicexz) pmaxslicexz = pslicexz(i,k)
           else
              pslicexz(i,k) = -100.0_dp
           endif
        else
           if(pslicexz(i,k) > -one)then
              if(pslicexz(i,k) < pminslicexz) pminslicexz = pslicexz(i,k)
              if(pslicexz(i,k) > pmaxslicexz) pmaxslicexz = pslicexz(i,k)
           endif
        endif
        if(v_log)then
           if(vslicexz(i,k) > zero)then
              vslicexz(i,k) = log10(vslicexz(i,k))
              if(vslicexz(i,k) < vminslicexz) vminslicexz = vslicexz(i,k)
              if(vslicexz(i,k) > vmaxslicexz) vmaxslicexz = vslicexz(i,k)
           else
              vslicexz(i,k) = -100.0_dp
           endif
        else
           if(vslicexz(i,k) > -one)then
              if(vslicexz(i,k) < vminslicexz) vminslicexz = vslicexz(i,k)
              if(vslicexz(i,k) > vmaxslicexz) vmaxslicexz = vslicexz(i,k)
           endif
        endif
        if(t_log)then
           if(tslicexz(i,k) > zero)then
              tslicexz(i,k) = log10(tslicexz(i,k))
              if(tslicexz(i,k) < tminslicexz) tminslicexz = tslicexz(i,k)
              if(tslicexz(i,k) > tmaxslicexz) tmaxslicexz = tslicexz(i,k)
           else
              tslicexz(i,k) = -100.0_dp
           endif
        else
           if(tslicexz(i,k) > -one)then
              if(tslicexz(i,k) < tminslicexz) tminslicexz = tslicexz(i,k)
              if(tslicexz(i,k) > tmaxslicexz) tmaxslicexz = tslicexz(i,k)
           endif
        endif
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j) &
  !$OMP REDUCTION(MIN:dminslicexy) REDUCTION(MIN:pminslicexy) REDUCTION(MIN:vminslicexy) REDUCTION(MIN:tminslicexy) &
  !$OMP REDUCTION(MAX:dmaxslicexy) REDUCTION(MAX:pmaxslicexy) REDUCTION(MAX:vmaxslicexy) REDUCTION(MAX:tmaxslicexy)
  !$OMP DO COLLAPSE(2)
  do j = 1,nyslice
     do i = 1,nxslice
        if(d_log)then
           if(dslicexy(i,j) > zero)then
              dslicexy(i,j) = log10(dslicexy(i,j))
              if(dslicexy(i,j) < dminslicexy) dminslicexy = dslicexy(i,j)
              if(dslicexy(i,j) > dmaxslicexy) dmaxslicexy = dslicexy(i,j)
           else
              dslicexy(i,j) = -100.0_dp
           endif
        else
           if(dslicexy(i,j) > -one)then
              if(dslicexy(i,j) < dminslicexy) dminslicexy = dslicexy(i,j)
              if(dslicexy(i,j) > dmaxslicexy) dmaxslicexy = dslicexy(i,j)
           endif
        endif
        if(p_log)then
           if(pslicexy(i,j) > zero)then
              pslicexy(i,j) = log10(pslicexy(i,j))
              if(pslicexy(i,j) < pminslicexy) pminslicexy = pslicexy(i,j)
              if(pslicexy(i,j) > pmaxslicexy) pmaxslicexy = pslicexy(i,j)
           else
              pslicexy(i,j) = -100.0_dp
           endif
        else
           if(pslicexy(i,j) > -one)then
              if(pslicexy(i,j) < pminslicexy) pminslicexy = pslicexy(i,j)
              if(pslicexy(i,j) > pmaxslicexy) pmaxslicexy = pslicexy(i,j)
           endif
        endif
        if(v_log)then
           if(vslicexy(i,j) > zero)then
              vslicexy(i,j) = log10(vslicexy(i,j))
              if(vslicexy(i,j) < vminslicexy) vminslicexy = vslicexy(i,j)
              if(vslicexy(i,j) > vmaxslicexy) vmaxslicexy = vslicexy(i,j)
           else
              vslicexy(i,j) = -100.0_dp
           endif
        else
           if(vslicexy(i,j) > -one)then
              if(vslicexy(i,j) < vminslicexy) vminslicexy = vslicexy(i,j)
              if(vslicexy(i,j) > vmaxslicexy) vmaxslicexy = vslicexy(i,j)
           endif
        endif
        if(t_log)then
           if(tslicexy(i,j) > zero)then
              tslicexy(i,j) = log10(tslicexy(i,j))
              if(tslicexy(i,j) < tminslicexy) tminslicexy = tslicexy(i,j)
              if(tslicexy(i,j) > tmaxslicexy) tmaxslicexy = tslicexy(i,j)
           else
              tslicexy(i,j) = -100.0_dp
           endif
        else
           if(tslicexy(i,j) > -one)then
              if(tslicexy(i,j) < tminslicexy) tminslicexy = tslicexy(i,j)
              if(tslicexy(i,j) > tmaxslicexy) tmaxslicexy = tslicexy(i,j)
           endif
        endif
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL
  
  if(.not.auto_adjust)then
     dminsliceyz = dmin ; dminslicexz = dmin ; dminslicexy = dmin
     dmaxsliceyz = dmax ; dmaxslicexz = dmax ; dmaxslicexy = dmax
     vminsliceyz = vmin ; vminslicexz = vmin ; vminslicexy = vmin
     vmaxsliceyz = vmax ; vmaxslicexz = vmax ; vmaxslicexy = vmax
     pminsliceyz = pmin ; pminslicexz = pmin ; pminslicexy = pmin
     pmaxsliceyz = pmax ; pmaxslicexz = pmax ; pmaxslicexy = pmax
     tminsliceyz = tmin ; tminslicexz = tmin ; tminslicexy = tmin
     tmaxsliceyz = tmax ; tmaxslicexz = tmax ; tmaxslicexy = tmax
  endif
  
  write(*,'(a)') '--------------------------------------------------------'
  write(*,'(a)') ' Limits for y-z slice:' 
  write(*,'(a,es11.3,es11.3)') ' Density    : ',dminsliceyz,dmaxsliceyz
  write(*,'(a,es11.3,es11.3)') ' Velocity   : ',vminsliceyz,vmaxsliceyz
  write(*,'(a,es11.3,es11.3)') ' Pressure   : ',pminsliceyz,pmaxsliceyz
  write(*,'(a,es11.3,es11.3)') ' Temperature: ',tminsliceyz,tmaxsliceyz
  write(*,'(a)') '--------------------------------------------------------'
  write(*,'(a)') ' Limits for x-z slice:' 
  write(*,'(a,es11.3,es11.3)') ' Density    : ',dminslicexz,dmaxslicexz
  write(*,'(a,es11.3,es11.3)') ' Velocity   : ',vminslicexz,vmaxslicexz
  write(*,'(a,es11.3,es11.3)') ' Pressure   : ',pminslicexz,pmaxslicexz
  write(*,'(a,es11.3,es11.3)') ' Temperature: ',tminslicexz,tmaxslicexz
  write(*,'(a)') '--------------------------------------------------------'
  write(*,'(a)') ' Limits for x-y slice:' 
  write(*,'(a,es11.3,es11.3)') ' Density    : ',dminslicexy,dmaxslicexy
  write(*,'(a,es11.3,es11.3)') ' Velocity   : ',vminslicexy,vmaxslicexy
  write(*,'(a,es11.3,es11.3)') ' Pressure   : ',pminslicexy,pmaxslicexy
  write(*,'(a,es11.3,es11.3)') ' Temperature: ',tminslicexy,tmaxslicexy
  write(*,'(a)') '--------------------------------------------------------'

  return
  
end subroutine scan_data_slices

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine WRITE_TXT:
!
!> Creates ASCII text files containing the slice data for density, velocity, pressure, temperature.
!<
subroutine write_txt(ifile,rank)

  use constants
  use plotparameters

  implicit none

  integer             :: i,j,k,icol,ifile,rank
  integer             :: idxyd,idxyv,idxyp,idxyt
  integer             :: idyzd,idyzv,idyzp,idyzt
  integer             :: idxzd,idxzv,idxzp,idxzt
  character (len=str) :: root
  character (len=str) :: fnamexyd,fnamexyv,fnamexyp,fnamexyt
  character (len=str) :: fnameyzd,fnameyzv,fnameyzp,fnameyzt
  character (len=str) :: fnamexzd,fnamexzv,fnamexzp,fnamexzt
  
  idxyd = 17
  idxyv = 18
  idxyp = 19
  idxyt = 20
  idyzd = 21
  idyzv = 22
  idyzp = 23
  idyzt = 24
  idxzd = 25
  idxzv = 26
  idxzp = 27
  idxzt = 28
  
  ! X-Y slices
  
  call generate_filename(.false.,.true.,ifile,rank,'dxy','.txt ',fnamexyd)
  write(*,*) 'writing density     image to '//trim(fnamexyd)
  open (idxyd,file=trim(fnamexyd),form='formatted')
  write(idxyd,*) '# ',nxslice,nyslice,x1slice(0),x1slice(nxslice),x2slice(0),x2slice(nyslice)
  
  call generate_filename(.false.,.true.,ifile,rank,'vxy','.txt ',fnamexyv)
  write(*,*) 'writing velocity    image to '//trim(fnamexyv)
  open (idxyv,file=trim(fnamexyv),form='formatted')
  write(idxyv,*) '# ',nxslice,nyslice,x1slice(0),x1slice(nxslice),x2slice(0),x2slice(nyslice)
  
  call generate_filename(.false.,.true.,ifile,rank,'pxy','.txt ',fnamexyp)
  write(*,*) 'writing pressure    image to '//trim(fnamexyp)
  open (idxyp,file=trim(fnamexyp),form='formatted')
  write(idxyp,*) '# ',nxslice,nyslice,x1slice(0),x1slice(nxslice),x2slice(0),x2slice(nyslice)
  
  call generate_filename(.false.,.true.,ifile,rank,'txy','.txt ',fnamexyt)
  write(*,*)'writing temperature image to '//trim(fnamexyt)
  open (idxyt,file=trim(fnamexyt),form='formatted')
  write(idxyt,*) '# ',nxslice,nyslice,x1slice(0),x1slice(nxslice),x2slice(0),x2slice(nyslice)
  
  do j = 1,nyslice
     do i = 1,nxslice
        write(idxyd,*) xn1slice(i),xn2slice(j),dslicexy(i,j)
        write(idxyv,*) xn1slice(i),xn2slice(j),vslicexy(i,j)
        write(idxyp,*) xn1slice(i),xn2slice(j),pslicexy(i,j)
        write(idxyt,*) xn1slice(i),xn2slice(j),tslicexy(i,j)
     enddo
     write(idxyd,*)
     write(idxyv,*)
     write(idxyp,*)
     write(idxyt,*)
  enddo
  
  close(idxyd)
  close(idxyv)
  close(idxyp)
  close(idxyt)
     
  ! Y-Z slices
  
  call generate_filename(.false.,.true.,ifile,rank,'dyz','.txt ',fnameyzd)
  write(*,*) 'writing density     image to ',trim(fnameyzd)
  open (idyzd,file=trim(fnameyzd),form='formatted')
  write(idyzd,*) '# ',nyslice,nzslice,x2slice(0),x2slice(nyslice),x3slice(0),x3slice(nzslice)
  
  call generate_filename(.false.,.true.,ifile,rank,'vyz','.txt ',fnameyzv)
  write(*,*) 'writing velocity    image to ',trim(fnameyzv)
  open (idyzv,file=trim(fnameyzv),form='formatted')
  write(idyzv,*) '# ',nyslice,nzslice,x2slice(0),x2slice(nyslice),x3slice(0),x3slice(nzslice)
  
  call generate_filename(.false.,.true.,ifile,rank,'pyz','.txt ',fnameyzp)
  write(*,*) 'writing pressure    image to ',trim(fnameyzp)
  open (idyzp,file=trim(fnameyzp),form='formatted')
  write(idyzp,*) '# ',nyslice,nzslice,x2slice(0),x2slice(nyslice),x3slice(0),x3slice(nzslice)
  
  call generate_filename(.false.,.true.,ifile,rank,'tyz','.txt ',fnameyzt)
  write(*,*) 'writing temperature image to ',trim(fnameyzt)
  open (idyzt,file=trim(fnameyzt),form='formatted')
  write(idyzt,*) '# ',nyslice,nzslice,x2slice(0),x2slice(nyslice),x3slice(0),x3slice(nzslice)
  
  do k = 1,nzslice
     do j = 1,nyslice
        write(idyzd,*) xn2slice(j),xn3slice(k),dsliceyz(j,k)
        write(idyzv,*) xn2slice(j),xn3slice(k),vsliceyz(j,k)
        write(idyzp,*) xn2slice(j),xn3slice(k),psliceyz(j,k)
        write(idyzt,*) xn2slice(j),xn3slice(k),tsliceyz(j,k)
     enddo
     write(idyzd,*)
     write(idyzv,*)
     write(idyzp,*)
     write(idyzt,*)
  enddo
  
  close(idyzd)
  close(idyzv)
  close(idyzp)
  close(idyzt)
     
  ! X-Z slices
  
  call generate_filename(.false.,.true.,ifile,rank,'dxz','.txt ',fnamexzd)
  write(*,*) 'writing density     image to ',trim(fnamexzd)
  open (idxzd,file=trim(fnamexzd),form='formatted')
  write(idxzd,*) '# ',nxslice,nzslice,x1slice(0),x1slice(nxslice),x3slice(0),x3slice(nzslice)
  
  call generate_filename(.false.,.true.,ifile,rank,'vxz','.txt ',fnamexzv)
  write(*,*) 'writing velocity    image to ',trim(fnamexzv)
  open (idxzv,file=trim(fnamexzv),form='formatted')
  write(idxzv,*) '# ',nxslice,nzslice,x1slice(0),x1slice(nxslice),x3slice(0),x3slice(nzslice)
  
  call generate_filename(.false.,.true.,ifile,rank,'pxz','.txt ',fnamexzp)
  write(*,*) 'writing pressure    image to ',trim(fnamexzp)
  open (idxzp,file=trim(fnamexzp),form='formatted')
  write(idxzp,*) '# ',nxslice,nzslice,x1slice(0),x1slice(nxslice),x3slice(0),x3slice(nzslice)
  
  call generate_filename(.false.,.true.,ifile,rank,'txz','.txt ',fnamexzt)
  write(*,*) 'writing temperature image to ',trim(fnamexzt)
  open (idxzt,file=trim(fnamexzt),form='formatted')
  write(idxzt,*) '# ',nxslice,nzslice,x1slice(0),x1slice(nxslice),x3slice(0),x3slice(nzslice)
  
  do k = 1,nzslice
     do i = 1,nxslice
        write(idxzd,*) xn1slice(i),xn3slice(k),dslicexz(i,k)
        write(idxzv,*) xn1slice(i),xn3slice(k),vslicexz(i,k)
        write(idxzp,*) xn1slice(i),xn3slice(k),pslicexz(i,k)
        write(idxzt,*) xn1slice(i),xn3slice(k),tslicexz(i,k)
     enddo
     write(idxzd,*)
     write(idxzv,*)
     write(idxzp,*)
     write(idxzt,*)
  enddo
  
  close(idxzd)
  close(idxzv)
  close(idxzp)
  close(idxzt)
     
  return

end subroutine write_txt

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine WRITE_PPM:
!
!> Creates a ppm image slice through the data for density, velocity, pressure and temperature.
!<
subroutine write_ppm(ifile,rank)

  use constants
  use plotparameters

  implicit none

  integer             :: i,j,k,icol,ifile,rank
  integer             :: idxyd,idxyv,idxyp,idxyt
  integer             :: idyzd,idyzv,idyzp,idyzt
  integer             :: idxzd,idxzv,idxzp,idxzt
  character (len=str) :: root
  character (len=str) :: fnamexyd,fnamexyv,fnamexyp,fnamexyt
  character (len=str) :: fnameyzd,fnameyzv,fnameyzp,fnameyzt
  character (len=str) :: fnamexzd,fnamexzv,fnamexzp,fnamexzt
     
  idxyd = 29
  idxyv = 30
  idxyp = 31
  idxyt = 32
  idyzd = 33
  idyzv = 34
  idyzp = 35
  idyzt = 36
  idxzd = 37
  idxzv = 38
  idxzp = 39
  idxzt = 40
  
  ! X-Y slices
  
  call generate_filename(.false.,.true.,ifile,rank,'dxy','.ppm ',fnamexyd)
  write(*,*) 'writing density     image to '//trim(fnamexyd)
  open (idxyd,file=trim(fnamexyd),status='replace')
  write(idxyd,'(a2)') 'P6'
  write(idxyd,'(i0,'' '',i0)') nxslice,nyslice
  write(idxyd,'(i0)') ncolors
  
  call generate_filename(.false.,.true.,ifile,rank,'vxy','.ppm ',fnamexyv)
  write(*,*) 'writing velocity    image to '//trim(fnamexyv)
  open (idxyv,file=trim(fnamexyv),status='replace')
  write(idxyv,'(a2)') 'P6'
  write(idxyv,'(i0,'' '',i0)') nxslice,nyslice
  write(idxyv,'(i0)') ncolors
  
  call generate_filename(.false.,.true.,ifile,rank,'pxy','.ppm ',fnamexyp)
  write(*,*) 'writing pressure    image to '//trim(fnamexyp)
  open (idxyp,file=trim(fnamexyp),status='replace')
  write(idxyp,'(a2)') 'P6'
  write(idxyp,'(i0,'' '',i0)') nxslice,nyslice
  write(idxyp,'(i0)') ncolors
  
  call generate_filename(.false.,.true.,ifile,rank,'txy','.ppm ',fnamexyt)
  write(*,*)'writing temperature image to '//trim(fnamexyt)
  open (idxyt,file=trim(fnamexyt),status='replace')
  write(idxyt,'(a2)') 'P6'
  write(idxyt,'(i0,'' '',i0)') nxslice,nyslice
  write(idxyt,'(i0)') ncolors
  
  do j = nyslice,1,-1
     do i = 1,nxslice
        if(dslicexy(i,j).le.dminslicexy)then
           icol = 0
        elseif(dslicexy(i,j).ge.dmaxslicexy)then
           icol = ncolors
        else
           icol = int(((dslicexy(i,j)-dminslicexy)/(dmaxslicexy-dminslicexy))*ncolors)
        endif
        write(idxyd,'(3a1)',advance='no') achar(red(icol)),achar(green(icol)),achar(blue(icol))
        if(vslicexy(i,j).le.vminslicexy)then
           icol = 0
        elseif(vslicexy(i,j).ge.vmaxslicexy)then
           icol = ncolors
        else
           icol = int(((vslicexy(i,j)-vminslicexy)/(vmaxslicexy-vminslicexy))*ncolors)
        endif
        write(idxyv,'(3a1)',advance='no') achar(red(icol)),achar(green(icol)),achar(blue(icol))
        if(pslicexy(i,j).le.pminslicexy)then
           icol = 0
        elseif(pslicexy(i,j).ge.pmaxslicexy)then
           icol = ncolors
        else
           icol = int(((pslicexy(i,j)-pminslicexy)/(pmaxslicexy-pminslicexy))*ncolors)
        endif
        write(idxyp,'(3a1)',advance='no') achar(red(icol)),achar(green(icol)),achar(blue(icol))
        if(tslicexy(i,j).le.tminslicexy)then
           icol = 0
        elseif(tslicexy(i,j).ge.tmaxslicexy)then
           icol = ncolors
        else
           icol = int(((tslicexy(i,j)-tminslicexy)/(tmaxslicexy-tminslicexy))*ncolors)
        endif
        write(idxyt,'(3a1)',advance='no') achar(red(icol)),achar(green(icol)),achar(blue(icol))
     enddo
  enddo
  
  close(idxyd)
  close(idxyv)
  close(idxyp)
  close(idxyt)
     
  ! Y-Z slices
  
  call generate_filename(.false.,.true.,ifile,rank,'dyz','.ppm ',fnameyzd)
  write(*,*) 'writing density     image to ',trim(fnameyzd)
  open (idyzd,file=trim(fnameyzd),status='replace')
  write(idyzd,'(a2)') 'P6'
  write(idyzd,'(i0,'' '',i0)') nyslice,nzslice
  write(idyzd,'(i0)') ncolors
  
  call generate_filename(.false.,.true.,ifile,rank,'vyz','.ppm ',fnameyzv)
  write(*,*) 'writing velocity    image to ',trim(fnameyzv)
  open (idyzv,file=trim(fnameyzv),status='replace')
  write(idyzv,'(a2)') 'P6'
  write(idyzv,'(i0,'' '',i0)') nyslice,nzslice
  write(idyzv,'(i0)') ncolors
  
  call generate_filename(.false.,.true.,ifile,rank,'pyz','.ppm ',fnameyzp)
  write(*,*) 'writing pressure    image to ',trim(fnameyzp)
  open (idyzp,file=trim(fnameyzp),status='replace')
  write(idyzp,'(a2)') 'P6'
  write(idyzp,'(i0,'' '',i0)') nyslice,nzslice
  write(idyzp,'(i0)') ncolors
  
  call generate_filename(.false.,.true.,ifile,rank,'tyz','.ppm ',fnameyzt)
  write(*,*) 'writing temperature image to ',trim(fnameyzt)
  open (idyzt,file=trim(fnameyzt),status='replace')
  write(idyzt,'(a2)') 'P6'
  write(idyzt,'(i0,'' '',i0)') nyslice,nzslice
  write(idyzt,'(i0)') ncolors
        
  do k = nzslice,1,-1
     do j = 1,nyslice
        if(dsliceyz(j,k).le.dminsliceyz)then
           icol = 0
        elseif(dsliceyz(j,k).ge.dmaxsliceyz)then
           icol = ncolors
        else
           icol = int(((dsliceyz(j,k)-dminsliceyz)/(dmaxsliceyz-dminsliceyz))*ncolors)
        endif
        write(idyzd,'(3a1)',advance='no') achar(red(icol)),achar(green(icol)),achar(blue(icol))
        if(vsliceyz(j,k).le.vminsliceyz)then
           icol = 0
        elseif(vsliceyz(j,k).ge.vmaxsliceyz)then
           icol = ncolors
        else
           icol = int(((vsliceyz(j,k)-vminsliceyz)/(vmaxsliceyz-vminsliceyz))*ncolors)
        endif
        write(idyzv,'(3a1)',advance='no') achar(red(icol)),achar(green(icol)),achar(blue(icol))
        if(psliceyz(j,k).le.pminsliceyz)then
           icol = 0
        elseif(psliceyz(j,k).ge.pmaxsliceyz)then
           icol = ncolors
        else
           icol = int(((psliceyz(j,k)-pminsliceyz)/(pmaxsliceyz-pminsliceyz))*ncolors)
        endif
        write(idyzp,'(3a1)',advance='no') achar(red(icol)),achar(green(icol)),achar(blue(icol))
        if(tsliceyz(j,k).le.tminsliceyz)then
           icol = 0
        elseif(tsliceyz(j,k).ge.tmaxsliceyz)then
           icol = ncolors
        else
           icol = int(((tsliceyz(j,k)-tminsliceyz)/(tmaxsliceyz-tminsliceyz))*ncolors)
        endif
        write(idyzt,'(3a1)',advance='no') achar(red(icol)),achar(green(icol)),achar(blue(icol))
     enddo
  enddo
  
  close(idyzd)
  close(idyzv)
  close(idyzp)
  close(idyzt)
     
  ! X-Z slices
  
  call generate_filename(.false.,.true.,ifile,rank,'dxz','.ppm ',fnamexzd)
  write(*,*) 'writing density     image to ',trim(fnamexzd)
  open (idxzd,file=trim(fnamexzd),status='replace')
  write(idxzd,'(a2)') 'P6'
  write(idxzd,'(i0,'' '',i0)') nxslice,nzslice
  write(idxzd,'(i0)') ncolors
  
  call generate_filename(.false.,.true.,ifile,rank,'vxz','.ppm ',fnamexzv)
  write(*,*) 'writing velocity    image to ',trim(fnamexzv)
  open (idxzv,file=trim(fnamexzv),status='replace')
  write(idxzv,'(a2)') 'P6'
  write(idxzv,'(i0,'' '',i0)') nxslice,nzslice
  write(idxzv,'(i0)') ncolors
  
  call generate_filename(.false.,.true.,ifile,rank,'pxz','.ppm ',fnamexzp)
  write(*,*) 'writing pressure    image to ',trim(fnamexzp)
  open (idxzp,file=trim(fnamexzp),status='replace')
  write(idxzp,'(a2)') 'P6'
  write(idxzp,'(i0,'' '',i0)') nxslice,nzslice
  write(idxzp,'(i0)') ncolors
  
  call generate_filename(.false.,.true.,ifile,rank,'txz','.ppm ',fnamexzt)
  write(*,*) 'writing temperature image to ',trim(fnamexzt)
  open (idxzt,file=trim(fnamexzt),status='replace')
  write(idxzt,'(a2)') 'P6'
  write(idxzt,'(i0,'' '',i0)') nxslice,nzslice
  write(idxzt,'(i0)') ncolors
        
  do k = nzslice,1,-1
     do i = 1,nxslice
        if(dslicexz(i,k).le.dminslicexz)then
           icol = 0
        elseif(dslicexz(i,k).ge.dmaxslicexz)then
           icol = ncolors
        else
           icol = int(((dslicexz(i,k)-dminslicexz)/(dmaxslicexz-dminslicexz))*ncolors)
        endif
        write(idxzd,'(3a1)',advance='no') achar(red(icol)),achar(green(icol)),achar(blue(icol))
        if(vslicexz(i,k).le.vminslicexz)then
           icol = 0
        elseif(vslicexz(i,k).ge.vmaxslicexz)then
           icol = ncolors
        else
           icol = int(((vslicexz(i,k)-vminslicexz)/(vmaxslicexz-vminslicexz))*ncolors)
        endif
        write(idxzv,'(3a1)',advance='no') achar(red(icol)),achar(green(icol)),achar(blue(icol))
        if(pslicexz(i,k).le.pminslicexz)then
           icol = 0
        elseif(pslicexz(i,k).ge.pmaxslicexz)then
           icol = ncolors
        else
           icol = int(((pslicexz(i,k)-pminslicexz)/(pmaxslicexz-pminslicexz))*ncolors)
        endif
        write(idxzp,'(3a1)',advance='no') achar(red(icol)),achar(green(icol)),achar(blue(icol))
        if(tslicexz(i,k).le.tminslicexz)then
           icol = 0
        elseif(tslicexz(i,k).ge.tmaxslicexz)then
           icol = ncolors
        else
           icol = int(((tslicexz(i,k)-tminslicexz)/(tmaxslicexz-tminslicexz))*ncolors)
        endif
        write(idxzt,'(3a1)',advance='no') achar(red(icol)),achar(green(icol)),achar(blue(icol))
     enddo
  enddo
  
  close(idxzd)
  close(idxzv)
  close(idxzp)
  close(idxzt)
     
  return

end subroutine write_ppm

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine WRITE_INF:
!
!> Writes small file with simulation info.
!<
subroutine write_inf(ifile,var)

  use morpheus_data_structure

  implicit none

  integer             :: ifile
  character (len=str) :: fname
  type(morpheus_data) :: var
  
  call generate_filename(.false.,.true.,ifile,0,'inf','.txt ',fname)
  write(*,*) 'writing information file to '//trim(fname)
  
  open(43,file=trim(fname),form='formatted')
  
  write(43,*) '# Number of dimensions'                     !  1
  write(43,*) var%ndim                                     !  2
  write(43,*) '# Number of variables'                      !  3
  write(43,*) var%nvar                                     !  4
  write(43,*) '# Total number of CPUs'                     !  5
  write(43,*) var%nproc                                    !  6
  write(43,*) '# Number of CPUs in each dimension'         !  7
  write(43,*) var%ncpu                                     !  8
  write(43,*) '# Timestep number'                          !  9
  write(43,*) var%it                                       ! 10
  write(43,*) '# Geometry'                                 ! 11
  write(43,*) var%geometry                                 ! 12
  write(43,*) '# Time'                                     ! 13
  write(43,*) var%t                                        ! 14
!   write(43,*) '# Time limit'                               ! 15
!   write(43,*) var%tlim                                     ! 16
  write(43,*) '# Global number of cells in all directions' ! 17
  write(43,*) var%nx                                       ! 18
  write(43,*) '# Number of ghost cells'                    ! 19
  write(43,*) var%nghost                                   ! 20
  write(43,*) '# Time unit'                                ! 21
  write(43,*) var%time                                     ! 22
  write(43,*) '# Mass unit'                                ! 23
  write(43,*) var%mass                                     ! 24
  write(43,*) '# Length unit'                              ! 25
  write(43,*) var%length                                   ! 26
  write(43,*) '# Temperature unit'                         ! 27
  write(43,*) var%degree                                   ! 28
  write(43,*) '# Is radiative cooling switched on?'        ! 29
  write(43,*) var%cooling                                  ! 30
  write(43,*) '# Is artificial viscosity switched on?'     ! 31
  write(43,*) var%viscosity                                ! 32
  write(43,*) '# Is gravity switched on?'                  ! 33
  write(43,*) var%gravity                                  ! 34
  write(43,*) '# Same boundary conditions?'                ! 35
  write(43,*) var%same_bc                                  ! 36
  write(43,*) '# Hydro CFL number'                         ! 37
  write(43,*) var%cn_hydro                                 ! 38
  write(43,*) '# Radiative cooling CFL'                    ! 39
  write(43,*) var%cn_cooling                               ! 40
  write(43,*) '# Artificial viscosity factor'              ! 41
  write(43,*) var%visc                                     ! 42
  write(43,*) '# Gamma'                                    ! 43
  write(43,*) var%g                                        ! 44
  write(43,*) '# Mu'                                       ! 45
  write(43,*) var%mu                                       ! 46
  write(43,*) '# Cavitation density'                       ! 47
  write(43,*) var%dcav                                     ! 48
  write(43,*) '# Cavitation pressre'                       ! 49
  write(43,*) var%pcav                                     ! 50
!   write(43,*) '# Cumulative radiated energy'               ! 51
!   write(43,*) var%cumul_erad                               ! 52
  write(43,*) '# Slope type'                               ! 53
  write(43,*) var%slope_type                               ! 54
  write(43,*) '# Riemann solver'                           ! 55
  write(43,*) var%solver                                   ! 56
  write(43,*) '# Number of sources'                        ! 57
  write(43,*) var%nsource                                  ! 58
  write(43,*) '# Geomerty direction 1'                     ! 59
  write(43,*) var%geom1                                    ! 60
  write(43,*) '# Geomerty direction 2'                     ! 61
  write(43,*) var%geom2                                    ! 62
  write(43,*) '# Geomerty direction 3'                     ! 63
  write(43,*) var%geom3                                    ! 64
  write(43,*) '# Radiative cooling curve file'             ! 65
  write(43,*) var%coolcurve                                ! 66
  
  close(43)
  
  return
  
end subroutine write_inf

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine FREE_SLICE_MEMORY:
!
!> Release memory
!<
subroutine free_slice_memory

  use plotparameters
  
  implicit none

  deallocate(dslicexy,dslicexz,dsliceyz)
  deallocate(pslicexy,pslicexz,psliceyz)
  deallocate(vslicexy,vslicexz,vsliceyz)
  deallocate(tslicexy,tslicexz,tsliceyz)
  deallocate( x1slice, x2slice, x3slice)
  deallocate(xn1slice,xn2slice,xn3slice)
  
  return
  
end subroutine free_slice_memory

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine WRITE_VTK:
!
!> Plots profiles from 1D, 2D or 3D data to screen in real-time during the simulation.
!! This routine uses PGPLOT. A density, pressure, velocity and/or temperature profile can be
!! plotted by setting the variables d_slice, v_slice, p_slice and t_slice in the namelist.
!! The variables profile1, profile2 and profile3 are used to choose which profiles are to be
!! plotted (x, y and/or z). In the case of a 1D run, only the profile1 (x) is plotted.
!!
!! Output is in BINARY format.
!!
!! Note: the syntax form='unformatted',access='stream' in the 'open' statement is used to
!! reproduce the behaviour of the form='binary' statement which is only supported by ifort.
!<
subroutine write_vtk(ifile,rank,var)

  use constants
  use morpheus_data_structure

  implicit none

  integer                                   :: i,j,k,l,ifile,rank,icpu,is1,is2,ii,jj,kk
  integer                                   :: nbytes_xyz,nbytes_rho,nbytes_pre,nbytes_tem,nbytes_vec
  integer                                   :: ip,ioff0,ioff1,ioff2,ioff3,ioff4,nx,ny,nz
  integer                                   :: nx1,nx2,nx3,nx4,ny1,ny2,ny3,ny4,nz1,nz2,nz3,nz4
  character (len=str)                       :: fname1,fname2
  character (len=500)                       :: string
  character (len=8  )                       :: offset
  character (len=2  )                       :: prec_string
  character (len=1  ), parameter            :: end_rec = char(10)    ! end-character for binary-record finalize
  real(dp)                                  :: x,y,z,u1,u2,u3,u4,u5,u6,u7,u8,u9
  real(dp), dimension(:,:,:,:), allocatable :: xyz,uvw
  type(morpheus_data)                       :: var
  
  if(var%ndim == 1)then
     write(*,*) 'Warning: Cannot create VTK file from 1D dataset'
     return
  endif
  
  write(prec_string,'(i2)') dp*8

  nx1 = 0 ; nx2 = var%nxloc(1) ; nx3 = var%nxlower(1)-1 ; nx4 = var%nxupper(1)
  ny1 = 0 ; ny2 = var%nxloc(2) ; ny3 = var%nxlower(2)-1 ; ny4 = var%nxupper(2)
  
  if(var%ndim == 2)then
     nz1 = 1 ; nz2 = 1 ; nz3 = 0 ; nz4 = 0
  else
     nz1 = 0 ; nz2 = var%nxloc(3) ; nz3 = var%nxlower(3)-1 ; nz4 = var%nxupper(3)
  endif
  
  ! Generate individual cpu file name
  call generate_filename(.true.,.false.,ifile,rank,'---','.vts ',fname1)
  
  is1 = index(trim(fname1),'/') + 1
  is2 = len_trim(fname1)

  write(*,*) 'writing VTK file to ',trim(fname1)
  
  nbytes_xyz   = 3 *  (nx2-nx1+1)  * (ny2-ny1+1)  * (nz2-nz1+1)   * dp
  nbytes_rho   =      var%nxloc(1) * var%nxloc(2) * var%nxloc(3)  * dp
  nbytes_pre   =      var%nxloc(1) * var%nxloc(2) * var%nxloc(3)  * dp
  nbytes_tem   =      var%nxloc(1) * var%nxloc(2) * var%nxloc(3)  * dp
  nbytes_vec   = 3 * (var%nxloc(1) * var%nxloc(2) * var%nxloc(3)) * dp
  
  ioff0 = 0                                 ! xyz coordinates
  ioff1 = ioff0 + sizeof(ip) + nbytes_xyz   ! density
  ioff2 = ioff1 + sizeof(ip) + nbytes_rho   ! pressure
  ioff3 = ioff2 + sizeof(ip) + nbytes_pre   ! temperature
  ioff4 = ioff3 + sizeof(ip) + nbytes_tem   ! velocity vectors
  
  open (41,file=trim(fname1),form='unformatted',access='stream')
  write(41) '<?xml version="1.0"?>'//end_rec
  write(41) '<VTKFile type="StructuredGrid" version="0.1" byte_order="LittleEndian">'//end_rec
  write(string,'(a,6(i6),a)') '   <StructuredGrid WholeExtent="',nx3,nx4,ny3,ny4,nz3,nz4,'">'
  write(41) trim(string)//end_rec
  write(string,'(a,6(i6),a)') '   <Piece Extent="',nx3,nx4,ny3,ny4,nz3,nz4,'">'
  write(41) trim(string)//end_rec
  write(41) '      <Points>'//end_rec
  write(offset,'(i8)') ioff0
  write(41) '         <DataArray type="Float'//prec_string//'" Name="Coordinates" NumberOfComponents="3" format="appended" offset="'//offset//'" />'//end_rec
  write(41) '      </Points>'//end_rec
  write(41) '      <CellData>'//end_rec
  write(offset,'(i8)') ioff1
  write(41) '         <DataArray type="Float'//prec_string//'" Name="Density" format="appended" offset="'//offset//'" />'//end_rec
  write(offset,'(i8)') ioff2
  write(41) '         <DataArray type="Float'//prec_string//'" Name="Pressure" format="appended" offset="'//offset//'" />'//end_rec
  write(offset,'(i8)') ioff3
  write(41) '         <DataArray type="Float'//prec_string//'" Name="Temperature" format="appended" offset="'//offset//'" />'//end_rec
  write(offset,'(i8)') ioff4
  write(41) '         <DataArray type="Float'//prec_string//'" Name="Velocity" NumberOfComponents="3" format="appended" offset="'//offset//'" />'//end_rec
  write(41) '      </CellData>'//end_rec
  write(41) '   </Piece>'//end_rec
  write(41) '   </StructuredGrid>'//end_rec
  write(41) '   <AppendedData encoding="raw">'//end_rec
  write(41) '_'
  
  allocate(xyz(3,nx1:nx2,ny1:ny2,nz1:nz2))
  allocate(uvw(3,nx1:nx2,ny1:ny2,nz1:nz2))

  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,ii,jj,kk,x,y,z,u1,u2,u3,u4,u5,u6,u7,u8,u9)
  !$OMP DO COLLAPSE(3)
  do k = nz1,nz2
     do j = ny1,ny2
        do i = nx1,nx2
        
           ii = max(i,1) ; jj = max(j,1) ; kk = max(k,1)
        
           select case(var%geometry)
           case(1)
              x  =  var%x1(i)
              y  =  var%x2(j)
              z  =  var%x3(k)
              u1 =  var%velocity(1,ii,jj,kk)
              u2 =  zero
              u3 =  zero
              u4 =  var%velocity(2,ii,jj,kk)
              u5 =  zero
              u6 =  zero
              u7 =  var%velocity(3,ii,jj,kk)
              u8 =  zero
              u9 =  zero
           case(2)
              x  =  var%x1(i) * sin(var%x2(j)) * cos(var%x3(k))
              y  =  var%x1(i) * sin(var%x2(j)) * sin(var%x3(k))
              z  =  var%x1(i) * cos(var%x2(j))
              u1 =  var%velocity(1,ii,jj,kk) * sin(var%xn2(jj)) * cos(var%xn3(kk))
              u2 =  var%velocity(2,ii,jj,kk) * cos(var%xn2(jj)) * cos(var%xn3(kk))
              u3 = -var%velocity(3,ii,jj,kk) * sin(var%xn3(kk))
              u4 =  var%velocity(1,ii,jj,kk) * sin(var%xn2(jj)) * sin(var%xn3(kk))
              u5 =  var%velocity(2,ii,jj,kk) * cos(var%xn2(jj)) * sin(var%xn3(kk))
              u6 =  var%velocity(3,ii,jj,kk) * cos(var%xn3(kk))
              u7 =  var%velocity(1,ii,jj,kk) * cos(var%xn2(jj))
              u8 = -var%velocity(2,ii,jj,kk) * sin(var%xn2(jj))
              u9 =  zero
           case(3)
              x  =  var%x1(i) * cos(var%x3(k))
              y  =  var%x1(i) * sin(var%x3(k))
              z  =  var%x2(j)
              u1 =  var%velocity(1,ii,jj,kk) * cos(var%xn3(kk))
              u2 =  zero
              u3 = -var%velocity(3,ii,jj,kk) * sin(var%xn3(kk))
              u4 =  var%velocity(1,ii,jj,kk) * sin(var%xn3(kk))
              u5 =  zero
              u6 = -var%velocity(3,ii,jj,kk) * cos(var%xn3(kk))
              u7 =  zero
              u8 =  var%velocity(2,ii,jj,kk)
              u9 =  zero
           end select
        
           xyz(1,i,j,k) = x
           xyz(2,i,j,k) = y
           xyz(3,i,j,k) = z
           
           uvw(1,i,j,k) = u1 + u2 + u3
           uvw(2,i,j,k) = u4 + u5 + u6
           uvw(3,i,j,k) = u7 + u8 + u9
           
        enddo
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL
  
  write(41) nbytes_xyz,((((xyz          (l,i,j,k),l=1,3),i=nx1,nx2),j=ny1,ny2),k=nz1,nz2)
  write(41) nbytes_rho,((( var%density    (i,j,k)       ,i=  1,nx2),j=  1,ny2),k=  1,nz2)
  write(41) nbytes_pre,((( var%pressure   (i,j,k)       ,i=  1,nx2),j=  1,ny2),k=  1,nz2)
  write(41) nbytes_tem,((( var%temperature(i,j,k)       ,i=  1,nx2),j=  1,ny2),k=  1,nz2)
  write(41) nbytes_vec,((((uvw          (l,i,j,k),l=1,3),i=  1,nx2),j=  1,ny2),k=  1,nz2)
  
  write(41) '   </AppendedData>'//end_rec
  write(41) '</VTKFile>'//end_rec
  close(41)
    
  if(var%nproc > 1)then
  
     ! Generate master file name
     call generate_filename(.false.,.false.,ifile,rank,'---','.pvts',fname2)
  
     if(rank == 0)then
        
        nx = var%nx(1)
        ny = var%nx(2)
        if(var%ndim == 2)then
           nz = 0
        else
           nz = var%nx(3)
        endif
        
        write(*,*) 'Writing master VTK file: '//trim(fname2)
        
        open (42,file=fname2,form='formatted')
        write(42,'(a)') '<?xml version="1.0"?>'
        write(42,'(a)') '<VTKFile type="PStructuredGrid" version="0.1" byte_order="LittleEndian">'
        write(42,'(a,6(i6),a)') '   <PStructuredGrid WholeExtent="',0,nx,0,ny,0,nz,'">'
        write(42,'(a)') '      <PPoints>'
        write(42,'(a)') '         <PDataArray type="Float'//prec_string//'" Name="Coordinates" NumberOfComponents="3" />'
        write(42,'(a)') '      </PPoints>'
        write(42,'(a)') '      <PCellData>'
        write(42,'(a)') '         <PDataArray type="Float'//prec_string//'" Name="Density" NumberOfComponents="1"/>'
        write(42,'(a)') '         <PDataArray type="Float'//prec_string//'" Name="Pressure" NumberOfComponents="1"/>'
        write(42,'(a)') '         <PDataArray type="Float'//prec_string//'" Name="Temperature" NumberOfComponents="1"/>'
        write(42,'(a)') '         <PDataArray type="Float'//prec_string//'" Name="Velocity" NumberOfComponents="3"/>'
        write(42,'(a)') '      </PCellData>'
        
     else
     
        open (42,file=fname2,form='formatted',access='append')
        
     endif
     
     ! Append piece to master VTK file
     write(42,'(a,6(i6),a)')'      <Piece Extent="',nx3,nx4,ny3,ny4,nz3,nz4,'" Source="'//fname1(is1:is2)//'">'
     write(42,'(a)') '      </Piece>'
  
     ! Close master VTK file
     if(rank == var%nproc-1)then
        write(42,'(a)') '   </PStructuredGrid>'
        write(42,'(a)') '</VTKFile>'
        close(42)
     endif
     
  endif

  deallocate(xyz,uvw)
  
  return

end subroutine write_vtk

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Subroutine WRITE_CUB:
!
!> Write full data cube in ASCII format.
!<
subroutine write_cub(ifile,rank,var)

  use constants
  use morpheus_data_structure

  implicit none

  integer             :: i,j,k,ifile,rank,unit
  character (len=str) :: fname
  type(morpheus_data) :: var

  ! Generate file name
  call generate_filename(.false.,.false.,ifile,rank,'---','.cub ',fname)
  
  if(rank == 0)then
     open(61,file=trim(fname),form='formatted',status='replace')
  else
     open(61,file=trim(fname),form='formatted',status='old',access='append')
  endif
  
  write(61,'(a3,10(a))') '#  ',var%geom1//'                    ',&
                               var%geom2//'                    ',&
                               var%geom3//'                    ',&
                               'Density              ',&
                               'u_'//var%geom1//'                  ',&
                               'u_'//var%geom2//'                  ',&
                               'u_'//var%geom3//'                  ',&
                               'u_tot                ',&
                               'Pressure             ',&
                               'Temperature'
  
  do k = 1,var%nxloc(3)
     do j = 1,var%nxloc(2)
        do i = 1,var%nxloc(1)
        
           write(61,'(10(es21.12))') var%xn1(i),var%xn2(j),var%xn3(k),var%density(i,j,k)  ,&
                                     var%velocity(1,i,j,k),var%velocity(2,i,j,k),var%velocity(3,i,j,k),&
                                     var%vel_mag(i,j,k),var%pressure(i,j,k),var%temperature(i,j,k)
           
        enddo
     enddo
  enddo
  
  close(61)
  
  return
  
end subroutine write_cub

!###################################################################################################
!###################################################################################################
!###################################################################################################

!  Function ATAN3:
!
!> Modified version of the atan2 fortran intrinsic function.
!<
function atan3(y,x)

  use constants              , only : zero,two,pi
  use morpheus_data_structure, only : dp

  implicit none
  
  real(dp), intent(in) :: x,y
  real(dp)             :: atan3
  
  atan3 = atan2(y,x)
  if(atan3 .lt. zero) atan3 = two*pi + atan3
  
end function atan3
