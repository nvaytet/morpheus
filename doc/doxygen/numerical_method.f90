!> \page numericalmethod Numerical Method
!!
!! The code solves the Euler equations of fluid flow using an explicit second order Godunov scheme. The fluxes accross the cell interfaces are calculated using a Riemann solver.
!!
!! \section finvol The finite volume scheme
!!
!! The finite volume scheme used in this thesis was originally due to van Leer (1979) and was further
!! developed by Falle (1991). \n
!!
!! The conservative variables in the code are
!! \f{eqnarray*}{
!!       U(1) & = & \rho \\\
!!       U(2) & = & \rho u_{i} \\\
!!       U(3) & = & \rho u_{j} \\\
!!       U(4) & = & \rho u_{k} \\\
!!       U(5) & = & \frac{1}{2} \rho u^{2} + \frac{p}{\gamma -1}
!! \f} \n
!!
!! The equations of fluid flow are used explicitly in their flux form, in
!! which
!! \f[
!!    \frac{\partial \mathbf{q}}{\partial t} + \frac{\partial \mathbf{F}_{x}}{\partial x} +
!!    \frac{\partial \mathbf{F}_{y}}{\partial y} + \frac{\partial \mathbf{F}_{z}}{\partial z} = \mathbf{s}
!! \f]
!! where
!! \f{eqnarray*}{
!!    \mathbf{q} & = & \left( ~\rho ~,~ \rho u_{i} ~,~ \rho u_{j} ~,~ \rho u_{k} ~,~ E ~\right) \\\
!!    \mathbf{F}_{i} & = & \left( ~\rho u_{i} ~,~ \rho u_{i}^{2} + p ~,~ \rho u_{i} u_{j} ~,~ \rho u_{i} u_{k} ~,~ u_{i} (E+p) ~\right) \\\
!!    \mathbf{F}_{j} & = & \left( ~\rho u_{j} ~,~ \rho u_{i} u_{j} ~,~ \rho u_{j}^{2} + p ~,~ \rho u_{j} u_{k} ~,~ u_{j} (E+p) ~ \right) \\\
!!    \mathbf{F}_{k} & = & \left( ~\rho u_{k} ~,~ \rho u_{i} u_{k} ~,~ \rho u_{j} u_{k} ~,~ \rho u_{k}^{2} + p ~,~ u_{k} (E+p) ~ \right) \\\
!!    \mathbf{s} & = & \left( ~ s_{\rho} ~,~ s_{i} + g_{i} ~,~ s_{j} + g_{j} ~,~ s_{k} + g_{k} ~,~ s_{E} ~ \right)
!! \f}
!! in which \f$ i \f$, \f$ j \f$ and \f$ k \f$ are coordinate indices, \f$E\f$ is the gas internal energy and \f$g\f$ and \f$s\f$ are geometrical and external source terms, respectively.
!! In finite volume form, this becomes
!! \f[
!!    \displaystyle \frac{\overline{q}_{n+1} - \overline{q}_{n}}{\Delta t} = \hat{s} - \frac{1}{V} \sum_{\zeta}
!!    \left(A_{\zeta}^{R} \hat{F}_{\zeta}^{R} - A_{\zeta}^{L} \hat{F}_{\zeta}^{L} \right)
!! \f]
!! where \f$ \Delta t \f$ is the timestep.
!!
!! <HR>
!!
!! \section riemann The Riemann Solver
!!
!! The initial setup for a Riemann problem is described by
!! \f[
!!    q(x) ~=~ \left\{ \begin{array}{ll}
!!    q_{l} & \textrm{if} ~~ x < x_{0}\\\
!!    q_{r} & \textrm{if} ~~ x > x_{0}
!!    \end{array} \right.
!! \f]
!! and is shown on Fig. 1a.
!!
!! \image html riemann.jpg
!!
!! <CENTER>
!! Figure 1: (a) Initial setup for a Riemann problem. The l and r values \n
!! designate values to the left and right of the discontinuity at \f$ x = x_{0}\f$. \n
!! (b) The state of the fluid at a later time \f$ t \f$. A shock wave (propagating \n
!! to the right), a rarefaction wave (propagating to the left) and a contact \n
!! discontinuity (between the two waves) develop. (c) The temporal behaviour \n
!! of the waves and contact discontinuity.
!! </CENTER>
!!
!! The break-up of the discontinuity generates a set of waves which connect the two initial states and propagate away from the
!! discontinuity. The structure of the flow is illustrated in Fig. 1b, and its temporal behaviour is shown in Fig. 1c. Three
!! discontinuities separate regions in which the state variables are constant. A shock wave propagates into the region of lower
!! pressure across which the density and pressure jump to higher values, with all three state variables being discontinuous. The
!! shock is followed by a contact discontinuity across which the density is again discontinuous but the pressure and velocity are
!! constant. Finally, a rarefaction wave, composed of an infinite number of weak solutions, travels in the opposite direction and
!! has a very different structure: all of the state variables are continuous and there is a smooth transition.
!! \n \n
!! Provided the timestep \f$\Delta t\f$ is sufficiently small, the waves do not cross the entire cell and the state at the interface
!! remains constant over this timestep. The region situated between the outer waves is described by the state
!! \f$(\rho^{*},u^{*},p^{*})\f$ which is determined from solving the jump conditions. The flux across the cell interface is then
!! computed from this state and the speed of the waves.
!! \n \n
!! The technique to solve the jump conditions is that described in van Leer (1979). They can be written in terms of the
!! Lagrangian wave speeds \f$W_{l}\f$ and \f$W_{r}\f$ given by
!! \f{eqnarray*}{
!!    W_{l}(u^{*} - u_{l}) + (p^{*} - p_{l}) & = & 0 \\\
!!    W_{r}(u^{*} - u_{r}) - (p^{*} - p_{r}) & = & 0 ~~~.
!! \f}
!!  Eliminating \f$u^{*}\f$ gives
!! \f[
!!    p^{*} = \frac{W_{r} p_{l} + W_{l} p_{r} - W_{r}W_{l}(u_{r} - u_{l})}{W_{r} + W_{l}} ~~~.
!! \f]
!! The wave speeds are computed in terms of \f$p^{*}\f$ and the pre-wave pressure \f$p\f$ according to
!! \f[
!!    W = C \left[ 1 + \left( \frac{\gamma + 1}{2\gamma} \right) \frac{p^{*} - p}{p} \right] ^{1/2} ~~~, ~~~ p^{*} \geq p
!! \f]
!! for a shock and
!! \f[
!!    W = C \left( \frac{\gamma - 1}{2\gamma} \right) \left[ \frac{1 - p^{*}/p}{1-(p^{*}/p)^{(\gamma - 1)/2\gamma}} \right] ~~~, ~~~ p^{*} < p
!! \f]
!! for a rarefaction, where \f$C = \displaystyle \sqrt{\gamma p \rho} = \rho c_{s}\f$ is the Lagrangian sound speed.
!! \n \n
!! It is evident that the value of \f$p^{*}\f$ is required before any quantity can be calculated. In practise, \f$C\f$ is taken as
!! an initial guess for the wave speeds, which is then used to obtain a reasonable first estimate of \f$p^{*}\f$. An iterative
!! procedure is then employed in order to obtain the desired level of accuracy. If the estimate of \f$p^{*}\f$ computed through
!! the jump conditions differs from the pressures on either side of the discontinuity region by more than 10%, the computation of
!! \f$p^{*}\f$ is repeated from the previous estimate until convergence of the solution is reached.
!! \n \n
!! Once a satisfactory value of \f$p^{*}\f$ is returned, the values of the other quantities between the waves (\f$\rho^{*}\f$ and
!! \f$u^{*}\f$) can be calculated. The velocity is found according to
!! \f[
!!    u^{*} = \displaystyle \frac{W_{r} u_{r} + W_{l} u_{l} - (p_{r} - p_{l})}{W_{r} + W_{l}}~~~.
!! \f]
!! This velocity is used to determine which waves cross the interfaces. If it is positive, the contact discontinuity will move
!! into the right cell and the density just behind it will be that of \f$\rho_{2}\f$ on Fig. 1b, given by
!! \f[
!!    \rho_{2} = \displaystyle \frac{W_{l} \rho_{l}}{W_{l} - (u^{*} - u_{l})\rho_{l}}~~~.
!! \f]
!! The proper motion of the gas as a whole can have an important impact on the flux calulations, as rarefaction waves can only
!! move at the sound speed in the gas with respect to this motion. Hence, if the gas is flowing into the right cell at a speed
!! greater than the local sound speed, both waves move into the right cell and the flux is given by the amount of gas that crosses
!! the interface, which is that contained in the left cell (\f$q_{l}\f$). In the case of a rarefaction wave situated on the other
!! side of the interface, the flux is given by the intermediate values
!! \f{eqnarray*}{
!!    u^{*} & = & \displaystyle \frac{\gamma - 1}{\gamma + 1} \left( u_{l} + \frac{2c_{l}}{\gamma - 1} \right) \\\
!!    p^{*} & = & \displaystyle p_{l} \left(\frac{u^{*}}{c_{l}}\right)^{2\gamma / (\gamma - 1)} \\\
!!    \rho^{*} & = & \displaystyle \frac{\gamma p^{*}}{(u^{*})^{2}} ~~~.
!! \f}
!! \n
!! If the contact discontinuity is on the left of the interface, the primitive quantities are computed using the fluid state in
!! the right cell \f$q_{r}\f$, i.e.
!! \f[
!!    \rho_{3} = \displaystyle \frac{W_{r} \rho_{r}}{W_{r} - (u^{*} - u_{r})\rho_{r}}~~~.
!! \f]
!! and again the intermediate values are used if the rarefaction is to the right. The appropriate values (left, right or
!! intermediate; denoted by a ') are then used to calculate the fluxes of mass (\f$\hat{F}_{1}\f$), momentum (\f$\hat{F}_{2}\f$)
!! and energy (\f$\hat{F}_{3}\f$) according to
!! \f{eqnarray*}{
!!    \hat{F}_{1} & = & \rho'u' \\\
!!    \hat{F}_{2} & = & p' + \rho'(u')^{2} \\\
!!    \hat{F}_{3} & = & u' \displaystyle \frac{\gamma}{\gamma - 1} \left( p' + \frac{\rho'(u')^2}{2} \right)~~~.
!! \f}
!!
!! These fluxes are used to update the fluid state solution in a cell at time \f$t + \Delta t\f$. In the code, the quantities
!! \f$q_{l}\f$ and \f$q_{r}\f$ are simply fed into a flux calculating routine which returns the appropriate fluxes.
!!
!! <HR>
!!
!! \section secondorder The second order scheme
!!
!! Treating the all fluid variables as constant throughout each cell is a simple first order approach to the problem. For
!! second order Godunov scheme, the idea is not to limit ourselves to the first neighbouring cells to compute fluxes. Indeed,
!! more information can be obtained from other cells in the vicinity of the interface in question. As shown in Fig. 2, the value
!! of a quantity on each side of an interface can be improved by considering quantity gradients with surrounding cells. The first
!! order method is initially used over half a timestep to find intermediate estimates of the fluid variables. From these, the
!! gradients between the n-1 and n, and n and n+1 cells are calculated and an average gradient \f$G_{AV}\f$ is calculated.
!! \f$q_{l}\f$ is then found by linearly interpolating \f$\overline{q}_{n}\f$ from the cell nodal coordinate \f$\hat{x}_n\f$ to
!! the cell edge (\f$x=n~\Delta x\f$) along this average gradient according to
!! \f[
!!   q_{l} = q_{n} + G_{AV}~(n~\Delta x - \hat{x}_n) ~~~.
!! \f]
!! An analogous technique is employed to calculate \f$q_{r}\f$ and the new values are then fed into the Riemann solver for higher
!! accuracy flux computations over a full timestep, as linear variations of the fluid variables over the width of the cell are
!! now taken into account. This method remains conservative as the value at the nodal coordinate is kept the same, only the
!! \f$l\f$ and \f$r\f$ values are changed for the flux calculations.
!!
!! \image html gradient.jpg
!!
!! <CENTER>
!! Figure 2: The second order Godunov scheme. \f$G_{AV}^{l}\f$ is the average of gradients between \f$q_{n-1}\f$ and \f$q_{n}\f$,\n
!! and \f$q_{n}\f$ and \f$q_{n+1}\f$. \f$G_{AV}^{l}\f$ is used to find the value of \f$q_{l}\f$ at the cell edge. \f$q_{r}\f$ is found in the same way \n
!! using \f$G_{AV}^{r}\f$. Overlaid in red is what the n and n+1 cells `look' like from the point of view of the flux \n
!! through the interface at \f$x = n~\Delta x\f$ where \f$\Delta x\f$ is the width of a cell, assumed here to be constant along \f$x\f$.
!! </CENTER>
!!
!! The average gradient is defined by
!! \f[
!!    G_{AV} = av \left( \frac{q_{n+1} - q_{n}}{\hat{x}_{n+1} - \hat{x}_n} , \frac{q_{n} - q_{n-1}}{\hat{x}_n - \hat{x}_{n-1}} \right)
!! \f]
!! where \f$av(a,b)\f$ is the average of \f$a\f$ and \f$b\f$. The averaging function can have many forms, defined by the user to
!! suit the simulation best (Falle 1991). It is designed to reduce the order of the scheme in unstable regions of discontinuity
!! such as shocks, i.e. when the second derivative is large, retaining second order accuracy elsewhere. It must have the following
!! properties:
!! \f{eqnarray*}{
!!    \lim _{a\to b} av(a,b) & = & (a + b)/2 \\\
!!    av(a,b) & = & 0 ~~\mathrm{if} ~~ab < 0 \\\
!!    \lim _{|a|/|b| \to 0} av(a,b) & = & a  \\\
!!    \lim _{|a|/|b| \to \infty} av(a,b) & = & b ~~~.
!! \f}
!! There are many functions with these properties, the simplest being of the form
!! \f[
!!    av(a,b) = \displaystyle \frac{|a|^{m}b + |b|^{m}a}{|a|^{m} + |b|^{m}} ~~~.
!! \f]
!! For \f$m = 0\f$, \f$av(a,b)\f$ reduces to the simple arithmetic mean and the scheme is second order everywhere. We must hence
!! ensure \f$m > 0\f$ if the averaging function is to serve any useful purpose. Two common choices are the two extreme possibilities
!! \f[
!!    \begin{array}{lclll}
!!    av(a,b) & = & a & \textrm{if} & a<b \\\
!!    av(a,b) & = & b & \textrm{if} & b<a \\\
!!    av(a,b) & = & 0 & \textrm{if} & ab<0
!!    \end{array}
!! \f]
!! which corresponds to \f$m = +\infty\f$ (also known as minmod averaging) and
!! \f[
!!    \begin{array}{lclll}
!!    av(a,b) & = & \displaystyle \frac{|a|b + |b|a}{|a| + |b|} & \textrm{if} & ab>0 \\\
!!    av(a,b) & = & 0                                           & \textrm{if} & ab<0
!!    \end{array}
!! \f]
!! which corresponds to \f$m = 1\f$ (often used for very weak shocks).
!!
!! <HR>
!!
!! \section timestep Maximum timestep stability
!!
!! The discretisation of the differential equations of fluid flow is subject to instabilities following the Courant, Friedrichs
!! and Lewy (CFL) condition, which can cause the non-convergence of the solution to Riemann problems (Courant et al. 1967). As
!! fluxes are computed for one interface at a time, in order to keep the scheme consistent over time, one cannot allow a wave
!! to cross an entire cell width in a given timestep \f$\Delta t\f$. The entire grid is thus searched for the minimum wave
!! crossing time
!! \f[
!!    \tau = \displaystyle \frac{W_{\zeta}}{\Delta l_{\zeta}}
!! \f]
!! where \f$W_{\zeta}\f$ is the wave speed in the \f$\zeta\f$ direction and \f$\Delta l_{\zeta}\f$ is the smallest width of the
!! cell in the same direction. In the case of a cartesian grid, the cell widths are the same everywhere in the cell along all
!! coordinate directions, however in the case of spherical polar coordinates for instance, along the \f$\theta\f$ direction the
!! cell will have a shorter width on its side closest to the origin \f$r = 0\f$.
!! 
!! This minimum crossing time is then applied to the whole grid as the maximum timestep that can be used to ensure global
!! stability. A new timestep is calculated at the start of each computational loop, and in practise is set equal to a fraction
!! (or so called Courant number) of \f$\tau\f$.
!!
!! <HR>
!!
!! \section cooling Radiative cooling
!!
!! Hot astrophysical gas is predisposed to radiate, with an intensity dependent on its temperature. Energy losses via radiative
!! cooling can significantly affect the dynamics of a system and should be included in simulations in order to obtain more
!! realistic descriptions. The cooling rate \f$\Lambda (T)\f$ as a function of gas temperature \f$T\f$ was taken from
!! Raymondet al. (1976) who calculate the energy losses via radiation for a plasma of typical abundances. The cooling law follows
!! \f[
!!    \Lambda (T) = 10^{\beta} \times 10^{-23}~\mathrm{erg~cm}^{3}~\mathrm{s}^{-1}
!! \f]
!! and the power \f$\beta\f$ is plotted against \f$\log(T)\f$ in Fig. 3. The cooling processes taken into account are forbidden and
!! semi-forbidden line transitions, dielectric recombination, Bremsstrahlung, radiative recombination and two-photon continua.
!! The elements included are H, He, C, N, O, Ne, Mg, Si, S, Ca, Fe and Ni. We note that below \f$10^{4}\f$ K, radiative cooling
!! becomes very ineffective. Above \f$10^{8}\f$ K, all the medium will be ionised and the gas will only radiate via free-free
!! Bremsstrahlung emission which can be described by a simple \f$\Lambda (T) \propto T^{1/2}\f$ cooling law. In the mid-temperature
!! range, the cooling is dominated by line cooling (free-bound and bound-bound) from the metals.
!!
!! \image html cooling.jpg
!!
!! <CENTER>
!! Figure 3: Cooling rate power index as a function of temperature. The solid line represents the cooling curve composed of data points \n
!! read off the Raymond et al. (1976) curve. The dashed line represents the simple Kahn et al. (1976) power-law approximation and \n
!! the dotted line shows the Bremsstrahlung contribution at high temperatures.
!! </CENTER>
!!
!! The curve was tabulated and the data points were connected together by straight lines thus permitting to return an accurate
!! value for \f$\beta\f$ for any required temperature. The temperature \f$T\f$ inside each cell in the grid is fed into a
!! cooling calculation routine; if \f$T\f$ falls in between two data points, the power \f$\beta\f$ is linearly interpolated. This
!! method accounts for the full range of \f$T\f$ seen in the simulations, including the important hydrogen peak around
!! \f$1.7 \times 10^{4}\f$ K.
!!
!! The energy loss is implemented simply as a source term by subtracting an amount of energy \f$E_{c}\f$ from the total energy
!! density \f$E\f$ of each cell at the end of the first and second order computation stages where
!! \f[
!!    E_{c} = \Gamma ~ \Delta t ~ \rho^{2} ~ \Lambda(T)
!! \f]
!! which applies to the entire cooling curve. \f$\Delta t\f$ is the timestep adapted to the order of the scheme (i.e. equal to
!! half a timestep for the first order stage and then to a full timestep for the second order) and \f$\Gamma\f$ is employed for
!! scaling between cgs units and the code units.
!!
!! When including radiative cooling into a numerical scheme another constraint must be taken into account, as the minimal timestep
!! is not only limited by the sound crossing time but also by a cooling timescale. The cooling timescale \f$\Delta t_{c}\f$ is an
!! approximation of the time it would take for the cell to lose all of its energy from the derived cooling rate \f$\Lambda(T)\f$
!! if it were kept constant. Numerically it is defined by
!! \f[
!!    \Delta t_{c} = \frac{p / (\gamma - 1)}{\Gamma \rho^{2} \Lambda(T)}
!! \f]
!! Both dynamical and cooling times are computed for each timestep and cell, and \f$\Delta t\f$ is taken to be the shorter of the
!! Courant limited dynamical time and 5\% of the cooling time.
!!
!! <HR>
!!
!! \section boundaries Boundary conditions
!!
!! Since any simulation can only be performed on some finite computational domain, the edges of the domain are subject to
!! boundary conditions, of which various types exist. The method to create the boundaries is to extend the grid to include a
!! few additional boundary cells on the edges called ghost cells. These cells provide the neighbouring cells, interior to the
!! domain, with the necessary values needed for updating their fluid state. The behaviour of these ghost cells is dependent on
!! the type of boundary:
!! <UL>
!! <LI> 1 - Free-flow boundary: assumes the quantities to be the same on each side of the edge interface, i.e. \f$q_{l} = q_{r}\f$.
!! <LI> 2 - Reflecting boundary: takes the boundary as a symmetry axis and reflects all velocities normal to the surface by having \f$u_{l} = - u_{r}\f$.
!! <LI> 3 - Cyclic boundary: joins the edges of a computational domain by ensuring that what flows out of one end of the grid flows back in the other side.
!! <LI> 4 - Fluid boundary: is a user-specified function of space and time which is used to represent in-flows in the grid, such as jets or winds.
!! </UL>
!< 
