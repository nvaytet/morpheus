!> \page geometries Grid geometries
!!
!! \section defs General definitions
!!
!! The three coordinate dimensions are \f$ \alpha, \beta, \gamma\f$.\n
!! \f$\zeta\f$ designates any coordinate direction.\n
!! The line elements are \f$d\alpha, d\beta, d\gamma\f$.\n
!! The cell size in direction \f$\zeta\f$ is defined by
!! \f[
!!    \Delta\zeta ~=~ \left( \zeta_{2} - \zeta_{1} \right)
!! \f]
!! The cell median coordinate in direction \f$\zeta\f$ is defined by
!! \f[
!!    \zeta_{{\mathrm m}} ~=~ \frac{1}{2}~\left( \zeta_{1} + \zeta_{2} \right)
!! \f]
!! The cell volume is defined by
!! \f[
!!    V ~=~ \int_{\alpha_{1}}^{\alpha_{2}} \!\!\int_{\beta_{1}}^{\beta_{2}}
!!          \!\!\int_{\gamma_{1}}^{\gamma_{2}} d\alpha~d\beta~d\gamma
!! \f]
!! The face area in direction \f$\alpha\f$ is defined by
!! \f[
!!    A_{\alpha} ~=~ \int_{\beta_{1}}^{\beta_{2}}\!\!\int_{\gamma_{1}}^{\gamma_{2}} d\beta~d\gamma
!! \f]
!!
!! The cell nodal coordinate in direction \f$\zeta\f$ is the volume averaged coordinate
!! \f[
!!    \hat{\zeta} ~=~ \frac{1}{V} ~ \int_{\alpha_{1}}^{\alpha_{2}} \!\! 
!!    \int_{\beta_{1}}^{\beta_{2}}\!\!\int_{\gamma_{1}}^{\gamma_{2}}  \zeta ~d\alpha~d\beta~d\gamma
!! \f]
!!
!! <hr>
!!
!! \section cartesian Cartesian
!!
!! The three coordinate dimensions are \f$ x, ~y, ~z\f$.\n
!! The line elements are \f$dx, ~dy, ~dz\f$.\n
!! The cell nodal coordinates are
!! \f$
!!    \hat{x} = x_{{\mathrm m}} ;~ \hat{y} = y_{{\mathrm m}} ;~ \hat{z} = z_{{\mathrm m}} .
!! \f$
!!
!! <b>1 Dimension</b> \n
!! 
!! The cell volume is defined by
!! \f[
!!    V ~=~ \Delta x
!! \f]
!! The face areas are
!! \f[
!!    A_{x}  = 1
!! \f]
!! The geometrical sources are zero.\n
!! The flux offsets are zero.
!!
!! <b>2 Dimensions</b> \n
!!
!! The cell volume is defined by
!! \f[
!!    V ~=~ \Delta x \Delta y
!! \f]
!! The face areas are
!! \f{eqnarray*}{
!!    A_{x} &=& \Delta y \\\
!!    A_{y} &=& \Delta x
!! \f}
!! The geometrical sources are zero. \n
!! The flux offsets are zero.
!!
!! <b>3 Dimensions</b> \n
!!
!! The cell volume is defined by
!! \f[
!!    V ~=~ \Delta x \Delta y \Delta z
!! \f]
!! The face areas are
!! \f{eqnarray*}{
!!    A_{x} &=& \Delta y \Delta z \\\
!!    A_{y} &=& \Delta x \Delta z \\\
!!    A_{z} &=& \Delta x \Delta y
!! \f}
!! The geometrical sources are zero.\n
!! The flux offsets are zero.
!!
!! <hr>
!!
!! \section spherical Spherical
!!
!! The three coordinate dimensions are \f$ r, ~\theta, ~\phi\f$.\n
!! The line elements are \f$dr, ~rd\theta, ~r\sin\theta d\phi\f$.\n
!! The cell nodal coordinates are
!! \f{eqnarray*}{
!!    \hat{r}~ & = & \frac{\displaystyle \int_{\phi_{1}}^{\phi_{2}} \!\! 
!!    \int_{\theta_{1}}^{\theta_{2}}\!\!\int_{r_{1}}^{r_{2}}  r~r^{2}\sin\theta~dr~d\theta~d\phi }
!!    {\displaystyle \int_{\phi_{1}}^{\phi_{2}} \!\! \int_{\theta_{1}}^{\theta_{2}}\!\!\int_{r_{1}}^{r_{2}} 
!!    r^{2}\sin\theta~dr~d\theta~d\phi } \\\
!!    ~ & = & \frac{\displaystyle \frac{1}{4} \left(r_{2}^{4} - r_{1}^{4} \right)\left(\phi_{2} - \phi_{1} \right) 
!!    \left( \cos\theta_{1} - \cos \theta_{2} \right)}{\displaystyle \frac{1}{3} \left(r_{2}^{3} - r_{1}^{3} 
!!    \right)\left(\phi_{2} - \phi_{1} \right) \left( \cos\theta_{1} - \cos \theta_{2} \right)} \\\
!!    ~ & = & \frac{3}{4} ~\frac{\displaystyle \left(r_{2}^{4} - r_{1}^{4} \right)}{\displaystyle 
!!    \left(r_{2}^{3} - r_{1}^{3} \right)} \\\
!!    ~ & ~ & ~ \\\
!!    \hat{\theta} & = & \frac{\displaystyle \int_{\phi_{1}}^{\phi_{2}} \!\! 
!!    \int_{\theta_{1}}^{\theta_{2}}\!\!\int_{r_{1}}^{r_{2}} \theta~r^{2}\sin\theta~dr~d\theta~d\phi 
!!    }{\displaystyle \int_{\phi_{1}}^{\phi_{2}} \!\! \int_{\theta_{1}}^{\theta_{2}}\!\!\int_{r_{1}}^{r_{2}} 
!!    r^{2}\sin\theta~dr~d\theta~d\phi } \\\
!!    ~ & = & \frac{\displaystyle \frac{1}{3} \left(r_{2}^{3} - r_{1}^{3} \right)\left(\phi_{2} - \phi_{1} 
!!    \right) \left( \theta_{1}\cos\theta_{1} - \theta_{2}\cos \theta_{2} + \sin\theta_{2} - 
!!    \sin\theta_{1}\right)}{\displaystyle \frac{1}{3} \left(r_{2}^{3} - r_{1}^{3} \right)\left(\phi_{2} - 
!!    \phi_{1} \right) \left( \cos\theta_{1} - \cos \theta_{2} \right)} \\\
!!    ~ & = & \frac{ \theta_{2}\cos\theta_{2} - \theta_{1}\cos\theta_{1} + \sin\theta_{1} - \sin\theta_{2} }{ 
!!    \cos\theta_{2} - \cos\theta_{1}} \\\
!!    ~ & ~ & ~ \\\
!!    \hat{\phi}~ & = & \frac{\displaystyle \int_{\phi_{1}}^{\phi_{2}} \!\! 
!!    \int_{\theta_{1}}^{\theta_{2}}\!\!\int_{r_{1}}^{r_{2}} \phi~r^{2}\sin\theta~dr~d\theta~d\phi 
!!    }{\displaystyle \int_{\phi_{1}}^{\phi_{2}} \!\! \int_{\theta_{1}}^{\theta_{2}}\!\!\int_{r_{1}}^{r_{2}} 
!!    r^{2}\sin\theta~dr~d\theta~d\phi } \\\
!!    ~ & = & \frac{\displaystyle \frac{1}{6} \left(r_{2}^{3} - r_{1}^{3} \right) \left(\phi_{2}^{2} - 
!!    \phi_{1}^{2} \right) \left( \cos\theta_{1} - \cos \theta_{2} \right)}{\displaystyle \frac{1}{3} 
!!    \left(r_{2}^{3} - r_{1}^{3} \right)\left(\phi_{2} - \phi_{1} \right) \left( \cos\theta_{1} - \cos 
!!    \theta_{2} \right)} \\\
!!    ~ & = & \phi_{\mathrm{m}}
!! \f}
!! The \f$\theta\f$-face centred \f$r\f$ coordinate is defined by
!! \f{eqnarray*}{
!!   r_{\theta} & = & \frac{\displaystyle \int_{\theta_{1}}^{\theta_{2}}\!\!\int_{r_{1}}^{r_{2}}  r~rd\theta 
!!   ~dr}{\displaystyle \int_{\theta_{1}}^{\theta_{2}}\!\!\int_{r_{1}}^{r_{2}} r ~dr~d\theta } \\\
!!   ~ & = & \frac{\displaystyle \frac{1}{3} \left(r_{2}^{3} - r_{1}^{3} \right)\left(\theta_{2} - \theta_{1} 
!!   \right) }{\displaystyle \frac{1}{2} \left(r_{2}^{2} - r_{1}^{2} \right)\left(\theta_{2} - \theta_{1} 
!!   \right) } \\\
!!   ~ & = & \frac{2}{3} ~\frac{\displaystyle \left(r_{2}^{3} - r_{1}^{3} \right)}{\displaystyle \left(r_{2}^{2} - 
!!   r_{1}^{2} \right)}
!! \f}
!! We also define
!! \f[
!!    \beta = \frac{\sin\theta_{2} - \sin\theta_{1}}{\cos\theta_{1} - \cos\theta_{2}}
!! \f]
!! and
!! \f{eqnarray*}{
!!    \theta_{*} & = & \frac{\displaystyle \frac{1}{2} ~\Delta\theta~(\sin\theta_{1} + \sin\theta_{2}) + 
!!    \cos\theta_{2} - \cos\theta_{1}}{\sin\theta_{2} - \sin\theta_{1}} ~+~ \theta_{{\mathrm m}}
!! \f}
!!
!! <b>1 Dimension</b> \n
!! 
!! The cell volume is defined by
!! \f{eqnarray*}{
!!    V & = & \int_{0}^{2\pi} \!\! \int_{0}^{\pi}\!\!\int_{r_{1}}^{r_{2}} 
!!    r^{2}\sin\theta~dr~d\theta~d\phi \\\
!!    ~ & = & \frac{4\pi}{3} \left(r_{2}^{3} - r_{1}^{3} \right)
!! \f}
!! The face areas are
!! \f{eqnarray*}{
!!     A_{r} & = & \int_{0}^{2\pi} \!\! \int_{0}^{\pi} r~d\theta~r\sin\theta~d\phi \\\
!!     ~     & = & 4\pi r^{2}
!! \f}
!! The geometrical sources are
!! \f[
!!    g_{\rho u_{r}} = \left\{ \frac{ 2P } {r} \right\}_{ [ r_{\theta} ] }
!! \f]
!! The flux offsets are zero.
!!
!! <b>2 Dimensions</b> \n
!!
!! The cell volume is defined by
!! \f{eqnarray*}{
!!    V & = & \int_{0}^{2\pi} \!\! \int_{\theta_{1}}^{\theta_{2}}\!\!\int_{r_{1}}^{r_{2}} 
!!    r^{2}\sin\theta~dr~d\theta~d\phi \\\
!!    ~ & = & \frac{2\pi}{3} \left(r_{2}^{3} - r_{1}^{3} \right) \left( \cos\theta_{1} - \cos \theta_{2} \right)
!! \f}
!! The face areas are
!! \f{eqnarray*}{
!!    A_{r} & = & \int_{0}^{2\pi} \!\! \int_{\theta_{1}}^{\theta_{2}} r~d\theta~r\sin\theta~d\phi \\\
!!  ~ & = & 2\pi r^{2} \int_{\theta_{1}}^{\theta_{2}} \sin\theta~d\theta \\\
!!  ~ & = & 2\pi r^{2} (\cos\theta_{1} - \cos\theta_{2}) \\\
!!  ~ & ~ & ~ \\\
!!    A_{\theta} & = & \int_{0}^{2\pi} \!\! \int_{r_{1}}^{r_{2}}~r\sin\theta~dr~d\phi \\\
!!  ~ & = & 2\pi\sin\theta \int_{r_{1}}^{r_{2}} r~dr \\\
!!  ~ & = & \pi \sin\theta~(r_{2}^{2} - r_{1}^{2})
!! \f}
!! The geometrical sources are
!! \f{eqnarray*}{
!!    g_{\rho u_{r}} & = & \left\{ \frac{ 2P + \rho~u_{\theta}^{2}} {r} \right\}_{ 
!!    [ r_{\theta} , \hat{\theta} ] } \\\
!!    ~ & ~ & ~ \\\
!!    g_{\rho u_{\theta}} & = & - ~\left\{ \frac{ \rho~(u_{r} u_{\theta}) } {r} \right\}_{ [ r_{\theta} , 
!!    \hat{\theta} ] }  ~+~ \beta~ \left\{ \frac{ P } {r} \right\}_{ [ r_{\theta} 
!!    , \theta_{*} ] }
!! \f}
!! The flux offsets are
!! \f[
!!    \begin{array}{lcl}
!!    \Delta\theta_{r}  & = & 0 \\\
!!    \Delta r_{\theta} & = & r_{\theta} - \hat{r}
!!    \end{array}
!! \f]
!!
!! <b>3 Dimensions</b> \n
!!
!! The cell volume is defined by
!! \f{eqnarray*}{
!!    V & = & \int_{\phi_{1}}^{\phi_{2}} \!\! \int_{\theta_{1}}^{\theta_{2}}\!\!\int_{r_{1}}^{r_{2}} 
!!    r^{2}\sin\theta~dr~d\theta~d\phi \\\
!!    ~ & = & \frac{1}{3} \left(r_{2}^{3} - r_{1}^{3} \right) \Delta\phi
!!    \left( \cos\theta_{1} - \cos \theta_{2} \right)
!! \f}
!! The face areas are
!! \f{eqnarray*}{
!!    A_{r} & = & \int_{\phi_{1}}^{\phi_{2}} \!\! \int_{\theta_{1}}^{\theta_{2}} r~d\theta~r\sin\theta~d\phi \\\
!!    ~ & = & r^{2} ~(\phi_{2} - \phi_{1}) \int_{\theta_{1}}^{\theta_{2}} \sin\theta~d\theta \\\
!!    ~ & = & r^{2} \Delta\phi ~(\cos\theta_{1} - \cos\theta_{2}) \\\
!!    ~ & ~ & ~ \\\
!!    A_{\theta} & = & \int_{\phi_{1}}^{\phi_{2}} \!\! \int_{r_{1}}^{r_{2}}~r\sin\theta~dr~d\phi \\\
!!    ~ & = & \sin\theta ~(\phi_{2} - \phi_{1}) \int_{r_{1}}^{r_{2}} r~dr \\\
!!    ~ & = & \frac{1}{2} ~\Delta\phi~ \sin\theta~(r_{2}^{2} - r_{1}^{2}) \\\
!!    ~ & ~ & ~ \\\
!!    A_{\phi} & = & \int_{\theta_{1}}^{\theta_{2}} \!\! \int_{r_{1}}^{r_{2}}~r~d\theta~dr \\\
!!    ~ & = & (\theta_{2} - \theta_{1}) \int_{r_{1}}^{r_{2}} r~dr \\\
!!    ~ & = & \frac{1}{2} ~\Delta\theta~(r_{2}^{2} - r_{1}^{2})
!! \f}
!! The geometrical sources are
!! \f{eqnarray*}{
!!    g_{\rho u_{r}} & = & \left\{ \frac{ 2P + \rho~(u_{\theta}^{2} + u_{\phi}^{2})} {r} \right\}_{ [ r_{\theta} 
!!    , \hat{\theta},\hat{\phi} ] } \\\
!!    ~ & ~ & ~ \\\
!!    g_{\rho u_{\theta}} & = & - ~\left\{ \frac{ \rho~(u_{r} u_{\theta}) } {r} \right\}_{ [ r_{\theta} , 
!!    \hat{\theta},\hat{\phi} ] }  ~+~ \beta~ \left\{ \frac{ P + \rho~ u_{\phi}^{2} } {r} \right\}_{ [ r_{\theta} 
!!    , \theta_{*},\hat{\phi} ] } \\\
!!    ~ & ~ & ~ \\\
!!    g_{\rho u_{\phi}} & = & - ~\left\{ \frac{\rho~(u_{r} u_{\phi}) } {r} \right\}_{ [ r_{\theta} , 
!!    \hat{\theta},\hat{\phi} ] }  ~-~ \beta~ \left\{ \frac{ \rho~ (u_{\theta} u_{\phi}) } {r} \right\}_{ [ 
!!    r_{\theta} , \theta_{*},\hat{\phi} ] }
!! \f}
!! The flux offsets are
!! \f[
!!    \begin{array}{lcllcl}
!!    \Delta\theta_{r}  & = & 0                    & ;~\Delta\phi_{r}      & = & 0 \\\
!!    \Delta r_{\theta} & = & r_{\theta} - \hat{r} & ;~\Delta\phi_{\theta} & = & 0 \\\
!!    \Delta r_{\phi}   & = & r_{\theta} - \hat{r} & ;~\Delta\theta_{\phi} & = & \theta_{{\mathrm m}} - \hat{\theta}
!!    \end{array}
!! \f]
!!
!! <hr>
!!
!! \section cylindrical Cylindrical
!!
!! The three coordinate dimensions are \f$ r, ~z, ~\phi\f$.\n
!! The line elements are \f$dr, ~dz, ~r d\phi\f$.\n
!! The cell nodal coordinates are
!! \f{eqnarray*}{
!!    \hat{r}~ & = & \frac{\displaystyle \int_{\phi_{1}}^{\phi_{2}} \!\! 
!!    \int_{z_{1}}^{z_{2}}\!\!\int_{r_{1}}^{r_{2}}  r~r~dr~dz~d\phi }
!!    {\displaystyle \int_{\phi_{1}}^{\phi_{2}} \!\! \int_{z_{1}}^{z_{2}}\!\!\int_{r_{1}}^{r_{2}} 
!!    r~dr~dz~d\phi } \\\
!!    ~ & = & \frac{\displaystyle \frac{1}{3} \left(r_{2}^{3} - r_{1}^{3} \right)\left(\phi_{2} - \phi_{1} \right) 
!!    \left( z_{2} - z_{1} \right)}{\displaystyle \frac{1}{2} \left(r_{2}^{2} - r_{1}^{2} 
!!    \right)\left(\phi_{2} - \phi_{1} \right) \left( z_{2} - z_{1} \right)} \\\
!!    ~ & = & \frac{2}{3} ~\frac{\displaystyle \left(r_{2}^{3} - r_{1}^{3} \right)}{\displaystyle 
!!    \left(r_{2}^{2} - r_{1}^{2} \right)} \\\
!!    ~ & ~ & ~ \\\
!!    \hat{\theta} & = & \frac{\displaystyle \int_{\phi_{1}}^{\phi_{2}} \!\! 
!!    \int_{z_{1}}^{z_{2}}\!\!\int_{r_{1}}^{r_{2}} z~r~dr~dz~d\phi 
!!    }{ \displaystyle\int_{\phi_{1}}^{\phi_{2}} \!\! \int_{z_{1}}^{z_{2}}\!\!\int_{r_{1}}^{r_{2}} 
!!    r~dr~dz~d\phi } \\\
!!    ~ & = & \frac{\displaystyle \frac{1}{4} \left(r_{2}^{2} - r_{1}^{2} \right)\left(\phi_{2} - \phi_{1} 
!!    \right) \left( z_{2}^{2} - z_{1}^{2}\right)}{\displaystyle \frac{1}{2} \left(r_{2}^{2} - r_{1}^{2}
!!    \right)\left(\phi_{2} - \phi_{1} \right) \left( z_{2} - z_{1}\right)} \\\
!!    ~ & = & z_{\mathrm{m}} \\\
!!    ~ & ~ & ~ \\\
!!    \hat{\phi}~ & = & \frac{\displaystyle \int_{\phi_{1}}^{\phi_{2}} \!\! 
!!    \int_{z_{1}}^{z_{2}}\!\!\int_{r_{1}}^{r_{2}} \phi~r~dr~dz~d\phi 
!!    }{\displaystyle \int_{\phi_{1}}^{\phi_{2}} \!\! \int_{z_{1}}^{z_{2}}\!\!\int_{r_{1}}^{r_{2}} 
!!    r~dr~dz~d\phi } \\\
!!    ~ & = & \frac{\displaystyle \frac{1}{4} \left(r_{2}^{2} - r_{1}^{2} \right) \left(\phi_{2}^{2} - 
!!    \phi_{1}^{2} \right) \left( z_{2} - z_{1}\right)}{\displaystyle \frac{1}{2} 
!!    \left(r_{2}^{2} - r_{1}^{2} \right)\left(\phi_{2} - \phi_{1} \right) \left( z_{2} - z_{1}\right)} \\\
!!    ~ & = & \phi_{\mathrm{m}}
!! \f}
!!
!! <b>1 Dimension</b> \n
!! 
!! The cell volume is defined by
!! \f{eqnarray*}{
!!    V & = & \int_{0}^{2\pi} \!\! \int_{r_{1}}^{r_{2}} r~dr~d\phi \\\
!!    ~ & = & \pi \left(r_{2}^{2} - r_{1}^{2} \right)
!! \f}
!! The face areas are
!! \f{eqnarray*}{
!!     A_{r} & = & \int_{0}^{2\pi} r~d\phi \\\
!!     ~     & = & 2\pi r
!! \f}
!! The geometrical sources are
!! \f[
!!    g_{\rho u_{r}} = \left\{ \frac{ P }{r} \right\}_{ [ r_{\mathrm{m}} ] }
!! \f]
!! The flux offsets are zero.
!!
!! <b>2 Dimensions</b> \n
!!
!! The cell volume is defined by
!! \f{eqnarray*}{
!!    V & = & \int_{0}^{2\pi} \!\! \int_{z_{1}}^{z_{2}}\!\!\int_{r_{1}}^{r_{2}} r~dr~dz~d\phi \\\
!!    ~ & = & \pi \left(r_{2}^{2} - r_{1}^{2} \right) \Delta z
!! \f}
!! The face areas are
!! \f{eqnarray*}{
!!    A_{r} & = & \int_{0}^{2\pi} \!\! \int_{z_{1}}^{z_{2}} r~dz~d\phi \\\
!!  ~ & = & 2\pi r \Delta z \\\
!!  ~ & ~ & ~ \\\
!!    A_{z} & = & \int_{0}^{2\pi} \!\! \int_{r_{1}}^{r_{2}} r~dz~d\phi \\\
!!  ~ & = & \pi \left(r_{2}^{2} - r_{1}^{2} \right)
!! \f}
!! The geometrical sources are
!! \f{eqnarray*}{
!!    g_{\rho u_{r}} & = & \left\{ \frac{ P }{r} \right\}_{[r_{\mathrm{m}},\hat{z}]} \\\
!!    ~ & ~ & ~ \\\
!!    g_{\rho u_{z}} & = & 0
!! \f}
!! The flux offsets are zero.
!!
!! <b>3 Dimensions</b> \n
!!
!! The cell volume is defined by
!! \f{eqnarray*}{
!!    V & = & \int_{\phi_{1}}^{\phi_{2}} \!\! \int_{z_{1}}^{z_{2}}\!\!\int_{r_{1}}^{r_{2}} r~dr~dz~d\phi \\\
!!    ~ & = & \frac{1}{2}\Delta z \Delta\phi \left(r_{2}^{2} - r_{1}^{2} \right)
!! \f}
!! The face areas are
!! \f{eqnarray*}{
!!    A_{r} & = & \int_{\phi_{1}}^{\phi_{2}} \!\! \int_{z_{1}}^{z_{2}} r~dz~d\phi \\\
!!    ~ & = & r \Delta z \Delta\phi \\\
!!    ~ & ~ & ~ \\\
!!    A_{z} & = & \int_{\phi_{1}}^{\phi_{2}} \!\! \int_{r_{1}}^{r_{2}} r~dr~d\phi \\\
!!    ~ & = & \frac{1}{2} \left(r_{2}^{2} - r_{1}^{2} \right) \Delta\phi \\\
!!    ~ & ~ & ~ \\\
!!    A_{\phi} & = & \int_{z_{1}}^{z_{2}} \!\! \int_{r_{1}}^{r_{2}}~dz~dr \\\
!!    ~ & = & \Delta r \Delta z
!! \f}
!! The geometrical sources are
!! \f{eqnarray*}{
!!    g_{\rho u_{r}} & = & \left\{ \frac{ P + \rho~u_{\phi}^{2} }{r} \right\}_{ [ r_{\mathrm{m}} 
!!    , \hat{z},\hat{\phi} ] } \\\
!!    ~ & ~ & ~ \\\
!!    g_{\rho u_{z}} & = & 0 \\\
!!    ~ & ~ & ~ \\\
!!    g_{\rho u_{\phi}} & = & - \left\{ \frac{ \rho~(u_{r} u_{\phi}) }{r} \right\}_{ [ r_{\mathrm{m}} , 
!!    \hat{z},\hat{\phi} ] }
!! \f}
!! The flux offsets are zero.
!! 
!<
