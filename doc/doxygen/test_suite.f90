!> \page testsuite Test Suite
!!
!! \section sod 1. Sod's shock tube (1D - Cartesian)
!!
!! This problem is initiated by a left region of high density and pressure and a right region of low density and pressure.
!! The gas is initially at rest everywhere. The discontinuity creates a shock wave which propagates towards the right and a
!! rarefaction wave propagating towards the left. The two waves are separated by a contact discontinuity. This test is the
!! basic standard test every hydro code should pass.
!!
!! <TABLE border="0">
!! <TR valign="top">
!! <TD width="50%">
!!
!! <B>Set up</B>
!!
!! Domain: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ -0.5 \leq x \leq 0.5 \f$ \n
!!
!! Boundary conditions: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Free flow everywhere \n
!!
!! Equation of state: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Adiabatic with \f$ \gamma = 7/5 \f$ \n
!!
!! Initial density: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ \rho = 1.0 \f$ for \f$ x \leq 0.0 \f$ and \f$ \rho = 0.125 \f$ for \f$ x > 0.0 \f$ \n
!!
!! Initial pressure: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ p = 1.0 \f$ for \f$ x \leq 0.0 \f$ and \f$ p = 0.1 \f$ for \f$ x > 0.0 \f$ \n
!!
!! Initial velocity: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Zero everywhere
!!
!! </TD>
!! <TD align="center" width="50%">
!!
!! <B>Results</B>
!!
!! \image html sod.png
!!
!! Density, velocity, pressure and temperature profiles for a \f$ N_{x} = 500\f$ grid at time \f$ t = 0.2 s \f$.
!!
!! </TD>
!! </TR>
!! </TABLE>
!!
!! <HR>
!!
!!\section iblastwaves 2. Interacting blastwaves (1D - Cartesian)
!!
!! This classic 1D Woodward-Collela interacting blastwave problem (Woodward &amp; Collela 1984) is concerned with the
!! interaction of waves from two Riemann problems with reflecting boundary conditions. The high pressures in left and
!! right regions create shock waves which travel towards each other and interact. The are no analytical solutions to
!! this problem but the solution can be compared to other codes.
!!
!! <TABLE border="0">
!! <TR valign="top">
!! <TD width="50%">
!! 
!! <B>Set up</B>
!!
!! Domain: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ 0.0 \leq x \leq 1.0 \f$ \n
!!
!! Boundary conditions: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Reflecting everywhere \n
!!
!! Equation of state: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Adiabatic with \f$ \gamma = 7/5 \f$ \n
!!
!! Initial density: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ \rho = 1.0 \f$ everywhere
!!
!! Initial pressure: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ p = 1000.0 \f$ for \f$ x \leq 0.1 \f$, \f$ p = 100.0 \f$ for \f$ x > 0.9 \f$ and \f$ p = 0.01 \f$ otherwise \n
!!
!! Initial velocity: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Zero everywhere
!!
!! </TD>
!! <TD align="center" width="50%">
!! 
!! <B>Results</B>
!!
!! \image html interacting-blastwaves.png
!!
!! Density, velocity, pressure and temperature profiles for a \f$N_{x} = 1000\f$ grid at time \f$ t = 3.7 \times 10^{-2} s \f$.
!!
!! </TD>
!! </TR>
!! </TABLE>
!!
!! <HR>
!!
!! \section sedov1d 3. Sedov explosion (1D - Spherical)
!!
!! This problem is initiated by a very small high pressure region at the centre of a uniform density medium.
!! A strong shock propagates outward. The density, velocity and pressure profiles have well known self-similar solutions.
!!
!! <TABLE border="0">
!! <TR valign="top">
!! <TD width="50%">
!!
!! <B>Set up</B>
!!
!! Domain: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ 0.0 \leq r \leq 2.0e16 \f$ \n
!!
!! Boundary conditions: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Reflecting at the inner \f$r\f$ boundary and free-flow at the outer \f$r\f$ boundary \n
!!
!! Equation of state: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Adiabatic with \f$ \gamma = 5/3 \f$ \n
!!
!! Initial density: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ \displaystyle \rho = 10^{-24}~\mathrm{g~cm}^{-3} \f$ \n
!!
!! Initial pressure: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ p = 1.36065 \times 10^{-13}~\mathrm{g~cm}^{-1}~\mathrm{s}^{-2} \f$ for \f$ r \geq 10^{14}~\mathrm{cm}\f$ and \f$ p = 9.21036 \times 10^{-1}~\mathrm{g~cm}^{-1}~\mathrm{s}^{-2} \f$ for \f$ r < 10^{14}~\mathrm{cm} \f$ \n
!!
!! Initial velocity: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Zero everywhere
!!
!! </TD>
!! <TD align="center" width="50%">
!! 
!! <B>Results</B>
!!
!! \image html sedov1d.png
!!
!! Density, velocity, pressure and temperature profiles for a \f$ N_{r} = 500\f$ grid at time \f$ t = 7.0 \times 10^{6} s \f$.
!!
!! </TD>
!! </TR>
!! </TABLE>
!!
!! <HR>
!!
!! \section primakoff 4. Primakoff explosion (1D - Spherical)
!!
!! This problem is initiated by a very small high pressure region at the centre of a \f$ \rho \propto 1/r^{2} \f$ medium.
!! A strong shock propagates outward. The density, velocity and pressure profiles have well known power-law solutions:
!! \f$ \rho \propto r, u \propto r, p \propto r^{3} \f$.
!!
!! <TABLE border="0">
!! <TR valign="top">
!! <TD width="50%">
!!
!! <B>Set up</B>
!!
!! Domain: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ 0.0 \leq r \leq 1.0e15 \f$ \n
!!
!! Boundary conditions: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Reflecting at the inner \f$r\f$ boundary and free-flow at the outer \f$r\f$ boundary \n
!!
!! Equation of state: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Adiabatic with \f$ \gamma = 5/3 \f$ \n
!!
!! Initial density: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ \displaystyle \rho(r) = \frac{6 \times 10^{12}~\mathrm{g~cm}^{-3}}{4\pi r^{2}} \f$ \n
!!
!! Initial pressure: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ p = 1.36065 \times 10^{-13}~\mathrm{g~cm}^{-1}~\mathrm{s}^{-2} \f$ for \f$ r \geq 1.5 \times 10^{12}~\mathrm{cm}\f$ and \f$ p = 7.368284 \times 10^{7}~\mathrm{g~cm}^{-1}~\mathrm{s}^{-2} \f$ for \f$ r < 1.5 \times 10^{12}~\mathrm{cm} \f$ \n
!!
!! Initial velocity: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Zero everywhere
!!
!! </TD>
!! <TD align="center" width="50%">
!! 
!! <B>Results</B>
!!
!! \image html primakoff.png
!!
!! Density, velocity, pressure and temperature profiles for a \f$ N_{r} = 500\f$ grid at time \f$ t = 7.0 \times 10^{6} s \f$.
!!
!! </TD>
!! </TR>
!! </TABLE>
!!
!! <HR>
!!
!! \section sedovxyz 5. Sedov blastwave (2D - Cartesian)
!!
!! This problem is initiated by an overpressured region in the center of the domain. The result is a strong outward moving spherical
!! shock with rarified fluid inside the sphere. The blast wave should remain perfectly spherically symmetric at early times. Eventually,
!! the reflecting boundaries lead to complex shock-shock and shock-contact discontinuity interactions. These interactions should
!! also create the Richtmyer-Meshkov instability in the very low-density region in the center of the grid (hydro case only). The
!! "fingers" produced by this instability should be perfectly symmetric top-to-bottom and left-to-right.
!!
!! <TABLE border="0">
!! <TR valign="top">
!! <TD width="50%">
!!
!! <B>Set up</B>
!!
!! Domain: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ -0.5 \leq x \leq 0.5 , ~ -0.75 \leq y \leq 0.75 \f$ \n
!!
!! Boundary conditions: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Reflecting everywhere \n
!!
!! Equation of state: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Adiabatic with \f$ \gamma = 5/3 \f$ \n
!!
!! Initial density: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ \rho = 1.0 \f$ everywhere
!!
!! Initial pressure: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ p = 1.0 \f$ for \f$ r > 0.1\f$ and \f$ p = 10.0 \f$ for \f$ r \le 0.1\f$, where \f$ r = \sqrt{x^{2}+y^{2}} \f$ \n
!!
!! Initial velocity: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Zero everywhere
!!
!! </TD>
!! <TD align="center" width="50%">
!! 
!! <B>Results</B>
!!
!! \image html sedovxyz.png
!!
!! Linear density maps for a \f$N_{x} = 256\f$ and \f$N_{y} = 384\f$ grid with density ranging from 0.08 to 2.9 at times 0.167s, 0.333s, 0.5s, 0.667s, 0.833s, 1s.
!!
!! </TD>
!! </TR>
!! </TABLE>
!!
!! <HR>
!!
!! \section sedovrtp 6. Sedov blastwave (2D - Spherical)
!!
!! This problem is initiated by an overpressured region in the center of the domain. The result is a strong outward moving spherical
!! shock with rarified fluid inside the sphere.
!!
!! <TABLE border="0">
!! <TR valign="top">
!! <TD width="50%">
!!
!! <B>Set up</B>
!!
!! Domain: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ 0.0 \leq r \leq 0.5 , ~ 0.0 \leq \theta \leq \frac{3\pi}{4} \f$ \n
!!
!! Boundary conditions: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Reflecting at the inner \f$r\f$ boundary and free-flow at the outer \f$r\f$ boundary \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Reflecting at both \f$\theta\f$ boundaries \n
!!
!! Equation of state: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Adiabatic with \f$ \gamma = 5/3 \f$ \n
!!
!! Initial density: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ \rho = 1.0 \f$ everywhere
!!
!! Initial pressure: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ p = 1.0 \f$ for \f$ r > 0.1\f$ and \f$ p = 10.0 \f$ for \f$ r \le 0.1\f$ \n
!!
!! Initial velocity: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Zero everywhere
!!
!! </TD>
!! <TD align="center" width="50%">
!! 
!! <B>Results</B>
!!
!! \image html sedovrtp.png
!!
!! Linear density maps for a \f$N_{r} = 100\f$ and \f$N_{\theta} = 50\f$ grid with density ranging from 0.08 to 1.2 at times 0.11s, 0.22s, 0.33s.
!!
!! </TD>
!! </TR>
!! </TABLE>
!!
!! <HR>
!!
!! \section sedovrtp 7. Sedov blastwave (3D - Cylindrical)
!!
!! This problem is initiated by an overpressured region in the center of the domain. The result is a strong outward moving spherical
!! shock with rarified fluid inside the sphere.
!!
!! <TABLE border="0">
!! <TR valign="top">
!! <TD width="50%">
!!
!! <B>Set up</B>
!!
!! Domain: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ 0.0 \leq r \leq 0.5 , ~ 0.0 \leq z \leq 0.5 , ~ 0.0 \leq \phi \leq \frac{\pi}{2} \f$ \n
!!
!! Boundary conditions: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Reflecting at the inner \f$r\f$ boundary and free-flow at the outer \f$r\f$ boundary \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Reflecting at both \f$z\f$ boundaries \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Reflecting at both \f$\phi\f$ boundaries \n
!!
!! Equation of state: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Adiabatic with \f$ \gamma = 5/3 \f$ \n
!!
!! Initial density: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ \rho = 1.0 \f$ everywhere
!!
!! Initial pressure: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ p = 1.0 \f$ for \f$ r > 0.1\f$ and \f$ p = 10.0 \f$ for \f$ r \le 0.1\f$ \n
!!
!! Initial velocity: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Zero everywhere
!!
!! </TD>
!! <TD align="center" width="50%">
!! 
!! <B>Results</B>
!!
!! \image html sedovrzp.png
!!
!! Linear density maps for a \f$N_{r} = 30\f$, \f$N_{z} = 30\f$, \f$N_{\phi} = 15\f$ grid with density ranging from 0.08 to 1.2 at times 0.11s, 0.22s, 0.33s.
!!
!! </TD>
!! </TR>
!! </TABLE>
!!
!! <HR>
!!
!! \section implosion 8. Hydrodynamical implosion (2D - Cartesian)
!!
!! This problem is initiated by an overpressured region above the line \f$x+y = 0.5\f$. This overpressure sends a shock wave towards the origin.
!! The shock hits the reflecting boundaries at x=0 and y=0, and the resulting reflections create jets that eventually interact with each other
!! at the origin. This interaction then results in the production of another jet along the diagonal, \f$x=y\f$. This jet interacts with the
!! shocks that are being reflected back and forth in the box.
!!
!! This problem is very good at testing the algorithm's ability to maintain symmetry. Throughout the evolution, the fluid must be symmetric
!! across \f$x=y\f$. If this symmetry is not exactly maintained, there will be no jet along \f$x=y\f$. Furthermore, the less numerical
!! diffusion there is, the longer and narrower this jet will be.
!!
!! <TABLE border="0">
!! <TR valign="top">
!! <TD width="50%">
!!
!! <B>Set up</B>
!!
!! Domain: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ 0.0 \leq x \leq 1.0 , ~ 0.0 \leq y \leq 1.0 \f$,  \n
!!
!! Boundary conditions: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Reflecting everywhere \n
!!
!! Equation of state: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Adiabatic with \f$ \gamma = 7/5 \f$ \n
!!
!! Initial density: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ \rho = 1.0 \f$ for \f$ x+y > 0.5\f$ and \f$ \rho = 0.125 \f$ for \f$ x+y \le 0.5\f$ \n
!!
!! Initial pressure: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ p = 1.0 \f$ for \f$ x+y > 0.5\f$ and \f$ p = 0.4 \f$ for \f$ x+y \le 0.5\f$ \n
!!
!! Initial velocity: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Zero everywhere
!!
!! </TD>
!! <TD align="center" width="50%">
!! 
!! <B>Results</B>
!!
!! \image html implosion.png
!!
!! Linear density map for a \f$N_{x} = 256\f$ and \f$N_{y} = 256\f$ grid at times t = 0.833s, 1.667s, 2.5s, 3.333s, 4.167s, 5s with density ranging from 0.4 to 1.1. 
!!
!! </TD>
!! </TR>
!! </TABLE>
!!
!! <HR>
!!
!! \section explosion 9. Hydrodynamical explosion (2D - Cartesian)
!!
!! This problem is initiated by an overpressured and overdensity region for \f$r < 0.4\f$ (Toro 1997). The overpressure sends a shock wave outwards.
!! A weak reverse shock is also reflected at the centre and passes through the contact discontinuity.
!! The problem is concerned with the evolution of an unstable contact discontinuity.
!!
!! <TABLE border="0">
!! <TR valign="top">
!! <TD width="50%">
!!
!! <B>Set up</B>
!!
!! Domain: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ 0.0 \leq x \leq 1.5 , ~ 0.0 \leq y \leq 1.5 \f$,  \n
!!
!! Boundary conditions: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; The inner x boundary condition is reflecting. \n
!! &nbsp;&nbsp;&nbsp;&nbsp; The inner y boundary condition is reflecting. \n
!! &nbsp;&nbsp;&nbsp;&nbsp; The outer x boundary condition is free-flow. \n
!! &nbsp;&nbsp;&nbsp;&nbsp; The outer y boundary condition is free-flow. \n
!!
!! Equation of state: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Adiabatic with \f$ \gamma = 7/5 \f$ \n
!!
!! Initial density: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ \rho = 1.0 \f$ for \f$ r < 0.4\f$ and \f$ \rho = 0.125 \f$ for \f$ r \ge 0.4\f$, where \f$ r = \sqrt{x^{2}+y^{2}} \f$ \n
!!
!! Initial pressure: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ p = 1.0 \f$ for \f$ r < 0.4\f$ and \f$ p = 0.1 \f$ for \f$ r \ge 0.4\f$, where \f$ r = \sqrt{x^{2}+y^{2}} \f$ \n
!!
!! Initial velocity: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Zero everywhere
!!
!! </TD>
!! <TD align="center" width="50%">
!! 
!! <B>Results</B>
!!
!! \image html explosion.png
!!
!! Linear density map for a \f$N_{x} = 256\f$ and \f$N_{y} = 256\f$ grid at times t = 0.533s, 1.067s, 1.6s, 2.133s, 2.667s, 3.2s with density ranging from 0.08 to 2.1.
!!
!! </TD>
!! </TR>
!! </TABLE>
!!
!! <HR>
!!
!! \section kelvin-helmholtz 10. Kelvin-Helmholtz instability (2D - Cartesian)
!!
!! The Kelvin-Helmholtz instability occurs when a perturbation is introduced to a system with a velocity
!! shear. Here, we run this test problem to demonstrate the algorithm's ability to evolve a linear
!! perturbation into nonlinear hydrodynamic turbulence.
!!
!! <TABLE border="0">
!! <TR valign="top">
!! <TD width="50%">
!!
!! <B>Set up</B>
!!
!! Domain: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ -0.5 \leq x \leq 0.5 , ~ -0.5 \leq y \leq 0.5 \f$,  \n
!!
!! Boundary conditions: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Periodic everywhere \n
!!
!! Equation of state: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Adiabatic with \f$ \gamma = 7/5 \f$ \n
!!
!! Initial density: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ \rho = 1.0 \f$ for \f$ \|y\| > 0.25\f$ and \f$ \rho = 2.0 \f$ for \f$ \|y\| \le 0.25\f$
!!
!! Initial pressure: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ p = 2.5 \f$ everywhere \n
!!
!! Initial velocity: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ u_{x} = 0.5 \f$ for \f$ \|y\| > 0.25\f$ and \f$ u_{x} = -0.5 \f$ for \f$ \|y\| \le 0.25\f$. We perturb the x and y velocities.
!! One can choose either a single mode perturbation, in which the perturbation is a sine wave with one wavelength in the x dimension, or a random mode 
!! perturbation. The amplitude of the perturbations is 0.05.
!! 
!! </TD>
!! <TD align="center" width="50%">
!! 
!! <B>Results</B>
!!
!! \image html kelvin-helmholtz.png
!!
!! Linear density maps for a \f$N_{x} = 256\f$ and \f$N_{y} = 256\f$ grid with density ranging from 0.5 to 2.2 at times 0.167s, 0.333s, 0.5s, 0.667s, 0.833s, 1s.
!!
!! </TD>
!! </TR>
!! </TABLE>
!!
!! <HR>
!!
!! \section rayleigh-taylor 11. Rayleigh-Taylor instability (2D - Cartesian)
!!
!! This instability results from perturbing a fluid of high density resting on top of a low-density
!! fluid where the gravitational acceleration is downward. This problem tests the implementation
!! of the gravity source term to the fluid equations.
!!
!! <TABLE border="0">
!! <TR valign="top">
!! <TD width="50%">
!!
!! <B>Set up</B>
!!
!! Domain: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ -0.25 \leq x \leq 0.25 , ~ -0.75 \leq y \leq 0.75 \f$,  \n
!!
!! Boundary conditions: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; The x boundaries are periodic. \n
!! &nbsp;&nbsp;&nbsp;&nbsp; The y boundaries are reflecting. \n
!!
!! Equation of state: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; Adiabatic with \f$ \gamma = 7/5 \f$ \n
!!
!! Initial density: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; \f$ \rho = 1.0 \f$ for \f$ y < 0.0\f$ and \f$ \rho = 2.0 \f$ for \f$ y \ge 0.0\f$ \n
!!
!! Initial pressure: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; The pressure is initialized to give hydrostatic equilibrium:
!!                          \f$ p = 2.5 \rho g y \f$ everywhere \n
!!
!! Initial velocity: \n
!! &nbsp;&nbsp;&nbsp;&nbsp; The velocity is perturbed in \f$x\f$ and \f$y\f$ with an amplitude of 0.01 \n
!!
!! </TD>
!! <TD align="center" width="50%">
!! 
!! <B>Results</B>
!!
!! \image html rayleigh-taylor.png
!!
!! Plotted below are linear density maps for a \f$N_{x} = 100\f$ and \f$N_{y} = 300\f$ grid with
!! density ranging from 0.8 to 2.2 at times 2s, 4s, 6s.
!!
!! </TD>
!! </TR>
!! </TABLE>
!<
