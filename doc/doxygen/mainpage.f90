!> \mainpage
!! <CENTER> <b>M</b>anchester <b>O</b>mni-geomet<b>R</b>ical <b>P</b>rogram for <b>H</b>ydrodynamical <b>EU</b>lerian <b>S</b>imulations \n
!!          Copyright &copy; N. Vaytet& Tim O'Brien (2008-2014)\n
!!          The University of Manchester \n
!!          Version 1.33 - 08/2014 \n \n \n
!!          3D MPI-OPENMP Eulerian second-order Godunov hydrodynamic simulation code in cartesian, 
!!          spherical and cylindrical coordinates.
!! </CENTER>
!!
!! <HR>
!!
!! <B>User guide:</B>
!!
!!
!!   - There are currently 10 examples of use of the code in the 'test' directory:
!!      - a 1D Sod test                                                            (cartesian)
!!      - a 1D interacting blastwaves test                                         (cartesian)
!!      - a 1D Primakoff blastwave                                                 (spherical)
!!      - a 2D Sedov blastwave inside a rectangular medium with reflecting walls   (cartesian)
!!      - a 2D Sedov spherical blastwave                                           (spherical)
!!      - a 3D Sedov spherical blastwave                                           (cylindrical)
!!      - a 2D hydrodynamical implosion                                            (cartesian)
!!      - a 2D hydrodynamical explosion                                            (cartesian)
!!      - a 2D Kelvin-Helmholtz instability with periodic boundaries               (cartesian)
!!      - a 2D Rayleigh-Taylor instability                                         (cartesian)
!!
!!
!!   - <B>These examples are all run in the test suite script which can be found in the 'bin'
!!     directory. You can run the full test suite by simply typing './run_test_suite.sh'.
!!     See the beginning of the script for further option (using MPI, etc...)</B>
!!
!!     
!!   - There are 15 files in the 'src' directory common to all runs:
!!      - morpheus.f90          : main program controlling initial setup and computational loop
!!      - allocate_arrays.f90   : (de)allocates the memory for the variables arrays
!!      - hydro.f90             : contains all the hydro routines (comp, flux,...)
!!      - data_output.f90       : contains all the routines for dumping data cubes
!!      - grid.f90              : contains the grid setup
!!      - boundaries.f90        : updates the boundary conditions
!!      - gravity.f90           : calculates the gravity potential
!!      - modules.f90           : contains all global variables
!!      - mpi.f90               : contains mpi domain decomposition & communications
!!      - radiative_cooling.f90 : calculates radiative cooling
!!      - timestep.f90          : computes the timestep
!!      - initialisation.f90    : contains physical constants and units and default parameters
!!      - solvers.f90           : contains the different Riemann solvers
!!      - terminal_output.f90   : outputs information to terminal
!!      - tools.f90             : contains useful routines and functions
!!
!!   - The 'test' directory contains the files specific to each different simulation.
!!     In each subdirectory there are three files:
!!      - setup.f90 : contains the routine to setup the initial conditions, the routine defining
!!        the units and physical constants and the allocation of data arrays
!!      - list.nml : contains all the simulation parameters which is read in as a namelist
!!      - extras.f90: contains all the additional routines specific to each problem (by default
!!        they are empty)
!!
!!
!! <B>Compilation:</B> use the Makefile in 'bin' directory
!!
!!   - Choose to use MPI or OPENMP (or both) by setting the variables MPI and OPENMP to 0 or 1
!!   - Set the correct problem path for the variable PROBLEM at line 28
!!   - Choose the correct compiler for the variable F90 (line 37/39)
!!   - Simply type 'make'
!!
!!
!! Description of namelist variables is given in the list.nml file.
!!
!!
!! Boundary conditions:
!!   - bc(1) = left   (lower x) boundary condition
!!   - bc(2) = right  (upper x) boundary condition
!!   - bc(3) = bottom (lower y) boundary condition
!!   - bc(4) = top    (upper y) boundary condition
!!   - bc(5) = front  (lower z) boundary condition
!!   - bc(6) = back   (upper z) boundary condition
!!   - set to 1 for FREE-FLOW
!!   - set to 2 for REFLECTIVE
!!   - set to 3 for PERIODIC
!!   - set to 4 for USER-SET INFLOW
!!
!!
!! Grid geometry:
!!   - 1 = cartesian
!!   - 2 = spherical
!!   - 3 = cylindrical
!!   
!! In the case of spherical and cylindrical geometries, the size of the grid for the 2nd
!! dimensions (2nd and 3rd in the spherical case) are set using negative multiples of pi.
!! For example to have a spherical grid of size R = 1.0, theta = pi, phi = 2*pi, set in
!! the namelist
!!   - lbox(1) =  1.0e0
!!   - lbox(2) = -1.0e0
!!   - lbox(3) = -2.0e0 
!!
!!
!! The code is run from the directory where the 'list.nml' file is located and data outputs are
!! written in the same directory.
!!
!! <HR>
!!
!! <B>To create a new simulation:</B>
!!
!! Create a new folder in the 'exe' directory.
!! Copy the setup.f90, list.nml and extras.f90 files from a test run into the new folder.
!!
!! Edit your quantities (size of grid lbox, initial density d1, velocity u1, pressure p1,
!! etc...) in the namelist list.nml.
!!
!!
!! Edit the setup.f90:
!!
!!   - Define the centre of the grid using the variables cen1, cen2 and cen3 by giving them a
!!     real value between 0 and 1.
!!
!!   - Define the inital state of the gas inside the grid: the array un(5,i,j,k) contains the
!!     conservative variables:
!!                unew(1) is the gas density
!!                unew(2) is the gas momentum in the x direction (rho * ux)
!!                unew(3) is the gas momentum in the y direction (rho * uy)
!!                unew(4) is the gas momentum in the z direction (rho * uz)
!!                unew(5) is the gas total energy (E = 0.5*(rho u*u) + P/(g-1))
!!
!!   - Add in any extra functions you need in the extras.f90 file: the 'extras_init' subroutine
!!     is called once at the start of the simulation, the 'extras' subroutine is call at every
!!     timestep before the computational step and the 'extras_end' subroutine is called once at
!!     the end of the run.
!!
!! <HR>
!!
!! <B>Data visualisation:</B>
!! 
!! Fortran:
!!
!!   - Use the 'visualize' program located in the 'vis/fortran' directory.
!!   - Compile with 'make'
!!   - Run executable from the 'exe/PROBLEM/' directory containing the simulation outputs with
!!     arguments: '../../vis/fortran/visualize filenameroot istart iend incr' where filenameroot
!!     is the basename of the output files ('morpheus' by default), and istart iend incr are the
!!     outputs starting and ending number and increment. For example, to read every other output
!!     from 0 to 10, use '../../vis/fortran/write_ppm_vtk morpheus 0 10 2'
!!   
!! Python:
!! 
!!   - There is a python utility to read morpheus binary outputs in the vis/python directory.
!!   - There is also an example plotting script in the 'exe/sedovxyz' directory.
!!   - Either set your PYTHONPATH to 'path_to_morpheus/vis/python' or create symbolic links to
!!     the files 'vis/python/rd_morpheus.py' and 'vis/python/fortranfile.py'
!!   - Run the script 'python plot_morpheus_data.py'
!!   
!! Gnuplot:
!!
!!   - There are example gnuplot scripts in all the test run directories.
!!   - They require the use of the fortran visualize program to create text files readable by
!!     gnuplot.
!!
!! <HR>
!!
!! <B>Create movie:</B>
!!
!!   - Create ppm images from the outputs using the 'visualize' program.
!!
!!   - Convert ppm files to png (using imagemagick):
!!      for f in *.ppm; do echo $f; convert $f ${f/ppm/png}; done;
!!
!!   - Create movie using mencoder:
!!      mencoder mf://*.png -mf w=512:h=768:fps=25:type=png -ovc copy -o output.avi -nosound
!!
!! <HR>
!!
!! Further detailed DOXYGEN documentation is available in html format by running 'doxygen' in
!! the doc directory.
!<
